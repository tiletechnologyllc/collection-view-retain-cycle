//
//  AppDelegate.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 9/20/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseFunctions
import CoreLocation
import SDWebImage
import Photos

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    lazy var locationManager:CLLocationManager = CLLocationManager()
//    weak var locationDelegate:MainLocationDelegate!
    var userLocation:GeoPoint!
    var didFindLocation:Bool = false
    
    lazy var functions = Functions.functions()

    var navigationController:UINavigationController?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        
        SDImageCache.shared().maxMemoryCost = 1024 * 1024 * 20 //Aprox 20 images
        
        //SDImageCache.shared().config.shouldCacheImagesInMemory = false //Default True => Store images in RAM cache for Fast performance
        
        SDImageCache.shared().config.shouldDecompressImages = false
        
        SDWebImageDownloader.shared().shouldDecompressImages = false
        
        SDImageCache.shared().config.diskCacheReadingOptions = NSData.ReadingOptions.mappedIfSafe
        
        self.locationManager.delegate = self
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        if let window = self.window {
            User.signedInUserID = "dVC63JFr8YWHGkiSsQFDsW9SmOF2"//user.uid
            self.enableBasicLocationServices()
            self.navigationController?.navigationBar.backgroundColor = colors.backgroundColor
            self.navigationController?.navigationBar.barTintColor = colors.toolBarColor
            self.navigationController?.navigationBar.tintColor = UIColor.white
            let mainVC:TestMainCollectionView = TestMainCollectionView()
            self.navigationController = UINavigationController(rootViewController: mainVC)
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            window.rootViewController = self.navigationController
            window.makeKeyAndVisible()
        }

        return true
    }

    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}
