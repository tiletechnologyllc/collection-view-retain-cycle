//
//  PreviewController.swift
//  CustomCamera
//
//  Created by Thomas M. Jumper on 1/22/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import UIKit
import Firebase
import Photos

class PreviewController: UIViewController {
    let keyboardSlider = SearchBarTopTagsKeyboardSlider()
    
    //@IBOutlet weak var photo:UIImageView!
    var image: UIImage!
    var asset:PHAsset? = nil{
        didSet{
            print("asset was sucessfully set")
            getImageFromAsset()
        }
    }
    //   @IBOutlet weak var TakenPhoto:UIImageView!
    //    @IBOutlet weak var SaveButton: UIButton!
    //    @IBOutlet weak var CancelButton: UIButton!
    //
    //    @IBOutlet weak var TopShadowView: UIView!
    //
    //    @IBOutlet weak var StackView: UIStackView!
    @IBOutlet weak var horrizontalStackView: UIStackView!
    var containerView:UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var postsButton: UIButton!
    
    var lineView:UIView!
    
    var searchBarTopTags:SearchBarTopTagsViewController!// = SearchBarTopTagsViewController()
    
    var searchButton:UIButton = UIButton()
    
//    weak var reloadSearchFeedScreenDelegate:ReloadSearchFeedScreenDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBarTopTags = SearchBarTopTagsViewController()
        searchBarTopTags.postsButtonDelegate = self
        searchBarTopTags.selectedSearchTagDelegate = self
        backButton.backgroundColor = UIColor.clear
        postsButton.backgroundColor = UIColor.clear
        
        backButton.translatesAutoresizingMaskIntoConstraints = false
        postsButton.translatesAutoresizingMaskIntoConstraints = false
        
        backButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        postsButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        backButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        postsButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.searchButton.setImage(#imageLiteral(resourceName: "icons8-widesearch-filled-100"), for: .normal)
        self.horrizontalStackView.insertArrangedSubview(self.searchButton, at: 1)
        searchButton.translatesAutoresizingMaskIntoConstraints = false
        searchButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        searchButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        searchButton.addTarget(self, action: #selector(searchButtonTapped), for: .touchUpInside)
        
        horrizontalStackView.translatesAutoresizingMaskIntoConstraints = false
        horrizontalStackView.distribution = .equalSpacing
        horrizontalStackView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        horrizontalStackView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        horrizontalStackView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
        
        self.view.backgroundColor = colors.backgroundColor
        
        print("The screen height is: \(self.view.bounds.height)")
        print("The screen origin is: \(self.view.frame.origin.y)")
        //Taken Photo
        //  TakenPhoto.image = self.image
        // TakenPhoto.contentMode = .scaleAspectFill
        // TakenPhoto.contentMode = .scaleToFill/Users/a123456/Dropbox/Tile in Progress/topCollectionView 0.04/topCollectionView/Extensions-SearchBarTopTags.swift
        //   TakenPhoto.contentMode = .scaleAspectFit
        //   TakenPhoto.translatesAutoresizingMaskIntoConstraints = false
        // TakenPhoto.bounds = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width*(self.image.size.height/self.image.size.width))
        // TakenPhoto.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive = true
        //  TakenPhoto.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.width*(self.image.size.height/self.image.size.width)).isActive = true
        
        containerView = UIView(frame: CGRect(x: 0, y: horrizontalStackView.bounds.height, width: self.view.bounds.width, height: self.view.bounds.height - horrizontalStackView.bounds.height))
        self.view.addSubview(containerView)
        //SearchBar Top Tags View Contoller
        self.addChild(searchBarTopTags)
        self.searchBarTopTags.view.frame = containerView.frame
        //   self.searchBarTopTags.view.frame = self.view.frame
        self.containerView.addSubview(searchBarTopTags.view)
        
        
        lineView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 4))
        self.view.addSubview(lineView)
        self.lineView.backgroundColor = colors.toolBarColor
        
        self.lineView.translatesAutoresizingMaskIntoConstraints = false
        lineView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        lineView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        lineView.heightAnchor.constraint(equalToConstant: 4).isActive = true
        lineView.topAnchor.constraint(equalTo: self.horrizontalStackView.bottomAnchor, constant: 0).isActive = true
        
        containerView.translatesAutoresizingMaskIntoConstraints = false
        // containerView.topAnchor.constraint(equalTo: self.horrizontalStackView.bottomAnchor).isActive = true
        containerView.topAnchor.constraint(equalTo: lineView.bottomAnchor,constant: 0).isActive = true
        containerView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        let pictureView = self.searchBarTopTags.PictureView
        pictureView?.image = self.image
        pictureView?.contentMode = .scaleAspectFill//.scaleAspectFit
        pictureView?.translatesAutoresizingMaskIntoConstraints = false
        //pictureView?.bounds = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width*(self.image.size.height/self.image.size.width))
        pictureView?.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive = true
        pictureView?.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 1.0, constant: 0).isActive = true
       // pictureView?.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.width*(self.image.size.height/self.image.size.width)).isActive = true
        pictureView?.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        pictureView?.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        //
        
        searchBarTopTags.view.translatesAutoresizingMaskIntoConstraints = false
        searchBarTopTags.view.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 0).isActive = true
        searchBarTopTags.view.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        searchBarTopTags.view.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        searchBarTopTags.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        
        
        //Test Gesture run
        //uncomment
        let testGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        searchBarTopTags.gestureTestView.addGestureRecognizer(testGesture)
        
        // viewTapGestureRecognizer.delegate = self as? UIGestureRecognizerDelegate
        
        // Do any additional setup after loading the view.
        //  TakenPhoto.sendSubview(toBack: horrizontalStackView)
        //  TakenPhoto.bringSubview(toFront: horrizontalStackView)
        
        //Remove
        //keyboardSlider.subscribeToKeyboardNotifications(view: self.view)
        keyboardSlider.subscribeToKeyboardNotifications(view: self.view, previewController: self)
        
        searchBarTopTags.topTagsCollectionView.delegate = self
        searchBarTopTags.searchTagsCollectionView.delegate = self
        searchBarTopTags.didMove(toParent: self)
        self.setUpLayout()
        
        self.view.bringSubviewToFront(horrizontalStackView)
        self.view.bringSubviewToFront(backButton)
        self.view.bringSubviewToFront(postsButton)
        self.searchBarTopTags.topTagsCollectionView.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        keyboardSlider.unsubscribeFromKeyboardNotifications()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    
    func getImageFromAsset(){
        let scale = UIScreen.main.scale
        // var targetSize: CGSize = CGSize(width: .bounds.width * scale, height: imageView.bounds.height * scale)
        
        if let asset = asset{
            print("asset exists in view will appear")
            PHImageManager.default().requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFill, options: nil, resultHandler: {(image,hashable) in
                if let image = image{
                    self.image = image
                    self.searchBarTopTags.PictureView.image = image
                    print("image sucessfully set")
                }else{
                    print("image was empty after request")
                }
            })
        }else{
            print("Asset foes not yet exist in view will appear")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    @IBAction func cancelButton_TouchUpInside(_ sender: Any) {
    //        print("Cancel Button Tapped")
    //        dismiss(animated: true, completion: nil)
    //    }
    
    @IBAction func saveButton_Touched(_ sender: Any) {
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        print("save button tapped 1: figure out if its rpofile picture or a regular")
        //add database here
        guard let selectIndexPath = searchBarTopTags.selectedIndexPath,let selectedTagName:String = searchBarTopTags.topFiveTags[selectIndexPath.item] else{
            print("Selected tag name not yet set, indexPath: \(searchBarTopTags.selectedIndexPath) and \(searchBarTopTags.topFiveTags[searchBarTopTags.selectedIndexPath?.item ?? 1])")
            return
        }
        print("Selected tag to be saved: \(selectedTagName)")
//        Post.createNewPost(tagName: selectedTagName, image: image, completion: {(post,tag) in
//            print("postID in completion: \(post.postID), tagID in completion: \(tag.tagID)")
//            self.reloadSearchFeedScreenDelegate.reloadSearchFeedScreen()
//        })
        //imagePost.defaultImages.insert(imagePost(image:self.image), at: 0)
        //self.presentingViewController?.dismiss(animated: true, completion: nil)
        performSegue(withIdentifier: "unwindToCollectionView", sender: self)
    }
    
    @IBAction func cancelButton_Touched(_ sender: UIButton) {
        print("cancel button tapped")
        performSegue(withIdentifier: "unwindToCollectionView", sender: self)
    }
    
    @objc func searchButtonTapped(){
        print("Search button tapped")
        if self.searchBarTopTags.keyboardIsOpen{
            self.searchBarTopTags.myView.isHidden = true
            self.searchBarTopTags.keyboardIsOpen = !self.searchBarTopTags.keyboardIsOpen
            self.searchBarTopTags.searchBar.resignFirstResponder()
//            self.searchBarTopTags.topTagsCollectionView.isHidden = true
        }
            
        else{
            self.searchBarTopTags.tagsSelected.removeAll()
            self.searchBarTopTags.topTagsCollectionView.reloadData()
            self.searchBarTopTags.myView.isHidden = false
            self.searchBarTopTags.keyboardIsOpen = !self.searchBarTopTags.keyboardIsOpen
            self.searchBarTopTags.searchBar.becomeFirstResponder()
            self.searchBarTopTags.topTagsCollectionView.isHidden = false
        }
    }
    
    
    //    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    //        if segue.identifier == "unwindToCollectionView"{
    //            let CVC = segue.destination as! CollectionViewController
    //            if let image = image{
    //                CVC.images.insert(imagePost(image:image), at: 0)
    //                print("Image is not nil")
    //            }
    //            let index = IndexPath(item: 0, section: 0)
    //            //CVC.collectionView?.reloadData()
    //            //  CVC.collectionView?.insertItems(at: [index])
    //            //CVC.collectionView?.reloadItems(at: [index])
    //            print("In prepare for segue: \(CVC.images.count)")
    //        }
    //    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //    override func viewWillDisappear(_ animated: Bool) {
    //        super.viewWillDisappear(animated)
    //        print("view will be dismissed")
    //    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("view has been dismissed")
    }
}

extension PreviewController:UIGestureRecognizerDelegate{
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil){
        print("inside of handle tap")
        //uncomment to make work
//        if self.searchBarTopTags.keyboardIsOpen{
//            self.searchBarTopTags.myView.isHidden = true
//            self.searchBarTopTags.keyboardIsOpen = !searchBarTopTags.keyboardIsOpen
//            self.searchBarTopTags.searchBar.resignFirstResponder()
//        }
//            
//        else{
//            self.searchBarTopTags.myView.isHidden = false
//            self.searchBarTopTags.keyboardIsOpen = !self.searchBarTopTags.keyboardIsOpen
//            self.searchBarTopTags.searchBar.becomeFirstResponder()
//        }
        if self.searchBarTopTags.keyboardIsOpen{
            self.searchBarTopTags.myView.isHidden = true
            self.searchBarTopTags.keyboardIsOpen = !self.searchBarTopTags.keyboardIsOpen
            self.searchBarTopTags.searchBar.resignFirstResponder()
            //            self.searchBarTopTags.topTagsCollectionView.isHidden = true
        }
            
        else{
            self.searchBarTopTags.tagsSelected.removeAll()
            self.searchBarTopTags.topTagsCollectionView.reloadData()
            self.searchBarTopTags.myView.isHidden = false
            self.searchBarTopTags.keyboardIsOpen = !self.searchBarTopTags.keyboardIsOpen
            self.searchBarTopTags.searchBar.becomeFirstResponder()
            self.searchBarTopTags.topTagsCollectionView.isHidden = false
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        //        if (touch.view?.isDescendant(of: self.collectionView))!{
        //            return false
        //        }
        //        return true
        
        print("Inside of recognizer")
        return true
    }
    
    
    @objc func viewTapped(gestureRecognizer:UIGestureRecognizer){
        
        print("Picture View Tapped")
        ////        let p = gestureRecognizer.location(in: self.collectionView)
        ////        let indexPath = self.collectionView.indexPathForItem(at: p)
        ////
        ////        print("The currentFirst responder is: \(self.view.currentFirstResponder())")
        ////
        ////        if let index = indexPath {
        ////            var cell = self.collectionView.cellForItem(at: index)
        ////            // do stuff with your cell, for example print the indexPath
        ////            print(index.row)
        ////        } else {
        ////            print("Could not find index path")
        ////        }
        //   //     self.view.becomeFirstResponder()
        
        
        //Good Code
        //        if searchBar.isFirstResponder{
        //            myView.isHidden = true
        //            searchBar.isHidden = true
        //            collectionView.isHidden = true
        //            searchBar.resignFirstResponder()
        //        }
        //        else{
        //            myView.isHidden = false
        //            searchBar.isHidden = false
        //            collectionView.isHidden = false
        //            self.view.bringSubview(toFront: collectionView)
        //            //searchBar.becomeFirstResponder()
        //            gestureRecognizer.cancelsTouchesInView = false
        //            self.searchBar.becomeFirstResponder()
        //            //self.collectionView.becomeFirstResponder()
        //        }
        
        if searchBarTopTags.keyboardIsOpen{
            searchBarTopTags.myView.isHidden = true
            searchBarTopTags.keyboardIsOpen = !searchBarTopTags.keyboardIsOpen
            searchBarTopTags.searchBar.resignFirstResponder()
        }
            
        else{
            searchBarTopTags.myView.isHidden = false
            searchBarTopTags.keyboardIsOpen = !searchBarTopTags.keyboardIsOpen
            searchBarTopTags.searchBar.becomeFirstResponder()
        }
        
        
    }
    
    func toolBarAddLine(fromPoint start: CGPoint, toPoint end:CGPoint,_ layer:CALayer? = nil) {
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        linePath.move(to: start)
        linePath.addLine(to: end)
        line.path = linePath.cgPath
        line.strokeColor = colors.toolBarColor.cgColor
        line.lineWidth = 4.5
        line.lineJoin = CAShapeLayerLineJoin.round
        self.view.layer.insertSublayer(line, above: layer)
    }
    
}

extension PreviewController: postsButtonDelegate{
    func disablePostButton(isEnabled: Bool) {
        if isEnabled{
            self.postsButton.isEnabled = true
        }
        else{
            self.postsButton.isEnabled = false
        }
    }
}


extension PreviewController:UICollectionViewDelegateFlowLayout{
    
    func setUpLayout(){
        //Top Tags CollectionView
        searchBarTopTags.topTagsCollectionView.contentInset = UIEdgeInsets(top: 2, left: 0, bottom: -2, right: 2)
        searchBarTopTags.topTagsCollectionView.translatesAutoresizingMaskIntoConstraints = false
        searchBarTopTags.topTagsCollectionView.topAnchor.constraint(equalTo: self.lineView.bottomAnchor, constant: 5).isActive = true
        searchBarTopTags.topTagsCollectionView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 5).isActive = true
        searchBarTopTags.topTagsCollectionView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -5).isActive = true
        searchBarTopTags.topTagsCollectionView.heightAnchor.constraint(equalToConstant:(self.view.bounds.width/3)+4).isActive = true
        
        //myView
        searchBarTopTags.myView.translatesAutoresizingMaskIntoConstraints = false
        searchBarTopTags.myView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        searchBarTopTags.myView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        searchBarTopTags.myView.heightAnchor.constraint(equalToConstant: 98).isActive = true
        searchBarTopTags.myViewBottomLayoutConstraint = searchBarTopTags.myView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0)
        searchBarTopTags.myViewBottomLayoutConstraint.isActive = true
        
        //PictureView
        searchBarTopTags.PictureView.translatesAutoresizingMaskIntoConstraints = false
        //        self.PictureView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        //        self.PictureView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        //        self.PictureView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        //        self.PictureView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        searchBarTopTags.PictureView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        searchBarTopTags.PictureView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0).isActive = true
        searchBarTopTags.PictureView.backgroundColor = .purple
        searchBarTopTags.view.sendSubviewToBack(searchBarTopTags.PictureView)
        
        //        //PictureView Background
        //        self.PictureViewBackground.translatesAutoresizingMaskIntoConstraints = false
        //        self.PictureViewBackground.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        //        self.PictureViewBackground.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        //        self.PictureViewBackground.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        //        self.PictureViewBackground.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        //        self.PictureViewBackground.backgroundColor = .yellow
        //       self.view.sendSubview(toBack: PictureViewBackground)
        
        //Search Tags CollectionView
        searchBarTopTags.searchTagsCollectionView.translatesAutoresizingMaskIntoConstraints = false
        searchBarTopTags.searchTagsCollectionView.leadingAnchor.constraint(equalTo: searchBarTopTags.myView.leadingAnchor, constant: 0).isActive = true
        searchBarTopTags.searchTagsCollectionView.trailingAnchor.constraint(equalTo: searchBarTopTags.myView.trailingAnchor, constant: 0).isActive = true
        searchBarTopTags.searchTagsCollectionView.bottomAnchor.constraint(equalTo: searchBarTopTags.myView.bottomAnchor, constant: -4).isActive = true
        searchBarTopTags.searchTagsCollectionView.topAnchor.constraint(equalTo: searchBarTopTags.searchBar.bottomAnchor, constant: 4).isActive = true
        searchBarTopTags.searchTagsCollectionView.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        //        Search Bar
        searchBarTopTags.searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBarTopTags.searchBar.topAnchor.constraint(equalTo: searchBarTopTags.myView.topAnchor, constant: 0).isActive = true
        searchBarTopTags.searchBar.leadingAnchor.constraint(equalTo: searchBarTopTags.myView.leadingAnchor, constant: 4).isActive = true
        //self.searchBar.trailingAnchor.constraint(equalTo: self.myView.trailingAnchor, constant: -4).isActive = true
        searchBarTopTags.searchBar.centerXAnchor.constraint(equalTo: searchBarTopTags.myView.centerXAnchor, constant: 0).isActive = true
        searchBarTopTags.searchBar.heightAnchor.constraint(equalToConstant: 45).isActive = true
        searchBarTopTags.searchBar.backgroundColor = .white
        searchBarTopTags.searchBar.layer.borderWidth = 1
        searchBarTopTags.searchBar.layer.cornerRadius = searchBarTopTags.searchBar.frame.width/50
        searchBarTopTags.searchBar.layer.borderColor = UIColor(red: 56/255, green: 217/255, blue: 169/255, alpha: 1.0).cgColor
        ////////////////////////////////////////////
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Inside of didSelectItemAterwertw")
        if collectionView == searchBarTopTags.searchTagsCollectionView{
            print("Inside of didSelectItemAterwertw")
            if indexPath.section == 1{
                print("indexPath selected: \(indexPath.item)")
                print("Size of currentGenericArray: \(searchBarTopTags.searchTagsArray.count)")
                
                print("Selected item: \(searchBarTopTags.searchTagsArray[indexPath.item])")
                if(searchBarTopTags.tagsSelected.contains(searchBarTopTags.searchTagsArray[indexPath.item])){
                    print("This element is already in the array")
                }
                
                searchBarTopTags.topFiveTags.removeAll()
                searchBarTopTags.topFiveTags = ["",searchBarTopTags.searchTagsArray[indexPath.item]]
                searchBarTopTags.wasSearchTagSelected = true
            //    searchBarTopTags.wasTopTagSelected = true
             //   searchBarTopTags.topTagsCollectionView.reloadData()
               
            
                
                //tagsSelected.insert(currentGenericArray[indexPath.item], at: 0)
                searchBarTopTags.tagsSelected.append(searchBarTopTags.searchTagsArray[indexPath.item])
                //tagsSelected.insert(currentGenericArray[indexPath.item], at: tagsSelected.count)
                if searchBarTopTags.genericArray.count > 0{
                    for i in 0...searchBarTopTags.genericArray.count-1{
                        if(searchBarTopTags.searchTagsArray[indexPath.item] == searchBarTopTags.genericArray[i]){
                            print("index:\(i)->Item removed: \(searchBarTopTags.genericArray[i])")
                            searchBarTopTags.genericArray.remove(at: i)
                            break
                        }
                    }
                }
                searchBarTopTags.searchTagsArray.remove(at: indexPath.item)
                //new stuff
                //currentGenericArray = genericArray
                searchBarTopTags.searchBar.text = ""
                //
                self.searchBarTopTags.myView.isHidden = true
                self.searchBarTopTags.keyboardIsOpen = false
                self.searchBarTopTags.searchBar.resignFirstResponder()
                self.postsButton.isEnabled = true
                searchBarTopTags.selectedIndexPath = IndexPath(item: 1, section: 0)
                searchBarTopTags.searchTagsCollectionView.reloadData()
                if searchBarTopTags.searchTagsCollectionView.numberOfItems(inSection: 1)>0{
                    searchBarTopTags.searchTagsCollectionView.scrollToItem(at: IndexPath(item: 0, section: 1), at: .right, animated: true)
                }
            }
            else if indexPath.section == 0{
                searchBarTopTags.searchTagsArray.append(searchBarTopTags.tagsSelected[indexPath.item])
                searchBarTopTags.tagsSelected.remove(at: indexPath.item)
                searchBarTopTags.searchTagsCollectionView.reloadData()
            }
        }
        else if collectionView == searchBarTopTags.topTagsCollectionView && indexPath == IndexPath(item: 5, section: 0) {
            //            searchBarTopTags.myView.isHidden = false
            //            searchBarTopTags.keyboardIsOpen = !searchBarTopTags.keyboardIsOpen
            //            searchBarTopTags.searchBar.becomeFirstResponder()
            if self.searchBarTopTags.keyboardIsOpen{
                self.searchBarTopTags.myView.isHidden = true
                self.searchBarTopTags.keyboardIsOpen = !self.searchBarTopTags.keyboardIsOpen
                self.searchBarTopTags.searchBar.resignFirstResponder()
            }
                
            else{
                self.searchBarTopTags.myView.isHidden = false
                self.searchBarTopTags.keyboardIsOpen = !self.searchBarTopTags.keyboardIsOpen
                self.searchBarTopTags.searchBar.becomeFirstResponder()
            }
            return
        }
        else{
            print("top tags selected: \(searchBarTopTags.topFiveTags[indexPath.item])")
           // searchBarTopTags.wasTopTagSelected = true
            let selectedTag:String = searchBarTopTags.topFiveTags[indexPath.item]
            searchBarTopTags.selectedIndexPath = IndexPath(item: 1, section: 0)//indexPath
            searchBarTopTags.wasSearchTagSelected = true
            searchBarTopTags.topFiveTags.removeAll()
            searchBarTopTags.topFiveTags = ["",selectedTag]
            self.postsButton.isEnabled = true
            self.searchBarTopTags.myView.isHidden = true
            self.searchBarTopTags.keyboardIsOpen = !self.searchBarTopTags.keyboardIsOpen
            self.searchBarTopTags.searchBar.resignFirstResponder()
          //  searchBarTopTags.topTagsCollectionView.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        //        if collectionView == self.searchBarTopTags.searchTagsCollectionView{
        //            if section == 0{
        //                return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        //            }
        //            else{
        //               return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        //            }
        //        }
        return UIEdgeInsets()
    }
    

    func getClosestTags(){
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate, let location = appDelegate.userLocation else{
            print("Error converting to AppDelegate in previewController")
            return
        }
        let longitude = location.longitude
        let latitude = location.latitude
        
        Functions.functions().httpsCallable("getClosestTags").call(["latitude": latitude, "longitude":longitude], completion: {(result,error) in
            if let error = error{
                print("Error getting top 3 tags for the array: \(error)")
            }
            
            guard let dataArr = result?.data as? [Any] else{
                print("Failed to parse top three tags dataArr")
                return
            }
            var tempArr:[String] = []
            for data in dataArr{
                print("data from dataArr: \(data)")
                
                guard let data = data as? [String:Any], let name = data["name"] as? String, let tagID = data["tagID"] as? String else{
                    print("Failed to parse top three tags data")
                    return
                }
            tempArr.append(name)
            }
            self.searchBarTopTags.topFiveTags.removeAll()
            self.searchBarTopTags.searchTagsArray.removeAll()
            self.searchBarTopTags.topFiveTags = Array(tempArr[0..<3])
            self.searchBarTopTags.searchTagsArray = tempArr
            tempArr.removeAll()
            self.searchBarTopTags.topTagsCollectionView.reloadData()
            self.searchBarTopTags.searchTagsCollectionView.reloadData()
        })
    }
    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    //        return 10
    //    }
}


