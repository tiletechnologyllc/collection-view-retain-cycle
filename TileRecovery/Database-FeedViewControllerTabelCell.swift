//
//  Database-FeedViewControllerTabelCell.swift
//  TileRecovery
//
//  Created by 123456 on 2/13/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit
import FirebaseFunctions
import FirebaseDatabase

extension FeedViewControllerTableCell{
    @objc func doubleTapped(){
        print("I was double tapped: \(self.postID)")
        if self.userLikedPost{
            print("User wants to unlike post")
            self.post.totalLikes -= 1
            self.userLikedPost = false
            functions.httpsCallable("unlikeButtonTapped").call(["postID":self.postID,"userID":User.signedInUserID], completion: {(result,error) in
                if let error = error{
                    print("Error liking post: \(error)")
                }
                print("Result from unliking Button post: \(result?.data)")
            })

        }else{
            print("User wants to like post")
            self.post.totalLikes += 1
             self.userLikedPost = true
            functions.httpsCallable("likeButtonTapped").call(["postID":self.postID,"userID":User.signedInUserID], completion: {(result,error) in
                if let error = error{
                    print("Error unliking post: \(error)")
                }
                print("Result from liking button post: \(result?.data)")
            })
        }
        self.likesLabel.text = String(self.post.totalLikes)
        Post.setPostToCache(post: self.post, tagUserID: self.post.tagID!)
    }
    
    func didUserLikePost(){
        guard let userID = User.signedInUserID, let postID = self.postID else{
            print("userID: \(User.signedInUserID) or postID: \(self.postID) dont exist")
            return
        }
        Database.database().reference(withPath: "UserLikes/\(userID)/\(postID)").observeSingleEvent(of: .value, with: {snapshot in
            if snapshot.exists(){
                print("user already liked post: \(self.postID)")
                self.userLikedPost = true
                print("snapshot data: \(snapshot.value)")
            }else{
                print("snpshot Data: \(snapshot.value)")
                print("snapshot path: \(snapshot.ref)")
                print("user does not like post: \(self.postID)")
                self.userLikedPost = false
            }
        }, withCancel: {error in
            print("Error checking if user already liked the post: \(error)")
            self.userLikedPost = false
        })
    }
    
    func reportPost(postID:String){
        functions.httpsCallable("postReported").call(["postID": postID, "userID":User.signedInUserID]) { (result, error) in
            if let error = error{
                print("There was an error reporting the post: \(error)")
                return
            }
            
            print("Result recieved from postReported: \(result?.data)")
            
        }
    }
}
