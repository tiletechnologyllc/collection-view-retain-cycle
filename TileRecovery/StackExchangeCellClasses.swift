//
//  Stack Exchange Cell Classes.swift
//  collectionViewAttemptOfPagingAttempt002
//
//  Created by 123456 on 8/23/18.
//  Copyright © 2018 TheRedCamaro. All rights reserved.
//

import Foundation
import UIKit

import Foundation

class CollectionViewManager: NSObject {
    
    public var cells: [CellModel] = []
    
    override init() {
        super.init()
        
        fillCells()
    }
    
    fileprivate func fillCells() {
        let arrayCellModels: [CellModel] = [
            RowModel(type: .MainSearchFeedScreenCell, title: "SearchFeedScreenCell"),
            RowModel(type: .MainCameraCollectionViewCell, title: "CameraCell"),
            RowModel(type: .MainProfileScreenCell, title: "ProfileScreenCell"),
            ]
        
        arrayCellModels.forEach { (cell) in
            cells.append(cell)
        }
    }
    
}

protocol CellModel: class{
    var type: CellTypes { get }
    var title: String { get }
}

enum CellTypes {
    case MainSearchFeedScreenCell
    case MainCameraCollectionViewCell
    case MainProfileScreenCell
    case MainFeedCollectionViewCell
    case FollowersProfileScreenCell
}

class RowModel: CellModel {
    var type: CellTypes
    var title: String
    
    init(type: CellTypes, title: String) {
        self.type = type
        self.title = title
    }
    
}

