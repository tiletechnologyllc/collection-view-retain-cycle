//
//  Post.swift
//  TileRecovery
//
//  Created by 123456 on 1/8/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import Firebase

class Post:CustomStringConvertible{
    static var storage:Storage!
    var storageRef:String
    var totalLikes:Int
    var storageReference:StorageReference? = nil
    var image:UIImage?
    var postID:String?
    var userID:String?
    var tagID:String?
    var tagName:String?
    let isUser:Bool!
    var userName:String?
    var photoURL:String
    var url:URL?{
        return URL(string: photoURL)
    }
    var weightedLikes:Int?
    //add in tagID
    
    init(storageRef:String,totalLikes:Int,postID:String,userID:String,tagID:String,isUser:Bool,photoURL:String,weightedLikes:Int? = nil){
        Post.storage = Storage.storage()
        self.storageRef = storageRef
        self.totalLikes = totalLikes
        self.postID = postID
        self.storageReference = Post.storage.reference(withPath: storageRef)
        self.tagID = tagID
        self.isUser = isUser
        self.userID = userID
        self.photoURL = photoURL
        self.weightedLikes = weightedLikes
    }
    
    var description: String {
     return "storageRef: \(self.storageRef), postID: \(self.postID)"
    }
    // static var PostCache = NSCache<AnyObject,AnyObject>()
    
    static var cache = NSCache<NSString,NSArray>(){
        didSet{
            print("Values added to cache)")
        }
    }
   //  var dictCache = NSCache<NSString,NSDictionary>()
    
    
    static func getPostsFromCache(tagID:String)->[Post]?{
        
        let NSTagID = tagID as NSString
        
        if let NSCachedPosts = cache.object(forKey: NSTagID){
            if let cachedPosts = NSCachedPosts as? [Post]{
                for post in cachedPosts{
                    print("Posts to be returned: \(post.postID)")
                }
                return cachedPosts
            }
            else{
                print("Unabel to convert cached posts to [Post]")
            }
        }
        
        //        if let postsFromCache = PostCache.object(forKey: tagID as AnyObject) as? [Post] {
        //            return postsFromCache
        //        }
        return nil
    }
    
    
    //    static func setPostCache(post:Post){
    //        PostCache.setObject(post, forKey: post.postID as AnyObject)
    //
    //    }
    
   
    
    static func setPostToCache(post:Post,tagUserID:String){
        print("Full post sent to cache: \(post)")
        print("Post sent to setPostToCahce: \(post.postID)")
        let posts = Post.getPostsFromCache(tagID: tagUserID)
        print("current posts: \(posts)")
        if var posts = posts{
            if posts.contains(post){
                //if crashing delete below here
                print("old posts: \(posts)")
                print("posts already contains that post: \(post.postID)")
               posts = posts.filter({(oldPost) in
                    print("old post postID: \(oldPost.postID), new post postID: \(post.postID)")
                    print("Is the old post already in the array: \((oldPost.postID == post.postID))")
                    return oldPost.postID != post.postID
                })
                print("new posts: \(posts)")
                //delete above here
                //return
            }
            //the tag already has posts in it
                posts.append(post)
                
           
            //PostCache.setObject(posts as AnyObject, forKey: tagID as AnyObject)
            let NSPosts = posts as NSArray
            let NSTagID = tagUserID as NSString
            cache.setObject(NSPosts, forKey: NSTagID)
            print("Post added to posts")
            for post in posts{
                print("posts after: \(post.postID)")
            }
            print("Size of NSPosts: \(NSPosts.count)")
        }
        else{
            //the tag doesnt already have posts in it
            let posts = [post]
            let NSPosts = posts as NSArray
            let NSTagID = tagUserID as NSString
            cache.setObject(NSPosts, forKey: NSTagID)
            print("Size of NSPosts: \(NSPosts.count)")
            print("Post added to posts")
            for post in posts{
                print("posts after: \(post.postID)")
            }
            print("Size of NSPosts: \(NSPosts.count)")
        }
        
    }
    
    static func createNewPost( tagName:String, image:UIImage, completion: ((Post,Tag)->Void)?){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{
            print("Failed to get app delegate")
            return
        }
        
        let name = tagName.lowercased()
        
        let location = appDelegate.userLocation
        let ref = Database.database().reference(withPath: "PostIDS").childByAutoId()
        
        guard let longitude = location?.longitude, let latitide = location?.latitude, let postID = ref.key,let userID = User.signedInUserID
/*       ,let imageData = image.jpegData(compressionQuality: 0.8)*/else{
            print("Error parsing location and postID")
            return
        }
 
        let imageData = image.compressDataFileSize()
        let storageMetaData:StorageMetadata = StorageMetadata()
        storageMetaData.contentType = "image/jpeg"
        let storageRef:StorageReference =  Storage.storage().reference(withPath: "Posts/\(postID).jpeg")
       storageRef.putData(imageData, metadata: storageMetaData, completion: {(metadata,error) in
            if let error = error{
                print("There was an error uploading the users post into storage: \(error)")
            }
        
        storageRef.downloadURL(completion: {(url,error) in
            if let error = error{
                print("Error getting downloadURL for uploaded post: \(error)")
            }
            if let url = url{
              
               let data:[String:Any] = ["longitude":longitude,"latitude":latitide,"userID":userID,"tagName":name,"photoURL":url.absoluteString,"postID":postID]
                print("Data to be sent: \(data)")
                if let completion = completion{
                    uploadPostCloudFunction(data: data,completion: completion)
                }else{
                    print("Completion was nil")
                }
                }
            })
        })
        
    }

    
    static func uploadPostCloudFunction(data:[String:Any],completion: ((Post,Tag)->Void)?){
    Functions.functions().httpsCallable("createPostData").call(data, completion: {(result,error) in
        if let error = error{
            print("Error calling create PostData function: \(error)")
            return
        }
        print("Result from calling createPostData: \(result?.data)")
    
        guard let resultData = result?.data as? [String:Any], let postData = resultData["postData"] as? [String:Any], let tagData = resultData["tagData"] as? [String:Any] else{
            print("Failed to parse result data")
            return
        }
        var tag:Tag!
        var post:Post!
        
        for (key,data) in postData{
            let postID = key
            print("key: \(key) => data: \(data)")
            guard let data = data as? [String:Any], let storageRef = data["storageRef"] as? String,
            let likes = data["likes"] as? [String:Any],
            let totalLikes = likes["totalLikes"] as? Int,
            let tagID = data["tagID"] as? String,
                let photoURL = data["photoURL"] as? String else{
                    print("Failed to parse postData")
                    return
            }
             post = Post(storageRef: storageRef, totalLikes: totalLikes, postID: postID, userID: User.signedInUserID
                , tagID: tagID, isUser: true, photoURL: photoURL)
        }
        
        for (key,data) in tagData{
            let tagID = key
            print("key: \(key) => data: \(data)")
            guard let data = data as? [String:Any], let name = data["name"] as? String, let location = data["location"] as? [String:Any], let storageRef = data["storageRef"] as? String, let photoURL = data["photoURL"] as? String else{
                print("Failed to parse tagData")
                return
            }
             tag = Tag(name: name, location: location, tagID: tagID, storageRef: storageRef, photoURL: photoURL)
        }
        
        if let tag = tag, let post = post, let completion = completion{
            Post.setPostToCache(post: post, tagUserID: post.tagID!)
            Tag.tagsCache.setObject(tag, forKey: tag.tagID as NSString)
            completion(post,tag)
        }else{
            print("Tag or post or completion were nil:\n\tpost:\(post),\n\ttag:\(tag),\n\tcompletion:\(completion)")
            return
        }
        
        
        
    })

    
    }
}

extension Post:Equatable {
    static func ==(lhs:Post,rhs:Post) -> Bool {
        return lhs.postID == rhs.postID && lhs.tagID == rhs.tagID && lhs.userID == rhs.userID
    }
}


