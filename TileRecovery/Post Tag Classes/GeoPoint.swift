//
//  GeoPoint.swift
//  TileRecovery
//
//  Created by 123456 on 1/8/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation

struct GeoPoint{
    var latitude:Double
    var longitude:Double
}
