//
//  InsideFeedCollectionViewCell.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 9/20/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import UIKit

class InsideFeedCollectionViewCell: UICollectionViewCell {
    var cellImageView: UIImageView!
    var cellLabel:UILabel!
    //    override func prepareForReuse() {
    //        self.cellImageView.image = nil
    //    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        cellImageView = UIImageView();
        cellImageView.contentMode = .scaleAspectFill
        self.addSubview(cellImageView)
        
        cellLabel = UILabel()
        self.cellImageView.addSubview(cellLabel)
        
        
        
        cellImageView.translatesAutoresizingMaskIntoConstraints = false
        
        
        cellImageView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        cellImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        cellImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        cellImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        
        cellLabel.translatesAutoresizingMaskIntoConstraints = false
        cellLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
        cellLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10).isActive = true
        cellLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        cellLabel.widthAnchor.constraint(equalTo: self.cellImageView.widthAnchor, constant: 0).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
