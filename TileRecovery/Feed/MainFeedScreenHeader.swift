//
//  MainFeedScreenHeader.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 2/10/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit
import FirebaseFunctions

class MainFeedScreenHeader:UICollectionReusableView{
    
    var headerLabel:UILabel!
    var followButton:FollowButton!
    
    var tagID:String!
    var doesSignedInUserFollow:Bool!
    
    weak var followButtonTappedDelegate:FollowButtonTappedDelegate!
    
    weak var reloadSegmentedViewControllerDelegate:ReloadSegmentedViewControllerDelegate?
    
    lazy var functions:Functions = Functions.functions()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let searchBarHeight = 45
        self.headerLabel = UILabel(frame: CGRect(x: 20, y: 0, width: Int(self.bounds.width), height: searchBarHeight*2))
        self.addSubview(headerLabel)
        headerLabel.textAlignment = .left
        headerLabel.baselineAdjustment = .alignBaselines
        headerLabel.font = UIFont.boldSystemFont(ofSize: 25)
        headerLabel.textColor = colors.toolBarColor
        
        followButton = FollowButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        self.addSubview(followButton)
        followButton.addTarget(self, action: #selector(followButtonTapped), for: .touchUpInside)
        
        followButton.translatesAutoresizingMaskIntoConstraints = false
        //followButton.leadingAnchor.constraint(equalTo: self.headerLabel.trailingAnchor, constant: 0).isActive = true
        followButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -16).isActive = true
        //followButton.topAnchor.constraint(equalTo: self.headerLabel.topAnchor, constant: 0).isActive = true
        followButton.bottomAnchor.constraint(equalTo: self.headerLabel.bottomAnchor, constant: 0).isActive = true
        followButton.heightAnchor.constraint(equalToConstant:25).isActive = true
        followButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        followButton.layer.borderWidth = 2
        followButton.layer.borderColor = colors.toolBarColor.cgColor
        
        followButton.setTitle("Follow", for: .normal)
        followButton.setTitleColor(colors.toolBarColor, for: .normal)
        followButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        
        followButton.layer.cornerRadius = followButton.bounds.width/2
    
        
        //        headerLabel.layer.borderColor = UIColor.red.cgColor
        //        headerLabel.layer.borderWidth = 4
        //
        //        self.layer.borderWidth = 4
        //        self.layer.borderColor = UIColor.yellow.cgColor
        self.bringSubviewToFront(followButton)
    }
    
    
    
    @objc func followButtonTapped(){
        print("Follow Button Tapped")
        if self.doesSignedInUserFollow == true{
            print("Tag: \(self.tagID) should be unfollowed")
            self.unselectedFollow()
            
            self.functions.httpsCallable("unfollowTapped").call(["followID":self.tagID,"userID":User.signedInUserID], completion: {(result,error) in
                if let error = error{
                    print("Error unfollowing tag: \(error)")
                }
                User.signedInUserFollowingCache.removeObject(forKey: self.tagID as NSString)
                print("\(self.tagID) was removed from the following cache")
                print("Result from unfollowing user: \(result?.data)")
            })
            self.followButtonTappedDelegate.followButtonTapped(doesUserFollow: self.doesSignedInUserFollow)
        }else{
            print("Tag: \(self.tagID) should be followed")
            self.selectedFollow()
            
            self.functions.httpsCallable("followTapped").call(["tagID":self.tagID,"userID":User.signedInUserID], completion: {(result,error) in
                if let error = error{
                    print("Error following tag: \(error)")
                }
                print("Result from following tag: \(result?.data)")
            })
            self.followButtonTappedDelegate.followButtonTapped(doesUserFollow: self.doesSignedInUserFollow)
        }
    }
    
    func setUpFollowButton(){
        print("Self.tagID: \(self.tagID)")
        print("cached item for: \(self.tagID) => \(User.signedInUserFollowingCache.object(forKey: self.tagID as NSString))")
        
        if let following = User.signedInUserFollowingCache.object(forKey: self.tagID as NSString){
            print("User does follow from cache: \(self.tagID)")
                self.selectedFollow()
        }else{
           User.checkForFollowing(followID: self.tagID, completion: {(isFollowing) in
            print("isFollowing: \(isFollowing)")
            if isFollowing{
                print("User does does Follow \(self.tagID)")
                self.selectedFollow()
            }
            else{
                print("User does not follow: \(self.tagID)")
                self.unselectedFollow()
            }
           })
        }

        }
    
    func unselectedFollow(){
                print("Tag: \(self.tagID) is not being followed")
                self.followButton.setTitle("Follow", for: .normal)
                self.followButton.setTitleColor(tileColor, for: .normal)
                self.followButton.backgroundColor = backgroundColor
                self.doesSignedInUserFollow = false
    }
    
    func selectedFollow(){
                print("Tag: \(self.tagID) is being followed")
                self.followButton.setTitle("Following", for: .normal)
                self.followButton.setTitleColor(backgroundColor, for: .normal)
                self.followButton.backgroundColor = tileColor
                self.doesSignedInUserFollow = true
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
