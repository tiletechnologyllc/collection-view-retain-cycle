//
//  ReloadSegementedViewController-Delegate.swift
//  TileRecovery
//
//  Created by 123456 on 2/13/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

protocol ReloadSegmentedViewControllerDelegate: class {
    func reloadSegmentedViewController()
}

extension MainViewController:ReloadSegmentedViewControllerDelegate{
    func reloadSegmentedViewController() {
        print("reload segmentedViewController called")
        for i  in 0..<self.collectionViewManager.cells.count{
            if self.collectionViewManager.cells[i].type == .MainProfileScreenCell{
                if let cell = MainCollectionView.cellForItem(at: IndexPath(item: i, section: 0)) as? MainProfileScreenCell{
                    cell.collectionView.reloadData()
                }
            }
        }
        //self.profileScreenSegmentedViewController.collectionView.reloadData()
    }
}
