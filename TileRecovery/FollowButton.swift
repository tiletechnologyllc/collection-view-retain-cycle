//
//  FollowButton.swift
//  TileRecovery
//
//  Created by 123456 on 2/18/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

class FollowButton:UIButton{
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        super.point(inside: point, with: event)
        let area = self.bounds.insetBy(dx: -10, dy: -30)
        print("point called")
        return area.contains(point)
    }
//    override func point(inside point: CGPoint, with _: UIEvent?) -> Bool {
//       // let margin: CGFloat = 5
//        let area = self.bounds.insetBy(dx: -10, dy: -30)
//        print("point: \(point)")
//        return area.contains(point)
//    }
}
