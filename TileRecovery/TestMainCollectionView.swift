//
//  TestMainCollectionView.swift
//  TileRecovery
//
//  Created by 123456 on 3/29/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

/*
 
 In Main FollowersProfileScreenCell
 -Add in alertPhotoAlbumDelegate:alertControllerPhotoAlbumDelegate
 -Add in cameraDelegate:alertControllerCameraDelegate
 -Remove SpecialAddCellDelegate
 -Add in addRemoveCell as Delegate
 
 */
import Foundation
import UIKit

fileprivate var resuseIdentifier:String = "FollowersProfileScreenCell"

class TestMainCollectionView:UIViewController{
    
    var selectedUser:User?
    var selectedTag:Tag?
    var cellTapped:Bool = false
    
    var collectionView:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.green
        cv.isPagingEnabled = true
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    
    var collectionViewManager = CollectionViewManager()
    
    var arr:[Int] = [0,1,2]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.green
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(FollowersProfileScreenCell.self, forCellWithReuseIdentifier: resuseIdentifier)
//        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        self.view.addSubview(collectionView)
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: self.view.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)
            ])

    }
    
  
    
    
}

extension TestMainCollectionView:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: resuseIdentifier, for: indexPath) as? FollowersProfileScreenCell else{
            fatalError("Unexpected Cell in dequeReuseable cell")
        }
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        if indexPath.item % 2 == 0{
        cell.backgroundColor = UIColor.red
        }else{
            cell.backgroundColor = UIColor.orange
        }
        
        cell.userID = self.selectedUser?.userID ?? User.signedInUserID
        
        cell.alertControllerDelegate = self
        cell.menuTableViewDelegate = self
        cell.selectedCellDelegate = self
        cell.specialAddOrRemoveCellDelegate = self
        

        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? FollowersProfileScreenCell else{
            fatalError("Unexcpected Cell Type")
        }
        cell.getFollowing()
        cell.getUsernameProfilePicture()
    }
}

extension TestMainCollectionView:UICollectionViewDelegate{

}

extension TestMainCollectionView:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: self.view.bounds.width - 0, height: self.view.bounds.height)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension TestMainCollectionView:AlertControllerDelegate{
    
    func presentAlertController(alertController: UIAlertController,completion:(() -> Void)? = nil) {
        self.present(alertController, animated: true, completion: {
            print("Alert View Controller Presented")
            if let completion = completion{
                completion()
            }
        })
    }
    
}

extension TestMainCollectionView:MenuTableViewDelegate{
    func removeMenuTableView(menuTableViewController:UIViewController) {
        menuTableViewController.willMove(toParent: nil)
        menuTableViewController.view.removeFromSuperview()
        menuTableViewController.removeFromParent()
    }
    
    func addMenuTableView(menuTableViewController: UIViewController) -> UIView {
        self.addChild(menuTableViewController)
        menuTableViewController.didMove(toParent: self)
        guard let menuTableView = menuTableViewController.view else{
            print("Menu tableView empty")
            return UIView()
        }
        
        return menuTableView
    }
}

protocol SelectedCellDelegate:class {
    func setSelectedCell(selectedUser: User?, selectedTag: Tag?) 
}

extension TestMainCollectionView:SelectedCellDelegate{
    func setSelectedCell(selectedUser: User? = nil, selectedTag: Tag? = nil) {
        print("set selectedCell called")
        if let selectedTag = selectedTag{
            print("TagID in selectedTag: \(selectedTag.tagID)")
            self.selectedTag = selectedTag
        }
        else if let selectedUser = selectedUser{
            self.selectedUser = selectedUser
            print("userID in selectedUser: \(self.selectedUser?.userID)")
            self.cellTapped = true
        }
        else{
            print("Failed to select either user or tag from setSelectedCell")
        }
        
    }
}

