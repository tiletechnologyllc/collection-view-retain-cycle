//
//  MainCameraCollectionViewCell2.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 9/20/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import UIKit
import AVFoundation

class MainCameraCollectionViewCell2: UICollectionViewCell {
    private var captureSession = AVCaptureSession()
    private var sessionQueue: DispatchQueue!
    private var captureDevice: AVCaptureDevice!
    private  var photoOutPut: AVCapturePhotoOutput!
    private var cameraPreviewLayer: AVCaptureVideoPreviewLayer!
    var image: UIImage?
    var usingFrontCamera = false
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCaptureSession()
        setupDevice()
        setupInput()
        setupPreviewLayer()
        startRunningCaptureSession()
    }
    func setupCaptureSession(){
        captureSession.sessionPreset = .photo
        sessionQueue = DispatchQueue(label: "session queue")
    }
    func setupDevice(usingFrontCamera: Bool = false){
        sessionQueue.async {
            let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: .video, position: .unspecified)
            let devices = deviceDiscoverySession.devices
            for device in devices {
                if usingFrontCamera && device.position == .front {
                    self.captureDevice = device
                } else if device.position == .back {
                    self.captureDevice = device
                }
            }
        }
    }
    func setupInput() {
        sessionQueue.async {
            do {
                let captureDeviceInput = try AVCaptureDeviceInput(device: self.captureDevice)
                if self.captureSession.canAddInput(captureDeviceInput) {
                    self.captureSession.addInput(captureDeviceInput)
                }
                self.photoOutPut = AVCapturePhotoOutput()
                self.photoOutPut.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format:[AVVideoCodecKey: AVVideoCodecType.jpeg])], completionHandler: nil)
                if self.captureSession.canAddOutput(self.photoOutPut) {
                    self.captureSession.addOutput(self.photoOutPut)
                }
            } catch {
                print(error)
            }
        }
    }
    func setupPreviewLayer() {
        cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        cameraPreviewLayer.videoGravity = .resizeAspectFill
        cameraPreviewLayer.connection?.videoOrientation = .portrait
        cameraPreviewLayer.frame = UIScreen.main.bounds
        layer.insertSublayer(cameraPreviewLayer, at: 0)
    }
    func startRunningCaptureSession() {
        captureSession.startRunning()
    }
    @IBAction func cameraButton_TouchUpInside(_ sender: Any) {
        let settings = AVCapturePhotoSettings()
        photoOutPut.capturePhoto(with: settings, delegate: self as! AVCapturePhotoCaptureDelegate)
    }
    //Flip to front and back camera
    @IBAction func FlipThe_camera(_ sender: UIButton) {
        captureSession.beginConfiguration()
        if let inputs = captureSession.inputs as? [AVCaptureDeviceInput] {
            for input in inputs {
                captureSession.removeInput(input)
            }
        }
        usingFrontCamera = !usingFrontCamera
        setupCaptureSession()
        setupDevice(usingFrontCamera: usingFrontCamera)
        setupInput()
        captureSession.commitConfiguration()
        startRunningCaptureSession()
    }
}

