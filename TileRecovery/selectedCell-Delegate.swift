//
//  selectedCell-Delegate.swift
//  TileRecovery
//
//  Created by 123456 on 1/16/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

protocol SelectedCellDelegate: class {
    func setSelectedCell(selectedUser:User?,selectedTag:Tag?)
}

extension MainViewController:SelectedCellDelegate{
    func setSelectedCell(selectedUser: User? = nil, selectedTag: Tag? = nil) {
        print("set selectedCell called")
        if let selectedTag = selectedTag{
            print("TagID in selectedTag: \(selectedTag.tagID)")
            self.selectedTag = selectedTag
        }
        else if let selectedUser = selectedUser{
            self.selectedUser = selectedUser
            print("userID in selectedUser: \(self.selectedUser?.userID)")
            self.cellTapped = true
            self.selectedUsers.append(selectedUser)
        }
        else{
            print("Failed to select either user or tag from setSelectedCell")
        }
        
    }
}
