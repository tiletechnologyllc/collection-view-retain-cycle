//import Foundation
//import UIKit
//import MessageUI
//
//class ContactUsTermsViewController:UIViewController,UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate{
//
//    var tableView:UITableView!
//    //var accountOptions:[String] = ["Change Username", "Change Password", "Change Email"]
//
//    var settingChoice:menuOption?
//    var dismissButton:UIButton!
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        tableView = UITableView(frame: self.view.frame)
//        tableView.backgroundColor = colors.backgroundColor
//        tableView.delegate = self
//        tableView.dataSource = self
//        tableView.separatorStyle = .none
//        self.view.addSubview(tableView)
//
//        dismissButton = UIButton()
//        self.view.addSubview(dismissButton)
//        dismissButton.backgroundColor = UIColor.brown
//        dismissButton.addTarget(self, action: #selector(dismissButtonTapped), for: .touchUpInside)
//
//        dismissButton.translatesAutoresizingMaskIntoConstraints = false
//        dismissButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
//        dismissButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
//        dismissButton.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20).isActive = true
//        dismissButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
//
//
//
//        tableView.translatesAutoresizingMaskIntoConstraints = false
//        tableView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
//        tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
//        tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
//        tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
//
//        //tableView.register(blockedAccountsCell.self, forCellReuseIdentifier: "blockedAccountsCell")
//        tableView.register(StaticCell.self, forCellReuseIdentifier: "staticCell")
//        self.view.bringSubviewToFront(dismissButton)
//    }
//
//    @objc func dismissButtonTapped(){
//        print("Dismiss Button tapped")
//
//        self.dismiss(animated: true, completion: {
//            print("view dismissed")
//        })
//    }
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 50
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        //  let cell = tableView.dequeueReusableCell(withIdentifier: "blockedAccountsCell", for: indexPath) as! blockedAccountsCell
//        let cell = tableView.dequeueReusableCell(withIdentifier: "staticCell", for: indexPath) as! StaticCell
//        cell.backgroundColor = colors.backgroundColor
//
//        if settingChoice == menuOption.contactUs{
//            cell.myLabel.text = "Put Contact Information here"
//        }
//        else if settingChoice == menuOption.terms{
//            cell.myLabel.text = "Terms snd conditions go here"
//        }
//
//        let bgColorView = UIView()
//        bgColorView.backgroundColor = colors.toolBarColor.withAlphaComponent(0.5)
//        cell.selectedBackgroundView = bgColorView
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let vw = UIView()
//        vw.backgroundColor = colors.backgroundColor
//        let titleLabel = UILabel(frame: CGRect(x: vw.center.x, y: 0, width: 200, height: 50))
//
//        if settingChoice == menuOption.contactUs{
//            titleLabel.text = "Contact Us"
//        }
//        else if settingChoice == menuOption.terms{
//            titleLabel.text = "Terms"
//        }
//
//        titleLabel.textAlignment = .center
//        titleLabel.font = UIFont.boldSystemFont(ofSize: 25)
//        titleLabel.textColor = colors.toolBarColor
//        vw.addSubview(titleLabel)
//        titleLabel.translatesAutoresizingMaskIntoConstraints = false
//        titleLabel.leadingAnchor.constraint(equalTo: vw.leadingAnchor, constant: 30).isActive = true
//        titleLabel.centerYAnchor.constraint(equalTo: vw.centerYAnchor, constant: 0).isActive = true
//
//        let borderBottom = UIView(frame: CGRect(x:0, y:40, width: tableView.bounds.size.width, height: 4.0))
//        borderBottom.backgroundColor = UIColor.self.init(red: 5/255, green: 16/255, blue: 28/255, alpha: 1.0)
//        borderBottom.backgroundColor = colors.toolBarColor
//        vw.addSubview(borderBottom)
//
//        return vw
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print("row was selected")
//        if settingChoice == menuOption.contactUs{
//            sendEmail()
//            tableView.deselectRow(at: indexPath, animated: true)
//        }
//    }
//
//    func sendEmail() {
//        print("send email called")
//        if MFMailComposeViewController.canSendMail() {
//            let mail = MFMailComposeViewController()
//            mail.mailComposeDelegate = self as? MFMailComposeViewControllerDelegate
//            mail.setToRecipients(["tiletechnologyllc@gmail.com"])
//            mail.setSubject("Subject")
//            mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
//
//            present(mail, animated: true)
//        } else {
//            // show failure alert
//        }
//    }
//
//    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
//        // Dismiss the mail compose view controller.
//        controller.dismiss(animated: true, completion: nil)
//    }
//
//}
import Foundation
import UIKit
import MessageUI

class ContactUsTermsViewController:UIViewController,UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate{
    
    var tableView:UITableView!
    //var accountOptions:[String] = ["Change Username", "Change Password", "Change Email"]
    
    var settingChoice:menuOption?
    var dismissButton:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView = UITableView(frame: self.view.frame)
        tableView.backgroundColor = colors.backgroundColor
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        self.view.addSubview(tableView)
        
        dismissButton = UIButton()
        self.view.addSubview(dismissButton)
        //dismissButton.backgroundColor = UIColor.brown
        dismissButton.addTarget(self, action: #selector(dismissButtonTapped), for: .touchUpInside)
        dismissButton.setImage(#imageLiteral(resourceName: "icons8-expand-arrow-96"), for: .normal)
        
        dismissButton.translatesAutoresizingMaskIntoConstraints = false
        dismissButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        dismissButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        dismissButton.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20).isActive = true
        dismissButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
        
        
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        
        //tableView.register(blockedAccountsCell.self, forCellReuseIdentifier: "blockedAccountsCell")
        tableView.register(StaticCell.self, forCellReuseIdentifier: "staticCell")
        self.view.bringSubviewToFront(dismissButton)
    }
    
    @objc func dismissButtonTapped(){
        print("Dismiss Button tapped")
        
        self.dismiss(animated: true, completion: {
            print("view dismissed")
        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //  let cell = tableView.dequeueReusableCell(withIdentifier: "blockedAccountsCell", for: indexPath) as! blockedAccountsCell
        let cell = tableView.dequeueReusableCell(withIdentifier: "staticCell", for: indexPath) as! StaticCell
        cell.backgroundColor = colors.backgroundColor
        
        if settingChoice == menuOption.contactUs{
            cell.myLabel.text = "Put Contact Information here"
        }
        else if settingChoice == menuOption.terms{
            cell.myLabel.text = "Terms snd conditions go here"
        }
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = colors.toolBarColor.withAlphaComponent(0.5)
        cell.selectedBackgroundView = bgColorView
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView()
        vw.backgroundColor = colors.backgroundColor
        let titleLabel = UILabel(frame: CGRect(x: vw.center.x, y: 0, width: 200, height: 50))
        
        if settingChoice == menuOption.contactUs{
            titleLabel.text = "Contact Us"
        }
        else if settingChoice == menuOption.terms{
            titleLabel.text = "Terms"
        }
        
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.boldSystemFont(ofSize: 25)
        titleLabel.textColor = colors.toolBarColor
        vw.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.leadingAnchor.constraint(equalTo: vw.leadingAnchor, constant: 30).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: vw.centerYAnchor, constant: 0).isActive = true
        
        let borderBottom = UIView(frame: CGRect(x:0, y:40, width: tableView.bounds.size.width, height: 4.0))
        borderBottom.backgroundColor = UIColor.self.init(red: 5/255, green: 16/255, blue: 28/255, alpha: 1.0)
        borderBottom.backgroundColor = colors.toolBarColor
        vw.addSubview(borderBottom)
        
        return vw
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("row was selected")
        if settingChoice == menuOption.contactUs{
            sendEmail()
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    func sendEmail() {
        print("send email called")
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self as? MFMailComposeViewControllerDelegate
            mail.setToRecipients(["tiletechnologyllc@gmail.com"])
            mail.setSubject("Customer Support")
            mail.setMessageBody("<p style = \"color: #38d9a9;\">You're so awesome!</p>", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
}

