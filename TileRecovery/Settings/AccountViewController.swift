//
//  AccountViewController.swift
//  collectionViewAttemptOfPagingAttempt002
//
//  Created by Thomas M. Jumper on 9/17/18.
//  Copyright © 2018 TheRedCamaro. All rights reserved.
//

import Foundation
import UIKit

enum accountOptions{
    case changeUsername
    case changePassword
    case changeEmail
}

struct Section {
    var name: String
    var items: [String]
    var collapsed: Bool
    var settingType:accountOptions?
    
    init(name: String, items: [String],settingType:accountOptions? ,collapsed: Bool = true) {
        self.name = name
        self.items = items
        self.collapsed = collapsed
        self.settingType = settingType
    }
}



class AccountViewController:UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    var doneButton:UIButton!
    var tableView:UITableView!
    var accountSettings:[String] = ["Change Username", "Change Password", "Change Email"]
    var passwordArray:[String] = [""]
    var newPassWord:String?
    var newEmail:String?
    var newUserName:String?
    var passwordsMatch:Bool = true
    
    //  var sections:[Section] = [Section(name: "Account", items: [],settingType: nil),Section(name: "Change Username", items: ["Enter New User Name"], settingType: accountOptions.changeUsername),Section(name: "Change Password", items: ["Enter new Password","Confirm new Password"],settingType: accountOptions.changePassword),Section(name: "Change Email", items: ["Enter New Email"],settingType:accountOptions.changeEmail)]
    
    var sections:[Section] = [Section(name: "Account", items: [], settingType: nil),Section(name: "Change Username", items: ["Enter New User Name"], settingType: accountOptions.changeUsername),Section(name: "Change Password", items: ["Enter new Password","Confirm new Password"], settingType: accountOptions.changePassword),Section(name: "Change Email", items: ["Enter New Email"], settingType: accountOptions.changeEmail)]
    var dismissButton:UIButton!
    var optionSelected:accountOptions?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView = UITableView(frame: self.view.frame)
        tableView.backgroundColor = colors.backgroundColor
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        self.view.addSubview(tableView)
        
        dismissButton = UIButton()
        self.view.addSubview(dismissButton)
        //dismissButton.backgroundColor = UIColor.brown
        dismissButton.addTarget(self, action: #selector(dismissButtonTapped), for: .touchUpInside)
        dismissButton.setImage(#imageLiteral(resourceName: "icons8-expand-arrow-96"), for: .normal)
        
        dismissButton.translatesAutoresizingMaskIntoConstraints = false
        dismissButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        dismissButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        dismissButton.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20).isActive = true
        dismissButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        
        doneButton = UIButton()
        self.view.addSubview(doneButton)
        doneButton.backgroundColor = colors.backgroundColor
        doneButton.layer.borderColor = colors.toolBarColor.cgColor
        doneButton.layer.borderWidth = 2
        doneButton.setTitleColor(colors.toolBarColor, for: .normal)
        doneButton.layer.cornerRadius = 60/5
        doneButton.layer.masksToBounds = true
        doneButton.clipsToBounds = true
        doneButton.setTitle("Done", for: .normal)
        doneButton.addTarget(self, action: #selector(doneButtonTapped), for: .touchUpInside)
        
        doneButton.translatesAutoresizingMaskIntoConstraints = false
        doneButton.heightAnchor.constraint(equalToConstant: 37).isActive = true
        doneButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        doneButton.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20).isActive = true
        doneButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
        
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        
        //tableView.register(blockedAccountsCell.self, forCellReuseIdentifier: "blockedAccountsCell")
        tableView.register(collapsibleTableViewCell.self, forCellReuseIdentifier: "collapsibleTableViewCell")
        tableView.register(CollapsibleTableViewHeader.self, forHeaderFooterViewReuseIdentifier: "header")
        self.view.bringSubviewToFront(dismissButton)
        
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableView.automaticDimension
        
        optionSelected = nil
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(endEditing))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func doneButtonTapped(){
        print("Done Button Tapped")
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func endEditing(){
        //tableView.keyboardDismissMode = .
        self.view.endEditing(true)
        self.tableView.endEditing(true)
    }
    
    @objc func dismissButtonTapped(){
        print("Dismiss Button tapped")
        let currentCells = tableView.visibleCells
        
        self.dismiss(animated: true, completion: {
            print("view dismissed")
        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if sections[section].collapsed{
            return 0
        }
        else{
            return sections[section].items.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //  let cell = tableView.dequeueReusableCell(withIdentifier: "blockedAccountsCell", for: indexPath) as! blockedAccountsCell
        
        if let accountOptionSelected = optionSelected{
            switch accountOptionSelected{
            case accountOptions.changeUsername:
                print("change user name")
            default:
                print("change username")
            }
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "collapsibleTableViewCell", for: indexPath) as! collapsibleTableViewCell
        cell.backgroundColor = colors.backgroundColor
        cell.incorrectPasswordLabel.isHidden = true
        if passwordsMatch == false && (indexPath == IndexPath(row: 0, section: 2) || indexPath == IndexPath(row: 1, section: 2) ){
            cell.backgroundColor = UIColor.red
            cell.incorrectPasswordLabel.isHidden = false
            cell.myTextField.backgroundColor = UIColor.red
        }
        else{
            cell.backgroundColor =  colors.backgroundColor
            cell.incorrectPasswordLabel.isHidden = true
            cell.myTextField.backgroundColor = colors.backgroundColor
        }
        cell.myTextField.placeholder = sections[indexPath.section].items[indexPath.row]
        cell.myTextField.delegate = self
        cell.textFieldType = sections[indexPath.section].settingType
        cell.myTextField.attributedPlaceholder = NSAttributedString(string: sections[indexPath.section].items[indexPath.row],
                                                                    attributes: [NSAttributedString.Key.foregroundColor: colors.toolBarColor.withAlphaComponent(0.75)])
        let bgColorView = UIView()
        bgColorView.backgroundColor = colors.toolBarColor.withAlphaComponent(0.5)
        cell.selectedBackgroundView = bgColorView
        return cell
    }
    
    //         func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //            let vw = UIView()
    //            vw.backgroundColor = colors.backgroundColor
    //            let titleLabel = UILabel(frame: CGRect(x: vw.center.x, y: 0, width: 200, height: 50))
    //
    //            titleLabel.text = "Account"
    //            titleLabel.textAlignment = .center
    //            titleLabel.font = UIFont.boldSystemFont(ofSize: 25)
    //            titleLabel.textColor = colors.toolBarColor
    //            vw.addSubview(titleLabel)
    //            titleLabel.translatesAutoresizingMaskIntoConstraints = false
    //            titleLabel.leadingAnchor.constraint(equalTo: vw.leadingAnchor, constant: 30).isActive = true
    //            titleLabel.centerYAnchor.constraint(equalTo: vw.centerYAnchor, constant: 0).isActive = true
    //
    //            let borderBottom = UIView(frame: CGRect(x:0, y:40, width: tableView.bounds.size.width, height: 4.0))
    //            borderBottom.backgroundColor = UIColor.self.init(red: 5/255, green: 16/255, blue: 28/255, alpha: 1.0)
    //            borderBottom.backgroundColor = colors.toolBarColor
    //            vw.addSubview(borderBottom)
    //
    //            return vw
    //        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let changeUserNameVC = ChangeUserNameViewController()
        //present(changeUserNameVC, animated: true, completion: nil)
        //            accountSettings.insert("type new account name", at: indexPath.item + 1)
        //            optionSelected = accountOptions.changeUsername
        //            tableView.reloadData()
        //            tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0{
            let vw = UIView()
            vw.backgroundColor = colors.backgroundColor
            let titleLabel = UILabel(frame: CGRect(x: vw.center.x, y: 0, width: 200, height: 50))
            
            titleLabel.text = "Account"
            titleLabel.textAlignment = .center
            titleLabel.font = UIFont.boldSystemFont(ofSize: 25)
            titleLabel.textColor = colors.toolBarColor
            vw.addSubview(titleLabel)
            titleLabel.translatesAutoresizingMaskIntoConstraints = false
            titleLabel.leadingAnchor.constraint(equalTo: vw.leadingAnchor, constant: 30).isActive = true
            titleLabel.centerYAnchor.constraint(equalTo: vw.centerYAnchor, constant: 0).isActive = true
            
            let borderBottom = UIView(frame: CGRect(x:0, y:40, width: tableView.bounds.size.width, height: 4.0))
            borderBottom.backgroundColor = UIColor.self.init(red: 5/255, green: 16/255, blue: 28/255, alpha: 1.0)
            borderBottom.backgroundColor = colors.toolBarColor
            vw.addSubview(borderBottom)
            
            return vw
        }
        else{
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as! CollapsibleTableViewHeader
            print("\(sections[section].name)")
            header.titleLabel.text = sections[section].name
            header.arrowLabel.text = ">"
            header.setCollapsed(sections[section].collapsed)
            
            header.section = section
            header.delegate = self
            
            return header
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
}



extension AccountViewController: CollapsibleTableViewHeaderDelegate {
    
    func toggleSection(_ header: CollapsibleTableViewHeader, section: Int) {
        let collapsed = !sections[section].collapsed
        
        // Toggle collapse
        sections[section].collapsed = collapsed
        header.setCollapsed(collapsed)
        
        tableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
}

extension AccountViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.placeholder = ""
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //Put Server Stuff here'
        var v : UIView = textField
        var count = 0
        repeat {
            v = v.superview!
            count += 1} while !(v is UITableViewCell)
        let cell = v as! collapsibleTableViewCell // or UITableViewCell or whatever
        if let indexPath = self.tableView.indexPath(for:cell){
            print("The index path of the current cell in use is: \(indexPath)")
            depositUserInput(indexPath: indexPath, text: textField.text!)
        }
        print("Return Button hit text enterd: \(textField.text)")
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        var v : UIView = textField
        var count = 0
        repeat {
            v = v.superview!
            count += 1} while !(v is UITableViewCell)
        let cell = v as! collapsibleTableViewCell // or UITableViewCell or whatever
        if let indexPath = self.tableView.indexPath(for:cell){
            print("The index path of the current cell in use is: \(indexPath)")
            depositUserInput(indexPath: indexPath, text: textField.text!)
        }
        print("Editing ended text entered: \(textField.text)")
    }
    
    func doPasswordsMatch()->Bool?{
        print("Inside of doPasswordsMatch: \(passwordArray.count)")
        print("passwordArray.count: \(passwordArray.count)")
        if passwordArray.count == 2{
            if passwordArray[0] == passwordArray[1]{
                //do some server stuff
                passwordsMatch = true
                print("Passwords Match")
                tableView.reloadData()
                return true
            }
            else{
                print("Passwords Don't Match")
                passwordsMatch = false
                tableView.reloadData()
                return false
            }
        }
        return nil
    }
    
    func depositUserInput(indexPath:IndexPath,text:String){
        if indexPath == IndexPath(row: 0, section: 2){
            if passwordArray.count > 0{
                passwordArray.remove(at: 0)
            }
            passwordArray.insert(text, at: 0)
            print("Text entered in username field")
            self.doPasswordsMatch()
        }
        else if indexPath == IndexPath(row: 1, section: 2){
            if passwordArray.count == 2{
                passwordArray.remove(at: 1)
            }
            
            passwordArray.insert(text, at: 1)
            print("Text entered in confirm username field")
            self.doPasswordsMatch()
        }
        else if indexPath == IndexPath(row: 0, section: 3){
            self.newUserName = text
        }
        else if indexPath == IndexPath(row: 0, section: 4){
            self.newEmail = text
        }
    }
    
}
