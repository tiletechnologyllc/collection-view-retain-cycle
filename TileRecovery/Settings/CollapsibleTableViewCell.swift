//
//  CollapsibleTableViewHeader.swift
//  collectionViewAttemptOfPagingAttempt002
//
//  Created by Thomas M. Jumper on 9/17/18.
//  Copyright © 2018 TheRedCamaro. All rights reserved.
//

//import Foundation
//import UIKit
//
//protocol CollapsibleTableViewHeaderDelegate {
//    func toggleSection(_ header: CollapsibleTableViewHeader, section: Int)
//}
//
//class CollapsibleTableViewHeader: UITableViewHeaderFooterView {
//
//    var delegate: CollapsibleTableViewHeaderDelegate?
//    var section: Int = 0
//
//    let titleLabel = UILabel()
//    let arrowLabel = UILabel()
//
//    override init(reuseIdentifier: String?) {
//        super.init(reuseIdentifier: reuseIdentifier)
//
//        // Content View
//        //contentView.backgroundColor = UIColor(hex: 0x2E3944)
//        contentView.backgroundColor = colors.backgroundColor
//
//        let marginGuide = contentView.layoutMarginsGuide
//        
//        // Arrow label
//        contentView.addSubview(arrowLabel)
//        arrowLabel.textColor = colors.toolBarColor
//        arrowLabel.translatesAutoresizingMaskIntoConstraints = false
//        arrowLabel.widthAnchor.constraint(equalToConstant: 12).isActive = true
//        arrowLabel.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
//        arrowLabel.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
//        arrowLabel.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true
//        arrowLabel.font = UIFont.boldSystemFont(ofSize: 15)
//        // Title label
//        contentView.addSubview(titleLabel)
//        titleLabel.textColor = colors.toolBarColor
//        titleLabel.translatesAutoresizingMaskIntoConstraints = false
//        titleLabel.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
//        titleLabel.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
//        titleLabel.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true
//        titleLabel.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
//        titleLabel.font = UIFont.boldSystemFont(ofSize: 15)
//        
//        //
//        // Call tapHeader when tapping on this header
//        //
//        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CollapsibleTableViewHeader.tapHeader(_:))))
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//    
//    //
//    // Trigger toggle section when tapping on the header
//    //
//    @objc func tapHeader(_ gestureRecognizer: UITapGestureRecognizer) {
//        guard let cell = gestureRecognizer.view as? CollapsibleTableViewHeader else {
//            return
//        }
//
//        delegate?.toggleSection(self, section: cell.section)
//    }
//    
//    func setCollapsed(_ collapsed: Bool) {
//        //
//        // Animate the arrow rotation (see Extensions.swf)
//        //
//        arrowLabel.rotate(collapsed ? 0.0 : .pi / 2)
//    }
//    
//}
//
//extension UIView {
//    
//    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
//        let animation = CABasicAnimation(keyPath: "transform.rotation")
//
//        animation.toValue = toValue
//        animation.duration = duration
//        animation.isRemovedOnCompletion = false
//        animation.fillMode = kCAFillModeForwards
//        
//        self.layer.add(animation, forKey: nil)
//    }
//    
//}

//
//  Collapse TableView.swift
//  collapsableTableViewSections
//
//  Created by 123456 on 9/16/18.
//  Copyright © 2018 123456. All rights reserved.
//

import Foundation
import UIKit

class collapsibleTableViewCell:UITableViewCell{
    
    var myLabel = UILabel()
    var myTextField = UITextField()
    var textFieldType:accountOptions?
    var incorrectPasswordLabel:UILabel = UILabel()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = colors.backgroundColor
        myTextField.backgroundColor = colors.backgroundColor
        myTextField.textColor = colors.toolBarColor
        //myTextField.autocapitalizationType = false
        myTextField.autocorrectionType = .no
        //myTextField.delegate = self
        
        
        myTextField.font = UIFont.boldSystemFont(ofSize: 15)
        
        self.contentView.addSubview(myTextField)
        
        myTextField.frame = CGRect(x: 20, y: 0, width: self.bounds.width, height: 30)
        myTextField.translatesAutoresizingMaskIntoConstraints = false
        myTextField.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        myTextField.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant: 40).isActive = true
        myTextField.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        myTextField.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        
        ///
        incorrectPasswordLabel.textColor = UIColor.white
        incorrectPasswordLabel.text = "Passwords Don't Match"
        incorrectPasswordLabel.font = UIFont.boldSystemFont(ofSize: 12)
        
        self.contentView.addSubview(incorrectPasswordLabel)
        
        incorrectPasswordLabel.frame = CGRect(x: 20, y: 0, width: self.bounds.width, height: 30)
        incorrectPasswordLabel.translatesAutoresizingMaskIntoConstraints = false
        incorrectPasswordLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        incorrectPasswordLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant: 40).isActive = true
        incorrectPasswordLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        incorrectPasswordLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        incorrectPasswordLabel.isHidden = true
        layoutSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    func passwordsDontMatch(){
        
    }
}

