//
//  settingsViewController.swift
//  collectionViewAttemptOfPagingAttempt002
//
//  Created by 123456 on 9/14/18.
//  Copyright © 2018 TheRedCamaro. All rights reserved.
//

import Foundation
import UIKit
protocol performSettingsSegue: class{
    func performSettingsSegue()
}

enum menuOption {
    case account
    case blockedAccounts
    case terms
    case contactUs
    case logOut
}

class SettingsViewController:UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    var tableView:UITableView!
    
    var menuItems:[String] = ["Account","Blocked Accounts","Terms","Contact Us","Log Out"]
    
    let interactor = Interactor()
    
    //var perfromSegueDegelate:performSettingsSegue!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = colors.backgroundColor
        tableView = UITableView(frame: self.view.bounds)
        tableView.delegate = self
        tableView.dataSource = self
        self.view.addSubview(tableView)
        tableView.separatorStyle = .none
        tableView.register(StaticCell.self, forCellReuseIdentifier: "staticCell")
        tableView.backgroundColor = colors.backgroundColor
    }
    
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1 {
            return "first section title"
        }
        return "second section title"
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView()
        vw.backgroundColor = colors.backgroundColor
        let titleLabel = UILabel(frame: CGRect(x: vw.center.x, y: 0, width: 200, height: 50))
        
        titleLabel.text = "Menu"
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.boldSystemFont(ofSize: 25)
        titleLabel.textColor = colors.toolBarColor
        vw.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.leadingAnchor.constraint(equalTo: vw.leadingAnchor, constant: 30).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: vw.centerYAnchor, constant: 0).isActive = true
        
        let borderBottom = UIView(frame: CGRect(x:0, y:40, width: tableView.bounds.size.width, height: 4.0))
        borderBottom.backgroundColor = UIColor.self.init(red: 5/255, green: 16/255, blue: 28/255, alpha: 1.0)
        borderBottom.backgroundColor = colors.toolBarColor
        vw.addSubview(borderBottom)
        
        return vw
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "staticCell", for: indexPath) as! StaticCell
        cell.backgroundColor = colors.backgroundColor
        cell.myLabel.backgroundColor = colors.backgroundColor
        cell.layoutSubviews()
        cell.myLabel.text = menuItems[indexPath.row]
        let bgColorView = UIView()
        bgColorView.backgroundColor = colors.toolBarColor.withAlphaComponent(0.5)
        cell.selectedBackgroundView = bgColorView
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let modalViewController = ModalViewController()
        modalViewController.transitioningDelegate = self
        modalViewController.interactor = interactor
        switch indexPath.row {
        case 0:
            modalViewController.settingChoice = menuOption.account
        case 1:
            modalViewController.settingChoice = menuOption.blockedAccounts
        case 2:
            modalViewController.settingChoice = menuOption.terms
        case 3:
            modalViewController.settingChoice = menuOption.contactUs
        case 4:
            //modalViewController.settingChoice = menuOption.logOut
            logoutTapped()
        default:
            modalViewController.settingChoice = menuOption.blockedAccounts
        }
        
        present(modalViewController, animated: true, completion: {
            print("View controller presented1")
        })
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func logoutTapped(){
        //do server logout stuff here
        print("Logout Tapped1")
    }
}

class StaticCell:UITableViewCell{
    
    var myLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = colors.backgroundColor
        myLabel.backgroundColor = colors.backgroundColor
        myLabel.textColor = colors.toolBarColor
        myLabel.font = UIFont.boldSystemFont(ofSize: 15)
        self.contentView.addSubview(myLabel)
        
        myLabel.frame = CGRect(x: 20, y: 0, width: self.bounds.width, height: 30)
        myLabel.translatesAutoresizingMaskIntoConstraints = false
        myLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        myLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant: 20).isActive = true
        myLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        myLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        
        layoutSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
}

extension SettingsViewController: UIViewControllerTransitioningDelegate{
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissAnimator()
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
}
