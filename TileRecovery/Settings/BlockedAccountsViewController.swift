//
//  AccountsViewController.swift
//  settingsScreens
//
//  Created by 123456 on 9/14/18.
//  Copyright © 2018 123456. All rights reserved.
//

//import Foundation
//import UIKit
//
//class BlockedAccountsViewController:UIViewController,UITableViewDelegate,UITableViewDataSource{
//
//    ///
//
//    //    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//    //        if let destinationViewController = segue.destinationViewController as? ModalViewController {
//    //            destinationViewController.transitioningDelegate = self
//    //            destinationViewController.interactor = interactor
//    //        }
//    //    }
//
//    ///
//
//
//
//    var tableView:UITableView!
//    var blockedAccounts:[String] = ["Blocked Account 1", "Blocked Account 2", "Blcoked Account 3"]
//
//    var dismissButton:UIButton!
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        tableView = UITableView(frame: self.view.frame)
//        tableView.backgroundColor = colors.backgroundColor
//        tableView.delegate = self
//        tableView.dataSource = self
//        tableView.separatorStyle = .none
//        self.view.addSubview(tableView)
//
//        dismissButton = UIButton()
//        self.view.addSubview(dismissButton)
//        dismissButton.backgroundColor = UIColor.brown
//        dismissButton.addTarget(self, action: #selector(dismissButtonTapped), for: .touchUpInside)
//
//        dismissButton.translatesAutoresizingMaskIntoConstraints = false
//        dismissButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
//        dismissButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
//        dismissButton.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20).isActive = true
//        dismissButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
//
//
//
//        tableView.translatesAutoresizingMaskIntoConstraints = false
//        tableView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
//        tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
//        tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
//        tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
//
//        //tableView.register(blockedAccountsCell.self, forCellReuseIdentifier: "blockedAccountsCell")
//        tableView.register(StaticCell.self, forCellReuseIdentifier: "staticCell")
//        self.view.bringSubviewToFront(dismissButton)
//    }
//
//    @objc func dismissButtonTapped(){
//        print("Dismiss Button tapped")
//
//        self.dismiss(animated: true, completion: {
//            print("view dismissed")
//        })
//    }
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 50
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return blockedAccounts.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        //  let cell = tableView.dequeueReusableCell(withIdentifier: "blockedAccountsCell", for: indexPath) as! blockedAccountsCell
//        let cell = tableView.dequeueReusableCell(withIdentifier: "staticCell", for: indexPath) as! StaticCell
//        cell.backgroundColor = colors.backgroundColor
//        cell.myLabel.text = blockedAccounts[indexPath.row]
//        let bgColorView = UIView()
//        bgColorView.backgroundColor = colors.toolBarColor.withAlphaComponent(0.5)
//        cell.selectedBackgroundView = bgColorView
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let vw = UIView()
//        vw.backgroundColor = colors.backgroundColor
//        let titleLabel = UILabel(frame: CGRect(x: vw.center.x, y: 0, width: 200, height: 50))
//
//        titleLabel.text = "Blocked Accounts"
//        titleLabel.textAlignment = .center
//        titleLabel.font = UIFont.boldSystemFont(ofSize: 25)
//        titleLabel.textColor = colors.toolBarColor
//        vw.addSubview(titleLabel)
//        titleLabel.translatesAutoresizingMaskIntoConstraints = false
//        titleLabel.leadingAnchor.constraint(equalTo: vw.leadingAnchor, constant: 30).isActive = true
//        titleLabel.centerYAnchor.constraint(equalTo: vw.centerYAnchor, constant: 0).isActive = true
//
//        let borderBottom = UIView(frame: CGRect(x:0, y:40, width: tableView.bounds.size.width, height: 4.0))
//        borderBottom.backgroundColor = UIColor.self.init(red: 5/255, green: 16/255, blue: 28/255, alpha: 1.0)
//        borderBottom.backgroundColor = colors.toolBarColor
//        vw.addSubview(borderBottom)
//
//        return vw
//    }
//
//}
//
//class blockedAccountsCell:UITableViewCell{
//
//    var label:UILabel!
//
//    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
//        super.init(style: style, reuseIdentifier: reuseIdentifier)
//        self.backgroundColor = colors.backgroundColor
//        label = UILabel(frame: self.frame)
//        label.font = UIFont.boldSystemFont(ofSize: 20)
//
//        self.addSubview(label)
//
//        label.translatesAutoresizingMaskIntoConstraints = false
//        label.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
//        label.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
//        label.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
//        label.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//
//}
//
////class StaticCell:UITableViewCell{
////
////    var myLabel = UILabel()
////
////    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
////        super.init(style: style, reuseIdentifier: reuseIdentifier)
////
////       // myLabel.backgroundColor = UIColor.yellow
////        self.backgroundColor = colors.backgroundColor
////        myLabel.textColor = colors.toolBarColor
////        myLabel.font = UIFont.boldSystemFont(ofSize: 15)
////        self.contentView.addSubview(myLabel)
////
////        myLabel.frame = CGRect(x: 20, y: 0, width: self.bounds.width, height: 30)
////        myLabel.translatesAutoresizingMaskIntoConstraints = false
////        myLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
////        myLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant: 20).isActive = true
////        myLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
////        myLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
////
////        layoutSubviews()
////    }
////
////    required init?(coder aDecoder: NSCoder) {
////        super.init(coder: aDecoder)
////    }
////
////    override func layoutSubviews() {
////        super.layoutSubviews()
////
////    }
////}
import Foundation
import  Firebase
import UIKit

struct BlockedUser {
    var blockedUserID:String
    var blockedUserName:String
}

class BlockedAccountsViewController:UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    ///
    
    //    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    //        if let destinationViewController = segue.destinationViewController as? ModalViewController {
    //            destinationViewController.transitioningDelegate = self
    //            destinationViewController.interactor = interactor
    //        }
    //    }
    
    ///
    
    
    
    var tableView:UITableView!
    var blockedAccounts:[BlockedUser] = [BlockedUser]()//["Blocked Account 1", "Blocked Account 2", "Blcoked Account 3"]
    
    var dismissButton:UIButton!
    
    lazy var database = Database.database()
    
    lazy var functions = Functions.functions()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Inside of viewDidLoad of BlockedAccountViewController")
        tableView = UITableView(frame: self.view.frame)
        tableView.backgroundColor = colors.backgroundColor
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        
        self.view.addSubview(tableView)
        
        dismissButton = UIButton()
        self.view.addSubview(dismissButton)
        //dismissButton.backgroundColor = UIColor.brown
        dismissButton.addTarget(self, action: #selector(dismissButtonTapped), for: .touchUpInside)
        dismissButton.setImage(#imageLiteral(resourceName: "icons8-expand-arrow-96"), for: .normal)
        
        dismissButton.translatesAutoresizingMaskIntoConstraints = false
        dismissButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        dismissButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        dismissButton.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20).isActive = true
        dismissButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
        
        
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        
        //tableView.register(blockedAccountsCell.self, forCellReuseIdentifier: "blockedAccountsCell")
        tableView.register(StaticCell.self, forCellReuseIdentifier: "staticCell")
        self.view.bringSubviewToFront(dismissButton)
        getBlockedUsers()
    }
    
    func getBlockedUsers(){
        print("get blocked users called")
        guard let userID = User.signedInUserID else{
            print("There was no userID in blockedAccountsViewController")
            return
        }
        database.reference(withPath: "BlockedUsers/\(userID)").observeSingleEvent(of: .value, with: { (snapshot) in
            var tempArr:[String] = [String]()
            
            guard snapshot.exists() else{
               // self.blockedAccounts = //["You have no blocked accounts"]
                self.tableView.reloadData()
                return
            }
            
            for child in snapshot.children{
                guard let child = child as? DataSnapshot else{
                    print("failed to parse child in blocked user accounts")
                    break
                }
                print("child: \(child.key)")
                tempArr.append(child.key)
            }
            self.getUserNamesOfBlockedUsers(userIDs: tempArr, completion: { (users) in
                if users.isEmpty == false{
                    self.blockedAccounts = users
                    self.tableView.reloadData()
                    tempArr.removeAll()
                }else{
                   self.showNoUsersLabel()
                }
            })
        }) { (error) in
            print("Error getting blocked user accounts: \(error)")
        //    self.blockedAccounts = ["You have no blocked accounts"]
        }
    }
    
    func showNoUsersLabel(){
        let label = UILabel(frame: CGRect.zero)
        label.text = "No Users Blocked!"
        label.textColor = tileColor
        label.textAlignment = .center
        
        self.tableView.addSubview(label)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.topAnchor.constraint(equalTo: self.tableView.topAnchor,constant: 0).isActive = true
        label.centerXAnchor.constraint(equalTo: self.tableView.centerXAnchor, constant: 0).isActive = true
        label.heightAnchor.constraint(equalToConstant: 100).isActive = true
        label.widthAnchor.constraint(equalTo: self.tableView.widthAnchor, constant: 0).isActive = true
    }
    
    func getUserNamesOfBlockedUsers(userIDs:[String], completion: @escaping ([BlockedUser]) -> Void){
        print("made it to getUsernames of blocked userIDs")
        let dispatchGroup = DispatchGroup()
        
        var users:[BlockedUser] = [BlockedUser]()
        for userID in userIDs{
            dispatchGroup.enter()
            print("inside of for loop: \(userID)")
            Database.database().reference(withPath: "Users/\(userID)").observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists(){
                    print("the snapshot exists: \(snapshot.value)")
                    guard let data = snapshot.value as? [String:Any], let username = data["username"] as? String else{
                        print("Failed to parse snapshot data")
                        return
                    }
                    let blockedUser = BlockedUser(blockedUserID: snapshot.key, blockedUserName: username)
                   users.append(blockedUser)
                    // usernames.append(username)
                    
                }else{
                    print("The snapshot does not exist")
                }
                dispatchGroup.leave()
            }) { (error) in
                print("There was an error getting the blockedAccounts usernames: \(error)")
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main, execute: {
//            if usernames.isEmpty{
//               // usernames = ["You have no blocked accounts"]
//            }
            completion(users)
        })
    }
    
    @objc func dismissButtonTapped(){
        print("Dismiss Button tapped")
        
        self.dismiss(animated: true, completion: {
            print("view dismissed")
        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return blockedAccounts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //  let cell = tableView.dequeueReusableCell(withIdentifier: "blockedAccountsCell", for: indexPath) as! blockedAccountsCell
        let cell = tableView.dequeueReusableCell(withIdentifier: "staticCell", for: indexPath) as! StaticCell
        cell.backgroundColor = colors.backgroundColor
        cell.myLabel.text = blockedAccounts[indexPath.row].blockedUserName
        let bgColorView = UIView()
        bgColorView.backgroundColor = colors.toolBarColor.withAlphaComponent(0.5)
        cell.selectedBackgroundView = bgColorView
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let unblockUser = self.blockedAccounts[indexPath.item]
        print("item selected in tableView controller: \(indexPath.item), user: \(self.blockedAccounts[indexPath.item])")
        self.unBlockUserTapped(unblockUser: unblockUser,indexPath: indexPath)
    }
    
    var isUserBlocked:Bool = false
    
    func unBlockUserTapped(unblockUser:BlockedUser, indexPath:IndexPath){
        print("block button tapped")
        let alertViewController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
//        let blockAction = UIAlertAction(title: "Block User", style: .destructive, handler: {(action) in
//            print("Block user")
//            self.blockUser()
//            self.isUserBlocked = true
//        })
        
        let unblockAction = UIAlertAction(title: "Unblock User", style: .default, handler: {(action) in
            self.unblockUser(user: unblockUser)
            self.isUserBlocked = false
            self.tableView.performBatchUpdates({
                self.blockedAccounts.remove(at: indexPath.item)
                self.tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            }) { (completed) in
                print("Completed performing batch updates: \(completed)")
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(action) in
            print("Cancel action tapped")
            self.tableView.deselectRow(at: indexPath, animated: true)
        })
        
//        if isUserBlocked == true{
            alertViewController.addAction(unblockAction)
//        }
//        else{
//            print("added block user to the alertViewController")
//            alertViewController.addAction(blockAction)
//        }
        
        alertViewController.addAction(cancelAction)
        present(alertViewController, animated: true, completion: {
            print("Alert View Controller presented")
        })
    }
    
//    func blockUser(user:BlockedUser){
//        print("Block User Called")
//        functions.httpsCallable("blockUser").call(["userID": User.signedInUserID, "blockedID":user.blockedUserID], completion: {(result,error) in
//            if let error = error{
//                print("Error occurred while running blockUser: \(error)")
//            }
//            guard let result = result else{
//                print("No return sent from function")
//                return
//            }
//
//            print("Result from blockUser: \(result)")
//        })
//    }
    
    func unblockUser(user:BlockedUser){
        print("Unblock user called")
        functions.httpsCallable("unblockUser").call(["userID": User.signedInUserID, "blockedID":user.blockedUserID], completion: {(result,error) in
            if let error = error{
                print("Error occurred while running unblockUser: \(error)")
            }
            guard let result = result else{
                print("No return sent from function")
                return
            }
            
            print("Result from unblockUser: \(result)")
        })
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView()
        vw.backgroundColor = colors.backgroundColor
        let titleLabel = UILabel(frame: CGRect(x: vw.center.x, y: 0, width: 200, height: 50))
        
        titleLabel.text = "Blocked Accounts"
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.boldSystemFont(ofSize: 25)
        titleLabel.textColor = colors.toolBarColor
        vw.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.leadingAnchor.constraint(equalTo: vw.leadingAnchor, constant: 30).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: vw.centerYAnchor, constant: 0).isActive = true
        
        let borderBottom = UIView(frame: CGRect(x:0, y:40, width: tableView.bounds.size.width, height: 4.0))
        borderBottom.backgroundColor = UIColor.self.init(red: 5/255, green: 16/255, blue: 28/255, alpha: 1.0)
        borderBottom.backgroundColor = colors.toolBarColor
        vw.addSubview(borderBottom)
        
        return vw
    }
    
}
