//
//  ModalViewController.swift
//  collectionViewAttemptOfPagingAttempt002
//
//  Created by Thomas M. Jumper on 9/17/18.
//  Copyright © 2018 TheRedCamaro. All rights reserved.
//

import UIKit

class ModalViewController: UIViewController {
    
    var interactor:Interactor? = nil
    var gestureRecognizer:UIPanGestureRecognizer!
    var settingChoice:menuOption?{
        didSet{
            print("setting choice selected: \(settingChoice)")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handleGesture(_:)))
        
        if let settingChoice = settingChoice{
            
            switch settingChoice {
            case menuOption.blockedAccounts:
                let blockedAccountsVC = BlockedAccountsViewController()
                addViewController(ViewController: blockedAccountsVC)
                blockedAccountsVC.tableView.addGestureRecognizer(gestureRecognizer)
            case menuOption.account:
                let AccountsVC = AccountViewController()
                addViewController(ViewController: AccountsVC)
                AccountsVC.tableView.addGestureRecognizer(gestureRecognizer)
            case menuOption.terms:
                let termsVC = ContactUsTermsViewController()
                termsVC.settingChoice = menuOption.terms
                addViewController(ViewController: termsVC)
                termsVC.tableView.addGestureRecognizer(gestureRecognizer)
            case menuOption.contactUs:
                let contactUsVC = ContactUsTermsViewController()
                contactUsVC.settingChoice = menuOption.contactUs
                addViewController(ViewController: contactUsVC)
                contactUsVC.tableView.addGestureRecognizer(gestureRecognizer)
            default:
                print("Something went wrong in viewController selection of the modal View contorller")
                assertionFailure("Something went wrong in viewController selection of the modal View contorller")
            }
        }
        
        
        //self.view.addGestureRecognizer(gestureRecognizer)
        
    }
    
    @IBAction func close(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func handleGesture(_ sender: UIPanGestureRecognizer) {
        
        let percentThreshold:CGFloat = 0.3
        
        // convert y-position to downward pull progress (percentage)
        let translation = sender.translation(in: view)
        let verticalMovement = translation.y / view.bounds.height
        let downwardMovement = fmaxf(Float(verticalMovement), 0.0)
        let downwardMovementPercent = fminf(downwardMovement, 1.0)
        let progress = CGFloat(downwardMovementPercent)
        
        guard let interactor = interactor else { return }
        
        switch sender.state {
        case .began:
            interactor.hasStarted = true
            dismiss(animated: true, completion: nil)
        case .changed:
            interactor.shouldFinish = progress > percentThreshold
            interactor.update(progress)
        case .cancelled:
            interactor.hasStarted = false
            interactor.cancel()
        case .ended:
            interactor.hasStarted = false
            interactor.shouldFinish
                ? interactor.finish()
                : interactor.cancel()
        default:
            break
        }
    }
    
    func addViewController(ViewController:UIViewController){
        self.addChild(ViewController)
        // ViewController.view.addGestureRecognizer(gestureRecognizer)
        self.view.addSubview(ViewController.view)
        ViewController.didMove(toParent: self)
        
        
        
        
        ViewController.view.translatesAutoresizingMaskIntoConstraints = false
        ViewController.view.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        ViewController.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        ViewController.view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        ViewController.view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
    }
}
