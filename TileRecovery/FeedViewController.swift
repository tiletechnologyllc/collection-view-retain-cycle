//
//  FeedViewController.swift
//  collectionViewAttemptOfPagingAttempt002
//
//  Created by Zach on 2/8/18.
//  Copyright © 2018 TheRedCamaro. All rights reserved.
//

import UIKit
import Firebase
import FirebaseUI

class FeedViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITableViewDataSourcePrefetching{
    
    var images = imagePost.defaultImages
    var posts:[Post] = [Post]()
    var indexPathPass = IndexPath(row: 0, section: 0)
    var tag:Tag!
    var name:String?
    var id:String!
    
    var senderType:CellTypes = CellTypes.MainSearchFeedScreenCell
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var tableView:UITableView!
    
    var toolBarHorrizontalStackView:UIStackView!
    var slideToFeedScreenButton:UIButton!
    var slideToProfileScreenButton:UIButton!
    var slideToCameraPhotoAlbumButton:UIButton!
    var toolBarView:UIView!
    
    let placeholder:UIImage = #imageLiteral(resourceName: "Screen Shot 2018-01-29 at 8.30.24 PM")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if let posts = Post.getPostsFromCache(tagID: self.tag.tagID){
//            print("Posts have been set: \(posts)")
//            self.posts = posts
//        }else{
//            print("Failed to get posts from cache")
//        }
        print("id used to fetch posts: \(id)")
        if let posts = Post.cache.object(forKey: id as NSString) as? [Post]{
        //            if let posts = Post.getPostsFromCache(tagID: id){
                print("Posts have been set: \(posts)")
                self.posts = posts
            }else{
                print("Failed to get posts from cache")
            }

        
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.prefetchDataSource = self
        print("View did load")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        tableView.backgroundColor =  colors.backgroundColor
        
        tableView.register(FeedViewControllerTableCell.self, forCellReuseIdentifier: "FeedTableViewControllerCell")
        
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 75, right: 0)
        
        print("indexPathPass: \(self.indexPathPass.section)")
        DispatchQueue.main.async {
            print("indexPathPass inside: \(self.indexPathPass.section)")
            //let indexPath = IndexPath(row: 2, section: 0)
//            self.tableView.scrollToRow(at: self.indexPathPass, at: .top, animated: false)
            if self.indexPathPass.item < self.posts.count{
                self.tableView.scrollToRow(at: self.indexPathPass, at: .top, animated: false)
            }
        }
        
        titleView.backgroundColor = colors.backgroundColor
        titleLabel.font = UIFont.boldSystemFont(ofSize: 25)
        titleLabel.textColor = colors.toolBarColor
        titleLabel.textAlignment = .left
        titleLabel.baselineAdjustment = .alignBaselines
        titleLabel.text = name ?? "Tag Goes Here"//self.tag.name ?? "Tag Goes tHere"
        
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        tableView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        tableView.allowsMultipleSelection = true
        tableView.allowsSelection = true

        createToolBar()
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func createToolBar(){
        //Set Up The Tool Bar
        toolBarView = UIView(frame: CGRect(x: 16, y: 0, width: self.view.bounds.width - 32, height: 50))
        //toolBarView.backgroundColor = UIColor(red: 56/255, green: 217/255, blue: 169/255, alpha: 0.10)
        //toolBarView.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.10)
        
        
        
        self.view.addSubview(toolBarView)
        
        
        toolBarView.translatesAutoresizingMaskIntoConstraints = false
        toolBarView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        toolBarView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        toolBarView.heightAnchor.constraint(equalToConstant: 75).isActive = true
        toolBarView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        
        //Create The Buttons
        slideToFeedScreenButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        slideToFeedScreenButton.setImage(#imageLiteral(resourceName: "icons8-america-100"), for: .normal)
        slideToFeedScreenButton.addTarget(self, action: #selector(slideToFeedScreen(_:)), for: .touchUpInside)
        
        slideToCameraPhotoAlbumButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        slideToCameraPhotoAlbumButton.setImage(#imageLiteral(resourceName: "icons8-unsplash-500"), for: .normal)
        slideToCameraPhotoAlbumButton.addTarget(self, action: #selector(showPhotoAlbum(_:)), for: .touchUpInside)
        
        slideToProfileScreenButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        slideToProfileScreenButton.setImage(#imageLiteral(resourceName: "icons8-thumbnails-96"), for: .normal)
        slideToProfileScreenButton.addTarget(self, action: #selector(slideToProfileScreenTapped(_:)), for: .touchUpInside )
        
        slideToCameraPhotoAlbumButton.translatesAutoresizingMaskIntoConstraints = false
        slideToCameraPhotoAlbumButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        slideToCameraPhotoAlbumButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        slideToProfileScreenButton.translatesAutoresizingMaskIntoConstraints = false
        slideToProfileScreenButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        slideToProfileScreenButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        slideToFeedScreenButton.translatesAutoresizingMaskIntoConstraints = false
        slideToFeedScreenButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        slideToFeedScreenButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        //Set Up The Horrizontal View
        toolBarHorrizontalStackView = UIStackView(arrangedSubviews: [slideToFeedScreenButton,slideToCameraPhotoAlbumButton,slideToProfileScreenButton])
        toolBarHorrizontalStackView.frame = CGRect(x: 16, y: 0, width: self.view.bounds.width - 32, height: 50)
        toolBarHorrizontalStackView.axis = .horizontal
        toolBarHorrizontalStackView.distribution = .equalSpacing
        toolBarHorrizontalStackView.alignment = .fill
        toolBarHorrizontalStackView.spacing = 0
        
        self.view.addSubview(toolBarHorrizontalStackView)
        
        toolBarHorrizontalStackView.translatesAutoresizingMaskIntoConstraints = false
        toolBarHorrizontalStackView.leadingAnchor.constraint(equalTo: toolBarView.leadingAnchor, constant: 30).isActive = true
        toolBarHorrizontalStackView.topAnchor.constraint(equalTo: toolBarView.topAnchor, constant: 5).isActive = true
        toolBarHorrizontalStackView.trailingAnchor.constraint(equalTo: toolBarView.trailingAnchor, constant: -30).isActive = true
        toolBarHorrizontalStackView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        let layer = toolBarView.layer
        toolBarAddLine(fromPoint: CGPoint(x: 30, y: self.view.frame.maxY - 80), toPoint: CGPoint(x: self.view.frame.maxX - 30, y: self.view.frame.maxY - 80),layer)
        
        setGradientColors(currentCell: nil)
    }
    
    enum buttonTypes{
        case  slideToFeedScreen
        case    slideToProfileScreen
        case   showPhotoAlbum
    }
    
    var buttonTapped:buttonTypes? = nil
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let MVC = segue.destination as! MainViewController
//        if let buttonTapped  = buttonTapped as? buttonTypes{
//            MVC.removeCell(sender: self, cellType: nil)
//            switch buttonTapped{
//            case buttonTypes.slideToFeedScreen:
//                MVC.MainCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: true)
//            case buttonTypes.showPhotoAlbum:
//                MVC.MainCollectionView.scrollToItem(at: IndexPath(item: 1, section: 0), at: .left, animated: true)
//            case buttonTypes.slideToProfileScreen:
//                for i in 0..<MVC.collectionViewManager.cells.count{
//                    if MVC.collectionViewManager.cells[i].type == .MainProfileScreenCell{
//                         MVC.MainCollectionView.scrollToItem(at: IndexPath(item: i, section: 0), at: .left, animated: true)
//                        break
//                    }
//                }
//               // MVC.MainCollectionView.scrollToItem(at: IndexPath(item: 2, section: 0), at: .left, animated: true)
//            }
//        }
    }
    
    @objc func slideToFeedScreen(_ sender:Any? = nil){
        print("Slide to Feed Screen Tapped")
        buttonTapped = buttonTypes.slideToFeedScreen
        performSegue(withIdentifier:  "unwindToMainViewController", sender: self)
    }
    
    @objc func showPhotoAlbum(_ sender:Any? = nil){
        print("Show photo album Tapped")
        buttonTapped = buttonTypes.showPhotoAlbum
        performSegue(withIdentifier:  "unwindToMainViewController", sender: self)
    }
    
    @objc  func slideToProfileScreenTapped(_ sender:Any? = nil){
        print("Slide to Profile Screen Tapped")
        buttonTapped = buttonTypes.slideToProfileScreen
        performSegue(withIdentifier:  "unwindToMainViewController", sender: self)
    }
    
    
    func setGradientColors(currentCell sender:CellTypes? = nil){
        let gradient = CAGradientLayer()
        
        gradient.frame = CGRect(x: 0, y: toolBarHorrizontalStackView.frame.minY, width: self.view.frame.width, height: 75)
        
        let greyColor = UIColor(hexString: "#f2f0ee")
        gradient.colors = [UIColor.clear.cgColor.copy(alpha: 0), greyColor.cgColor.copy(alpha: 0.5)]
        toolBarView.layer.insertSublayer(gradient, at: 0)
        //          let layer = toolBarView.layer
        
        
    }
    
    func toolBarAddLine(fromPoint start: CGPoint, toPoint end:CGPoint,_ layer:CALayer? = nil) {
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        linePath.move(to: start)
        linePath.addLine(to: end)
        line.path = linePath.cgPath
        line.strokeColor = colors.toolBarColor.cgColor
        line.lineWidth = 4.5
        line.lineJoin = CAShapeLayerLineJoin.round
        self.view.layer.insertSublayer(line, above: layer)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count// images.count
    }
    
    var heightsArr:[IndexPath:CGFloat] = [IndexPath:CGFloat]()
    var heightsDict:[String:CGFloat] = [String:CGFloat]()
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if let height = self.heightsDict[self.posts[indexPath.item].photoURL]{
            return height
        }
        else if let image = self.posts[indexPath.row].image{
            let aspectRatio = image.size.height/image.size.width
            let imageHeight = self.view.frame.width*aspectRatio
            return imageHeight
        }else{
            return 20
        }
//        if let height = self.heightsArr[indexPath]{
//            return height
//        }else{
//            return 100
//        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedTableViewControllerCell", for: indexPath) as! FeedViewControllerTableCell
        
        if senderType == CellTypes.MainProfileScreenCell{
            cell.buttonType = reportDeleteButtonType.deleteButton
        }
        else if senderType == CellTypes.MainFeedCollectionViewCell{
            cell.buttonType = reportDeleteButtonType.reportButton
        }
        let post = self.posts[indexPath.item]
        cell.setUpButtons()
        
         cell.likesLabel.text = String(post.totalLikes)
        
 //       cell.cellImageView.image = post.image
//        cell.cellImageView.image = self.images[indexPath.row].image
//        cell.cellImageView.image = cell.cellImageView.image?.resizeImageWith()
        //self.images[indexPath.row].image = cell.cellImageView.image
        print("posts: \(String(describing:posts[indexPath.item]))")
        print("Inside of cell for row at: \(indexPath.row), \(self.posts[indexPath.row].postID)")
//        self.posts[indexPath.row].image = cell.cellImageView.image
//        cell.ublurredImage = self.posts[indexPath.row].image//self.images[indexPath.row].image
        let placeholder = #imageLiteral(resourceName: "Screen Shot 2018-01-29 at 8.30.24 PM")
        //let storageRef = Storage.storage().reference(withPath: "\(self.posts[indexPath.row].storageRef)")
        
        //cell.cellImageView.sd_setImage(with: storageRef , placeholderImage: placeholder)
        if let image = posts[indexPath.item].image{
            cell.ublurredImage = image
            cell.cellImageView.image = image
            let aspectRatio = image.size.height/image.size.width
            let imageHeight = self.view.frame.width*aspectRatio
            //self.heightsArr[indexPath] = imageHeight
            self.heightsDict[self.posts[indexPath.item].photoURL] = imageHeight
        } else{
        let url = URL(string: posts[indexPath.item].photoURL)
        cell.cellImageView.sd_setImage(with: url, placeholderImage: placeholder, options: SDWebImageOptions.refreshCached, completed: {(image,error,imageCacheType,storageReference) in
            if let error = error{
                print("Uh-Oh an error has occured: \(error.localizedDescription)" )
            }
            guard let image = image else{
                return
            }
                let aspectRatio = image.size.height/image.size.width
                let imageHeight = self.view.frame.width*aspectRatio
                //self.heightsArr[indexPath] = imageHeight
                 self.heightsDict[self.posts[indexPath.item].photoURL] = imageHeight
                cell.ublurredImage = image
//                DispatchQueue.main.async {
//                    let aspectRatio = image.size.height/image.size.width
//                    cell.cellImageView.image = image
//                    let imageHeight = self.view.frame.width*aspectRatio
////                    tableView.beginUpdates()
//                    self.heightsArr[indexPath] = imageHeight
////                    tableView.endUpdates()
//                }
            })
        }
//        cell.ublurredImage = cell.cellImageView.image
        cell.deletePhotoDelegate = self
        //        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(userSwipedOnCell(_:)))
        //        leftSwipe.direction = .left
        //
        //        cell.addGestureRecognizer(leftSwipe)
        //
        //cell.cellImageView.image = cell.cellImageView.image?.resizeImageWith()
        
        cell.cellLabel.text = self.posts[indexPath.item].userName
        
        cell.cellLabel.textColor = backgroundColor
        cell.likesLabel.textColor = backgroundColor
        cell.likesLabel.textAlignment = .right
        
        cell.postID = self.posts[indexPath.item].postID
        cell.post = self.posts[indexPath.item]
        cell.didUserLikePost()
        print("postID: \(self.posts[indexPath.item].postID)")
        print("storageRef: \(self.posts[indexPath.item].storageRef)")
        cell.backgroundColor =  UIColor(displayP3Red: 0, green: 255, blue: 1, alpha: 0.99)
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    @IBAction func swipeGesture_back(_ sender: UISwipeGestureRecognizer) {
        print("Swipe Gesture Swiped")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print ("didSelectRow in FeedViewController \(indexPath.row)")
    }
    
    
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        var urls:[URL] = [URL]()
        for indexPath in indexPaths{
            if let url = self.posts[indexPath.item].url{
                urls.append(url)
            }
        }
//        for url in urls{
//
//        }
        SDWebImagePrefetcher.shared().prefetchURLs(urls)
        
        for url in urls{
        
            if let key = SDWebImageManager.shared().cacheKey(for: url){
                    SDWebImageManager.shared().imageCache?.queryCacheOperation(forKey: key, done: {(image,data,cacheType) in
                        guard let image = image else{
                            return
                        }
                        DispatchQueue.main.async {
                            let aspectRatio = image.size.height/image.size.width
                            let imageHeight = self.view.frame.width * aspectRatio
                            self.heightsDict[url.absoluteString] = imageHeight
                        }
                    })
            }else{
                SDWebImageManager.shared().imageDownloader?.downloadImage(with: url, progress: nil, completed: {(image,data,error,completed) in
                    guard let image = image else{
                        return
                    }
                    DispatchQueue.main.async {
                        let aspectRatio = image.size.height/image.size.width
                        let imageHeight = self.view.frame.width * aspectRatio
                        self.heightsDict[url.absoluteString] = imageHeight
                    }
                })
            }
        
        }
    }
    
    //    var isBlur:Bool = false
    //
    //    @objc func userSwipedOnCell(_ sender:UISwipeGestureRecognizer){
    //        print("userSwipedCell")
    //        if sender.direction == .left{
    //            print("User swiped cell left")
    //            if isBlur{
    //               // blurEffect(image: self.images[indexPath.row].image)
    //                isBlur = !isBlur
    //            }
    //            else{
    //
    //                isBlur = !isBlur
    //            }
    //        }
    //    }
    
    //    var context = CIContext(options: nil)
    //
    //    func blurEffect(image:UIImage)->UIImage {
    //
    //        let currentFilter = CIFilter(name: "CIGaussianBlur")
    //        let beginImage = CIImage(image: image)
    //        currentFilter!.setValue(beginImage, forKey: kCIInputImageKey)
    //        currentFilter!.setValue(10, forKey: kCIInputRadiusKey)
    //
    //        let cropFilter = CIFilter(name: "CICrop")
    //        cropFilter!.setValue(currentFilter!.outputImage, forKey: kCIInputImageKey)
    //        cropFilter!.setValue(CIVector(cgRect: beginImage!.extent), forKey: "inputRectangle")
    //
    //        let output = cropFilter!.outputImage
    //        let cgimg = context.createCGImage(output!, from: output!.extent)
    //        let processedImage = UIImage(cgImage: cgimg!)
    //     //   cellImageView.image = processedImage
    //    return processedImage
    //    }

    
}









