//
//  SpecialAddCellDelegate-TestMainCollectionView.swift
//  TileRecovery
//
//  Created by 123456 on 3/29/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

protocol SpecialAddCellDelegate: class {
    func addCell(sender:Any?,user:User )
    func addCell(sender:Any?,tag:Tag)
    func removeAllExcessCells(at index:Int,scrollDirection:ScrollDirection,currentPage:Int)
    func removeLastCell()
}

extension TestMainCollectionView:SpecialAddCellDelegate{
    func addCell(sender: Any?, user: User) {
        print("add cell called")
        collectionView.performBatchUpdates({
            let newElement = arr.count
            arr.append(newElement)
            collectionView.insertItems(at: [IndexPath(item: newElement, section: 0)])
        }) { (completed) in
            if completed{
                print("sucessfully added cell: \(self.collectionView.numberOfItems(inSection: 0))")
                self.collectionView.scrollToItem(at: IndexPath(item: self.arr.count - 1, section: 0), at: .left, animated: true)
            }else{
                print("Batch updates were unsucessful")
                self.collectionView.reloadData()
            }
        }
    }
    
    func addCell(sender: Any?, tag: Tag) {
        collectionView.performBatchUpdates({
            let newElement = arr.count
            arr.append(newElement)
            collectionView.insertItems(at: [IndexPath(item: newElement, section: 0)])
        }) { (completed) in
            if completed{
                print("sucessfully added cell: \(self.collectionView.numberOfItems(inSection: 0))")
                self.collectionView.scrollToItem(at: IndexPath(item: self.arr.count - 1, section: 0), at: .left, animated: true)
            }else{
                print("Batch updates were unsucessful")
                self.collectionView.reloadData()
            }
        }
    }
    
    func removeAllExcessCells(at index: Int, scrollDirection: ScrollDirection, currentPage: Int) {
        print("remove all excess called, not yet needed")
    }
    
    func removeLastCell() {
        print("remove last cell called")
        if arr.count - 1 <= 0{
            print("reached root cell")
            return
        }
        collectionView.performBatchUpdates({
            let oldElement = arr.count - 1
            arr.remove(at: oldElement)
            collectionView.deleteItems(at: [IndexPath(item: oldElement, section: 0)])
        }) { (completed) in
            if completed{
                print("sucessfully removed cell: \(self.collectionView.numberOfItems(inSection: 0))")
                self.collectionView.scrollToItem(at: IndexPath(item: self.arr.count - 1, section: 0), at: .right, animated: true)
            }else{
                print("Batch updates were unsucessful")
                self.collectionView.reloadData()
            }
           
        }
    }
    
    
}
