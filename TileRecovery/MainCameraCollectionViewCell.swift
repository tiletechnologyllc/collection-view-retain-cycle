//
//  MainCameraCollectionViewCell.swift
//  collectionViewAttemptOfPagingAttempt002
//
//  Created by Zach on 2/5/18.
//  Copyright © 2018 TheRedCamaro. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

protocol gestureDelegate: class{
    func gestureDelegate()
}

protocol previewSegueDelegate: class {
    //func previewSegueDelegate(image:UIImage,device:AVCaptureDevice?)
    //func previewSegueDelegate()
    func previewSegueDelegate(image:UIImage?,device:AVCaptureDevice?,albumTypeSender:photoAlbumSender?,  cameraTypeSender:cameraSender?, asset:PHAsset?)
}
