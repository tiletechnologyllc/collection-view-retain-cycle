//
//  LogInViewController.swift
//  TableViewDropDown
//
//  Created by 123456 on 10/9/18.
//  Copyright © 2018 123456. All rights reserved.
//

import Foundation
import UIKit

class LogInViewController:UIViewController{
  
    var loginLabel:UILabel = UILabel()
    var usernameTextField:UITextField = UITextField()
    var passwordTextField:UITextField = UITextField()
    
    var enterButton:UIButton = UIButton()
    var resetPasswordButton:UIButton = UIButton()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.view.backgroundColor = backgroundColor
        
        self.view.addSubview(loginLabel)
        self.view.addSubview(usernameTextField)
        self.view.addSubview(passwordTextField)
        self.view.addSubview(resetPasswordButton)
        
        loginLabel.text = "Login"
        loginLabel.textColor = tileColor
        loginLabel.font = UIFont.boldSystemFont(ofSize: 36)
        
        usernameTextField.placeholder = "Username"
        
        
        usernameTextField.textColor = tileColor
        usernameTextField.backgroundColor = backgroundColor
        usernameTextField.attributedPlaceholder = NSAttributedString(string: usernameTextField.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : tileColor.withAlphaComponent(0.5)])
       
        passwordTextField.placeholder = "Password"
        passwordTextField.textColor = tileColor
        
        passwordTextField.isSecureTextEntry = true
        passwordTextField.backgroundColor = backgroundColor
        passwordTextField.attributedPlaceholder = NSAttributedString(string: passwordTextField.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : tileColor.withAlphaComponent(0.5)])
    
        resetPasswordButton.setTitle("Reset Password", for: .normal)
        resetPasswordButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        resetPasswordButton.setTitleColor(tileColor, for: .normal)
        resetPasswordButton.addTarget(self, action: #selector(resetPassWordButtonTapped), for: .touchUpInside)
        
        setUpLayout()
        
//        loginLabel.translatesAutoresizingMaskIntoConstraints = false
//        loginLabel.topAnchor.constraint.isActive = true
//        loginLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 16).isActive = true
//        loginLabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
//        loginLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setUpLayout(){
        loginLabel.translatesAutoresizingMaskIntoConstraints = false
        usernameTextField.translatesAutoresizingMaskIntoConstraints = false
        passwordTextField.translatesAutoresizingMaskIntoConstraints = false


        passwordTextField.topAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -15).isActive = true
        passwordTextField.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        passwordTextField.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.75).isActive = true
        passwordTextField.textAlignment = .center
        addRelativeLine(superView: passwordTextField)
        // passwordTextField.widthAnchor.constraint(equalToConstant: 295).isActive = true

//        passwordTextField.bottomAnchor.constraint(equalTo: self.passwordTextField.topAnchor, constant: -15).isActive = true
        usernameTextField.bottomAnchor.constraint(equalTo: self.passwordTextField.topAnchor, constant: -30).isActive = true
        usernameTextField.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        usernameTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        usernameTextField.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.75).isActive = true
        usernameTextField.textAlignment = .center
        addRelativeLine(superView: usernameTextField)
        
        loginLabel.heightAnchor.constraint(equalToConstant: 75).isActive = true
        loginLabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
        loginLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        loginLabel.bottomAnchor.constraint(equalTo: self.usernameTextField.topAnchor, constant: -50).isActive = true
        loginLabel.leadingAnchor.constraint(equalTo: self.usernameTextField.leadingAnchor, constant: 0).isActive = true
        loginLabel.textAlignment = .center
//        usernameTextField.layer.borderColor = UIColor.blue.cgColor
//        usernameTextField.layer.borderWidth = 4
//
//        passwordTextField.layer.borderColor = UIColor.blue.cgColor
//        passwordTextField.layer.borderWidth = 4

        
        
        
        passwordTextField.font = UIFont.boldSystemFont(ofSize: 25)
        usernameTextField.font = UIFont.boldSystemFont(ofSize: 25)
    }
    
    @objc func resetPassWordButtonTapped(){
            print("Reset the password")
    }
    
    func addRelativeLine(superView:UIView){
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 0.5))
        view.backgroundColor = tileColor
        self.view.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.topAnchor.constraint(equalTo: superView.bottomAnchor, constant: 10).isActive = true
        view.leadingAnchor.constraint(equalTo: superView.leadingAnchor, constant: 0).isActive = true
        view.trailingAnchor.constraint(equalTo: superView.trailingAnchor, constant: 0).isActive = true
        view.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
    }

}
