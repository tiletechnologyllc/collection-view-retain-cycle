//
//  LoggedInScreenViewController .swift
//  TableViewDropDown
//
//  Created by 123456 on 10/22/18.
//  Copyright © 2018 123456. All rights reserved.
//

import Foundation
import  UIKit
import Firebase


class loggedinScreenViewController:UIViewController{
    
    var loggedInLabel:UILabel = UILabel()
    var logoutButton:UIButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(loggedInLabel)
        self.view.addSubview(logoutButton)
        
        self.view.backgroundColor = backgroundColor
        
        logoutButton.setTitleColor(backgroundColor, for: .normal)
        logoutButton.setTitle("Logout", for: .normal)
        logoutButton.backgroundColor = tileColor
        logoutButton.addTarget(self, action: #selector(logoutButtonTapped(_:)), for: .touchUpInside)
        
        loggedInLabel.textColor = tileColor
        
        logoutButton.translatesAutoresizingMaskIntoConstraints = false
        logoutButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        logoutButton.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0).isActive = true
        logoutButton.widthAnchor.constraint(equalToConstant: 200).isActive = true
        logoutButton.heightAnchor.constraint(equalToConstant: 100).isActive = true
       
        
        loggedInLabel.translatesAutoresizingMaskIntoConstraints = false
        loggedInLabel.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 25).isActive = true
        loggedInLabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
        loggedInLabel.heightAnchor.constraint(equalToConstant: 100).isActive = true
        loggedInLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        
    }
    
    @objc func logoutButtonTapped(_ sender:UIButton?){
        print("SignOut Button Tapped")
        do{
            try   Auth.auth().signOut()
        }
        catch {
            print("Error signing out )")
        }
        
        
        print("Successful signout")
        
        let entryVC = EntryScreenViewController()
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        present(entryVC, animated: false, completion: nil)
    }
    
}

