//
//  LogInViewController.swift
//  TableViewDropDown
//
//  Created by 123456 on 10/9/18.
//  Copyright © 2018 123456. All rights reserved.
//

import Foundation
import UIKit
import  Firebase

class LogInViewController:UIViewController{
  
    lazy var functions = Functions.functions()
    
    var containerView:UIView = UIView()
    
    var loginLabel:UILabel = UILabel()
    var usernameEmailTextField:UITextField = UITextField()
    var passwordTextField:UITextField = UITextField()
    
    
    var enterButton:UIButton = UIButton()
    var resetPasswordButton:UIButton = UIButton()
    var backButton:UIButton = UIButton()
    
    var handle:AuthStateDidChangeListenerHandle!
    
    private var userPassword:String?
    var username:String?
    private var userEmail:String?
    
    var containerViewBottomAnchor:NSLayoutConstraint!
    
    var firstShift:Bool = true
    
    
    var incorrectLabel:UILabel = UILabel()
    
    var incorrectLabelTopAnchor:NSLayoutConstraint!
    var incorrectLabelLeadingAnchor:NSLayoutConstraint!
    var incorrectLabelTrailingAnchor:NSLayoutConstraint!
    var incorrectLabelHeightAnchor:NSLayoutConstraint!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            // ...
        }
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Auth.auth().removeStateDidChangeListener(handle!)
            NotificationCenter.default.removeObserver(self)
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.containerView.backgroundColor = backgroundColor
        
        self.view.backgroundColor = backgroundColor
        
        self.view.addSubview(containerView)
        
        self.containerView.addSubview(loginLabel)
        self.containerView.addSubview(usernameEmailTextField)
        self.containerView.addSubview(passwordTextField)
        self.containerView.addSubview(resetPasswordButton)
        self.containerView.addSubview(enterButton)
        self.containerView.addSubview(backButton)
        
        loginLabel.text = "Login"
        loginLabel.textColor = tileColor
        loginLabel.font = UIFont.boldSystemFont(ofSize: 36)
        
        usernameEmailTextField.placeholder = "Username"
        
        
        usernameEmailTextField.textColor = tileColor
        usernameEmailTextField.backgroundColor = backgroundColor
        usernameEmailTextField.attributedPlaceholder = NSAttributedString(string: usernameEmailTextField.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : tileColor.withAlphaComponent(0.5)])
       usernameEmailTextField.delegate = self
        
        passwordTextField.placeholder = "Password"
        passwordTextField.textColor = tileColor
        
        passwordTextField.isSecureTextEntry = true
        passwordTextField.keyboardType = .default
        passwordTextField.textContentType = UITextContentType.password
        //passwordTextField.autocorrectionType = UITextAutocorrectionType.yes
        passwordTextField.backgroundColor = backgroundColor
        passwordTextField.attributedPlaceholder = NSAttributedString(string: passwordTextField.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : tileColor.withAlphaComponent(0.5)])
        passwordTextField.delegate = self
        
        resetPasswordButton.setTitle("Reset Password", for: .normal)
        resetPasswordButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        resetPasswordButton.setTitleColor(tileColor, for: .normal)
        resetPasswordButton.addTarget(self, action: #selector(resetPassWordButtonTapped), for: .touchUpInside)
        
        enterButton.setTitle("Tile Away", for: .normal)
        enterButton.setTitleColor(backgroundColor, for: .normal)
        enterButton.backgroundColor = tileColor
        enterButton.layer.cornerRadius = 5
        enterButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        enterButton.addTarget(self, action: #selector(enterButtonTapped), for: .touchUpInside)
        
        
        
        
        passwordTextField.textContentType = UITextContentType.password
        setUpLayout()
        
        passwordTextField.addTarget(self, action: #selector(textFieldDidChangeEditing(textField:)), for: .editingChanged)
        usernameEmailTextField.addTarget(self, action: #selector(textFieldDidChangeEditing(textField:)), for: .editingChanged)
    
        usernameEmailTextField.backgroundColor = backgroundColor
        passwordTextField.backgroundColor = backgroundColor
        
        self.view.addSubview(incorrectLabel)
        incorrectLabel.translatesAutoresizingMaskIntoConstraints = false
    }
    

    
//    @objc func enterButtonTapped(){
//        if usernameEmailTextField.isFirstResponder{
//            usernameEmailTextField.resignFirstResponder()
//        }
//        else{
//            passwordTextField.resignFirstResponder()
//        }
//        print("Enter Button Tapped:\n\temail:\(self.userEmail)\n\tpassword\(self.userPassword)\n\tusername\(self.username)")
//
//        if let email = self.userEmail, let password = self.userPassword, self.userPassword != "", self.userEmail != ""{
//            Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
//                if let user = user{
//                    print("User: \(user.user.uid) logged in")
//                    //Add Segue to next Screen
//                    self.segueToLoggedIn(user: user.user.displayName, userID: user.user.uid)
//                }
//                if let error = error{
//                    print("Error logging in: \(error)")
//                    //Display the error
//                    self.usernameEmailTextField.text = ""
//                    self.passwordTextField.text = ""
//                    self.incorrectUsernameLabel.isHidden = false
//                    self.incorrectPasswordLabel.isHidden = false
//                    self.incorrectUsernameLabel.backgroundColor = UIColor.red
//                    self.incorrectPasswordLabel.backgroundColor = UIColor.red
//                }
//            }
//        }
//        else if let username = self.username, let password = self.userPassword, self.username !=  "", self.userPassword != ""{
//            //Add in the cloud function to return a token
//            //Add Segue To next screen
//            print("User tried to login with username")
////            var serverToken:String? = nil
////            self.loginUser(username: username, password: password, completionHandler:{(token) in
////                if let token = token{
////                    print("token gotten: \(token)")
////                    serverToken = token
////                }
////                else{
////                    print("Error getting token from server")
////                    serverToken = nil
////                }
////            })
////            print("\n\n\n\n\n")
//            if let token = self.loginUser(username: username, password: password){
//       //     if let token = serverToken{
//                Auth.auth().signIn(withCustomToken: token, completion: {(tokenAuthResult,error) in
//                    if let error = error{
//                        print("Error logging in \(error.localizedDescription)")
//                        //Display the error
//                        self.usernameEmailTextField.text = ""
//                        self.passwordTextField.text = ""
//                        self.incorrectUsernameLabel.isHidden = false
//                        self.incorrectPasswordLabel.isHidden = false
//                        self.incorrectUsernameLabel.backgroundColor = UIColor.red
//                        self.incorrectPasswordLabel.backgroundColor = UIColor.red
//                    }
//                    if let user = tokenAuthResult?.user{
//                    print("The user sucessfully loggedIn: \(user.uid)")
//                        self.segueToLoggedIn(user: user.displayName , userID: user.uid)
//                    }
//                    else{
//                        print("User not logged in")
//                    }
//                })
//            }
//            else{
//                print("Error logging in with username")
//                //Display the error
//                self.usernameEmailTextField.text = ""
//                self.passwordTextField.text = ""
//                self.incorrectUsernameLabel.isHidden = false
//                self.incorrectPasswordLabel.isHidden = false
//                self.incorrectUsernameLabel.backgroundColor = UIColor.red
//                self.incorrectPasswordLabel.backgroundColor = UIColor.red
//            }
//        }
//
//        else{
//            print("please enter username or password")
//            self.incorrectUsernameLabel.isHidden = false
//            self.incorrectPasswordLabel.isHidden = false
//            self.incorrectUsernameLabel.backgroundColor = UIColor.red
//            self.incorrectPasswordLabel.backgroundColor = UIColor.red
//        }
//
//
////        print("Enter Button tapped")
////
////       Auth.auth().createUser(withEmail: "hi.com", password: "123", completion: {(user,error)
////
////       })
////
////
////        Auth.auth().signIn(withCustomToken: customToken ?? "") { (user, error) in
////            // ...
////            Auth.auth().currentUser?.updateEmail(to: "123@123.com", completion: nil)
////            if let error = error{
////                print("An error has occured: \(error)")
////            }
////            else{
////                print("Succes setting up user:\n\tuserID: \(user?.user.uid)\n\tid: \(user?.user.displayName)")
////            }
////      }
//
////        if let username = self.userName, let password = self.userPassword{
////            Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
////                // ...
////                guard let user = authResult?.user else { return }
////            }
////        }
//    }
    
    
    @objc func enterButtonTapped(){
        if usernameEmailTextField.isFirstResponder{
            usernameEmailTextField.resignFirstResponder()
        }
        else{
            passwordTextField.resignFirstResponder()
        }
        print("Enter Button Tapped:\n\temail:\(self.userEmail)\n\tpassword\(self.userPassword)\n\tusername\(self.username)")
        
        if let email = self.userEmail, let password = self.userPassword, self.userPassword != "", self.userEmail != ""{
            Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
                if let user = user{
                    print("User: \(user.user.uid) logged in")
                    //Add Segue to next Screen
                    User.signedInUser = User(userID: user.user.uid, username: user.user.displayName!, photoURL: User.defaultProfilePhotoURL)
                    User.signedInUserID = user.user.uid
                    self.segueToLoggedIn(user: user.user.displayName, userID: user.user.uid)
                }
                if let error = error{
                    print("Error logging in: \(error)")
                    //Display the error
                    self.usernameEmailTextField.text = ""
                    self.passwordTextField.text = ""
                    self.isInvalidEntry(isInvalid: false, superView: self.usernameEmailTextField, message: "Invalid Email or password entered")
                }
            }
        }
        else if let username = self.username, let password = self.userPassword, self.username !=  "", self.userPassword != ""{
            //Add in the cloud function to return a token
            //Add Segue To next screen
            print("User tried to login with username")
            var serverToken:String? = nil
            self.loginUser(username: username, password: password, completionHandler:{(token) in
                if let token = token{
                    Auth.auth().signIn(withCustomToken: token, completion: {(tokenAuthResult,error) in
                        if let error = error{
                            print("Error logging in \(error.localizedDescription)")
                            //Display the error
                            self.usernameEmailTextField.text = ""
                            self.passwordTextField.text = ""
                            self.isInvalidEntry(isInvalid: true, superView: self.usernameEmailTextField, message: "Invalid Username or email entered")
                        }
                        if let user = tokenAuthResult?.user{
                            print("The user sucessfully loggedIn: \(user.uid)")
                            self.segueToLoggedIn(user: user.displayName , userID: user.uid)
                        }
                        else{
                            print("User not logged in")
                        }
                    })
                }
                else{
                    print("Error logging in with username: \(username)")
                    //Display the error
                    self.usernameEmailTextField.text = ""
                    self.passwordTextField.text = ""
                    self.isInvalidEntry(isInvalid: true, superView: self.usernameEmailTextField, message: "Invalud username or email entered")
                }
            })
            print("\n\n\n\n\n")
        }
            
        else{
            print("please enter username or password")
            self.isInvalidEntry(isInvalid: true, superView: self.usernameEmailTextField, message: "Invalud username or email entered")
        }
        
        
        //        print("Enter Button tapped")
        //
        //       Auth.auth().createUser(withEmail: "hi.com", password: "123", completion: {(user,error)
        //
        //       })
        //
        //
        //        Auth.auth().signIn(withCustomToken: customToken ?? "") { (user, error) in
        //            // ...
        //            Auth.auth().currentUser?.updateEmail(to: "123@123.com", completion: nil)
        //            if let error = error{
        //                print("An error has occured: \(error)")
        //            }
        //            else{
        //                print("Succes setting up user:\n\tuserID: \(user?.user.uid)\n\tid: \(user?.user.displayName)")
        //            }
        //      }
        
        //        if let username = self.userName, let password = self.userPassword{
        //            Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
        //                // ...
        //                guard let user = authResult?.user else { return }
        //            }
        //        }
    }
    
    
    var loginUserCompletionHandler:(String?)->String? = { (token) in
        if let token = token{
            print("Token Downloaded In Closure:\n\(token)")
            return token
        }
        else{
            print("Token not downloaded")
            return nil
        }
        
        
    }
    
    var voidLoginUserCompletionHandler:(String?)->Void = { (token) in
        if let token = token{
            print("Token Downloaded In Void Closure:\n\(token)")
        }
        else{
            print("Token not downloaded in void closure")
        }
        
        
    }
    
    
    func loginUser(username:String,password:String, completionHandler: @escaping (String?)->Void){
        var tokenResult:String?
        functions.httpsCallable("safeLoginUser").call(["username":username.lowercased(), "password":password],completion: {(result,error) in
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    let code = FunctionsErrorCode(rawValue: error.code)
                    let message = error.localizedDescription
                    let details = error.userInfo[FunctionsErrorDetailsKey]
                    
                    print("Error message: \(message)")
                    completionHandler(nil)
                    tokenResult = nil
                }
            }
                
            else  if let result = result?.data as? String{
                print("The token returned from the server is: \(result)")
                completionHandler(result)
            }
            else{
                print("The server returned null, no such username exists already")
                completionHandler(nil)
                tokenResult = nil
            }
            
        })
        
        print("returned: \(tokenResult)")
    }
    
    
    func loginUser(username:String,password:String)->String?{
        var tokenResult:String?
        functions.httpsCallable("loginUser").call(["username":username.lowercased(), "password":password],completion: {(result,error) in
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    let code = FunctionsErrorCode(rawValue: error.code)
                    let message = error.localizedDescription
                    let details = error.userInfo[FunctionsErrorDetailsKey]
                    
                    print("Error message: \(message)")
                    tokenResult = nil
                }
            }
                
            else  if let result = result?.data as? String{
                print("The token returned from the server is: \(result)")
               return tokenResult = result
            }
            else{
                print("The server returned null, no such username exists already")
                tokenResult = nil
            }
            
        })
        
        print("returned: \(tokenResult)")
        return tokenResult
    }
    
    @objc func backButtonTapped(){
        //        self.dismiss(animated: true, completion: {
        //            print("Sign Up Screen Dismissed")
        //        })
        //       self.navigationController?.popViewController(animated: true)
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        dismiss(animated: true, completion: nil)
    }
    
    func setUpLayout(){
        
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        loginLabel.translatesAutoresizingMaskIntoConstraints = false
        usernameEmailTextField.translatesAutoresizingMaskIntoConstraints = false
        passwordTextField.translatesAutoresizingMaskIntoConstraints = false
        enterButton.translatesAutoresizingMaskIntoConstraints = false

        containerView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        containerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        containerView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        containerViewBottomAnchor =  containerView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0)
        containerViewBottomAnchor.isActive = true
        
        passwordTextField.topAnchor.constraint(equalTo: self.containerView.centerYAnchor, constant: -15).isActive = true
        passwordTextField.centerXAnchor.constraint(equalTo: self.containerView.centerXAnchor, constant: 0).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        passwordTextField.widthAnchor.constraint(equalTo: self.containerView.widthAnchor, multiplier: 0.75).isActive = true
        passwordTextField.textAlignment = .left
        addRelativeLine(superView: passwordTextField)
        // passwordTextField.widthAnchor.constraint(equalToConstant: 295).isActive = true

//        passwordTextField.bottomAnchor.constraint(equalTo: self.passwordTextField.topAnchor, constant: -15).isActive = true
        usernameEmailTextField.bottomAnchor.constraint(equalTo: self.passwordTextField.topAnchor, constant: -30).isActive = true
        usernameEmailTextField.centerXAnchor.constraint(equalTo: self.containerView.centerXAnchor, constant: 0).isActive = true
        usernameEmailTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        usernameEmailTextField.widthAnchor.constraint(equalTo: self.containerView.widthAnchor, multiplier: 0.75).isActive = true
        usernameEmailTextField.textAlignment = .left
        addRelativeLine(superView: usernameEmailTextField)
        
        loginLabel.heightAnchor.constraint(equalToConstant: 75).isActive = true
        loginLabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
        loginLabel.centerXAnchor.constraint(equalTo: self.containerView.centerXAnchor).isActive = true
        loginLabel.bottomAnchor.constraint(equalTo: self.usernameEmailTextField.topAnchor, constant: -50).isActive = true
        loginLabel.leadingAnchor.constraint(equalTo: self.usernameEmailTextField.leadingAnchor, constant: 0).isActive = true
        loginLabel.textAlignment = .center
//        usernameEmailTextField.layer.borderColor = UIColor.blue.cgColor
//        usernameEmailTextField.layer.borderWidth = 4
//
//        passwordTextField.layer.borderColor = UIColor.blue.cgColor
//        passwordTextField.layer.borderWidth = 4

        enterButton.bottomAnchor.constraint(equalTo: self.containerView.bottomAnchor, constant: -50).isActive = true
        enterButton.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor, constant: 16).isActive = true
        enterButton.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor, constant: -16).isActive = true
        enterButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        resetPasswordButton.translatesAutoresizingMaskIntoConstraints = false
        resetPasswordButton.topAnchor.constraint(equalTo: self.passwordTextField.bottomAnchor, constant: 60).isActive = true
        resetPasswordButton.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor, constant: 16).isActive = true
        resetPasswordButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        // termsAndPrivacy.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.5).isActive = true
        resetPasswordButton.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor, constant: -16).isActive = true
        resetPasswordButton.setTitle("Reset Password?", for: .normal)
        resetPasswordButton.setTitleColor(tileColor, for: .normal)
        resetPasswordButton.titleLabel!.font = UIFont.boldSystemFont(ofSize: 15)
        resetPasswordButton.backgroundColor = backgroundColor
        
        backButton.setImage(#imageLiteral(resourceName: "icons8-back-96"), for: .normal)
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        
        passwordTextField.font = UIFont.boldSystemFont(ofSize: 25)
        usernameEmailTextField.font = UIFont.boldSystemFont(ofSize: 25)
        loginLabel.font = UIFont.boldSystemFont(ofSize: 50)
        
        backButton.translatesAutoresizingMaskIntoConstraints = false
        backButton.topAnchor.constraint(equalTo: self.containerView.topAnchor, constant: 35).isActive = true
        backButton.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor, constant: 5).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        backButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    @objc func resetPassWordButtonTapped(){
            print("Reset the password")
    }
    
    func addRelativeLine(superView:UIView){
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 0.5))
        view.backgroundColor = tileColor
        self.view.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.topAnchor.constraint(equalTo: superView.bottomAnchor, constant: 10).isActive = true
        view.leadingAnchor.constraint(equalTo: superView.leadingAnchor, constant: 0).isActive = true
        view.trailingAnchor.constraint(equalTo: superView.trailingAnchor, constant: 0).isActive = true
        view.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
    }

    func addInvalidPassWordLabel(superView:UIView,incorrectPasswordLabel:UILabel){
        print("Add invalid Password called")
        // let incorrectPasswordLabel:UILabel = UILabel()
        //self.view.addSubview(incorrectPasswordLabel)
        incorrectPasswordLabel.translatesAutoresizingMaskIntoConstraints = false
        incorrectPasswordLabel.topAnchor.constraint(equalTo: superView.bottomAnchor, constant: -10).isActive = true
        incorrectPasswordLabel.leadingAnchor.constraint(equalTo: superView.leadingAnchor,constant: 0).isActive = true
        incorrectPasswordLabel.trailingAnchor.constraint(equalTo: superView.trailingAnchor, constant: 0).isActive = true
        incorrectPasswordLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        incorrectPasswordLabel.backgroundColor = UIColor.red
        incorrectPasswordLabel.textColor = backgroundColor
        incorrectPasswordLabel.text = "Invalid Password, Try Again"
        incorrectPasswordLabel.font = UIFont.boldSystemFont(ofSize: 15)
        incorrectPasswordLabel.isHidden = false
        superView.backgroundColor = UIColor.red
    }

    func isInvalidEntry(isInvalid:Bool, superView:UIView,message:String = "Invalid Entry"){
        
        if isInvalid == true{
            if !(incorrectLabelHeightAnchor == nil || incorrectLabelTrailingAnchor == nil || incorrectLabelTopAnchor == nil || incorrectLabelLeadingAnchor == nil) {
                NSLayoutConstraint.deactivate([incorrectLabelHeightAnchor,incorrectLabelTrailingAnchor,incorrectLabelTopAnchor,incorrectLabelLeadingAnchor])
            }
            
            self.incorrectLabelTopAnchor = incorrectLabel.topAnchor.constraint(equalTo: superView.bottomAnchor, constant: -10)
            self.incorrectLabelLeadingAnchor =  incorrectLabel.leadingAnchor.constraint(equalTo: superView.leadingAnchor,constant: 0)
            self.incorrectLabelTrailingAnchor = incorrectLabel.trailingAnchor.constraint(equalTo: superView.trailingAnchor, constant: 0)
            self.incorrectLabelHeightAnchor =    incorrectLabel.heightAnchor.constraint(equalToConstant: 20)
            
            NSLayoutConstraint.activate([incorrectLabelTopAnchor,incorrectLabelLeadingAnchor,incorrectLabelTrailingAnchor,incorrectLabelHeightAnchor ])
            
            incorrectLabel.backgroundColor = UIColor.red
            incorrectLabel.textColor = backgroundColor
            incorrectLabel.text = message
            incorrectLabel.font = UIFont.boldSystemFont(ofSize: 15)
            incorrectLabel.isHidden = false
            superView.backgroundColor = UIColor.red
            
            self.view.layoutIfNeeded()
            // self.scollContainerView.layoutIfNeeded()
        }
        else{
            print("valid entry")
            incorrectLabel.isHidden = true
            superView.backgroundColor = backgroundColor
        }
    }
    
}


extension LogInViewController:UITextFieldDelegate{
   
    func textFieldDidBeginEditing(_ textField: UITextField) {
          textField.autocorrectionType = .yes
        self.isInvalidEntry(isInvalid: false, superView: self.usernameEmailTextField)
    }
    
    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//
//        print("The text field ended editing")
//
////        if textField == passwordTextField{
////            print("password: \(passwordTextField.text)")
////        }
////        else
//            if textField == usernameEmailTextField{
//            print("username: \(usernameEmailTextField.text)")
//        }
//
//    }
//
//
//}
    
    func segueToLoggedIn(user:String?,userID:String){
        User.signedInUserID = userID
        print("Segue to logged in called for user: \(userID)")
//        let loggedInVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainCollectionViewStoryboard") as! MainViewController
        //MainViewController()//loggedinScreenViewController()
        //loggedInVC.loggedInLabel.text = "Hello \(userID)"
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
//
//        if let loggedInVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainCollectionViewStoryboard") as? MainViewController{
//            present(loggedInVC, animated: false, completion:{
//                print("Preview has been performed")
//                let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                appDelegate.enableBasicLocationServices()
//
//                print("Current loation from appDelegate: \(appDelegate.userLocation)")
//
//                if let userLocation = appDelegate.userLocation{
//                    appDelegate.locationDelegate.setLocation(location: userLocation)
//                }else{
//                    print("The user location has not yet been set")
//                }
//
//                print("location in MVC: \(loggedInVC.userLocation)")
//
////                appDelegate.enableBasicLocationServices()
////                print("Location: \(appDelegate.userLocation)")
//            })
//        }
    }
    
  
    @objc private func textFieldDidChangeEditing(textField:UITextField){
        print("The textfield is editiing")
        if textField == usernameEmailTextField{
            print("Username textfield text: \(usernameEmailTextField.text)")
            if let usernameEmail = usernameEmailTextField.text{
                
          //      if usernameEmail != "" && !usernameEmail.isEmpty {
                    if isValidEmail(testStr: usernameEmail){
                    self.userEmail = usernameEmail
                    }
                    else{
                        self.username = usernameEmail
                    }
             //   }
            }
        }

        if textField == passwordTextField{
            if let password = passwordTextField.text{
                print("passwordTextField text: \(passwordTextField.text)")
            //if password != "" && !password.isEmpty {
                print("password in didChangeEditing: \(textField)")
            self.userPassword = password
         //       }
            }
            }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
}

extension LogInViewController{
    @objc func handleKeyboardNotification(_ notification: Notification) {
        if firstShift{
            firstShift = false
        if let userInfo = notification.userInfo {
            
            let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
            
            let isKeyboardShowing = notification.name ==  UIResponder.keyboardWillShowNotification
            
            containerViewBottomAnchor.constant = isKeyboardShowing ? -keyboardFrame!.height : 0
            
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        }
    }
    }
}
