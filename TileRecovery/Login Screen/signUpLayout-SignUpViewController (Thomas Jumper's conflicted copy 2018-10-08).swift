//
//  signUpLayout-SignUpViewController.swift
//  TableViewDropDown
//
//  Created by 123456 on 10/8/18.
//  Copyright © 2018 123456. All rights reserved.
//

import Foundation
import UIKit

extension ViewController{

func setUpLayout(){
    signUpLabel.translatesAutoresizingMaskIntoConstraints = false
    signUpLabel.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 50).isActive = true
    signUpLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 16).isActive = true
    signUpLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
    signUpLabel.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.5).isActive = true
    signUpLabel.text = "Sign Up"
    signUpLabel.textColor = tileColor
    signUpLabel.font = UIFont.boldSystemFont(ofSize: 36)
    
    userNameTextField.translatesAutoresizingMaskIntoConstraints = false
    userNameTextField.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 16).isActive = true
    userNameTextField.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -16).isActive = true
    userNameTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
    userNameTextField.topAnchor.constraint(equalTo: self.signUpLabel.bottomAnchor, constant: 30).isActive = true
    userNameTextField.placeholder = "Username"
    //addRelativeLine(constraint: userNameTextField.bottomAnchor)
    addRelativeLine(superView: userNameTextField)

    
    firstNameTextField.translatesAutoresizingMaskIntoConstraints = false
    firstNameTextField.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 16).isActive = true
    firstNameTextField.trailingAnchor.constraint(equalTo: self.view.centerXAnchor, constant: -5).isActive = true
    firstNameTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
    firstNameTextField.topAnchor.constraint(equalTo: self.userNameTextField.bottomAnchor, constant: 30).isActive = true
    firstNameTextField.placeholder = "First Name"
    addRelativeLine(superView: firstNameTextField)
    
    lastNameTextField.translatesAutoresizingMaskIntoConstraints = false
    lastNameTextField.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -16).isActive = true
    lastNameTextField.leadingAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 5).isActive = true
    lastNameTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
    lastNameTextField.topAnchor.constraint(equalTo: self.userNameTextField.bottomAnchor, constant: 30).isActive = true
    lastNameTextField.placeholder = "Last Name"
    addRelativeLine(superView: lastNameTextField)
    
    emailTextField.translatesAutoresizingMaskIntoConstraints = false
    emailTextField.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 16).isActive = true
    emailTextField.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -16).isActive = true
    emailTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
    emailTextField.topAnchor.constraint(equalTo: self.firstNameTextField.bottomAnchor, constant: 30).isActive = true
    emailTextField.placeholder = "Email"
    addRelativeLine(superView: emailTextField)
    
    phoneNumberTextField.translatesAutoresizingMaskIntoConstraints = false
    phoneNumberTextField.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 16).isActive = true
    phoneNumberTextField.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -16).isActive = true
    phoneNumberTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
    phoneNumberTextField.topAnchor.constraint(equalTo: self.emailTextField.bottomAnchor, constant: 30).isActive = true
    phoneNumberTextField.placeholder = "Phone (optional)"
    addRelativeLine(superView: phoneNumberTextField)
    
    passwordTextField.translatesAutoresizingMaskIntoConstraints = false
    passwordTextField.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 16).isActive = true
    passwordTextField.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -16).isActive = true
    passwordTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
    passwordTextField.topAnchor.constraint(equalTo: self.phoneNumberTextField.bottomAnchor, constant: 30).isActive = true
    passwordTextField.placeholder = "Password"
    addRelativeLine(superView: passwordTextField)
    
    birthDayLabel.translatesAutoresizingMaskIntoConstraints = false
    birthDayLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 16).isActive = true
    birthDayLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -16).isActive = true
    birthDayLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
    birthDayLabel.topAnchor.constraint(equalTo: self.passwordTextField.bottomAnchor, constant: 30).isActive = true
    birthDayLabel.text = "Birthday"
   
    
    showMonthsButton.translatesAutoresizingMaskIntoConstraints = false
    showDaysButton.translatesAutoresizingMaskIntoConstraints = false
    showYearsButton.translatesAutoresizingMaskIntoConstraints = false
    
    dayTableView.translatesAutoresizingMaskIntoConstraints = false
    monthsTableView.translatesAutoresizingMaskIntoConstraints = false
    yearTableView.translatesAutoresizingMaskIntoConstraints = false
    
    //dayTableView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0).isActive = true
    dayTableView.leadingAnchor.constraint(equalTo: showDaysButton.leadingAnchor, constant: 0).isActive = true
    dayTableView.widthAnchor.constraint(equalTo: showDaysButton.widthAnchor, multiplier: 1.0).isActive = true
    dayTableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
    //dayTableView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.4).isActive = true
    dayTableView.topAnchor.constraint(equalTo: self.birthDayLabel.bottomAnchor, constant: 50).isActive = true
    
    //monthsTableView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0).isActive = true
    monthsTableView.leadingAnchor.constraint(equalTo: showMonthsButton.leadingAnchor, constant: 0).isActive = true
    monthsTableView.widthAnchor.constraint(equalTo: showMonthsButton.widthAnchor, multiplier: 1.0).isActive = true
    monthsTableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
    // monthsTableView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.4).isActive = true
    monthsTableView.topAnchor.constraint(equalTo: self.birthDayLabel.bottomAnchor, constant: 50).isActive = true
    
   // yearTableView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0).isActive = true
    yearTableView.leadingAnchor.constraint(equalTo: showYearsButton.leadingAnchor, constant: 0).isActive = true
    yearTableView.widthAnchor.constraint(equalTo: showYearsButton.widthAnchor, multiplier: 1.0).isActive = true
    yearTableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
    //yearTableView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.4).isActive = true
    yearTableView.topAnchor.constraint(equalTo: self.birthDayLabel.bottomAnchor, constant: 50).isActive = true
    
    showMonthsButton.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1/3).isActive = true
    showMonthsButton.bottomAnchor.constraint(equalTo: monthsTableView.topAnchor, constant: 0).isActive = true
    showMonthsButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
    showMonthsButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    
    showDaysButton.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1/3).isActive = true
    showDaysButton.bottomAnchor.constraint(equalTo: dayTableView.topAnchor, constant: 0).isActive = true
    showDaysButton.leadingAnchor.constraint(equalTo: self.showMonthsButton.trailingAnchor, constant: 0).isActive = true
    showDaysButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    
    showYearsButton.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1/3).isActive = true
    showYearsButton.bottomAnchor.constraint(equalTo: yearTableView.topAnchor, constant: 0).isActive = true
    showYearsButton.leadingAnchor.constraint(equalTo: self.showDaysButton.trailingAnchor, constant: 0).isActive = true
    showYearsButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    
    }
    
    func addLine(fromPoint start: CGPoint, toPoint end:CGPoint) {
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        linePath.move(to: start)
        linePath.addLine(to: end)
        line.path = linePath.cgPath
        line.strokeColor = UIColor.red.cgColor
        line.lineWidth = 4
        line.lineJoin = CAShapeLayerLineJoin.round
        self.view.layer.addSublayer(line)
    }
    
    func addRelativeLine(superView:UIView){
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 0.5))
        view.backgroundColor = tileColor
        self.view.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.topAnchor.constraint(equalTo: superView.bottomAnchor, constant: 10).isActive = true
        view.leadingAnchor.constraint(equalTo: superView.leadingAnchor, constant: 0).isActive = true
        view.trailingAnchor.constraint(equalTo: superView.trailingAnchor, constant: 0).isActive = true
        view.heightAnchor.constraint(equalToConstant: 4).isActive = true
        
    }

}
