//
//  VerifyViewController.swift
//  TableViewDropDown
//
//  Created by 123456 on 10/23/18.
//  Copyright © 2018 123456. All rights reserved.
//test

import Foundation
import UIKit
import Firebase

class VerifyViewController:UIViewController,UITextFieldDelegate{
    
    var containerView:UIView = UIView()
    
    var backButton:UIButton = UIButton()
    var buttonVerifyEmail:UIButton = UIButton()
    var buttonResendCode:UIButton = UIButton()
    var phoneNumber:String!
    var enterCodeTextField:UITextField = UITextField()
    var verificationID:String?
    var verificationCode:String?
    var newUser:userInfo!
    
    var digitTextFields:[UITextField] = Array(repeating: UITextField(), count: 6)
   
    var popUpView:UIView!
    var cancelButton:UIButton!
    var popUpLabel:UILabel!
    var blurEffectView:UIVisualEffectView!
    
    var awaitLabel:UILabel = UILabel()
    var titleLabel:UILabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        if let currentUser = Auth.auth().currentUser{
            print("The current user has been verified")
            segueToLoggedIn(user: "User", userID: currentUser.uid)
        }
        
        self.view.addSubview(containerView)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        containerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        containerView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        containerView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        containerView.layer.borderColor = UIColor.orange.cgColor
        containerView.layer.borderWidth = 4
        
        self.containerView.backgroundColor = backgroundColor
        self.containerView.addSubview(buttonVerifyEmail)
        buttonVerifyEmail.setTitle("Verify Code", for: .normal)
        buttonVerifyEmail.backgroundColor = tileColor
        buttonVerifyEmail.setTitleColor(backgroundColor, for: .normal)
        buttonVerifyEmail.titleLabel?.textColor = backgroundColor
        buttonVerifyEmail.addTarget(self, action: #selector(buttonVerifyEmailTapped), for: .touchUpInside)
        buttonVerifyEmail.layer.borderColor = UIColor.red.cgColor
        buttonVerifyEmail.layer.borderWidth = 4
        
        self.containerView.addSubview(buttonResendCode)
        buttonResendCode.setTitle("Resend Code", for: .normal)
        buttonResendCode.backgroundColor = tileColor
        buttonResendCode.setTitleColor(backgroundColor, for: .normal)
        buttonResendCode.titleLabel?.textColor = backgroundColor
        buttonResendCode.addTarget(self, action: #selector(buttonResendCodeTapped), for: .touchUpInside)
        
        
        self.containerView.backgroundColor = backgroundColor
        self.containerView.addSubview(backButton)
        backButton.setImage(#imageLiteral(resourceName: "icons8-back-96"), for: .normal)
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        
        backButton.translatesAutoresizingMaskIntoConstraints = false
        backButton.topAnchor.constraint(equalTo: self.containerView.topAnchor, constant: 35).isActive = true
        backButton.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor, constant: 5).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        backButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        
        buttonResendCode.translatesAutoresizingMaskIntoConstraints = false
        buttonResendCode.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor, constant: 16).isActive = true
        buttonResendCode.centerYAnchor.constraint(equalTo: self.containerView.centerYAnchor).isActive = true
        buttonResendCode.heightAnchor.constraint(equalToConstant: 50).isActive = true
        //buttonResendCode.widthAnchor.constraint(equalToConstant: 200).isActive = true
        buttonResendCode.trailingAnchor.constraint(equalTo: self.containerView.centerXAnchor, constant: -5).isActive = true
        buttonResendCode.layer.cornerRadius = 29/5
        
        buttonVerifyEmail.translatesAutoresizingMaskIntoConstraints = false
        buttonVerifyEmail.leadingAnchor.constraint(equalTo: self.containerView.centerXAnchor,constant: 5).isActive = true
        buttonVerifyEmail.centerYAnchor.constraint(equalTo: self.containerView.centerYAnchor).isActive = true
        buttonVerifyEmail.heightAnchor.constraint(equalToConstant: 50).isActive = true
      //  buttonVerifyEmail.widthAnchor.constraint(equalToConstant: 200).isActive = true
        buttonVerifyEmail.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor, constant: -16).isActive = true
        buttonVerifyEmail.layer.cornerRadius = 29/5
        buttonVerifyEmail.isUserInteractionEnabled = true
        buttonVerifyEmail.becomeFirstResponder()
        
        self.containerView.addSubview(enterCodeTextField)
        enterCodeTextField.placeholder = "Enter Code"
        enterCodeTextField.textColor = tileColor
        enterCodeTextField.contentHorizontalAlignment = UITextField.ContentHorizontalAlignment.center
        enterCodeTextField.contentVerticalAlignment = UITextField.ContentVerticalAlignment.center
        enterCodeTextField.textAlignment = .left
        enterCodeTextField.font = UIFont.boldSystemFont(ofSize: 30)
        enterCodeTextField.textContentType = UITextContentType.oneTimeCode
        enterCodeTextField.keyboardType = .numberPad
        enterCodeTextField.delegate = self
        enterCodeTextField.translatesAutoresizingMaskIntoConstraints = false
        enterCodeTextField.centerXAnchor.constraint(equalTo: self.containerView.centerXAnchor).isActive = true
        enterCodeTextField.bottomAnchor.constraint(equalTo: self.buttonVerifyEmail.topAnchor,constant: 30).isActive = true
        enterCodeTextField.heightAnchor.constraint(equalToConstant: 200).isActive = true
        enterCodeTextField.widthAnchor.constraint(equalToConstant: 200).isActive = true
        enterCodeTextField.addTarget(self, action: #selector(textFieldChangedEditing(textField:)), for: .editingChanged)
        
        self.containerView.addSubview(awaitLabel)
        awaitLabel.text = "We sent a code to your phone ..."
        awaitLabel.textColor = tileColor
        awaitLabel.textAlignment = .center
        awaitLabel.font = UIFont.boldSystemFont(ofSize: 17)
        awaitLabel.translatesAutoresizingMaskIntoConstraints = false
        awaitLabel.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor, constant: 16).isActive = true
        awaitLabel.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor, constant: -16).isActive = true
        awaitLabel.bottomAnchor.constraint(equalTo: self.enterCodeTextField.topAnchor, constant: 30).isActive = true
        awaitLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.containerView.addSubview(titleLabel)
        titleLabel.textAlignment = .center
        titleLabel.textColor = tileColor
        titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
        titleLabel.text = "Phone Number Verification"
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.topAnchor.constraint(equalTo: self.containerView.topAnchor, constant: 35).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: self.containerView.centerXAnchor, constant: 0).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: self.backButton.trailingAnchor, constant: 15).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor, constant: -16).isActive = true
   
        containerView.bringSubviewToFront(backButton)
        containerView.bringSubviewToFront(buttonResendCode)
        containerView.bringSubviewToFront(buttonVerifyEmail)
    }
    
    

    
    @objc func backButtonTapped(){
        print("Go Back Tapped")
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        dismiss(animated: true, completion: nil)
        
    }
    
    func SegueToSignUpScreen(){
        let signUpVC = SignUpScreenViewController()
        //        self.present(signUpVC, animated: false, completion: {
        //            print("pushed new viewController")
        //        })
        
        //self.navigationController?.pushViewController(signUpVC, animated: true)
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        present(signUpVC, animated: false, completion: nil)
    }
    
    @objc func buttonVerifyEmailTapped(){
        print("Button")
        
        let db = Database.database()
        let usersRef = db.reference(withPath: "Users")
        
        if let verificationCode = self.enterCodeTextField.text, let verificationID = self.verificationID{
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID,
            verificationCode: verificationCode.digits)
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if let error = error {
                // ...
                print("Error in entering code")
                self.errorVerifyingAlert(message: "Error entering code")
                return
            }
            // User is signed in
            // ...
            print("user signed in with uid: \(authResult?.user.uid)")
            guard let authResult = authResult else{
                print("Eroor getting authResult")
                return
            }
            
            if let email = self.newUser.email,let password = self.newUser.password{
                let emailCredential = EmailAuthProvider.credential(withEmail: email, password: password)
                authResult.user.linkAndRetrieveData(with: emailCredential, completion: {(authResult,error) in
                    if let error = error{
                        print("Error linking the acounts together")
                    }
                })
//            authResult.user.updateEmail(to: email, completion: {(error) in
//                if let error = error{
//                    print("Error adding email to the current user")
//                }
//                else{
//                    print("email update a success")
//                }
//            })
            }
            else{
                print("Email field empty, couldnt add email to authentication")
            }
            print("HERE LIES THE ERROR!!!")
            print("self.newUser.data: \(self.newUser.userData)")
            print("self.authResult.user.uid: \(authResult.user.uid)")
            print("Usernaame: \(self.newUser.userData!["username"])")
          
            let photoDefaultURL:String = "https://storage.googleapis.com/pullupanddownfirebase.appspot.com/Profile%20Pictures%2F8EyBxZTKgGPFU63W8sWu2Z8NhEl2.jpeg?GoogleAccessId=firebase-adminsdk-ma36y%40pullupanddownfirebase.iam.gserviceaccount.com&Expires=17315190361&Signature=MJ2qWCkqNrThCH%2FOe8mM1Y3Bm1Nbiy0%2ByGj%2F%2FDD9UjbIItj4EcrQkYNIez2IG4REwN%2FGInpWAd0voS6uZ04UeuNk3FB1Fi87rwtKr8BRNxoFxfIHNe%2ByCtU2AYbOLK6nBVWg6TFH8OH7ZVE0C9Rf%2Fzo3Kc9mUOGVXG0v%2FnThjZqvHWlddaEln9K6JehqkhCCqhq2X9jKWAKvtEZM69600buKqjSgi0U0%2FIKC7m%2BSth1ufeRlpCWjbbyWJU6siAMGkxIF6e57avfmZRBSmMzL2Ubpn0lY00Nv%2BJr8kYVx9OASL79OsDVoRMqWN%2F51b14Re%2BkRuo%2FAo9O6bK84dVCXNw%3D%3D"
            
                       if var data = self.newUser.userData{

                        let username:String = data["username"] as! String
                        
                        var uploadData:[String:Any] = ["username":username,"blocked": false,"photoURL":photoDefaultURL]
               
                        let newUser:User = User(userID: authResult.user.uid, username: username, photoURL: photoDefaultURL)
                        
                        let userID = authResult.user.uid
                        print("userID: \(userID)")
                        print("Upload Data: \(uploadData)")
                        //["testData":true,"moreTestData":123,"testDataName":"testName"]
                        
                        //
                        let dispatchGroup:DispatchGroup = DispatchGroup()
                        dispatchGroup.enter()
                        if let newUser = self.newUser{
                            Database.database().reference(withPath: "UserData/\(userID)").setValue(["birthday":newUser.Birthday?.timeIntervalSince1970,"firstName":newUser.firstName,"lastName":newUser.lastName], withCompletionBlock: {(error,reference) in
                                if let error = error{
                                    print("An error occurred adding in the userData: \(error)")
                                }
                                dispatchGroup.leave()
                            })
                        }
                        
                        dispatchGroup.enter()
                        Database.database().reference(withPath: "Users/\(userID)").setValue(uploadData, withCompletionBlock: {(error,reference) in
                            if let error = error{
                                   print("Error uploading user data to firebase: \(error)")
                            }else{
                                print("Completed the upload to the database: \(uploadData), => \(reference)")
                            }
                            dispatchGroup.leave()
                        })
                        dispatchGroup.notify(queue: .main, execute: {
                            User.signedInUserID = authResult.user.uid
                            User.signedInUser = newUser
                             self.segueToLoggedIn(user: authResult.user.displayName, userID: authResult.user.uid)
                            })
                       }
            else{
                print("Data dictionary is empty")
            }
            //self.segueToLoggedIn(user: "user", userID: authResult.user.uid)
            }
        }
        else{
            print("Text Field Empty")
            errorVerifyingAlert(message: "Please Enter a Code")
        }
    }
    
    @objc func buttonResendCodeTapped(){
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                //self.showMessagePrompt(error.localizedDescription)
                print("An error has occured while trying to authenticate phone number: \(error.localizedDescription)")
                self.errorVerifyingAlert(message: "An error occured while trying to authenticate phone number")
                return
            }
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
            // Sign in using the verificationID and the code sent to the user
            // ...
            self.verificationID = verificationID
            print("Verification id created")
            //semd to firestore
            
        }
    }
    
    func segueToLoggedIn(user:String?,userID:String){
//        let loggedInVC = loggedinScreenViewController()
//        loggedInVC.loggedInLabel.text = "Hello \(userID)"
////        let transition = CATransition()
////        transition.duration = 0.3
////        transition.type = CATransitionType.push
////        transition.subtype = CATransitionSubtype.fromRight
////        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
////        view.window!.layer.add(transition, forKey: kCATransition)
//
//        present(loggedInVC, animated: false, completion: nil)
//        if let loggedInVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainCollectionViewStoryboard") as? MainViewController{
//            present(loggedInVC, animated: false, completion:{
//                let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                appDelegate.enableBasicLocationServices()
//                
//                if let userLocation = appDelegate.userLocation{
//                    appDelegate.locationDelegate.setLocation(location: userLocation)
//                }else{
//                    print("The user location has not yet been set")
//                }
//            })
//        }
    }

    func errorVerifyingAlert(message:String = "An Error Occured While Trying to Verify Code"){
        
        self.popUpView = UIView()
        
        
        popUpLabel = UILabel()
        popUpView.backgroundColor = backgroundColor
        popUpLabel.textColor = tileColor
        popUpLabel.text = message
        popUpLabel.textAlignment = .center
        popUpLabel.baselineAdjustment = .alignCenters
        
        self.cancelButton = UIButton()
        self.cancelButton.setTitle("Dismiss", for: .normal)
        self.cancelButton.setTitleColor(backgroundColor, for: .normal)
        self.cancelButton.backgroundColor = tileColor
        
        //self.cancelButton.layer.cornerRadius = 29/5
        self.cancelButton.addTarget(self, action: #selector(dismissAlert), for: .touchUpInside)
        // show on screen
        self.view.addSubview(popUpView)
        self.popUpView.addSubview(cancelButton)
        self.popUpView.addSubview(popUpLabel)
        
        
        
        popUpView.layer.cornerRadius = 29/5
        popUpView.layer.masksToBounds = true
        
        
        
        popUpView.translatesAutoresizingMaskIntoConstraints = false
        popUpView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        popUpView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0).isActive = true
        popUpView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        popUpView.widthAnchor.constraint(equalToConstant: self.view.frame.width - 46).isActive = true
        
        popUpLabel.translatesAutoresizingMaskIntoConstraints = false
        popUpLabel.topAnchor.constraint(equalTo: self.popUpView.topAnchor, constant: 0).isActive = true
        popUpLabel.centerXAnchor.constraint(equalTo: self.popUpView.centerXAnchor, constant: 0).isActive = true
        popUpLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        popUpLabel.leadingAnchor.constraint(equalTo: self.popUpView.leadingAnchor, constant: 0).isActive = true
        popUpLabel.trailingAnchor.constraint(equalTo: self.popUpView.trailingAnchor, constant: 0).isActive = true
        
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        cancelButton.bottomAnchor.constraint(equalTo: self.popUpView.bottomAnchor, constant: 0).isActive = true
        cancelButton.leadingAnchor.constraint(equalTo: self.popUpView.leadingAnchor, constant: 0).isActive = true
        cancelButton.trailingAnchor.constraint(equalTo: self.popUpView.trailingAnchor, constant: 0).isActive = true
        cancelButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        self.blurEffectView = UIVisualEffectView(effect: blurEffect)
        self.blurEffectView.frame = self.view.bounds
        self.blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.containerView.addSubview(self.blurEffectView)
        
        self.popUpLabel.layoutIfNeeded()
    }
    
    @objc func dismissAlert(){
        print("Dismiss tapped")
        popUpView.removeFromSuperview()
        blurEffectView.removeFromSuperview()
        NSLayoutConstraint.deactivate(popUpView.constraints)
        NSLayoutConstraint.deactivate(cancelButton.constraints)
        NSLayoutConstraint.deactivate(popUpLabel.constraints)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if textField == enterCodeTextField{
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }

        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

        return updatedText.count < 12
        }
        else{
            return true
        }
    }

    
    @objc func textFieldChangedEditing(textField:UITextField){
        if textField == enterCodeTextField{
            print("Activated")
            if var text = textField.text{
                textField.text = text.digits.inserting(separator: "-", every: 1)
                print("output text \(text.digits.inserting(separator: "-", every: 1))")
                print("The current count of the text: \(text.count)")
                if text.count == 10{
                    enterCodeTextField.resignFirstResponder()
                }
            }
        }
    }
}



extension String {
    func inserting(separator: String, every n: Int) -> String {
        var result: String = ""
        let characters = Array(self.characters)
        stride(from: 0, to: characters.count, by: n).forEach {
            result += String(characters[$0..<min($0+n, characters.count)])
            if $0+n < characters.count {
                result += separator
            }
        }
        return result
    }
}


extension Collection {
    public func chunk(n: IndexDistance) -> [SubSequence] {
        var res: [SubSequence] = []
        var i = startIndex
        var j: Index
        while i != endIndex {
            j = index(i, offsetBy: n, limitedBy: endIndex) ?? endIndex
            res.append(self[i..<j])
            i = j
        }
        return res
    }
}

