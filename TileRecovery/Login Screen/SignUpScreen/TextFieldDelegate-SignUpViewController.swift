//
//  TextFieldDelegate-SignUpViewController.swift
//  TableViewDropDown
//
//  Created by 123456 on 10/10/18.
//  Copyright © 2018 123456. All rights reserved.
//

import Foundation
import UIKit

extension SignUpScreenViewController:UITextFieldDelegate{
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
      activeField = nil
        print("Delegate called with text: \(textField.text)")
        if textField == passwordTextField{
            if let password = passwordTextField.text{
            if !checkTextSufficientComplexity(text: password){
//                incorrectPasswordLabel.isHidden = false
//                passwordTextField.backgroundColor = UIColor.red
                print("called from password")
                self.isInvalidEntry(isInvalid: true, superView: passwordTextField,message: "Password Too Simple")
                allFieldsAccountedFor = false
            }
            else{
                self.newUser.password = passwordTextField.text
                allFieldsAccountedFor = true
            }
            }
            else{
                self.isInvalidEntry(isInvalid: true, superView: passwordTextField,message: "No Password Entered")
            }
        }
        
        if textField == phoneNumberTextField{
                if var phoneNumber = phoneNumberTextField.text, !phoneNumber.isEmpty, phoneNumber != "", phoneNumber.digits.count == 10{
                phoneNumber = "+1" + phoneNumber.digits
                    print("phoneNumber sent to server: \(phoneNumber)")
                    self.checkPhoneNumber(phoneNumber: phoneNumber.lowercased(), completionHandler: {(isTaken) in
                        
                        print("The value of isTaken:\(isTaken)")
                        if let isTaken = isTaken{
                            if isTaken == true{
                                print("The phoneNumber is already in use")
                                self.isInvalidEntry(isInvalid: true, superView: self.phoneNumberTextField,message: "Phone Number already in use")
                                self.allFieldsAccountedFor = false
                            }
                            else{
                                print("Good to go")
                                self.newUser.phoneNumber = self.phoneNumberTextField.text
                                self.allFieldsAccountedFor = true
                                print("NewUSer phone number: \(self.newUser.phoneNumber)")
                            }
                        }
                        else{
                            print("Invalid phoneNumber")
                            self.isInvalidEntry(isInvalid: true, superView: self.phoneNumberTextField,message: "check network")
                            self.allFieldsAccountedFor = false
                        }
                    })
                    
  
                }
                else{
                    print("Invalid phone number entered")
                    self.isInvalidEntry(isInvalid: true, superView: phoneNumberTextField)
                    allFieldsAccountedFor = false
            }
            }
            print("Check Sufficient: \(checkTextSufficientComplexity(text: textField.text!))")
        
        if textField == userNameTextField{
            if let username = userNameTextField.text, !username.isEmpty, isValidUsername{
                
                self.checkUserName(username: username.lowercased(), completionHandler: {(isTaken) in
                   
                    print("The value of isTaken:\(isTaken)")
                    if let isTaken = isTaken{
                        if isTaken == true{
                            print("The username is already in use")
                            self.isInvalidEntry(isInvalid: true, superView: self.userNameTextField,message: "Username already in use")
                            self.allFieldsAccountedFor = false
                        }
                        else{
                            print("Good to go")
                            self.newUser.username = username
                            self.allFieldsAccountedFor = true
                        }
                    }
                    else{
                        print("Invalid Username")
                        self.isInvalidEntry(isInvalid: true, superView: self.userNameTextField,message: "check network")
                        self.allFieldsAccountedFor = false
                    }
                })
                
           
            }
            else{
                print("Invalid Username")
                self.isInvalidEntry(isInvalid: true, superView: userNameTextField)
                allFieldsAccountedFor = false
            }
        }
        
        if textField == emailTextField{
            print("isValidEmail: \(self.isValidEmail(testStr: "fhfh"))")
            if let email = emailTextField.text, !email.isEmpty, isValidEmail(testStr: email){
                self.checkEmail(email: email.lowercased(), completionHandler: {(isTaken) in
                    
                    print("The value of isTaken:\(isTaken)")
                    if let isTaken = isTaken{
                        if isTaken == true{
                            print("The username is already in use")
                            self.isInvalidEntry(isInvalid: true, superView: self.emailTextField,message: "Email already in use")
                            self.allFieldsAccountedFor = false
                        }
                        else{
                            print("Good to go")
                            self.newUser.email = email
                            self.allFieldsAccountedFor = true
                        }
                    }
                    else{
                        print("Invalid Username")
                        self.isInvalidEntry(isInvalid: true, superView: self.emailTextField,message: "check network")
                        self.allFieldsAccountedFor = false
                    }
                })
                
                
      
            }
            else{
                print("Invalid Email")
                self.isInvalidEntry(isInvalid: true, superView: emailTextField, message:  "Invalid Email Entered")
                allFieldsAccountedFor = false
            }
        }
        
        if textField == firstNameTextField{
            if let firstName = firstNameTextField.text, !firstName.isEmpty{
                self.newUser.firstName = firstName
                allFieldsAccountedFor = true
            }
            else{
                self.isInvalidEntry(isInvalid: true, superView: firstNameTextField)
                allFieldsAccountedFor = false
            }
        }
        
        if textField == lastNameTextField{
            if let lastName = lastNameTextField.text, !lastName.isEmpty{
                self.newUser.lastName = lastName
                allFieldsAccountedFor = true
            }
            else{
                 self.isInvalidEntry(isInvalid: true, superView: lastNameTextField)
                allFieldsAccountedFor = false
                
            }
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
                //self.isInvalidEntry(isInvalid: false, superView: textField)
       activeField = textField
        if textField == passwordTextField{
            activeField = passwordTextField

//            incorrectPasswordLabel.isHidden = true
//            passwordTextField.backgroundColor = backgroundColor
            self.isInvalidEntry(isInvalid: false, superView: passwordTextField)
        }
        if textField == userNameTextField{
            isInvalidEntry(isInvalid: false, superView: userNameTextField)
        }
        
        if textField == emailTextField{
            isInvalidEntry(isInvalid: false, superView: emailTextField )
        }
        if textField == firstNameTextField{
            isInvalidEntry(isInvalid: false, superView: firstNameTextField )
        }
        if textField == lastNameTextField{
            isInvalidEntry(isInvalid: false, superView: lastNameTextField )
        }
        if textField == phoneNumberTextField{
            activeField = textField
            isInvalidEntry(isInvalid: false, superView: phoneNumberTextField)
        }
    }
    
    func checkTextSufficientComplexity( text : String) -> Bool{
        
        
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        var texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        var capitalresult = texttest.evaluate(with: text)
        print("\(capitalresult)")
        
        
        let numberRegEx  = ".*[0-9]+.*"
        var texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        var numberresult = texttest1.evaluate(with: text)
        print("\(numberresult)")
        
        
        let specialCharacterRegEx  = ".*[!&^%$#@()/]+.*"
        var texttest2 = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)
        
        var specialresult = texttest2.evaluate(with: text)
        print("\(specialresult)")
        
        return capitalresult || numberresult || specialresult
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == ""{
        print("back space pressed")
            backSpacePressed = true
        }
        else{
            backSpacePressed = false
        }
            return true
    }
    
    @objc func textFieldDidCange(textField:UITextField){
        if textField == self.phoneNumberTextField{
            if let phoneNumber = self.phoneNumberTextField.text{
                if !phoneNumber.isEmpty {//&& phoneNumber.digits.isPhoneNumber{
                    print("Phone Number entered")
                    if phoneNumber.count == 1 && !backSpacePressed{
                        self.phoneNumberTextField.text?.insert("(", at: self.phoneNumberTextField.text!.startIndex)
                    }
                    else if phoneNumber.count == 4 && !backSpacePressed{
                        self.phoneNumberTextField.text = phoneNumber + ") "
                    }
                    else if phoneNumber.count == 9 && !backSpacePressed{
                        self.phoneNumberTextField.text = phoneNumber + "-"
                    }
                }
            }
        }
        
        print("The textfield is editiing")
        if textField == userNameTextField{
            print("Username textfield text: \(userNameTextField.text)")
            if let username = userNameTextField.text, !username.isEmpty{
                //check if valid user name and modify isValidUserName
            }
        }
        
    }
    
    
  
//    func isValidEmail(testStr:String) -> Bool {
//        // print("validate calendar: \(testStr)")
//        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
//
//
//
//        // "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
//
//        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
//        return emailTest.evaluate(with: testStr)
//    }
    
    func isValidEmail(testStr:String) -> Bool {
        print("validate emilId in textFieldDelegate: \(testStr)")
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField == phoneNumberTextField{
//            if let phoneNumber = phoneNumberTextField.text{
//                let textRange = Range(NSRange(location: 0, length: 1))
//
//            }
//        }
//        return true
//    }
//

//    func isPhoneNumber(phoneNumber:String)->Bool{
//        do {
//            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
//            let matches = detector.matches(in: phoneNumber, options: [], range: NSMakeRange(0, phoneNumber.count))
//            if let res = matches.first {
//                var result = (res.resultType == .phoneNumber) && (res.range.location == 0) && (res.range.length == phoneNumber.count)
//                return result
//            } else {
//                print("Not vali phone number")
//                return false
//            }
//        } catch {
//            print("Not valid phone number")
//            return false
//        }
//    }
    
}


extension String {
    var digits: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
    }
    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.characters.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.characters.count && self.characters.count == 10
            } else {
                return false
            }
        } catch {
            return false
        }
    }
    /**
         Adds a given prefix to self, if the prefix itself, or another required prefix does not yet exist in self.
         Omit `requiredPrefix` to check for the prefix itself.
         */
        mutating func addPrefixIfNeeded(_ prefix: String, requiredPrefix: String? = nil) {
            guard !self.hasPrefix(requiredPrefix ?? prefix) else { return }
            self = prefix + self
        }
    
}
