//
//  signUpLayout-SignUpViewController.swift
//  TableViewDropDown
//
//  Created by 123456 on 10/8/18.
//  Copyright © 2018 123456. All rights reserved.
//

import Foundation
import UIKit

extension SignUpScreenViewController{

func setUpLayout(){
    scollContainerView.translatesAutoresizingMaskIntoConstraints = false
    scollContainerView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
    scollContainerView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
    scollContainerView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
    scollContainerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
    
//    scollContainerView.layer.borderColor = UIColor.red.cgColor
//    scollContainerView.layer.borderWidth = 4

    backButton.translatesAutoresizingMaskIntoConstraints = false
    backButton.topAnchor.constraint(equalTo: self.scollContainerView.topAnchor, constant: 35).isActive = true
    backButton.leadingAnchor.constraint(equalTo: self.scollContainerView.leadingAnchor, constant: 5).isActive = true
    backButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    backButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
    //backButton.widthAnchor.constraint(equalTo: self.scollContainerView.widthAnchor, multiplier: 0.5).isActive = true
    //backButton.text = "Sign Up"
    //backButton.textColor = tileColor
    //backButton.font = UIFont.boldSystemFont(ofSize: 36)
    
    signUpLabel.translatesAutoresizingMaskIntoConstraints = false
    signUpLabel.topAnchor.constraint(equalTo: self.backButton.bottomAnchor, constant: -10).isActive = true
    signUpLabel.leadingAnchor.constraint(equalTo: self.scollContainerView.leadingAnchor, constant: 16).isActive = true
    signUpLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
    signUpLabel.widthAnchor.constraint(equalTo: self.scollContainerView.widthAnchor, multiplier: 0.5).isActive = true
    signUpLabel.text = "Sign Up"
    signUpLabel.textColor = tileColor
    signUpLabel.font = UIFont.boldSystemFont(ofSize: 36)
    

    userNameTextField.translatesAutoresizingMaskIntoConstraints = false
    userNameTextField.leadingAnchor.constraint(equalTo: self.scollContainerView.leadingAnchor, constant: 16).isActive = true
    userNameTextField.trailingAnchor.constraint(equalTo: self.scollContainerView.trailingAnchor, constant: -16).isActive = true
    userNameTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
    userNameTextField.topAnchor.constraint(equalTo: self.signUpLabel.bottomAnchor, constant: 30).isActive = true
    userNameTextField.widthAnchor.constraint(equalTo: self.scollContainerView.widthAnchor, multiplier: 1.0, constant: -32).isActive = true
    userNameTextField.placeholder = "Username"
    userNameTextField.textColor = tileColor
    userNameTextField.textContentType = UITextContentType.username
    userNameTextField.attributedPlaceholder = NSAttributedString(string: userNameTextField.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : tileColor.withAlphaComponent(0.5)])
    //addRelativeLine(constraint: userNameTextField.bottomAnchor)
    addRelativeLine(superView: userNameTextField)
    
    firstNameTextField.translatesAutoresizingMaskIntoConstraints = false
    firstNameTextField.leadingAnchor.constraint(equalTo: self.scollContainerView.leadingAnchor, constant: 16).isActive = true
    firstNameTextField.trailingAnchor.constraint(equalTo: self.scollContainerView.centerXAnchor, constant: -5).isActive = true
    firstNameTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
    firstNameTextField.topAnchor.constraint(equalTo: self.userNameTextField.bottomAnchor, constant: 30).isActive = true
    firstNameTextField.placeholder = "First Name"
    firstNameTextField.textContentType = UITextContentType.name
    firstNameTextField.textColor = tileColor
    firstNameTextField.attributedPlaceholder = NSAttributedString(string: firstNameTextField.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : tileColor.withAlphaComponent(0.5)])
    addRelativeLine(superView: firstNameTextField)

    lastNameTextField.translatesAutoresizingMaskIntoConstraints = false
    lastNameTextField.trailingAnchor.constraint(equalTo: self.scollContainerView.trailingAnchor, constant: -16).isActive = true
    lastNameTextField.leadingAnchor.constraint(equalTo: self.scollContainerView.centerXAnchor, constant: 5).isActive = true
    lastNameTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
    lastNameTextField.topAnchor.constraint(equalTo: self.userNameTextField.bottomAnchor, constant: 30).isActive = true
    lastNameTextField.placeholder = "Last Name"
    lastNameTextField.textContentType = UITextContentType.familyName
    lastNameTextField.textColor = tileColor
    lastNameTextField.attributedPlaceholder = NSAttributedString(string: lastNameTextField.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : tileColor.withAlphaComponent(0.5)])
    addRelativeLine(superView: lastNameTextField)

    emailTextField.translatesAutoresizingMaskIntoConstraints = false
    emailTextField.leadingAnchor.constraint(equalTo: self.scollContainerView.leadingAnchor, constant: 16).isActive = true
    emailTextField.trailingAnchor.constraint(equalTo: self.scollContainerView.trailingAnchor, constant: -16).isActive = true
    emailTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
    emailTextField.topAnchor.constraint(equalTo: self.firstNameTextField.bottomAnchor, constant: 30).isActive = true
    emailTextField.placeholder = "Email"
    emailTextField.textColor = tileColor
    emailTextField.attributedPlaceholder = NSAttributedString(string: emailTextField.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : tileColor.withAlphaComponent(0.5)])
    emailTextField.textContentType = UITextContentType.emailAddress
    addRelativeLine(superView: emailTextField)

    countryCodeButton.translatesAutoresizingMaskIntoConstraints = false
    countryCodeButton.leadingAnchor.constraint(equalTo: self.scollContainerView.leadingAnchor, constant: 16).isActive = true
    countryCodeButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    countryCodeButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
    countryCodeButton.topAnchor.constraint(equalTo: self.emailTextField.bottomAnchor, constant: 30).isActive = true
    countryCodeButton.setTitle("+1", for: .normal)
    countryCodeButton.setTitleColor(tileColor, for: .normal)
    addRelativeLine(superView: countryCodeButton)
    
    phoneNumberTextField.translatesAutoresizingMaskIntoConstraints = false
    phoneNumberTextField.leadingAnchor.constraint(equalTo: self.countryCodeButton.trailingAnchor, constant: 16).isActive = true
    phoneNumberTextField.trailingAnchor.constraint(equalTo: self.scollContainerView.trailingAnchor, constant: -16).isActive = true
    phoneNumberTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
    phoneNumberTextField.topAnchor.constraint(equalTo: self.emailTextField.bottomAnchor, constant: 30).isActive = true
    phoneNumberTextField.placeholder = "Phone (optional)"
    phoneNumberTextField.textColor = tileColor
    phoneNumberTextField.attributedPlaceholder = NSAttributedString(string: phoneNumberTextField.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : tileColor.withAlphaComponent(0.5)])
    addRelativeLine(superView: phoneNumberTextField)
    
    passwordTextField.translatesAutoresizingMaskIntoConstraints = false
    passwordTextField.leadingAnchor.constraint(equalTo: self.scollContainerView.leadingAnchor, constant: 16).isActive = true
    passwordTextField.trailingAnchor.constraint(equalTo: self.scollContainerView.trailingAnchor, constant: -16).isActive = true
    passwordTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
    passwordTextField.topAnchor.constraint(equalTo: self.phoneNumberTextField.bottomAnchor, constant: 30).isActive = true
    passwordTextField.placeholder = "Password"
    passwordTextField.textColor = tileColor
    passwordTextField.attributedPlaceholder = NSAttributedString(string: passwordTextField.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : tileColor.withAlphaComponent(0.5)])
    addRelativeLine(superView: passwordTextField)

    birthDayLabel.translatesAutoresizingMaskIntoConstraints = false
    birthDayLabel.leadingAnchor.constraint(equalTo: self.scollContainerView.leadingAnchor, constant: 16).isActive = true
    birthDayLabel.trailingAnchor.constraint(equalTo: self.scollContainerView.trailingAnchor, constant: -16).isActive = true
    birthDayLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
    birthDayLabel.topAnchor.constraint(equalTo: self.passwordTextField.bottomAnchor, constant: 30).isActive = true
    birthDayLabel.textColor = tileColor
    birthDayLabel.text = "Birthday"
    birthDayLabel.font = UIFont.boldSystemFont(ofSize: 15)


    showMonthsButton.translatesAutoresizingMaskIntoConstraints = false
    showDaysButton.translatesAutoresizingMaskIntoConstraints = false
    showYearsButton.translatesAutoresizingMaskIntoConstraints = false

    dayTableView.translatesAutoresizingMaskIntoConstraints = false
    monthsTableView.translatesAutoresizingMaskIntoConstraints = false
    yearTableView.translatesAutoresizingMaskIntoConstraints = false

    //dayTableView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0).isActive = true
    dayTableView.leadingAnchor.constraint(equalTo: showDaysButton.leadingAnchor, constant: 0).isActive = true
    dayTableView.widthAnchor.constraint(equalTo: showDaysButton.widthAnchor, multiplier: 1.0).isActive = true
    dayTableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
    //dayTableView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.4).isActive = true
    dayTableView.topAnchor.constraint(equalTo: self.birthDayLabel.bottomAnchor, constant: 50).isActive = true

    //monthsTableView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0).isActive = true
    monthsTableView.leadingAnchor.constraint(equalTo: showMonthsButton.leadingAnchor, constant: 0).isActive = true
    monthsTableView.widthAnchor.constraint(equalTo: showMonthsButton.widthAnchor, multiplier: 1.0).isActive = true
    monthsTableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
    // monthsTableView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.4).isActive = true
    monthsTableView.topAnchor.constraint(equalTo: self.birthDayLabel.bottomAnchor, constant: 50).isActive = true

   // yearTableView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0).isActive = true
    yearTableView.leadingAnchor.constraint(equalTo: showYearsButton.leadingAnchor, constant: 0).isActive = true
    yearTableView.widthAnchor.constraint(equalTo: showYearsButton.widthAnchor, multiplier: 1.0).isActive = true
    yearTableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
    //yearTableView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.4).isActive = true
    yearTableView.topAnchor.constraint(equalTo: self.birthDayLabel.bottomAnchor, constant: 50).isActive = true

    showMonthsButton.widthAnchor.constraint(equalTo: self.scollContainerView.widthAnchor, multiplier: 1/3, constant: -24).isActive = true
    showMonthsButton.bottomAnchor.constraint(equalTo: monthsTableView.topAnchor, constant: 0).isActive = true
    showMonthsButton.leadingAnchor.constraint(equalTo: self.scollContainerView.leadingAnchor, constant: 16).isActive = true
    showMonthsButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    addRelativeLine(superView: showMonthsButton)
    showMonthsButton.imageEdgeInsets = UIEdgeInsets(top: 25, left: 85, bottom: 0, right: 0)
   // showMonthsButton.titleEdgeInsets = UIEdgeInsets(top: 25, left: -150, bottom: 0, right: 0)
    showMonthsButton.titleEdgeInsets = UIEdgeInsets(top: 25, left: 0, bottom: 0, right: 0)
    showMonthsButton.contentHorizontalAlignment = .left

    showDaysButton.widthAnchor.constraint(equalTo: self.scollContainerView.widthAnchor, multiplier: 1/3, constant: -24).isActive = true
    showDaysButton.bottomAnchor.constraint(equalTo: dayTableView.topAnchor, constant: 0).isActive = true
    showDaysButton.leadingAnchor.constraint(equalTo: self.showMonthsButton.trailingAnchor, constant: 16).isActive = true
    showDaysButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    addRelativeLine(superView: showDaysButton)
    showDaysButton.imageEdgeInsets = UIEdgeInsets(top: 25, left: 85, bottom: 0, right: 0)
   // showDaysButton.titleEdgeInsets = UIEdgeInsets(top: 25, left: -150, bottom: 0, right: 0)
   showDaysButton.titleEdgeInsets = UIEdgeInsets(top: 25, left: 0, bottom: 0, right: 0)
    showDaysButton.contentHorizontalAlignment = .left

    
    showYearsButton.widthAnchor.constraint(equalTo: self.scollContainerView.widthAnchor, multiplier: 1/3, constant: -24).isActive = true
    showYearsButton.bottomAnchor.constraint(equalTo: yearTableView.topAnchor, constant: 0).isActive = true
    showYearsButton.leadingAnchor.constraint(equalTo: self.showDaysButton.trailingAnchor, constant: 16).isActive = true
    showYearsButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    addRelativeLine(superView: showYearsButton)
    showYearsButton.imageEdgeInsets = UIEdgeInsets(top: 25, left: 85, bottom: 0, right: 0)
   // showYearsButton.titleEdgeInsets = UIEdgeInsets(top: 25, left: -150, bottom: 0, right: 0)
    showYearsButton.titleEdgeInsets = UIEdgeInsets(top: 25, left: 0, bottom: 0, right: 0)
    showYearsButton.contentHorizontalAlignment = .left
    
    termsAndPrivacy.translatesAutoresizingMaskIntoConstraints = false
    termsAndPrivacy.topAnchor.constraint(equalTo: self.showYearsButton.bottomAnchor, constant: 20).isActive = true
    termsAndPrivacy.leadingAnchor.constraint(equalTo: self.scollContainerView.leadingAnchor, constant: 16).isActive = true
    termsAndPrivacy.heightAnchor.constraint(equalToConstant: 55).isActive = true
   // termsAndPrivacy.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.5).isActive = true
    termsAndPrivacy.trailingAnchor.constraint(equalTo: self.scollContainerView.trailingAnchor, constant: -16).isActive = true
    termsAndPrivacy.text = "By signing up, you agree to our Terms of Service & Privacy Policy."
    termsAndPrivacy.textColor = tileColor
    termsAndPrivacy.font = UIFont.boldSystemFont(ofSize: 15)
    termsAndPrivacy.backgroundColor = backgroundColor
    //termsAndPrivacy.numberOfLines = 0

    signUpButton.translatesAutoresizingMaskIntoConstraints = false
    signUpButton.topAnchor.constraint(equalTo: self.termsAndPrivacy.bottomAnchor, constant: 5).isActive = true
    //signUpButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -50).isActive = true
    signUpButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 16).isActive = true
    signUpButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -16).isActive = true
    signUpButton.heightAnchor.constraint(equalToConstant: 60).isActive = true

//    termsAndPrivacy.backgroundColor = UIColor.clear
//    view.bringSubviewToFront(signUpButton)
//    view.sendSubviewToBack(termsAndPrivacy)

//    incorrectPasswordLabel.translatesAutoresizingMaskIntoConstraints = false
//    incorrectPasswordLabel.topAnchor.constraint(equalTo: self.passwordTextField.bottomAnchor, constant: -10).isActive = true
//    incorrectPasswordLabel.leadingAnchor.constraint(equalTo: self.passwordTextField.leadingAnchor,constant: 0).isActive = true
//    incorrectPasswordLabel.trailingAnchor.constraint(equalTo: self.passwordTextField.trailingAnchor, constant: 0).isActive = true
//    incorrectPasswordLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
//    incorrectPasswordLabel.backgroundColor = UIColor.red
//    incorrectPasswordLabel.textColor = backgroundColor
//    incorrectPasswordLabel.text = "Password to simple"
//    incorrectPasswordLabel.font = UIFont.boldSystemFont(ofSize: 15)
//    incorrectPasswordLabel.isHidden = true
//    passwordTextField.backgroundColor = backgroundColor
    
   // self.isInvalidEntry(isInvalid: true, superView: passwordTextField)
    
//    incorrectLabel.translatesAutoresizingMaskIntoConstraints = false
//    self.incorrectLabelTopAnchor = incorrectLabel.topAnchor.constraint(equalTo: self.passwordTextField.bottomAnchor, constant: -10)
//    self.incorrectLabelTopAnchor.isActive = true
//self.incorrectLabelLeadingAnchor =  incorrectLabel.leadingAnchor.constraint(equalTo: self.passwordTextField.leadingAnchor,constant: 0)
//    self.incorrectLabelLeadingAnchor.isActive = true
//    self.incorrectLabelTrailingAnchor = incorrectLabel.trailingAnchor.constraint(equalTo: self.passwordTextField.trailingAnchor, constant: 0)
//    self.incorrectLabelTrailingAnchor.isActive = true
//   self.incorrectLabelHeightAnchor =    incorrectLabel.heightAnchor.constraint(equalToConstant: 20)
//    incorrectLabelHeightAnchor.isActive = true
//    incorrectLabel.backgroundColor = UIColor.red
//    incorrectLabel.textColor = backgroundColor
//    incorrectLabel.text = "Invalid Entry"
//    incorrectPasswordLabel.font = UIFont.boldSystemFont(ofSize: 15)
//    incorrectPasswordLabel.isHidden = false
//    passwordTextField.backgroundColor = backgroundColor
    
    incorrectLabel.translatesAutoresizingMaskIntoConstraints = false
   // self.isInvalidEntry(isInvalid: true, superView: passwordTextField)
    
    scollContainerView.sendSubviewToBack(signUpButton)
    scollContainerView.bringSubviewToFront(monthsTableView)
    scollContainerView.bringSubviewToFront(dayTableView)
    scollContainerView.bringSubviewToFront(yearTableView)
    

    }

    func addLine(fromPoint start: CGPoint, toPoint end:CGPoint) {
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        linePath.move(to: start)
        linePath.addLine(to: end)
        line.path = linePath.cgPath
        line.strokeColor = UIColor.red.cgColor
        line.lineWidth = 4
        line.lineJoin = CAShapeLayerLineJoin.round
        self.scollContainerView.layer.addSublayer(line)
    }

    func addRelativeLine(superView:UIView){
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 0.5))
        view.backgroundColor = tileColor
        self.scollContainerView.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.topAnchor.constraint(equalTo: superView.bottomAnchor, constant: 10).isActive = true
        view.leadingAnchor.constraint(equalTo: superView.leadingAnchor, constant: 0).isActive = true
        view.trailingAnchor.constraint(equalTo: superView.trailingAnchor, constant: 0).isActive = true
        view.heightAnchor.constraint(equalToConstant: 2).isActive = true

    }

//    func isInvalidEntry(isInvalid:Bool, superView:UIView,message:String = "Invalid Entry"){
//
//        if isInvalid == true{
//            print("invalid entry")
//            if !(incorrectLabelHeightAnchor == nil || incorrectLabelTrailingAnchor == nil || incorrectLabelTopAnchor == nil || incorrectLabelLeadingAnchor == nil) {
//                NSLayoutConstraint.deactivate([incorrectLabelHeightAnchor,incorrectLabelTrailingAnchor,incorrectLabelTopAnchor,incorrectLabelLeadingAnchor])
//            }
//
//        self.incorrectLabelTopAnchor = incorrectLabel.topAnchor.constraint(equalTo: superView.bottomAnchor, constant: -10)
//        self.incorrectLabelLeadingAnchor =  incorrectLabel.leadingAnchor.constraint(equalTo: superView.leadingAnchor,constant: 0)
//        self.incorrectLabelTrailingAnchor = incorrectLabel.trailingAnchor.constraint(equalTo: superView.trailingAnchor, constant: 0)
//        self.incorrectLabelHeightAnchor =    incorrectLabel.heightAnchor.constraint(equalToConstant: 20)
//
//        NSLayoutConstraint.activate([incorrectLabelTopAnchor,incorrectLabelLeadingAnchor,incorrectLabelTrailingAnchor,incorrectLabelHeightAnchor ])
//
//        incorrectLabel.backgroundColor = UIColor.red
//        incorrectLabel.textColor = backgroundColor
//        incorrectLabel.text = message
//        incorrectLabel.font = UIFont.boldSystemFont(ofSize: 15)
//        incorrectLabel.isHidden = false
//        superView.backgroundColor = UIColor.red
//
//        self.view.layoutIfNeeded()
//       // self.scollContainerView.layoutIfNeeded()
//        }
//        else{
//            print("valid entry")
//            incorrectLabel.isHidden = true
//            superView.backgroundColor = backgroundColor
//        }
//    }
    
    func isInvalidEntry(isInvalid:Bool, superView:UIView,message:String = "Invalid Entry"){
        
        addInvalidLabel(isInvalid: isInvalid, superView: superView, message: message)
    }
    
    func addInvalidLabel(isInvalid:Bool, superView:UIView, message:String = "Invalid Entry"){

        var subLabel:UILabel? = nil
        for view in superView.subviews{
            if let label = view as? UILabel{
                subLabel = label
            }
        }

        if isInvalid == true{

            if subLabel == nil{
                print("no sublabel found")
                let label = UILabel()
                label.translatesAutoresizingMaskIntoConstraints = false
                superView.addSubview(label)
                label.topAnchor.constraint(equalTo: superView.bottomAnchor, constant: -10).isActive = true
                label.leadingAnchor.constraint(equalTo: superView.leadingAnchor,constant: 0).isActive = true
                label.trailingAnchor.constraint(equalTo: superView.trailingAnchor, constant: 0).isActive = true
                label.heightAnchor.constraint(equalToConstant: 20).isActive = true
                subLabel = label
            }

            if let label = subLabel{
                label.backgroundColor = UIColor.red
                label.textColor = backgroundColor
                label.text = message
                label.font = UIFont.boldSystemFont(ofSize: 15)
                label.isHidden = false
                superView.backgroundColor = UIColor.red
            }
        }
        else{
            print("valid entry entered ")
            if let label = subLabel{
            label.isHidden = true
            }
            superView.backgroundColor = backgroundColor
        }

    }
}


