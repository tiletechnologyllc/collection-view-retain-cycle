//
//  SignUpButtonTapped-SignUpScreen.swift
//  TableViewDropDown
//
//  Created by 123456 on 10/26/18.
//  Copyright © 2018 123456. All rights reserved.
//

import Foundation
import UIKit
import Firebase

extension SignUpScreenViewController{
    
    @objc func signUpButtonTapped(){
        phoneNumberTextField.resignFirstResponder()
        //Auth.auth().
        print("new user: \(newUser)")
        if newUser.isNill == false{

        guard let phoneNumber =  newUser.phoneNumber else{
            print("No phone number entered")
            return
        }
        let verifyViewController = VerifyViewController()
        verifyViewController.phoneNumber = phoneNumber
        verifyViewController.newUser = self.newUser
        
        let dispatchGroup = DispatchGroup()
            
        dispatchGroup.enter()
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                //self.showMessagePrompt(error.localizedDescription)
                print("An error has occured while trying to authenticate phone number: \(error.localizedDescription)")
                return
            }
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
            // Sign in using the verificationID and the code sent to the user
            // ...
            verifyViewController.verificationID = verificationID
            print("Verification id created")
            //semd to firestore
           dispatchGroup.leave()
            
//                            let transition = CATransition()
//                            transition.duration = 0.03
//                            transition.type = CATransitionType.push
//                            transition.subtype = CATransitionSubtype.fromRight
//                            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//                            self.view.window!.layer.add(transition, forKey: kCATransition)
//
//                            self.present(verifyViewController, animated: false, completion: {
//                                print("presented Verify View Controller")
//                            })

        }

        dispatchGroup.notify(queue: .main, execute: {
            let transition = CATransition()
            transition.duration = 0.03
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromRight
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            self.view.window!.layer.add(transition, forKey: kCATransition)

            self.present(verifyViewController, animated: false, completion: {
                print("presented Verify View Controller")
            })
        })


        }

        else{
            print("Not all fields have been filled")
            emptyFieldsAlert()
        }
    }
    
     func emptyFieldsAlert(){
    
        self.popUpView = UIView()

        
        popUpLabel = UILabel()
        popUpView.backgroundColor = backgroundColor
        popUpLabel.textColor = tileColor
        popUpLabel.text = "Please Fill in All Fields"
        popUpLabel.textAlignment = .center
        popUpLabel.baselineAdjustment = .alignCenters
        
        self.cancelButton = UIButton()
        self.cancelButton.setTitle("Dismiss", for: .normal)
        self.cancelButton.setTitleColor(backgroundColor, for: .normal)
        self.cancelButton.backgroundColor = tileColor
        
        //self.cancelButton.layer.cornerRadius = 29/5
        self.cancelButton.addTarget(self, action: #selector(dismissAlert), for: .touchUpInside)
        // show on screen
        self.view.addSubview(popUpView)
        self.popUpView.addSubview(cancelButton)
        self.popUpView.addSubview(popUpLabel)
        
        
        
        popUpView.layer.cornerRadius = 29/5
        popUpView.layer.masksToBounds = true

      
        
        popUpView.translatesAutoresizingMaskIntoConstraints = false
        popUpView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        popUpView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0).isActive = true
        popUpView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        popUpView.widthAnchor.constraint(equalToConstant: self.view.frame.width - 46).isActive = true
        
        popUpLabel.translatesAutoresizingMaskIntoConstraints = false
        popUpLabel.topAnchor.constraint(equalTo: self.popUpView.topAnchor, constant: 0).isActive = true
        popUpLabel.centerXAnchor.constraint(equalTo: self.popUpView.centerXAnchor, constant: 0).isActive = true
        popUpLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        popUpLabel.leadingAnchor.constraint(equalTo: self.popUpView.leadingAnchor, constant: 0).isActive = true
        popUpLabel.trailingAnchor.constraint(equalTo: self.popUpView.trailingAnchor, constant: 0).isActive = true
        
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        cancelButton.bottomAnchor.constraint(equalTo: self.popUpView.bottomAnchor, constant: 0).isActive = true
        cancelButton.leadingAnchor.constraint(equalTo: self.popUpView.leadingAnchor, constant: 0).isActive = true
        cancelButton.trailingAnchor.constraint(equalTo: self.popUpView.trailingAnchor, constant: 0).isActive = true
        cancelButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        self.blurEffectView = UIVisualEffectView(effect: blurEffect)
        self.blurEffectView.frame = self.scollContainerView.bounds
        self.blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        scollContainerView.addSubview(self.blurEffectView)
        
        self.popUpLabel.layoutIfNeeded()
    }
    
    @objc func dismissAlert(){
        print("Dismiss tapped")
        popUpView.removeFromSuperview()
        blurEffectView.removeFromSuperview()
        NSLayoutConstraint.deactivate(popUpView.constraints)
        NSLayoutConstraint.deactivate(cancelButton.constraints)
        NSLayoutConstraint.deactivate(popUpLabel.constraints)
    }

}
