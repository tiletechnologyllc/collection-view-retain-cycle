//
//  DatabaseChecks-SignUpViewController.swift
//  TableViewDropDown
//
//  Created by 123456 on 10/29/18.
//  Copyright © 2018 123456. All rights reserved.
//

import Foundation
import UIKit
import Firebase

extension SignUpScreenViewController{
    
    
    func checkUserName(username:String, completionHandler: @escaping (Bool?)->Void){
        var isValueAlreadyExisting:Bool? = nil
        functions.httpsCallable("isUsernameAlreadyTaken").call(["username":username.lowercased()], completion: {(result,error) in
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    let code = FunctionsErrorCode(rawValue: error.code)
                    let message = error.localizedDescription
                    let details = error.userInfo[FunctionsErrorDetailsKey]
                    
                    print("Error message: \(message)")
                    completionHandler(nil)
                    isValueAlreadyExisting = nil
                }
            }
                
            else  if let result = result?.data as? Bool{
                print("The token returned from the server is: \(result)")
                completionHandler(result)
            }
            else{
                print("The server returned null, no such username exists already")
                completionHandler(nil)
                isValueAlreadyExisting = nil
            }
            
        })
        
        print("returned: \(isValueAlreadyExisting)")
    }
    
    
    func checkEmail(email:String, completionHandler: @escaping (Bool?)->Void){
        var isValueAlreadyExisting:Bool? = nil
        functions.httpsCallable("isEmailAlreadyTaken").call(["email":email.lowercased()], completion: {(result,error) in
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    let code = FunctionsErrorCode(rawValue: error.code)
                    let message = error.localizedDescription
                    let details = error.userInfo[FunctionsErrorDetailsKey]
                    
                    print("Error message: \(message)")
                    completionHandler(nil)
                    isValueAlreadyExisting = nil
                }
            }
                
            else  if let result = result?.data as? Bool{
                print("The token returned from the server is: \(result)")
                completionHandler(result)
            }
            else{
                print("The server returned null, no such username exists already")
                completionHandler(nil)
                isValueAlreadyExisting = nil
            }
            
        })
        
        print("returned: \(isValueAlreadyExisting)")
    }
    
    
    func checkPhoneNumber(phoneNumber:String, completionHandler: @escaping (Bool?)->Void){
        var isValueAlreadyExisting:Bool? = nil
        functions.httpsCallable("isPhoneNumberAlreadyTaken").call(["phoneNumber":phoneNumber.lowercased()], completion: {(result,error) in
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    let code = FunctionsErrorCode(rawValue: error.code)
                    let message = error.localizedDescription
                    let details = error.userInfo[FunctionsErrorDetailsKey]
                    
                    print("Error message: \(message)")
                    completionHandler(nil)
                    isValueAlreadyExisting = nil
                }
            }
                
            else  if let result = result?.data as? Bool{
                print("The token returned from the server is: \(result)")
                completionHandler(result)
            }
            else{
                print("The server returned null, no such username exists already")
                completionHandler(nil)
                isValueAlreadyExisting = nil
            }
            
        })
        
        print("returned: \(isValueAlreadyExisting)")
    }
}
