//
//  ViewController.swift
//  TableViewDropDown
//
//  Created by 123456 on 10/8/18.
//  Copyright © 2018 123456. All rights reserved.
//

import UIKit

var tileColor:UIColor = UIColor(red: 56/255, green: 217/255, blue: 169/255, alpha: 1.0)
var backgroundColor:UIColor = UIColor(red: 230/255, green: 252/255, blue: 245/255, alpha: 1.0)

class SignUpScreenViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate {
   
    
    var monthsTableView: UITableView!
    var dayTableView: UITableView!
    var yearTableView: UITableView!
    
    var signUpLabel:UILabel = UILabel()
    var userNameTextField:UITextField = UITextField()
    var firstNameTextField:UITextField = UITextField()
    var lastNameTextField:UITextField = UITextField()
    var emailTextField:UITextField = UITextField()
    var phoneNumberTextField:UITextField = UITextField()
    var passwordTextField:UITextField = UITextField()
    var birthDayLabel:UILabel = UILabel()
    var termsAndPrivacy:UITextView = UITextView()
    var signUpButton:UIButton = UIButton()
    //var birthdayDatePicker:UIDatePicker = UIDatePicker()
    
    
    
    
    var showMonthsButton: UIButton!
    var showDaysButton: UIButton!
    var showYearsButton: UIButton!
    
    //var month:[String]// = ["January","February","March","April","May","June","July","August","September","November","December"]
    var monthSelected = 8
    var yearSelected = 1996
    var dateSelected = 7
    
    var daysInMonth = 31
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.view.backgroundColor = backgroundColor
        
        monthsTableView = UITableView()
        dayTableView = UITableView()
        yearTableView = UITableView()
        
        showMonthsButton = UIButton()
        showDaysButton = UIButton()
        showYearsButton = UIButton()
        
        showMonthsButton.addTarget(self, action: #selector(pickDateSelected(_:)), for: .touchUpInside)
        showDaysButton.addTarget(self, action: #selector(pickDateSelected(_:)), for: .touchUpInside)
        showYearsButton.addTarget(self, action: #selector(pickDateSelected(_:)), for: .touchUpInside)
        
        showYearsButton.setTitle("Year", for: .normal)
        showDaysButton.setTitle("Day", for: .normal)
        showMonthsButton.setTitle("Month", for: .normal)
        
        showMonthsButton.setImage(UIImage(named: "expandArrow"), for: .normal)
        showYearsButton.setImage(UIImage(named: "expandArrow"), for: .normal)
        showDaysButton.setImage(UIImage(named: "expandArrow"), for: .normal)
        
        showYearsButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        showDaysButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        showMonthsButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        
        showMonthsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
        showYearsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
        showDaysButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
        
        showMonthsButton.setTitleColor(tileColor, for: .focused)
        showYearsButton.setTitleColor(tileColor, for: .focused)
        showDaysButton.setTitleColor(tileColor, for: .focused)
        
        self.view.addSubview(monthsTableView)
        self.view.addSubview(dayTableView)
        self.view.addSubview(yearTableView)
        
        self.view.addSubview(showMonthsButton)
        self.view.addSubview(showDaysButton)
        self.view.addSubview(showYearsButton)
        
        monthsTableView.delegate = self
        monthsTableView.dataSource = self
        monthsTableView.layer.cornerRadius = 5
       // monthsTableView.layer.borderWidth = 1
       // monthsTableView.layer.borderColor = UIColor.black.cgColor
        monthsTableView.separatorStyle = .none
        monthsTableView.backgroundColor = backgroundColor
        monthsTableView.isHidden = true
        
        yearTableView.delegate = self
        yearTableView.dataSource = self
        yearTableView.layer.cornerRadius = 5
        yearTableView.backgroundColor = backgroundColor
       // yearTableView.layer.borderWidth = 1
       // yearTableView.layer.borderColor = UIColor.black.cgColor
        yearTableView.separatorStyle = .none
        yearTableView.isHidden = true
        
        dayTableView.delegate = self
        dayTableView.dataSource = self
        dayTableView.layer.cornerRadius = 5
//        dayTableView.layer.borderWidth = 1
//        dayTableView.layer.borderColor = UIColor.black.cgColor
        dayTableView.backgroundColor = backgroundColor
        dayTableView.separatorStyle = .none
        dayTableView.isHidden = true
        
        
        showMonthsButton.layer.cornerRadius = 5
//        showMonthsButton.layer.borderColor = tileColor.cgColor
//        showMonthsButton.layer.borderWidth = 1
        
        showDaysButton.layer.cornerRadius = 5
//        showDaysButton.layer.borderColor = tileColor.cgColor
//        showDaysButton.layer.borderWidth = 1
        
        showYearsButton.layer.cornerRadius = 5
//        showYearsButton.layer.borderColor =  tileColor.cgColor
//        showYearsButton.layer.borderWidth = 1
        
        let month = Calendar.current.monthSymbols
        for mon in month{
            print(mon)
        }
        
        self.view.addSubview(signUpLabel)
        self.view.addSubview(userNameTextField)
        self.view.addSubview(firstNameTextField)
        self.view.addSubview(lastNameTextField)
        self.view.addSubview(emailTextField)
        self.view.addSubview(phoneNumberTextField)
        self.view.addSubview(passwordTextField)
        self.view.addSubview(birthDayLabel)
        self.view.addSubview(termsAndPrivacy)
        self.view.addSubview(signUpButton)
        
        passwordTextField.isSecureTextEntry = true
        
        signUpButton.backgroundColor = tileColor
        signUpButton.setTitleColor(backgroundColor, for: .normal)
        signUpButton.setTitle("Sign Up", for: .normal)
        signUpButton.layer.cornerRadius = 5
        

        termsAndPrivacy.isUserInteractionEnabled = true
        
        setUpLayout()
        
        self.view.sendSubviewToBack(termsAndPrivacy)
        self.view.sendSubviewToBack(signUpButton)
        
        termsAndPrivacy.textContainer.lineBreakMode = .byWordWrapping
        termsAndPrivacy.text = "By signing up, you agree to our Terms of Service & Privacy Policy."
        termsAndPrivacy.textColor = tileColor
        
        termsAndPrivacy.isEditable = false
        
        let attributedString = NSMutableAttributedString(string: "By signing up, you agree to our Terms of Service & Privacy Policy.")
        attributedString.addAttribute(.link, value: "https://tilesocial.com", range: NSRange(location: 31, length: 17))
        attributedString.addAttribute(.link, value: "https://tilesocial.com", range: NSRange(location: 51, length: 14))
        
        //attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: tileColor , range: NSRange(location: 31, length: 17))
       
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: tileColor , range:  NSRange(location: 51, length: 14))
        
        termsAndPrivacy.attributedText = attributedString
        termsAndPrivacy.textColor = tileColor.withAlphaComponent(0.7)
        termsAndPrivacy.tintColor = tileColor
    }
    
    


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == monthsTableView{
            return Calendar.current.monthSymbols.count
        }
        else if tableView == yearTableView{
            return 150
        }
        else if tableView == dayTableView{
            let dateComponents = DateComponents(year: yearSelected, month: monthSelected)
            let calendar = Calendar.current
            let date = calendar.date(from: dateComponents)!
            
            let range = calendar.range(of: .day, in: .month, for: date)!
            let numDays = range.count
            daysInMonth = numDays
            return numDays
        }
        
        return 0
        
        }
    
    func numberOfSections(in tableView: UITableView) -> Int {
         return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor(red: 230/255, green: 252/255, blue: 245/255, alpha: 1.0)
        
        if tableView == monthsTableView{
            showMonthsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
           // showYearsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
           // showDaysButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            
        showMonthsButton.setTitle("Month", for: .normal)
            
        let cell = UITableViewCell()
        cell.textLabel?.text = "\(Calendar.current.monthSymbols[indexPath.row])"
        cell.textLabel?.textAlignment = .center
        cell.selectedBackgroundView = backgroundView
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        cell.textLabel?.textColor = tileColor
        cell.textLabel?.backgroundColor = backgroundColor
        cell.backgroundColor = backgroundColor
        return cell
        }
        else if tableView == dayTableView{
            
           // showMonthsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
           // showYearsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            showDaysButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            
            showDaysButton.setTitle("Day", for: .normal)
            
            let cell = UITableViewCell()
            
            
            cell.textLabel?.text = "\(indexPath.row + 1)"
            cell.textLabel?.textAlignment = .center
            cell.selectedBackgroundView = backgroundView
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
            cell.textLabel?.textColor = tileColor
            cell.textLabel?.backgroundColor = backgroundColor
            cell.backgroundColor = backgroundColor
            return cell
            
        }
            
        else if tableView == yearTableView{
            
            //showMonthsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            showYearsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            //showDaysButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            
            showYearsButton.setTitle("Year", for: .normal)
            
            let cell = UITableViewCell()
            let year = Calendar.current.component(.year, from: Date())
            
            cell.textLabel?.isEnabled = true
            
            if monthSelected == 2 && dateSelected == 29{
                if  (year - 13 - indexPath.row) % 4 != 0 {
                    cell.textLabel?.isEnabled = false
                }
                else{
                    cell.textLabel?.isEnabled = true
                }
            }
            cell.textLabel?.text = "\(year - 13 - indexPath.row)"
            cell.textLabel?.textAlignment = .center
            cell.selectedBackgroundView = backgroundView
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
            cell.textLabel?.textColor = tileColor
            cell.textLabel?.backgroundColor = backgroundColor
            cell.backgroundColor = backgroundColor
            return cell
        }
        return UITableViewCell()
    }

    @objc func pickDateSelected(_ sender: UIButton) {
        if sender == showMonthsButton{
            showMonthsButton.setTitleColor(tileColor.withAlphaComponent(1.0), for: .normal)
            showYearsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            showDaysButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            print("showMonthsButton selected")
            if monthsTableView.isHidden == true{
                monthsTableView.isHidden = false
            }
            else{
                monthsTableView.isHidden = true
            }
        }
        else if sender == showDaysButton{
            showMonthsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            showYearsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            showDaysButton.setTitleColor(tileColor.withAlphaComponent(1.0), for: .normal)
            if dayTableView.isHidden == true{
                dayTableView.isHidden = false
            }
            else{
                dayTableView.isHidden = true
            }
        }
        else if sender == showYearsButton{
            showMonthsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            showYearsButton.setTitleColor(tileColor.withAlphaComponent(1.0), for: .normal)
            showDaysButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            if yearTableView.isHidden == true{
                yearTableView.isHidden = false
            }
            else{
                yearTableView.isHidden = true
            }
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        
        
        if tableView == monthsTableView{
        let selectedMonth = Calendar.current.monthSymbols[indexPath.row]
        monthSelected = indexPath.row + 1
        dayTableView.reloadData()
     //   monthsTableView.reloadData()
        yearTableView.reloadData()
        showMonthsButton.setTitle(Calendar.current.monthSymbols[indexPath.row], for: .normal)
        monthsTableView.isHidden = true
        }
        
        else if tableView == dayTableView{
            dateSelected = indexPath.row + 1
        //    dayTableView.reloadData()
        //    monthsTableView.reloadData()
            yearTableView.reloadData()
            showDaysButton.setTitle(String(dateSelected), for: .normal)
            dayTableView.isHidden = true
        }
        
        else if tableView == yearTableView{
            let year = Calendar.current.component(.year, from: Date())
            yearSelected = year - 13 - indexPath.row
            showYearsButton.setTitle(String(yearSelected), for: .normal)
            yearTableView.isHidden = true
        }
        
//        var monthName:String = ""
//
//        switch selectedMonth {
//        case 1:
//            print("January")
//            monthName = "January"
//        case 2:
//            print("February")
//            monthName = "February"
//        case 3:
//            print("March")
//            monthName = "March"
//        case 4:
//            print("April")
//            monthName = "April"
//        case 5:
//            print("May")
//            monthName = "May"
//        case 6:
//            print("June")
//            monthName = "June"
//        case 7:
//            print("July")
//            monthName = "July"
//        case 8:
//            print("August")
//            monthName = "August"
//        case 9:
//            print("September")
//            monthName = "Septemeber"
//        case 10:
//            print("October")
//            monthName = "October"
//        case 11:
//            print("November")
//            monthName = "November"
//        case 12:
//            print("December")
//            monthName = "December"
//        default:
//            print("Invalid Month")
//            assert(true, "Invalid Month Selected")
//        }
        
    }
    
    func isDateValid(month:Int,day:Int,year:Int)->Bool{
        let dateString = String(day)+":"+String(month)+":"+String(year)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd:MM:yyyy"
        if dateFormatter.date(from: dateString) != nil {
            print("date is valid")
            return true
        } else {
            print("date is invalid")
            return false
        }
    }
}



