//
//  MainViewController.swift
//  collectionViewAttemptOfPagingAttempt002
//
//  Created by Zach on 1/31/18.
//  Copyright © 2018 TheRedCamaro. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import CoreLocation
import Firebase
import SDWebImage

class MainViewController: UIViewController,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource, gestureDelegate,previewSegueDelegate,UICollectionViewDataSourcePrefetching {
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        
        for indexPath in indexPaths{
        let item = collectionViewManager.cells[indexPath.item]
            if item.type == CellTypes.FollowersProfileScreenCell{
                print("")
            }
        }
    }
    
    
    lazy var collectionViewManager = CollectionViewManager()
    
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    @IBOutlet weak var MainCollectionView: UICollectionView!
    //    var searchFeedScreenViewController:SearchFeedScreenViewController! = SearchFeedScreenViewController()
    //    var profileScreenSegmentedViewController:segmentedViewController! = segmentedViewController()
    //    var friendsProfileScreenSegmentedViewController:segmentedViewController! = segmentedViewController()
    
    var searchFeedScreenViewController:SearchFeedScreenViewController!
 //   var profileScreenSegmentedViewController:segmentedViewController!
  //  var followersProfileScreenSegmentedViewController:segmentedViewController!
    @IBOutlet weak var ContainerView: UIView!
    
    var toolBarHorrizontalStackView:UIStackView!
    var slideToFeedScreenButton:UIButton!
    var slideToProfileScreenButton:UIButton!
    var slideToCameraPhotoAlbumButton:UIButton!
    var toolBarView:UIView!
    
    var MCCVC:MainCameraCollectionViewCellViewController!
    //remove if bad idea
    var albumType:photoAlbumSender?
    var cameraType:cameraSender?
    
    var numberOfItemsInSection = 3
    var insertFeedScreen:Bool = false
    
    var currentPage:Int = 0
    
    var userLocation:GeoPoint!
    
    weak var searchFeedScreenLocationDelegate:SearchFeedScreenLocationDelegate!
    
    
    var selectedUser:User?{
        didSet{
            print("selectedUser set: \(selectedUser?.userID)")
        }
    }
    var selectedTag:Tag?{
        didSet{
            print("selectedTag set: \(selectedTag?.tagID)")
        }
    }
    var cellTapped:Bool = false
    var selectedUsers:[User] = [User]()
    var selectedUsersDict:[IndexPath:User] = [IndexPath:User]()
    var removeCellCount:Int = 0
    
    var shouldRemoveFollowersProfileScreen:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Image post count: \(imagePost.defaultImages.count)")
        // Do any additional setup after loading the view.

        if self.MainCollectionView == nil{
            print("The main collection view is empty")
            assertionFailure("The main collectionView is empty")
        }
        MainCollectionView.delegate = self
        MainCollectionView.dataSource = self
        MainCollectionView?.backgroundColor = colors.backgroundColor
        MainCollectionView?.isPagingEnabled = true
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        MainCollectionView?.collectionViewLayout = layout
        //MainCollectionView.contentInset = UIEdgeInsets(top: titleToolBarView.frame.height, left: 0, bottom: 0, right: 0)
        MainCollectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.MainCollectionView.register(MainSearchFeedScreenCell.self, forCellWithReuseIdentifier: "SearchFeedScreenCell")
        self.MainCollectionView.register(MainProfileScreenCell.self, forCellWithReuseIdentifier: "ProfileScreenCell")
        self.MainCollectionView.register(FollowersProfileScreenCell.self, forCellWithReuseIdentifier: "FollowersProfileScreenCell")
        //Be Sure to delete
        self.MainCollectionView.register(blankButtonCell.self, forCellWithReuseIdentifier: "blankButtonCell")
        
    self.MainCollectionView.register(MainCameraCollectionViewCell3.self, forCellWithReuseIdentifier: "CameraCell2")
        //self.MainCollectionView.register(MainCameraCollectionViewCell.self, forCellWithReuseIdentifier: "CameraCell")
        
     //Just added in
//    self.MainCollectionView.register(MainFeedCollectionViewCell.self, forCellWithReuseIdentifier: "MainFeedCollectionViewCell")
        
        print("sure;y I was called")
        
        //New stuff
        addChild(VC)
        VC.view.translatesAutoresizingMaskIntoConstraints = false
        self.slidingView.addSubview(VC.view)
        self.slidingView.didAddSubview(VC.view)
        VC.didMove(toParent: self)
        self.view.backgroundColor = UIColor.gray
        
        self.view.addSubview(self.slidingView)
        //self.view.addSubview(self.tapToSlide)
        
        slidingPanGesture =  UIPanGestureRecognizer(target: self , action: #selector(slidingViewPanGesture(_:)))
        mainViewPanGesture = UIPanGestureRecognizer(target: self, action: #selector(mainViewPanGesture(_:)))
        // tapToSlide.addTarget(self, action: #selector(tapToSlideTapped(_:)), for: .touchUpInside)
        self.slidingView.addGestureRecognizer(slidingPanGesture)
        self.view.addGestureRecognizer(mainViewPanGesture)
        setUpLayout()
        
        slidingView.bringSubviewToFront(VC.view)
        VC.previewSegueDelegate = self
        //
        
        let photos = PHPhotoLibrary.authorizationStatus()
        if photos == .notDetermined {
            PHPhotoLibrary.requestAuthorization({status in
                if status == .authorized{
                    print("permission granted")
                    DispatchQueue.main.async {
                        //self.VC.fetchAssets()
                        self.VC.collectionView.reloadData()
                    }
                } else {
                    print("permission not granted")
                }
            })
        }
        
        self.slidingView.backgroundColor = .clear
        self.view.backgroundColor = .cyan
        
        //        let removeButton = UIButton(frame: CGRect(x: 0 , y: 0, width: self.view.frame.width/2, height: self.view.frame.width/2))
        //
        //        self.MainCollectionView.addSubview(removeButton)
        //        MainCollectionView.bringSubview(toFront: removeButton)
        //
        //
        //        removeButton.backgroundColor = .green
        //
        //        removeButton.translatesAutoresizingMaskIntoConstraints = false
        //        removeButton.centerYAnchor.constraint(equalTo: MainCollectionView.centerYAnchor).isActive = true
        //        removeButton.centerXAnchor.constraint(equalTo: MainCollectionView.centerXAnchor).isActive = true
        //        removeButton.heightAnchor.constraint(equalToConstant: 300).isActive = true
        //        removeButton.widthAnchor.constraint(equalToConstant: 300).isActive = true
        //
        //        removeButton.addTarget(self, action: #selector(removeButtonTapped), for: .touchUpInside)
        //
        
        searchFeedScreenViewController = SearchFeedScreenViewController()
//        profileScreenSegmentedViewController  = segmentedViewController()
       // followersProfileScreenSegmentedViewController = segmentedViewController()
        
        createToolBar()
        
        //addLine(fromPoint: CGPoint(x:toolBarHorrizontalStackView.frame.minX,y:self.view.bounds.height - 110), toPoint: CGPoint(x:toolBarHorrizontalStackView.frame.maxX,y:self.view.bounds.height - 110))
        
        MCCVC = MainCameraCollectionViewCellViewController()
        
        self.appDelegate.locationDelegate = self
        self.searchFeedScreenLocationDelegate = searchFeedScreenViewController
        self.MainCollectionView.prefetchDataSource = self
    }
    
    func createToolBar(){
        //Set Up The Tool Bar
        toolBarView = UIView(frame: CGRect(x: 16, y: 0, width: self.view.bounds.width - 32, height: 50))
        //toolBarView.backgroundColor = UIColor(red: 56/255, green: 217/255, blue: 169/255, alpha: 0.10)
        //toolBarView.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.10)
        
        
        
        self.view.addSubview(toolBarView)
        
        
        toolBarView.translatesAutoresizingMaskIntoConstraints = false
        toolBarView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        toolBarView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        toolBarView.heightAnchor.constraint(equalToConstant: 75).isActive = true
        toolBarView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        
        //Create The Buttons
        slideToFeedScreenButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        slideToFeedScreenButton.setImage(#imageLiteral(resourceName: "icons8-america-100"), for: .normal)
//        slideToFeedScreenButton.addTarget(self, action: #selector(slideToFeedScreen(_:)), for: .touchUpInside)
        
        slideToCameraPhotoAlbumButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        slideToCameraPhotoAlbumButton.setImage(#imageLiteral(resourceName: "icons8-unsplash-500"), for: .normal)
        slideToCameraPhotoAlbumButton.addTarget(self, action: #selector(showPhotoAlbum(_:)), for: .touchUpInside)
        
        slideToProfileScreenButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        slideToProfileScreenButton.setImage(#imageLiteral(resourceName: "icons8-thumbnails-96"), for: .normal)
//        slideToProfileScreenButton.addTarget(self, action: #selector(slideToProfileScreenTapped(_:)), for: .touchUpInside )
        
        slideToCameraPhotoAlbumButton.translatesAutoresizingMaskIntoConstraints = false
        slideToCameraPhotoAlbumButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        slideToCameraPhotoAlbumButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        slideToProfileScreenButton.translatesAutoresizingMaskIntoConstraints = false
        slideToProfileScreenButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        slideToProfileScreenButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        slideToFeedScreenButton.translatesAutoresizingMaskIntoConstraints = false
        slideToFeedScreenButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        slideToFeedScreenButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        //Set Up The Horrizontal View
        toolBarHorrizontalStackView = UIStackView(arrangedSubviews: [slideToFeedScreenButton,slideToCameraPhotoAlbumButton,slideToProfileScreenButton])
        toolBarHorrizontalStackView.frame = CGRect(x: 16, y: 0, width: self.view.bounds.width - 32, height: 50)
        toolBarHorrizontalStackView.axis = .horizontal
        toolBarHorrizontalStackView.distribution = .equalSpacing
        toolBarHorrizontalStackView.alignment = .fill
        toolBarHorrizontalStackView.spacing = 0
        
        self.view.addSubview(toolBarHorrizontalStackView)
        
        toolBarHorrizontalStackView.translatesAutoresizingMaskIntoConstraints = false
        toolBarHorrizontalStackView.leadingAnchor.constraint(equalTo: toolBarView.leadingAnchor, constant: 30).isActive = true
        toolBarHorrizontalStackView.topAnchor.constraint(equalTo: toolBarView.topAnchor, constant: 5).isActive = true
        toolBarHorrizontalStackView.trailingAnchor.constraint(equalTo: toolBarView.trailingAnchor, constant: -30).isActive = true
        //toolBarHorrizontalStackView.bottomAnchor.constraint(equalTo: toolBarView.bottomAnchor, constant: -50).isActive = true
        toolBarHorrizontalStackView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        
        //        let gradient = CAGradientLayer()
        //
        //        gradient.frame = CGRect(x: 0, y: toolBarHorrizontalStackView.frame.minY, width: self.view.frame.width, height: 75)
        //
        //        let greyColor = UIColor(hexString: "#f2f0ee")
        //        gradient.colors = [UIColor.clear.cgColor.copy(alpha: 0), greyColor.cgColor.copy(alpha: 0.5)]
        //
        ////        toolBarView.layer.borderColor = UIColor.red.cgColor
        ////        toolBarView.layer.borderWidth = 2
        //
        //
        //       // toolBarView.layer.insertSublayer(gradient, below: self.toolBarView.layer)
        //        toolBarView.layer.insertSublayer(gradient, at: 0)
        //        let layer = toolBarView.layer
        //        toolBarAddLine(fromPoint: CGPoint(x: 30, y: self.view.frame.maxY - 80), toPoint: CGPoint(x: self.view.frame.maxX - 30, y: self.view.frame.maxY - 80),layer)
        
        let layer = toolBarView.layer
        toolBarAddLine(fromPoint: CGPoint(x: 30, y: self.view.frame.maxY - 80), toPoint: CGPoint(x: self.view.frame.maxX - 30, y: self.view.frame.maxY - 80),layer)
        
        setGradientColors(currentCell: nil)
    }
    
    func setGradientColors(currentCell sender:CellTypes? = nil){
        let gradient = CAGradientLayer()
        
        gradient.frame = CGRect(x: 0, y: toolBarHorrizontalStackView.frame.minY, width: self.view.frame.width, height: 75)
        
        let greyColor = UIColor(hexString: "#f2f0ee")
        gradient.colors = [UIColor.clear.cgColor.copy(alpha: 0), greyColor.cgColor.copy(alpha: 0.5)]
        toolBarView.layer.insertSublayer(gradient, at: 0)
        //          let layer = toolBarView.layer
        
        
    }
    
    
    @objc func removeButtonTapped(sender:Any?,cellType:Any?){
        print("Remove Button Tapped does it work")
        
//        guard let cellType = cellType as? CellTypes else{
//            print("ERRRORRR Remove Button Tapped Went Wrong")
//            return
//        }
//
//        if (sender as? SearchFeedScreenViewController) != nil && cellType == CellTypes.MainFeedCollectionViewCell{
//            self.removeCell(sender: searchFeedScreenViewController, cellType: cellType)
//            self.searchFeedScreenViewController.collectionView.isUserInteractionEnabled = true
//        }
//        else if sender as? segmentedViewController != nil && cellType == CellTypes.MainFeedCollectionViewCell{
//            print("Remove button sender: segmentedViewController")
//            self.removeCell(sender: profileScreenSegmentedViewController,cellType: cellType)
//            self.profileScreenSegmentedViewController.collectionView.isUserInteractionEnabled = true
//        }
//
//        else if cellType == CellTypes.FollowersProfileScreenCell {
//            print("Remove main Profile screen")
//            if sender as? SearchFeedScreenViewController != nil{
//                self.removeCell(sender: searchFeedScreenViewController, cellType: cellType)
//                self.profileScreenSegmentedViewController.collectionView.isUserInteractionEnabled = true
//            }
//        }
        print("We got nowhere inside of the remove Button tapped")
        //self.profileScreenSegmentedViewController.collectionView.isUserInteractionEnabled = true
    }
    
    
    
    
    var previousPageSwipe:Int = 0
    var gradientLayer:CGLayer!
    
    var counterInScrollView:Int = 0
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let x:CGFloat = scrollView.contentOffset.x
        let w:CGFloat = scrollView.bounds.size.width
        //self.currentPage = Int(ceil(x/w))
        print("contentSize.width: \(scrollView.contentSize.width)")
        print("scrollView.frame: \(scrollView.frame.width)")
        //self.currentPage = Int(round(scrollView.contentOffset.x/scrollView.contentSize.width))
        self.currentPage = Int(scrollView.contentOffset.x/scrollView.frame.size.width)
        slideButtonTapped = false
        print("Cell count: \(collectionViewManager.cells.count)")
        print("In scrollViewDidEndDecelerating The current page \(currentPage)")
        
        for cell in collectionViewManager.cells{
            print("Cells: \(cell.type)")
        }
        
        let currentCell:CellModel = collectionViewManager.cells[currentPage]
        
        var nextCell:CellModel? //= collectionViewManager.cells[currentPage + 1]
        var previousCell:CellModel?// = collectionViewManager.cells[currentPage - 1]
       
        if(currentPage + 1 >= collectionViewManager.cells.count){
            nextCell = nil
        }else{
            nextCell = collectionViewManager.cells[currentPage + 1]
        }
        
        if (currentPage - 1 < 0){
           previousCell = nil
        }else{
           previousCell = collectionViewManager.cells[currentPage - 1]
        }
        
        var scrollDirection:ScrollDirection!

        if previousPageSwipe >= currentPage{
            scrollDirection = .left
        }else{
            scrollDirection = .right
        }

        print("previous page: \(previousPageSwipe), currentPage: \(currentPage), scrollDirection: \(scrollDirection)")
        
        print("current cell type: \(collectionViewManager.cells[currentPage].type)")
        print("next cell type: \(nextCell?.type)")
        print("previous cell type: \(previousCell?.type)")
        
        if(currentCell.type == .MainCameraCollectionViewCell && previousCell?.type != .MainSearchFeedScreenCell){
            print("should remove should be called: current cell MainCameraCollectionViewCell previous MainSearchFeedScreenCell")
            //remove page one after
//            if self.shouldRemoveFollowersProfileScreen == true{
//                removeButtonTapped(sender: searchFeedScreenViewController, cellType: CellTypes.FollowersProfileScreenCell)
//                self.shouldRemoveFollowersProfileScreen = false
//            }else{
//                removeButtonTapped(sender:searchFeedScreenViewController,cellType: CellTypes.MainFeedCollectionViewCell)
//            }
            self.removeAllExcessCells(scrollDirection: scrollDirection,currentPage:currentPage)
        }else if currentCell.type == .MainCameraCollectionViewCell && previousCell?.type == .FollowersProfileScreenCell{
            print("should remove should be called: current cell MainCameraCollectionViewCell previous FollowersProfileScreenCell")
            self.removeAllExcessCells(scrollDirection: scrollDirection, currentPage: currentPage)
        }else if currentCell.type == .MainSearchFeedScreenCell && nextCell?.type != .MainCameraCollectionViewCell{
                        print("should remove should be called: current cell MainSearchFeedScreenCell next MainCameraCollectionViewCell")
            self.removeAllExcessCells(scrollDirection: scrollDirection,currentPage:currentPage)
        }else if currentCell.type == .MainSearchFeedScreenCell && nextCell?.type == .FollowersProfileScreenCell{
              print("should remove should be called: current cell MainSearchFeedScreenCell next FollowersProfileScreenCell")
            self.removeAllExcessCells(scrollDirection: scrollDirection,currentPage:currentPage)
        }else if currentCell.type == .MainProfileScreenCell && nextCell?.type == .FollowersProfileScreenCell{
                //print("removed all excess cells called")
                print("should remove should be called: current cell MainProfileScreenCell next FollowersProfileScreenCell")
            self.removeAllExcessCells(at:currentPage, scrollDirection: scrollDirection,currentPage: currentPage)
        }else if currentCell.type == .MainProfileScreenCell && nextCell?.type == CellTypes.MainFeedCollectionViewCell{
            print("should remove should be called: current cell MainProfileScreenCell next MainFeedCollectionViewCell")
            self.removeAllExcessCells(scrollDirection: scrollDirection, currentPage: currentPage)
        }else if currentCell.type == .FollowersProfileScreenCell && nextCell?.type == .FollowersProfileScreenCell{
                //removes followers cells greater than or equal to this point
                print("should remove should be called: current cell FollowersProfileScreenCell next FollowersProfileScreenCell")
            self.removeAllExcessCells(at: currentPage, scrollDirection: scrollDirection, currentPage: currentPage)
        }else if currentCell.type == .FollowersProfileScreenCell && nextCell?.type == .MainFeedCollectionViewCell{
             print("should remove should be called: current cell FollowersProfileScreenCell next MainFeedCollectionViewCell")
            self.removeAllExcessCells(at: currentPage, scrollDirection: scrollDirection, currentPage: currentPage)
        }
        previousPageSwipe = currentPage
        
//        if ((MainCollectionView.cellForItem(at: IndexPath(item:2,section:0)) as? MainCameraCollectionViewCell3) != nil) && currentPage == 1{
//            print("About to remove cell from scrollViewDidEndDecelerating")
//            if self.shouldRemoveFollowersProfileScreen == true{
//                removeButtonTapped(sender: searchFeedScreenViewController, cellType: CellTypes.FollowersProfileScreenCell)
//                self.shouldRemoveFollowersProfileScreen = false
//            }else{
//            removeButtonTapped(sender:searchFeedScreenViewController,cellType: CellTypes.MainFeedCollectionViewCell)
//            }
//       //     self.MainCollectionView.scrollToItem(at: IndexPath(item: 1, section: 0), at: .left, animated: false)
//        }
//        else if currentPage == 0 && collectionViewManager.cells.count == 4{
//            removeButtonTapped(sender:searchFeedScreenViewController, cellType: CellTypes.MainFeedCollectionViewCell)
//            self.MainCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: false)
//        }
//        else if currentPage == 2 && collectionViewManager.cells.count == 4{
//            print("Decelerating count 4 page 3")
//            removeCell(sender: profileScreenSegmentedViewController, cellType: CellTypes.MainProfileScreenCell)
//            self.MainCollectionView.scrollToItem(at: IndexPath(item: 2, section: 0), at: .left, animated: true)
//        }
//        else if currentPage >= 3 && currentPage < collectionViewManager.cells.count - 1{
//            self.counterInScrollView += 1
//            print("Counter in scrollView: \(self.counterInScrollView)")
//            print("\n")
//            print("\tDeleting page: \(currentPage)")
//            print("\tIn currentPage == collectionViewManager.cells.count")
//            print("\n")
//            self.cellTapped = false
//            print("Current page: \(currentPage)")
//            print("collectionViewManager.cell.count: \(collectionViewManager.cells.count - 1)")
//           var counter = 0
//            while(currentPage < collectionViewManager.cells.count - 1 && collectionViewManager.cells.count > 3){
//              counter += 1
//                print("Times called: \(counter)")
//            removeCell(sender: followersProfileScreenSegmentedViewController, cellType: CellTypes.FollowersProfileScreenCell)
//            }
//        }
//        print("Remove excess cells about to be called")
//        self.removeExcessCells()
    }
    
    
    
    
    
    
    ///
    var slidingView = { () -> UIView in
        let view = UIView()
        // view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .blue
        return view
    }()
    
    //    var tapToSlide:UIButton = {
    //        let btn = UIButton()
    //        btn.translatesAutoresizingMaskIntoConstraints = false
    //        btn.backgroundColor = .orange
    //        btn.setTitle("Tap To Slide", for: .normal)
    //        return btn
    //    }()
    
    
    
    var slidingViewBottomConstraint:NSLayoutConstraint!
    
    var slideUP = true
    var firstSlide = true
    var centerSliding:CGPoint!
    var counter:Int = 0
    var slidingPanGesture: UIPanGestureRecognizer!
    var mainViewPanGesture:UIPanGestureRecognizer!
    var originalCenter:CGPoint!
    lazy var VC = PhotoAlbumViewController()
    
    @IBOutlet weak var titleToolBarView: UIView!
    func setUpLayout(){
        //Title Tool Bar
        titleToolBarView.translatesAutoresizingMaskIntoConstraints = false
        titleToolBarView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        titleToolBarView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        titleToolBarView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        titleToolBarView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        //        //containerView
        //        containerView.translatesAutoresizingMaskIntoConstraints = false
        //        containerView.topAnchor.constraint(equalTo: self.titleToolBarView.bottomAnchor, constant: 0).isActive = true
        //        containerView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        //        containerView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        //        containerView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        
        //CollectionView-MainCollectionView
        MainCollectionView.translatesAutoresizingMaskIntoConstraints = false
        // MainCollectionView.bottomAnchor.constraint(equalTo: self.containerView.bottomAnchor, constant: 0).isActive = true
        MainCollectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        MainCollectionView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        MainCollectionView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        MainCollectionView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        
        //slidingView
        slidingView.translatesAutoresizingMaskIntoConstraints = false
        // slidingViewBottomConstraint = slidingView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant:self.view.bounds.height)
        slidingViewBottomConstraint = slidingView.topAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0)
        slidingViewBottomConstraint.isActive = true
        slidingView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        slidingView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        //   slidingView.heightAnchor.constraint(equalToConstant: (7/8)*UIScreen.main.bounds.maxY).isActive = true
        //    slidingView.topAnchor.constraint(equalTo: titleToolBarView.bottomAnchor, constant: 0).isActive = true
        slidingView.heightAnchor.constraint(equalToConstant: self.view.frame.height).isActive = true
        
        //button
        //        tapToSlide.leadingAnchor.constraint(equalTo: self.view.layoutMarginsGuide.leadingAnchor, constant: 0).isActive = true
        //        tapToSlide.bottomAnchor.constraint(equalTo: self.view.layoutMarginsGuide.bottomAnchor, constant: -20).isActive = true
        //        tapToSlide.widthAnchor.constraint(equalToConstant: 120).isActive = true
        //        tapToSlide.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        //collectionView for the photo album
        self.VC.view.leadingAnchor.constraint(equalTo: slidingView.leadingAnchor, constant: 0).isActive = true
        self.VC.view.trailingAnchor.constraint(equalTo: slidingView.trailingAnchor, constant: 0).isActive = true
        self.VC.view.bottomAnchor.constraint(equalTo: slidingView.bottomAnchor, constant: 0).isActive = true
        self.VC.view.topAnchor.constraint(equalTo: slidingView.topAnchor, constant: 0).isActive = true
    }
    
    @objc func slidingViewPanGesture(_ sender:UIPanGestureRecognizer){
        let translation = sender.translation(in: self.view)
        if sender.state == UIGestureRecognizer.State.began{
            originalCenter = self.slidingView.center
        }
        
        if translation.y >= 0{
            self.slidingView.center = CGPoint(x: self.slidingView.center.x, y: self.slidingView.center.y + translation.y)
            sender.setTranslation(CGPoint.zero, in: self.view)
        }
        
        if slidingPanGesture.state == UIGestureRecognizer.State.ended{
            if translation.y == 0{
                UIView.animate(withDuration: 1.0, animations: {
                    self.slidingView.transform = CGAffineTransform(translationX: 0, y: self.slidingView.frame.height)
                }, completion: nil)
                self.slideUP = !self.slideUP
            }
            else{
                UIView.animate(withDuration: 0.5, animations: {
                    self.slidingView.center = self.centerSliding
                }, completion: nil)
            }
        }
    }
    
    //    @objc func tapToSlideTapped(_ sender:Any?){
    
    
    
    @objc func showPhotoAlbum(_ sender:Any?){
        print ("This is correct function")
        //check if button works from screen
        for cell in MainCollectionView.visibleCells{
            if cell as? MainCameraCollectionViewCell3 == nil{
                self.MainCollectionView.scrollToItem(at: IndexPath(item: 1, section: 0), at: .left, animated: true)
                //return
              break
            }
        }
        
        //        if isInCameraScreen == false{
        //            self.MainCollectionView.scrollToItem(at: IndexPath(item: 1, section: 0), at: .left, animated: true)
        //            isInCameraScreen = true
        //            return
        //        }
        
        if (sender as? FollowersProfileScreenCell) != nil{
            print("the sender was a segmented View controller")
            albumType = photoAlbumSender.profileImage
        }
        else{
            albumType = photoAlbumSender.feedPostImage
        }
        
        if firstSlide{
            centerSliding = self.slidingView.center
            firstSlide = false
        }
        else{
            self.slidingView.center = centerSliding
        }
        if(slideUP){
            
            UIView.animate(withDuration: 1.0, animations: {
                self.slidingView.transform = CGAffineTransform(translationX: 0, y: -self.slidingView.frame.height)
                
            }, completion: nil)
        }
        else{
            UIView.animate(withDuration: 1.0, animations: {
                self.slidingView.transform = CGAffineTransform.identity
                //self.slidingView.transform  = CGAffineTransform(translationX: 0, y: (7/8)*UIScreen.main.bounds.height + 50)
            }, completion: nil)
        }
        slideUP = !slideUP
    }
    
    @objc func mainViewPanGesture(_ sender:UIPanGestureRecognizer){
        
        if MainCollectionView.visibleCells[0] as? MainCameraCollectionViewCell3 != nil{
        if sender.state == UIGestureRecognizer.State.ended{
            if firstSlide{
                centerSliding = self.slidingView.center
                firstSlide = false
            }
            else{
                self.slidingView.center = centerSliding
            }
            UIView.animate(withDuration: 1.0, animations: {
                self.slidingView.transform = CGAffineTransform(translationX: 0, y: -UIScreen.main.bounds.height)
            }, completion: nil)
            slideUP = !slideUP
        }
        }
    }
    
    ///
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        print("memory warning recieved")
        User.followersCache.removeAllObjects()
//        User.followingCache.removeAllObjects()
//        User.allFollowingCache.removeAllObjects()
        User.PostsCache.removeAllObjects()
        User.UsersCache.removeAllObjects()
        Tag.tagsCache.removeAllObjects()
        Post.cache.removeAllObjects()
        SDImageCache.shared().clearMemory()
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionViewManager.cells.count
    }
    
    
    var isInCameraScreen:Bool = false
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let item = collectionViewManager.cells[indexPath.item]
        
        print("\n")
        for item in collectionViewManager.cells{
            print(item.type)
        }
        print("\n")
        
        
        switch item.type {
        case .MainSearchFeedScreenCell:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchFeedScreenCell", for: indexPath) as! MainSearchFeedScreenCell
            // insert delegate here
            print("Main collectionvIew userlocation: \(self.userLocation)")
            searchFeedScreenViewController.userLocation = self.userLocation
            searchFeedScreenViewController.addOrRemoveCellDelegate = self
            if let searchbar = searchFeedScreenViewController.feedSearchBar, let text = searchbar.text, !text.isEmpty {
                searchFeedScreenViewController.placeholder.isHidden = true
                print("text is not empty: \(text)")
            }
            self.addChild(searchFeedScreenViewController)
            cell.containerView.addSubview(searchFeedScreenViewController.view)
            searchFeedScreenViewController.didMove(toParent: self)
            return cell
            
        case .MainCameraCollectionViewCell:
            
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CameraCell", for: indexPath) as! MainCameraCollectionViewCell
//            cell.backgroundColor = .black
//            cell.gdelegate = self
//            cell.pdelegate = self
//            if cameraType == cameraSender.profileImage{
//                print("Camera flipped to front")
//                cell.usingFrontCamera = false
//                cell.FlipThe_camera(nil)
//            }
//
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CameraCell2", for: indexPath) as! MainCameraCollectionViewCell3
           // cell.viewWillAppear(true)
          //  cell.viewWillDisappear(true)
            
            MCCVC.pdelegate = self
            self.addChild(MCCVC)
            cell.containerView.addSubview(MCCVC.view)
            MCCVC.didMove(toParent: self)
            
            cell.backgroundColor = UIColor.black
          if cameraType == cameraSender.profileImage{
                                print("Camera flipped to front")
                                MCCVC.usingFrontCamera = false
                        MCCVC.changeCamera(nil)
                            }
            
            
            return cell
            
            //        else if indexPath.item == 1{
            //            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainCameraCollectionViewCell2", for: indexPath) as! MainCameraCollectionViewCell2
            //            cell.backgroundColor = .black
            //            return cell
            //        }
        //index path is 2
        case .MainProfileScreenCell:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileScreenCell", for: indexPath) as! MainProfileScreenCell
            print("Index Path: \(indexPath.item)")
            print("DQD prolife cell")
       //     self.selectedUsers = []
            // insert delegate here
            cell.setUpCollectionView()
            cell.labelTitle = User.signedInUser?.username ?? "username"
//            cell.alertPhotoAlbumDelegate = self
//            cell.alertCameraAlbumDelegate = self
            cell.addOrRemoveCellDelegate = self
            cell.profilePostPassDelegate = self
            cell.selectedCellDelegate = self
//            cell.addViewDelegate = self
            cell.alertControllerDelegate = self
            cell.menuTableViewDelegate = self

            cell.backgroundColor = colors.backgroundColor
//            profileScreenSegmentedViewController.getUsernameProfilePicture()
//            profileScreenSegmentedViewController.getFollowing()
            
            return cell
            
//        case .MainFeedCollectionViewCell:
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainFeedCollectionViewCell", for: indexPath) as! MainFeedCollectionViewCell
//            cell.reloadSegmentedViewControllerDelegate = self
//            //
//            print("selected tag ID in Main View Controller: \(self.searchFeedScreenViewController.selectedTag?.tagID)")
//            cell.posts.removeAll()
////            if let tag = self.searchFeedScreenViewController.selectedTag {
////                //cell.selectedTag = tag
////                cell.tagID = tag.tagID
////                cell.tagName = tag.name
////                namePass = tag.name
////                idPass = tag.tagID
////            }
////            else if let tag = self.profileScreenSegmentedViewController.selectedTag{
////                cell.tagID = tag.tagID
////                cell.tagName = tag.name
////                namePass = tag.name
////                idPass = tag.tagID
////            }
////            else{
////                if let selectedTag = selectedTag{
////                    cell.tagID = selectedTag.tagID
////                    cell.tagName = selectedTag.name
////                    namePass = selectedTag.name
////                    idPass = selectedTag.name
////                }
////                    else{
////                print("Tag was nil")
////                    }
////
////            }
//
//
//
//            cell.delegate = self as CollectionCellDelegate
//            cell.viewDelegate = self
//            //cell.FeedCollectionView.reloadData()
//            return cell
//
            
        case .FollowersProfileScreenCell:
            print("Index Path in cellForItem: \(indexPath)")
            print("In Friends Profile Screen Cell: \(indexPath.item - 3)")
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FollowersProfileScreenCell", for: indexPath) as! FollowersProfileScreenCell
            //            friendsProfileScreenSegmentedViewController.labelTitle = "friends label title \(indexPath.item)"
            //            friendsProfileScreenSegmentedViewController.alertPhotoAlbumDelegate = self
            //            friendsProfileScreenSegmentedViewController.alertCameraAlbumDelegate = self
            //            friendsProfileScreenSegmentedViewController.addOrRemoveCellDelegate = self
            //            self.addChildViewController(friendsProfileScreenSegmentedViewController)
            //            cell.containerView.addSubview(friendsProfileScreenSegmentedViewController.view)
            //            friendsProfileScreenSegmentedViewController.didMove(toParentViewController: self)
            cell.setUpCollectionView()
            cell.collectionView.isUserInteractionEnabled = true
//            cell.addOrRemoveCellDelegate = self
//            cell.addViewDelegate = self
//            cell.profilePostPassDelegate = self
            cell.alertControllerDelegate = self
            cell.selectedCellDelegate = self
            cell.menuTableViewDelegate = self
            
//            cell.segementedViewController.collectionView.isUserInteractionEnabled = true
//            cell.segementedViewController.addOrRemoveCellDelegate = self
//            cell.addViewDelegate = self
//            cell.segementedViewController.profilePostPassDelegate = self
//            cell.segementedViewController.selectedCellDelegate = self
            //may cause error later on
//            if selectedUsers.isEmpty{
//                print("Selected Users is empty")
//            }
//
////            if self.cellTapped == false && !self.selectedUsers.isEmpty{
////                print("selectedUsers in cellForItemAt: \(self.selectedUsers)")
////                print("indexPath.item-3: \(indexPath.item - 3)")
////                self.selectedUser = self.selectedUsers[indexPath.item - 3]
////                }
//            self.selectedUser = self.selectedUsersDict[indexPath]
//
//            print("Selected User: \(self.selectedUser?.userID)")
//            print("Selected Tag: \(self.selectedTag)")
//
//
//
//            if let user = self.selectedUser{
//                cell.userID = user.userID
//                cell.segementedViewController.userID = user.userID
////                cell.segementedViewController.getUsernameProfilePicture()
////                cell.segementedViewController.getFollowing()
////                cell.segementedViewController.collectionView.reloadData()
//                print("cell userID: \(cell.userID)")
//            }else{
//                print("Unable to parse userID in followers cell")
//            }
            
            
            // cell.segementedViewController.labelTitle = "Friends username: \(indexPath.item)"
            
            //            let label = UILabel(frame: CGRect(x: 0, y: 200, width: 200, height: 200))
            //            cell.addSubview(label)
            //            label.backgroundColor = .orange
            //            label.text = "\(indexPath.item)"
            //            print("\(indexPath.item)")
            //            cell.setTextLabelTitle = "Friends username: \(indexPath.item)"
            //            label.textAlignment = .center
            //
            //            label.translatesAutoresizingMaskIntoConstraints = false
            //            label.centerXAnchor.constraint(equalTo: cell.centerXAnchor).isActive = true
            //            label.centerYAnchor.constraint(equalTo: cell.centerYAnchor).isActive = true
            //            label.widthAnchor.constraint(equalToConstant: 200).isActive = true
            //            label.heightAnchor.constraint(equalToConstant: 200).isActive = true
            
            cell.backgroundColor = colors.backgroundColor
            
            return cell
            
        default:
            print("Default reached check code")
            let cell = UICollectionViewCell()
            return cell
        }
    }
    
    
    func setProfileImage(image:UIImage){
        //let profileScreen = self.MainCollectionView.cellForItem(at: IndexPath(item:3,section:0)) as! MainProfileScreenCell
        //profileScreen.segementedViewController.userProfileImage.image = image
        
        guard let userID = User.signedInUserID else{
            print("no signed in userID")
            return
        }
        
        //come back and adjust compression
        let downloadURL:URL
//        guard let data = image.jpegData(compressionQuality: 0.8) else{
//            return print("Error setting profile picture")
//        }
        let data = image.compressDataFileSize()
        let meta = StorageMetadata()
        meta.contentType = "image/jpeg"
        let storageRef:StorageReference = Storage.storage().reference(withPath: "Profile Pictures/\(userID).jpeg")
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        
        storageRef.putData(data, metadata: meta, completion: {(metaData,error) in
                    if let error = error{
                        print("An error occured while uploading image: \(error)")
                        return
                    }
//                    guard let metadata = metaData, let ref = metadata.storageReference else{
//                        print("Error getting metadata from image upload")
//                        return
//                    }
            
            print("Storageref just uploaded: \(metaData?.path)")
            dispatchGroup.leave()

//            self.profileScreenSegmentedViewController.getUsernameProfilePicture()
        })
        
        dispatchGroup.enter()
        
        storageRef.downloadURL(completion: {(url,error) in
            if let error = error {
                print("An error occurred while getting the download URL: \(error)")
            }
            guard let url = url else{
                print("Error getting downlaod url")
                return
            }
            User.updateProfilePictureInCache(imageURL: url.absoluteString)
            
            let userID = User.signedInUserID!
            Database.database().reference(withPath: "Users/\(userID)").updateChildValues(["photoURL":url.absoluteString], withCompletionBlock: {(error,reference) in
                if let error = error{
                    print("There was an error while updating the photoURL in the database: \(error)")
                }
                print("Update a success: \(url)")
                dispatchGroup.leave()
            })
        })
        
        dispatchGroup.notify(queue: .main, execute: { [weak self] in
            print("End of dispatch group completed")
            guard let self = self else{
                assert(true, "Self was nil")
                print("self was nil")
                return
            }
            for i in 0..<self.collectionViewManager.cells.count{
                if self.collectionViewManager.cells[i].type == CellTypes.MainProfileScreenCell{
                    if let cell =  self.MainCollectionView.cellForItem(at: IndexPath(item: i, section: 0)) as? MainProfileScreenCell{
                        cell.getUsernameProfilePicture()
                        cell.userProfileImage.image = image
                    }
                }
            }
//
//            self.profileScreenSegmentedViewController.getUsernameProfilePicture()
//             self.profileScreenSegmentedViewController.userProfileImage.image = image
        })
        //self.profileScreenSegmentedViewController.userProfileImage.image = image
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        for cell in MainCollectionView.visibleCells{
            if let cell = cell as? MainCameraCollectionViewCell3{
                MCCVC.viewWillTransition(to: size, with: coordinator)
            }
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.MainCollectionView.frame.width, height: self.MainCollectionView.frame.height)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    var indexPass:IndexPath = IndexPath()
    
    func selectedItem(index:IndexPath,senderCellType:CellTypes,name:String,id:String) {
        tableViewSenderType = senderCellType
        indexPass = index
        namePass = name
        idPass = id
        performSegue(withIdentifier: "tableViewSegue", sender: self)
    }
    
    var tableViewSenderType:CellTypes!
    var namePass:String!
    var idPass:String!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "tableViewSegue"{
            // let tableView = segue.destination as! TableViewController
            //let tableView = segue.destination as! FeedViewController
            var tableView = segue.destination as! FeedViewController
            print("index to be passed from prepare: \(indexPass.item)")
            tableView.tag = self.searchFeedScreenViewController.selectedTag
            tableView.id = idPass
            tableView.name = namePass
            tableView.indexPathPass = indexPass
            tableView.senderType = tableViewSenderType
        }
        else if segue.identifier == "showPhotoSegue"{
            print("ready for segue 1")
            let previewVC = segue.destination as! PreviewController
            if let devicePass = devicePass{
                if devicePass.position == AVCaptureDevice.Position.front{
                    self.imagePass = UIImage(cgImage: (self.imagePass.cgImage!), scale: (self.imagePass.scale), orientation: .leftMirrored)
                }
                print("Sent photo from photo album")
            }
            print("ready for segue")
            previewVC.getClosestTags()
            previewVC.reloadSearchFeedScreenDelegate = self
            if let asset = sender as? PHAsset{
                previewVC.asset = asset
            }else{
            previewVC.image = self.imagePass
            }
            if previewVC.image == nil {
                print("no image before segue")
                return
            }
        }
        else if segue.identifier == "ProfilePreviewSegue"{
            print("preparing for ProfilePreviewSegue")
            let profilePreviewVC = segue.destination as! ProfilePreviewViewController
            profilePreviewVC.imagePass = imagePass
        }
        else{
            print("something went wrong in prepareForSegue")
        }
    }
    
    func gestureDelegate() {
        print("tapped")
    }
    var imagePass:UIImage = UIImage()
    var devicePass:AVCaptureDevice?
    var shouldsave:Bool = false
    var assetPass:PHAsset = PHAsset()
//    lazy var imageManager = PHImageManager.default()
    
    
    func previewSegueDelegate(image:UIImage?,device:AVCaptureDevice?, albumTypeSender :photoAlbumSender?,  cameraTypeSender:cameraSender?, asset:PHAsset? = nil) {
        print("inside of previewSegueDrlegate")
       
        guard let image = image else{
            if let asset = asset{
                print("asset was not nil")
                    if(self.albumType == photoAlbumSender.profileImage){
                        let imageOptions = PHImageRequestOptions()
                        imageOptions.deliveryMode = .highQualityFormat
                        PHImageManager.default().requestImage(for: asset, targetSize: UIScreen.main.bounds.size,
                                                  contentMode: .aspectFill, options: imageOptions) { (image, hashable)  in
                                                    if let loadedImage = image {
                                                        DispatchQueue.main.async {
                                                                self.imagePass = loadedImage
                                                                self.performSegue(withIdentifier: "ProfilePreviewSegue", sender: self)
                                                        }
                                        }
                        }
                    }else{
                        performSegue(withIdentifier: "showPhotoSegue", sender: asset)
                    }
                }
            return
        }
        
        self.imagePass = image
        self.devicePass = device
        
        print("album type in previewSegueDelegate: \(albumTypeSender)")
        if self.albumType == photoAlbumSender.profileImage{
            // self.setProfileImage(image: image)
            if shouldsave{
                self.setProfileImage(image: image)
            }
            UIView.animate(withDuration: 1.0, animations: {
                self.slidingView.transform = CGAffineTransform.identity
                //self.slidingView.transform  = CGAffineTransform(translationX: 0, y: (7/8)*UIScreen.main.bounds.height + 50)
            }, completion: nil)
            self.albumType = photoAlbumSender.feedPostImage
            self.cameraType = cameraSender.feedScreenCamera
            performSegue(withIdentifier: "ProfilePreviewSegue", sender: self)
        }
        else if self.cameraType == cameraSender.profileImage{
            // self.setProfileImage(image: image)
            if shouldsave{
                self.setProfileImage(image: image)
            }
            self.MainCollectionView.scrollToItem(at: IndexPath(item: 2, section: 0), at: .left, animated:false)
            print("Im in camera type camera sender profile image")
            self.cameraType = cameraSender.feedScreenCamera
            self.albumType = photoAlbumSender.feedPostImage
            performSegue(withIdentifier: "ProfilePreviewSegue", sender: self)
            
        }
        else{
            print("show photo segue delegate")
            performSegue(withIdentifier: "showPhotoSegue", sender: self)
        }
        
    }
    
    //    func previewSegueDelegate(){
    //        print("preview delegate ran a sucess")
    //    }
    
    var previousPage:CGFloat = 0
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let x:CGFloat = scrollView.contentOffset.x
        let w:CGFloat = scrollView.bounds.size.width
        var currentPage = Int(ceil(x/w))
  //      var inCameraScreen = false
//        print("\n\tprevious page: \(previousPage)\n\tcurrent page: \(currentPage)")
        if previousPage >= x/w && Int(floor(x/w)) == 1 {
//            print("\tIm in the floor: \(Int(floor(x/w)))")
            isInCameraScreen = true
        }
        else if currentPage == 1 && previousPage <= x/w{
            isInCameraScreen = true
        }
        previousPage = x/w
        
//        print("\ncurrent page in scrollViewDidScroll: \(x/w)")
//        print("current amount scrolled: \(scrollView.contentOffset.x)")
//        print("scroll to button tapped: \(slideButtonTapped)")
        if currentPage <= 2{
            let screenWidth = self.view.frame.width
            //print("screenWidth: \(screenWidth)")
            var heightToMoveBy = (self.view.frame.maxY - self.toolBarView.frame.minY)*2
            var fractionMoved:CGFloat = 1 + (scrollView.contentOffset.x - (screenWidth*CGFloat(currentPage)))/screenWidth
//            print("Fraction moved: \(fractionMoved)")
//            print("Amount to Move Down: \(fractionMoved*37.5)")
//            print("Rouneded: \((fractionMoved*10).rounded() )")
            if fractionMoved < 0.5{
                if slideButtonTapped{
                    return
                }
                slideToCameraPhotoAlbumButton.transform = CGAffineTransform(translationX: 0, y: (fractionMoved*heightToMoveBy))
            }
            else if (fractionMoved*10).rounded() == 6 || (fractionMoved*10).rounded() == 4 {
        //        print("fracrtionMoved current page: \((fractionMoved*10).rounded())")
                if isInCameraScreen{
                    self.slideToCameraPhotoAlbumButton.setImage(#imageLiteral(resourceName: "icons8-photo-gallery-100"), for: .normal)
                    toolBarView.layer.isHidden = true
                    isInCameraScreen = !isInCameraScreen
                }
                else{
                    toolBarView.layer.isHidden = false
                    self.slideToCameraPhotoAlbumButton.setImage(#imageLiteral(resourceName: "icons8-unsplash-500"), for: .normal)
                }
            }
            else{
                if slideButtonTapped{
                    return
                }
                slideToCameraPhotoAlbumButton.transform = CGAffineTransform(translationX: 0, y: (1-fractionMoved)*(heightToMoveBy))
            }
            
            
            
            for currentCell in self.MainCollectionView.indexPathsForVisibleItems{
                if currentCell.item == 1{
                    self.MainCollectionView.isDirectionalLockEnabled = false
                }
            }
            
        }
        
    }
    
    @IBAction func unwindToMainView(segue:UIStoryboardSegue){
        print("unwind performed successfully")
    }
    
    //Im not sure if this is important or not
    //Decide if we should implement
    //Change index to 1 when the camera has been fixed
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if let cell = cell as? FollowersProfileScreenCell{
            User.PostsCache.removeAllObjects()
            User.UsersCache.removeAllObjects()
            User.followersCache.removeAllObjects()
            Tag.tagsCache.removeAllObjects()
            Post.cache.removeAllObjects()
            SDImageCache.shared().clearMemory()
            self.removeMenuTableView(menuTableViewController: cell.menuTableView)
//            cell.collectionView.removeFromSuperview()
//            cell.collectionView = nil
            
           
        }
        
        if let cell = cell as? MainProfileScreenCell{
            self.removeMenuTableView(menuTableViewController: cell.menuTableView)
//            cell.collectionView.removeFromSuperview()
//            cell.collectionView = nil
            
        }
        
        if let cell = MainCollectionView.cellForItem(at: indexPath) as? MainCameraCollectionViewCell3{
            MCCVC.viewWillDisappear(true)
        }
        
//        if let cell = cell as? MainFeedCollectionViewCell{
//            print("Set all selected tags to nil")
//            self.searchFeedScreenViewController.selectedTag = nil
//       //     self.profileScreenSegmentedViewController.selectedTag = nil
//            self.selectedTag = nil
//            //self.profileScreenSegmentedViewController.getFollowing()
//            User.PostsCache.removeAllObjects()
//            User.UsersCache.removeAllObjects()
//            User.followersCache.removeAllObjects()
//            Tag.tagsCache.removeAllObjects()
//            Post.cache.removeAllObjects()
//            SDImageCache.shared().clearMemory()
//        }
        
//        if let cell = cell as? FollowersProfileScreenCell{
//            if  let nextCell = self.MainCollectionView.cellForItem(at: IndexPath(item: indexPath.item - 1, section: 0)) as? FollowersProfileScreenCell{
//               nextCell.segementedViewController.getUsernameProfilePicture()
//                nextCell.segementedViewController.getFollowing()
//            }
//            else if let nextCell = self.MainCollectionView.cellForItem(at: IndexPath(item: indexPath.item - 1, section: 0)) as? MainProfileScreenCell{
//              nextCell.segementedViewController.getUsernameProfilePicture()
//              nextCell.segementedViewController.getFollowing()
//            }else{
//                print("next cell never selected")
//            }
//        }
//
//        if let cell = cell as? MainProfileScreenCell{
//            if  let nextCell = self.MainCollectionView.cellForItem(at: IndexPath(item: indexPath.item - 1, section: 0)) as? FollowersProfileScreenCell{
//                nextCell.segementedViewController.getUsernameProfilePicture()
//                nextCell.segementedViewController.getFollowing()
//            }else{
//                print("next cell never selected")
//            }
//        }
//
    
        
//        if indexPath.item ==  0{
//            if let cameraCell = cell as? MainCameraCollectionViewCell{
//                //cameraCell.captureSession.stopRunning()
//                print("Camera turned off")
//            }
//        }
     
        
        //        if indexPath.item == collectionViewManager.cells.count{
        //            //return here to remove view controller
        //        }
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = true // for navigation bar hide
        titleToolBarView.isHidden = true
        navigationController?.isToolbarHidden = true
        navigationController?.isNavigationBarHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        //       UIApplication.shared.statusBarStyle = .lightContent
        //   UIApplication.statusBarBackgroundColor = .blue
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if let cell = cell as? MainSearchFeedScreenCell{
            self.searchFeedScreenViewController.collectionView.isUserInteractionEnabled = true
        }
        
// if let tag = self.selectedTag{
//                print("Selected tag from profile screen: \(tag.tagID)")
//                cell.tagID = tag.tagID
//                cell.tagName = tag.name
//                namePass = tag.name
//                idPass = tag.tagID
//            }
//            else{
//                if let selectedTag = selectedTag{
//                    print("Selected tag from other: \(selectedTag.tagID)")
//                    cell.tagID = selectedTag.tagID
//                    cell.tagName = selectedTag.name
//                    namePass = selectedTag.name
//                    idPass = selectedTag.name
//                }
//                else{
//                    print("Tag was nil")
//                }
//                cell.FeedCollectionView.reloadData()
//            }
//        }
//
//        if let cell = cell as? MainProfileScreenCell{
//            print("will display cell about to show main profile screen cell")
//            self.selectedUsers.removeAll()
//            print("collectionViewManager.cells: \(self.collectionViewManager.cells)")
//            for collectionCell in  self.collectionViewManager.cells{
//                if collectionCell.type == CellTypes.FollowersProfileScreenCell{
//                    print("Followers Profile Screen Cell")
//                }
//            }
//        cell.setUpCollectionView()
//            cell.followButton.isHidden = true
//            cell.collectionView.isUserInteractionEnabled = true
//            cell.getUsernameProfilePicture()
//            if cell.segmentSelected == 1{
//               cell.getFollowers()
//            }else if cell.segmentSelected == 2{
//                cell.getUserPosts()
//            } else{
//                cell.getFollowing()
//            }
//            cell.setUpToggleButton()
//        }
//
//        if indexPath.item == collectionViewManager.cells.count{
//            //return here to add view controller
//        }
//
//        if let cell = cell as? FollowersProfileScreenCell {
//            guard let selectedUser = self.selectedUsersDict[indexPath] else{
//                print("selected user not found in selectedUsersDict for indexPath: \(indexPath)")
//                return
//            }
//            print("Cell about to be displayed inside of willDisplay for item: \(indexPath)")
//            print("Selected Useser: \(selectedUser.userID),=> \(selectedUser.username)")
//            print("SegementedViewController userID: \(cell.userID)")
//            cell.userID = (self.selectedUsersDict[indexPath]?.userID)!
////            cell.collectionView.isUserInteractionEnabled = true
//            cell.usernameLabel?.text = selectedUser.username ?? "@FriendsUserName: \(indexPath.item)"
//
//            cell.getUsernameProfilePicture()
//            if cell.segmentSelected == 1{
//                cell.getFollowers()
//            }else if cell.segmentSelected == 2{
//                cell.getUserPosts()
//            } else{
//                cell.getFollowing()
//            }
//
//
//            if selectedUser.userID == User.signedInUserID{
//                print("userIDS are the same")
//                print("selectedUser: \(selectedUser.userID), signedInUser: \(User.signedInUserID)")
//                cell.followButton.isHidden = true
//                cell.toggleButton.isHidden = true
//            }else{
//                cell.followButton.isHidden = false
//                cell.toggleButton.isHidden = false
//                cell.userProfileImage.isUserInteractionEnabled = false
////                cell.segementedViewController.toggleButton.isHidden = true
//                cell.scrollView.contentSize = self.view.bounds.size
//                cell.scrollView.isScrollEnabled = false
//                cell.setUpToggleButton()
//            }
//
//
//            if let following = User.signedInUserFollowingCache.object(forKey: selectedUser.userID as NSString){
//                print("Selected User is being followed")
//                cell.selectedFollow()
//            }else{
//                print("User: \(selectedUser.userID) is not being followed")
//                cell.unselectedFollow()
//            }
//
//            //            for user in selectedUsers{
////                print("selectedUSers userId: \(user.userID)")
////            }
//            if cellTapped == false{
//                print("selectedUsersCount: \(self.selectedUsers.count)")
//                print("Index Path: \(indexPath)")
//              //  print("selectedUsersAdjusted: \(self.selectedUsers[indexPath.item-3].userID)")
//
//            }
//            //uncomment these two lines below
////            cell.segementedViewController.getUsernameProfilePicture()
////            cell.segementedViewController.getFollowing()
//
//         //   self.selectedUser = self.selectedUsers.popLast()
//         //   cell.segementedViewController.selectedUser = self.selectedUser
//        //print("Selected user popped: \(self.selectedUser.userID)")
//        }
    }
    
    var slideButtonTapped:Bool = false
    
    @objc func slideToFeedScreen(_ sender: UIButton) {
        slideButtonTapped = true
        MainCollectionView.scrollToItem(at: IndexPath(item:0,section:0), at: .left, animated: true)
        searchFeedScreenViewController.collectionView.isUserInteractionEnabled = true
//        for item in 0..<collectionViewManager.cells.count{
//            if collectionViewManager.cells[item].type == CellTypes.MainFeedCollectionViewCell{
//                collectionViewManager.cells.remove(at: item)
//                UIView.performWithoutAnimation {
//                //MainCollectionView.deleteItems(at: [IndexPath(item: item, section: 0)])
//               MainCollectionView.reloadData()
//                }
//                break
//            }
//        }
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
       let currentPage = Int(scrollView.contentOffset.x/scrollView.frame.width)
        print("Scrollview Did end scolling animation: \(currentPage)")
        self.currentPage = currentPage
        //come back to here to fix buttons
        if slideButtonTapped{
            slideButtonTapped = false
            print("collectionViewManagerCells.count: \(collectionViewManager.cells.count)")
      
            
            var item = 0
            
            while  item<collectionViewManager.cells.count{
            print("currentItem: \(item)")
                print("The count: \(collectionViewManager.cells.count)")
            if item >= collectionViewManager.cells.count - 1{
                print("Ive been called")
                break
            }
                           item += 1
            if (collectionViewManager.cells[item].type == CellTypes.MainFeedCollectionViewCell) || (collectionViewManager.cells[item].type == CellTypes.FollowersProfileScreenCell){
                print("item removed: \(item)")
                collectionViewManager.cells.remove(at: item)
                UIView.performWithoutAnimation {
                    //MainCollectionView.deleteItems(at: [IndexPath(item: item, section: 0)])
                    MainCollectionView.reloadData()
                }
                //break
                item = 0
            }
    
            }
        }
    }
    
    
    
    @objc func slideToProfileScreenTapped(_ sender: UIButton) {
        print("Slide to profile screen tapped")
        print("Count of slide: \(MainCollectionView.numberOfItems(inSection: 0))")
    
        print("Cells in collectionViewManager")
        
        for cell in self.collectionViewManager.cells{
            print("\(cell.type)")
        }
        
        slideButtonTapped = true
        for item in 0..<collectionViewManager.cells.count{
            
            if collectionViewManager.cells[item].type == CellTypes.MainProfileScreenCell{
                print("Profile Cell is located at: \(item)")
                MainCollectionView.scrollToItem(at: IndexPath(item: item,section:0), at: .left, animated: true)
                searchFeedScreenViewController.collectionView.isUserInteractionEnabled = true
                return
                break
            }
            
        }
  

        for item in 0..<MainCollectionView.numberOfItems(inSection: 0){
            print("Count: \(MainCollectionView.numberOfItems(inSection: 0))")
            
            print("typeOfCell: \(String(describing: MainCollectionView.cellForItem(at: IndexPath(item: item, section: 0)) as? MainProfileScreenCell)))")
            
            if let cell = MainCollectionView.dataSource?.collectionView(self.MainCollectionView, cellForItemAt: IndexPath(item: item, section: 0)) as? MainProfileScreenCell{
                MainCollectionView.reloadData()
                MainCollectionView.scrollToItem(at: IndexPath(item: item, section: 0), at: .left, animated: true)
                //slideButtonTapped = true
                print("Location: \(item)")
                return
            }else {
                print("Not of type main profile screen cell")
            }
            
        }
        
        //self.currentPage == 2
        
        if MainCollectionView.numberOfItems(inSection: 0) > 3{
            collectionViewManager.cells.removeSubrange(3..<MainCollectionView.numberOfItems(inSection: 0))
            self.currentPage = 2
            self.MainCollectionView.reloadData()
        }
        
        slideButtonTapped = true
        self.MainCollectionView.scrollToItem(at: IndexPath(item:2, section: 0), at: .left, animated: true)
        

    }
    
    func addLine(fromPoint start: CGPoint, toPoint end:CGPoint) {
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        linePath.move(to: start)
        linePath.addLine(to: end)
        line.path = linePath.cgPath
        line.strokeColor = colors.toolBarColor.cgColor
        line.lineWidth = 4.5
        line.lineJoin = CAShapeLayerLineJoin.round
        self.view.layer.addSublayer(line)
    }
    
    func toolBarAddLine(fromPoint start: CGPoint, toPoint end:CGPoint,_ layer:CALayer? = nil) {
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        linePath.move(to: start)
        linePath.addLine(to: end)
        line.path = linePath.cgPath
        line.strokeColor = colors.toolBarColor.cgColor
        line.lineWidth = 4.5
        line.lineJoin = CAShapeLayerLineJoin.round
        self.view.layer.insertSublayer(line, above: layer)
    }
}

extension UIApplication {
    class var statusBarBackgroundColor: UIColor? {
        get {
            return (shared.value(forKey: "statusBar") as? UIView)?.backgroundColor
        } set {
            (shared.value(forKey: "statusBar") as? UIView)?.backgroundColor = newValue
        }
    }
}


