//
//  MainCameraCollectionViewCell3.swift
//  TileRecovery
//
//  Created by 123456 on 9/25/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Photos


class MainCameraCollectionViewCell3:UICollectionViewCell{
    
    var containerView:UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        containerView = UIView(frame: self.frame)
        self.addSubview(containerView)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        containerView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        containerView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        containerView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}


