//
//  User.swift
//  TableViewDropDown
//
//  Created by 123456 on 10/23/18.
//  Copyright © 2018 123456. All rights reserved.
//

import Foundation

///add in to lower function

struct userInfo {
    var username:String?{
        didSet{
            username = username?.lowercased()
        }
    }
    var firstName:String?{
        didSet{
            username = username?.lowercased()
        }
    }
    var lastName:String?{
        didSet{
            lastName = lastName?.lowercased()
        }
    }
    var email:String? {
        didSet{
            email = setEmail(email: email)?.lowercased()
        }
    }

    var password:String?
    var Birthday:Date?
    var phoneNumber:String? {
        didSet{
            if let phoneNumber = self.phoneNumber{
                let phone:String = phoneNumber.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
                if phone != ""{
                    self.phoneNumber = "+1" + phone
                    
                }
                else{
                    self.phoneNumber = nil
                }
            }
            else{
                self.phoneNumber = nil
            }
            print("The phone number saved is: \(self.phoneNumber)")
        }
    }
    
    
    init() {
        username = String()
        firstName = String()
        lastName = String()
        email = nil
        password = String()
        Birthday = Date()
    }
    
    func getPhoneNumber()->String?{
        return self.phoneNumber
    }
    

    
//    mutating func setPhoneNumber(phoneNumber: String){
//        let phone:String = phoneNumber.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
//        if phone != ""{
//            self.phoneNumber = phone
//
//        }
//        else{
//            self.phoneNumber = nil
//        }
//    }
    
    func setEmail(email:String?)->String?{
        if let email = email{
            if isValidEmail(testStr: email){
                return email
            }
        }
        
        return nil
        
    }
//    
//    private func isValidEmail(testStr:String) -> Bool {
//        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
//        
//        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
//        return emailTest.evaluate(with: testStr)
//    }
    
    func isValidEmail(testStr:String) -> Bool {
        print("validate emilId: \(testStr)")
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    var isNill:Bool{
        if self.username == nil || self.Birthday == nil || self.email == nil || self.password == nil || self.firstName == nil || self.lastName == nil || self.phoneNumber == nil{
            return true
        }
        print("username: \(self.username)\n")
        print("Birthday: \(self.Birthday)\n")
        print("email: \(self.email)\n")
        print("password: \(self.password)\n")
        print("firstName: \(self.firstName)\n")
        print("lastName: \(self.lastName)\n")
        print("phoneNumber: \(self.phoneNumber)\n")
        return false
    }
    var userid:String?
    var userData:[String:Any]?{
        print("self.isNill in userInfo: \(self.isNill)")
        if self.isNill == false{
        return ["username":username,"FirstName":firstName,"LastName":lastName,"password":password,"email":email,"phoneNumber":phoneNumber,"Birthday":Birthday]
        }
        else {
            return nil
        }
        }
}
