//
//  Posts.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 9/20/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

class imagePost{
    var image:UIImage!
    var imageURL:String!
    init(image:UIImage){
        self.image = image
    }
    init(imageURL:String){
        self.imageURL = imageURL
    }
    init(imageURL:String, image:UIImage){
        self.image = image
        self.imageURL = imageURL
    }
    static var defaultImages:[imagePost] = [imagePost(image:#imageLiteral(resourceName: "DJI_0022")),imagePost(image:#imageLiteral(resourceName: "DJI_0044")),imagePost(image:#imageLiteral(resourceName: "DJI_0150"))    ,imagePost(image:#imageLiteral(resourceName: "DJI_0024")),imagePost(image:#imageLiteral(resourceName: "DJL_0004")),imagePost(image:#imageLiteral(resourceName: "85154-rininger_2.jpg")),imagePost(image:#imageLiteral(resourceName: "DJL_0001")),imagePost(image:#imageLiteral(resourceName: "DJL_0002")),imagePost(image:#imageLiteral(resourceName: "DJL_0003")),imagePost(image:#imageLiteral(resourceName: "DJI_0087-1")), imagePost(image:#imageLiteral(resourceName: "IMG_4967.jpg")),imagePost(image:#imageLiteral(resourceName: "IMG_7589 (1).jpg")),imagePost(image:#imageLiteral(resourceName: "IMG_8307.jpg")),imagePost(image:#imageLiteral(resourceName: "launch_36_gal_01.jpg")),imagePost(image:#imageLiteral(resourceName: "Ship.jpeg")),imagePost(image:#imageLiteral(resourceName: "IMG_0002.jpg")), imagePost(image:#imageLiteral(resourceName: "IMG_0002.jpg")),imagePost(image:#imageLiteral(resourceName: "IMG_0028.jpg")),imagePost(image:#imageLiteral(resourceName: "IMG_0042.jpg")),imagePost(image:#imageLiteral(resourceName: "IMG_0054.jpg")),imagePost(image:#imageLiteral(resourceName: "IMG_0118.jpg")),imagePost(image:#imageLiteral(resourceName: "IMG_4905.jpg")),imagePost(image:#imageLiteral(resourceName: "IMG_4967.jpg")), imagePost(image:#imageLiteral(resourceName: "IMG_5050.jpg")),imagePost(image:#imageLiteral(resourceName: "IMG_0568.jpg")),imagePost(image:#imageLiteral(resourceName: "IMG_5050.jpg")),imagePost(image:#imageLiteral(resourceName: "IMG_0533.jpg")),imagePost(image:#imageLiteral(resourceName: "IMG_0009.jpg"))]
}



class colors{
    //#38d9a9
    static let toolBarColor = UIColor(red: 56/255, green: 217/255, blue: 169/255, alpha: 1.0)
    //#e6fcf5
    static let backgroundColor = UIColor(red: 230/255, green: 252/255, blue: 245/255, alpha: 1.0)
}

let tileColor:UIColor = UIColor(red: 56/255, green: 217/255, blue: 169/255, alpha: 1.0)
let backgroundColor:UIColor = UIColor(red: 230/255, green: 252/255, blue: 245/255, alpha: 1.0)
