//
//  DelegateCollectionView-segmentedViewController.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 9/20/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

extension segmentedViewController:UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // print("profile cell was selected: \(indexPath), in segment: \(segmentSelected)")
        switch segmentSelected{
        case 0:
            print("profile cell was selected: \(indexPath), in segment: \(segmentSelected)")
            print("add cell called: follow.count: \(following.count)")
            for follow in following{
                print("follow: \(follow)")
            }
            collectionView.isUserInteractionEnabled = false
            if let followingSelected = self.following[indexPath.item] as? Tag{
                self.selectedTag = followingSelected
                self.selectedUser = nil
                print("TagID: \(followingSelected.tagID)")
                self.selectedCellDelegate?.setSelectedCell(selectedUser: nil, selectedTag: followingSelected)
               // self.addOrRemoveCellDelegate?.addCell(sender: self,cellType: CellTypes.MainFeedCollectionViewCell)
                self.addOrRemoveCellDelegate?.addCell(sender: self, tag: followingSelected)
            }
            else if let followingSelected = self.following[indexPath.item] as? User{
                self.selectedUser = followingSelected
                self.selectedTag = nil
                self.selectedCellDelegate!.setSelectedCell(selectedUser: followingSelected, selectedTag: nil)
                print("Inside of selectedView controller selected userID: \(selectedUser.userID)")
                //self.addOrRemoveCellDelegate?.addCell(sender: self, cellType: CellTypes.FollowersProfileScreenCell)
                self.addOrRemoveCellDelegate?.addCell(sender: self, user: followingSelected)
            }
            else{
                print("The type selected was neither a user or a tag")
            }
            
        case 1:
            print("profile cell was selected: \(indexPath), in segment: \(segmentSelected)")
           // print("selected cell: \(indexPath)")
            // collectionView.isUserInteractionEnabled = false
            if let followerSelected = self.followers[indexPath.item] as? User{
                self.selectedCellDelegate!.setSelectedCell(selectedUser: followerSelected, selectedTag: nil)
                self.selectedUser = followerSelected
                self.selectedTag = nil
                print("Inside of selectedView controller selected userID: \(selectedUser.userID)")
                //self.addOrRemoveCellDelegate?.addCell(sender: self, cellType: CellTypes.FollowersProfileScreenCell)
                self.addOrRemoveCellDelegate?.addCell(sender: self, user: followerSelected)
            }else if let followingSelected = self.followers[indexPath.item] as? Tag{
                self.selectedCellDelegate?.setSelectedCell(selectedUser: nil, selectedTag: followingSelected)
                self.selectedTag = followingSelected
                self.selectedUser = nil
                self.addOrRemoveCellDelegate?.addCell(sender: self,cellType: CellTypes.MainFeedCollectionViewCell)
            }
           
   
            
        case 2:
            print("profile cell was selected: \(indexPath), in segment: \(segmentSelected)")
            //add in official userID
            Post.cache.removeAllObjects()
            Post.cache.setObject(self.userPosts as NSArray, forKey: self.userID as NSString)
            self.profilePostPassDelegate?.profilePostPass(indexPath: indexPath, segmentType: segmentType.PostsCells,name:"username123",id: self.userID)
            
        default:
            print("profile cell was selected: \(indexPath), in segment: \(segmentSelected)")
            break
        }
    }
}


//
//protocol ProfilePostsSelected: class{
//    func profilePostPass(indexPath:IndexPath, segmentType:segmentType?,name:String,id:String)
//}
//
//extension MainViewController:ProfilePostsSelected{
//    func profilePostPass(indexPath: IndexPath, segmentType:segmentType?,name:String,id:String) {
//        print("profile post pass called")
//        self.selectedItem(index: indexPath, senderCellType: CellTypes.MainProfileScreenCell,name:name,id:id)
//    }
//}
