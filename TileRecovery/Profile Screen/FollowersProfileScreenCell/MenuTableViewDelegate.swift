//
//  MenuTableViewDelegate.swift
//  TileRecovery
//
//  Created by 123456 on 3/25/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

protocol  MenuTableViewDelegate: class{
    func addMenuTableView(menuTableViewController:UIViewController)->UIView
    func removeMenuTableView(menuTableViewController:UIViewController)
}


