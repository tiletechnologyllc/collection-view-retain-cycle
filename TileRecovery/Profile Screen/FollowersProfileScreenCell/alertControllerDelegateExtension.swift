//
//  alertControllerDelegateExtension.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 9/20/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit


//protocol alertControllerPhotoAlbumDelegate: class {
//    func showPhotoAlbumAlertControllerDelegate(sender:segmentedViewController)
//}
//
//protocol alertControllerCameraDelegate: class{
//    func showCameraAlertControllerDelegate(sender:segmentedViewController)
//}

//extension MainViewController:alertControllerPhotoAlbumDelegate{
//    func showPhotoAlbumAlertControllerDelegate(sender:segmentedViewController) {
//        //self.tapToSlideTapped(sender)
//        self.showPhotoAlbum(sender)
//    }
//}
//
//extension MainViewController:alertControllerCameraDelegate{
//    func showCameraAlertControllerDelegate(sender: segmentedViewController) {
//        print("Inside of camera")
//        for i in 0..<self.collectionViewManager.cells.count{
//            if self.collectionViewManager.cells[i].type == .MainCameraCollectionViewCell{
//              self.MainCollectionView.scrollToItem(at: IndexPath(item: i, section: 0), at: .left, animated:false)
//             break
//            }
//        }
//        //self.MainCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated:false)
//        
//        self.cameraType = cameraSender.profileImage
//    }
//}

enum photoAlbumSender{
    case feedPostImage
    case profileImage
}

enum cameraSender{
    case feedScreenCamera
    case profileImage
}

