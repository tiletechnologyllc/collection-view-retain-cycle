//
//  PostsCells.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 9/20/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

class PostsCells:UICollectionViewCell{
    var postImageView:UIImageView!
    var postLabel:UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        postImageView = UIImageView(frame: self.frame)
        self.addSubview(postImageView)
        postImageView.layer.cornerRadius = self.bounds.width/20
        
        postLabel = UILabel(frame: CGRect(x: 0, y: 119, width: 134, height: 20.5))
        self.addSubview(postLabel)
        
        postLabel.textAlignment = .center
        //followingCellLabel.adjustsFontSizeToFitWidth = true
        postLabel.textColor = colors.backgroundColor
        postLabel.translatesAutoresizingMaskIntoConstraints = false
        postLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant:10).isActive = true
        postLabel.topAnchor.constraint(equalTo: self.topAnchor,constant:2).isActive = true
        postLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        self.bringSubviewToFront(postLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
