//
//  AlertControllerDelegate-FollowersProfileScreenCell.swift
//  TileRecovery
//
//  Created by 123456 on 3/25/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

protocol AlertControllerDelegate: class{
    func presentAlertController(alertController:UIAlertController, completion:(()->Void)?)
}


