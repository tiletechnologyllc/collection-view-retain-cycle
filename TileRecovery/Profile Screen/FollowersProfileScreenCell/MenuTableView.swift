import Foundation
import UIKit
import FirebaseAuth
//protocol performSettingsSegue{
//    func performSettingsSegue()
//}

class MenuTableView:UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    var tableView:UITableView!
    
    var menuItems:[String] = ["Account","Blocked Accounts","Terms","Contact Us","Log Out"]
    
    let interactor = Interactor()
    
    //var perfromSegueDegelate:performSettingsSegue!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = colors.backgroundColor
        tableView = UITableView(frame: self.view.bounds)
        tableView.delegate = self
        tableView.dataSource = self
        self.view.addSubview(tableView)
        tableView.separatorStyle = .none
        tableView.register(StaticCell.self, forCellReuseIdentifier: "staticCell")
        tableView.backgroundColor = colors.backgroundColor
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 40).isActive = true
        tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor,  constant: 0).isActive = true
        tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
    }
    
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1 {
            return "first section title"
        }
        return "second section title"
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView()
        vw.backgroundColor = colors.backgroundColor
        let titleLabel = UILabel(frame: CGRect(x: vw.center.x, y: 0, width: 200, height: 50))
        
        titleLabel.text = "Menu"
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.boldSystemFont(ofSize: 25)
        titleLabel.textColor = colors.toolBarColor
        vw.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.leadingAnchor.constraint(equalTo: vw.leadingAnchor, constant: 30).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: vw.centerYAnchor, constant: 0).isActive = true
        
        let borderBottom = UIView(frame: CGRect(x:0, y:40, width: tableView.bounds.size.width, height: 4.0))
        borderBottom.backgroundColor = UIColor.self.init(red: 5/255, green: 16/255, blue: 28/255, alpha: 1.0)
        borderBottom.backgroundColor = colors.toolBarColor
        vw.addSubview(borderBottom)
        
        return vw
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "staticCell", for: indexPath) as! StaticCell
        cell.backgroundColor = colors.backgroundColor
        cell.myLabel.backgroundColor = colors.backgroundColor
        cell.layoutSubviews()
        cell.myLabel.text = menuItems[indexPath.row]
        let bgColorView = UIView()
        bgColorView.backgroundColor = colors.toolBarColor.withAlphaComponent(0.5)
        cell.selectedBackgroundView = bgColorView
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let modalViewController = ModalViewController()
        modalViewController.transitioningDelegate = self
        modalViewController.interactor = interactor
        
        print("Index Path selected: \(indexPath.row)")
        switch indexPath.row {
        case 0:
            modalViewController.settingChoice = menuOption.account
        case 1:
            modalViewController.settingChoice = menuOption.blockedAccounts
        case 2:
            modalViewController.settingChoice = menuOption.terms
        case 3:
            modalViewController.settingChoice = menuOption.contactUs
        case 4:
            self.logOutTapped()
        default:
            modalViewController.settingChoice = menuOption.blockedAccounts
        }
        
        present(modalViewController, animated: true, completion: {
            print("View controller presented")
        })
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func logOutTapped(){
        print("SignOut Button Tapped")
        do{
            try Auth.auth().signOut()
        }
        catch {
            print("Error signing out )")
        }
        
        
        print("Successful signout")
        
//        let entryVC = EntryScreenViewController()
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
//        present(entryVC, animated: false, completion: nil)
    }
}


//extension MainViewController:performSettingsSegue{
//    func performSettingsSegue() {
//        performSegue(withIdentifier: "settingsSegue", sender: self)
//    }
//}






extension MenuTableView: UIViewControllerTransitioningDelegate{
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissAnimator()
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
}






