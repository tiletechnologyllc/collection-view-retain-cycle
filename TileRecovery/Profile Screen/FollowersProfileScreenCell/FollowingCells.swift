//
//  FollowingCells.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 9/20/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

class FollowingCells:UICollectionViewCell{
    var followingImageView:UIImageView!
    var followingCellLabel:UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        // print("made it to cell init in pinnedCells")
        followingImageView = UIImageView(frame: self.frame)
        self.addSubview(followingImageView)
        //        pinnedImageView.contentMode = .scaleAspectFill
        //        pinnedImageView.translatesAutoresizingMaskIntoConstraints = false
        //        pinnedImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        //        pinnedImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        //        pinnedImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        //        pinnedImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        
        followingImageView.layer.cornerRadius = self.bounds.width/20
        
        followingCellLabel = UILabel(frame: CGRect(x: 0, y: 119, width: 134, height: 20.5))
        self.addSubview(followingCellLabel)
        
        followingCellLabel.textAlignment = .center
        //followingCellLabel.adjustsFontSizeToFitWidth = true
        followingCellLabel.textColor = colors.backgroundColor
        followingCellLabel.translatesAutoresizingMaskIntoConstraints = false
        followingCellLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        followingCellLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        followingCellLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        followingCellLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        self.bringSubviewToFront(followingCellLabel)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

