//
//  FriendsProfileCel.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 9/20/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

protocol addViewControllerToCell: class{
    func addViewControllerToCell(segmentedViewController:segmentedViewController)
}

class FriendsProfileScreenCell:UICollectionViewCell{
    
    var contentViewController:segmentedViewController!
    var segementedViewController:segmentedViewController = segmentedViewController()
    var containerView:UIView!
    weak var addViewDelegate:addViewControllerToCell?
    
    var userID:String!
    
    var setTextLabelTitle:String?{
        didSet{
            print("Text Label Title was set")
            print(setTextLabelTitle)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //add layout and view controller
        print("inside of init")
        print("userID inside of cell: \(self.userID)")
        self.segementedViewController.userID = self.userID
        
        containerView = UIView(frame: self.frame)
        self.addSubview(containerView)
        containerView.backgroundColor = backgroundColor
        
        
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        containerView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        segementedViewController.view.frame = self.containerView.bounds
        self.containerView.addSubview(segementedViewController.view)
        self.addViewDelegate?.addViewControllerToCell(segmentedViewController: segementedViewController)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
    
}

extension MainViewController:addViewControllerToCell{
    func addViewControllerToCell(segmentedViewController:segmentedViewController){
        segmentedViewController.addOrRemoveCellDelegate = self
        self.addChild(segmentedViewController)
    }
}

