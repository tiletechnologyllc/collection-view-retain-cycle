//
//  MainProfileScreenCell.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 9/20/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//


import Foundation
import UIKit
import Firebase
import SDWebImage

class MainProfileScreenCell:UICollectionViewCell{
    
    deinit {
        print("Main profile screen cell deinit called")
    }
    
//    weak var addViewDelegate:addViewControllerToCell?
    weak var alertControllerDelegate:AlertControllerDelegate?
    weak var menuTableViewDelegate:MenuTableViewDelegate?
    var setTextLabelTitle:String?{
        didSet{
            print("Text Label Title was set")
            print(setTextLabelTitle)
        }
    }
    
    var getFollowingCount:Int = 0
    
    var scrollView:UIScrollView!
    var menuView:UIView!
    var mainView:UIView!
    var overLap:CGFloat!
    
    var followers:[User] = [User]()
    var following:[Any] = [Any]()
    var userPosts:[Post] = [Post]()
    
    var toggleButton:UIButton!
    
    var menuTableView:MenuTableView!
    var buttonBarLeftAnchor:NSLayoutConstraint!
    
    var selectedTag:Tag!
    var selectedUser:User!
    weak var selectedCellDelegate:SelectedCellDelegate?
    
    let queue = DispatchQueue(label: "testFirebaseQueue")
    
    var userID:String = User.signedInUserID{//"dVC63JFr8YWHGkiSsQFDsW9SmOF2" {
        didSet{
            print("userID changed: \(userID)")
            // self.collectionView.reloadData()
            //print("Reload called")
            getFollowingCount = 0
        }
    }
    
    var followingDispatchGroup:DispatchGroup = DispatchGroup()
    
    lazy var functions = Functions.functions()
    
    var doesSignedInUserFollow:Bool = false
    
    let placeHolder = #imageLiteral(resourceName: "Screen Shot 2018-01-29 at 8.30.24 PM")
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        print("Inside of followers Cell Init")
        // Do any additional setup after loading the view, typically from a nib.
        print("View did load called in segmentedViewController: \(self.userID)")
        
        overLap = 1/3*self.contentView.bounds.width
        menuTableView = MenuTableView()
        
        setUpScrollView()
        setUpMenuTableView()
        // setUpViewControllers()
        
        headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.contentView.frame.width, height: 100))
        
        //segmented controller
        segmentedControl = UISegmentedControl(items: ["Following","Followers","Posts"])
        segmentedControl.frame =  CGRect(x: 0, y: headerView.frame.height, width: self.contentView.frame.width, height: 100)
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.layer.cornerRadius = 5
        segmentedControl.addTarget(self, action: #selector(segmentedControlValueChanged), for: .valueChanged)
        segmentedControl.setTitleTextAttributes([
            NSAttributedString.Key.font : UIFont(name: "DINCondensed-Bold", size: 18),                       //THOMASSSSSSSSSSSSSSSSSSSSS
            NSAttributedString.Key.foregroundColor: UIColor(red: 56/255, green: 217/255, blue: 169/255, alpha: 0.5)
            ], for: .normal)
        
        
        segmentedControl.setTitleTextAttributes([
            NSAttributedString.Key.font : UIFont(name: "DINCondensed-Bold", size: 18),
            NSAttributedString.Key.foregroundColor: colors.toolBarColor
            ], for: .selected)
        segmentedControl.tintColor = .clear
        
        userProfileImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        usernameLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        //usernameLabel?.text = "username"
        
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 5, bottom: 0, right: 5)
        collectionView = UICollectionView(frame: self.contentView.frame, collectionViewLayout: layout)
        collectionView.collectionViewLayout = layout
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(FollowingCells.self, forCellWithReuseIdentifier: "followingCells")
        collectionView.register(FollowersCells.self, forCellWithReuseIdentifier: "followersCells")
        collectionView.register(PostsCells.self, forCellWithReuseIdentifier: "postsCells")
        collectionView.backgroundColor = colors.backgroundColor
        self.mainView.addSubview(collectionView)
        
        self.mainView.addSubview(headerView)
        self.headerView.addSubview(userProfileImage)
        self.headerView.addSubview(usernameLabel!)
        self.mainView.addSubview(segmentedControl)
        
        userProfileImage.isUserInteractionEnabled = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.profileImage(_:)))
        userProfileImage.addGestureRecognizer(tapGestureRecognizer)
        
        followButton = FollowButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        self.headerView.addSubview(followButton)
        followButton.addTarget(self, action: #selector(followButtonTapped), for: .touchUpInside)
        
        toggleButton = UIButton(frame: CGRect(x: 0.75*self.contentView.bounds.width, y: 15, width: 50, height: 50))
        self.headerView.addSubview(toggleButton)
        
        self.collectionView.isUserInteractionEnabled = true
        
        collectionView.isPrefetchingEnabled = true
        setupLayoutView()
        setUpActionController()
    }
    
    func setUpCollectionView(){
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 5, bottom: 0, right: 5)
        collectionView = UICollectionView(frame: self.contentView.frame, collectionViewLayout: layout)
        collectionView.collectionViewLayout = layout
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(FollowingCells.self, forCellWithReuseIdentifier: "followingCells")
        collectionView.register(FollowersCells.self, forCellWithReuseIdentifier: "followersCells")
        collectionView.register(PostsCells.self, forCellWithReuseIdentifier: "postsCells")
        collectionView.backgroundColor = colors.backgroundColor
        self.mainView.addSubview(collectionView)
        
        setupLayoutView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    func setUpToggleButton(){
        
        //toggleButton.backgroundColor = UIColor.brown
        if self.userID == User.signedInUserID{
            print("we're on the profile page")
            toggleButton.setImage(#imageLiteral(resourceName: "icons8-menu-filled-500"), for: .normal)
            toggleButton.addTarget(self, action: #selector(toggle), for: .touchUpInside)
        }else{
            print("we're not on the profile page")
            toggleButton.setImage(#imageLiteral(resourceName: "icons8-more-filled-90"), for: .normal)
            toggleButton.addTarget(self, action: #selector(blockButtonTapped), for: .touchUpInside)
        }
    }
    
    var isUserBlocked:Bool = false
    @objc func blockButtonTapped(){
        print("block button tapped")
        let username = usernameLabel?.text ?? "User"
        let alertViewController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        var title:String
        
        let blockAction = UIAlertAction(title: "Block User", style: .destructive, handler: {(action) in
            print("Block user")
            self.blockUser()
            self.followButton.setTitle("Follow", for: .normal)
            self.followButton.backgroundColor = self.backgroundColor
            self.followButton.setTitleColor(tileColor, for: .normal)
            self.isUserBlocked = true
        })
        
        let unblockAction = UIAlertAction(title: "Unblock User", style: .default, handler: {(action) in
            self.unblockUser()
            self.followButton.backgroundColor = tileColor
            self.followButton.setTitleColor(colors.backgroundColor, for: .normal)
            self.isUserBlocked = false
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(action) in
            print("Cancel action tapped")
        })
        
        if isUserBlocked == true{
            alertViewController.addAction(unblockAction)
        }else{
            print("added block user to the alertViewController")
            alertViewController.addAction(blockAction)
        }
        
        alertViewController.addAction(cancelAction)
        self.alertControllerDelegate?.presentAlertController(alertController: alertViewController, completion: nil)
        //        present(alertViewController, animated: true, completion: {
        //            print("Alert View Controller presented")
        //        })
    }
    
    func blockUser(){
        print("Block User Called")
        self.collectionView.isHidden = true
        functions.httpsCallable("blockUser").call(["userID": User.signedInUserID, "blockedID":self.userID], completion: {(result,error) in
            if let error = error{
                print("Error occurred while running blockUser: \(error)")
            }
            guard let result = result else{
                print("No return sent from function")
                return
            }
            
            print("Result from blockUser: \(result)")
        })
    }
    
    func unblockUser(){
        print("Unblock user called")
        self.collectionView.isHidden = false
        functions.httpsCallable("unblockUser").call(["userID": User.signedInUserID, "blockedID":self.userID], completion: {(result,error) in
            if let error = error{
                print("Error occurred while running unblockUser: \(error)")
            }
            guard let result = result else{
                print("No return sent from function")
                return
            }
            
            print("Result from unblockUser: \(result)")
        })
    }
    
    @objc func toggle(){
        toggleLeftMenu(animated: true)
    }
    
    
    func setUpScrollView(){
        scrollView = UIScrollView(frame: self.contentView.bounds)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.bounces = false
        scrollView.isPagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.backgroundColor = .blue
        self.contentView.addSubview(scrollView)
        
        scrollView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
        self.scrollView.contentSize = CGSize(width: self.contentView.bounds.width + self.contentView.bounds.width*2/3, height: self.contentView.bounds.height)
        
        menuView = UIView(frame: CGRect(x: self.contentView.bounds.maxX, y: 0, width: self.contentView.bounds.width*2/3, height: self.contentView.bounds.height))
        mainView = UIView(frame: self.contentView.bounds)
        menuView.translatesAutoresizingMaskIntoConstraints = false
        mainView.translatesAutoresizingMaskIntoConstraints = false
        mainView.backgroundColor = colors.backgroundColor
        menuView.backgroundColor = UIColor.yellow
        
        
        
        self.scrollView.addSubview(menuView)
        self.scrollView.addSubview(mainView)
        
        mainView.topAnchor.constraint(equalTo: self.scrollView.topAnchor).isActive = true
        mainView.heightAnchor.constraint(equalTo: self.scrollView.heightAnchor, multiplier: 1.0, constant: 0).isActive = true
        mainView.trailingAnchor.constraint(equalTo: self.menuView.leadingAnchor).isActive = true
        mainView.widthAnchor.constraint(equalToConstant: self.contentView.bounds.width).isActive = true
        
        menuView.topAnchor.constraint(equalTo: self.scrollView.topAnchor).isActive = true
        menuView.heightAnchor.constraint(equalTo: self.scrollView.heightAnchor, multiplier: 1.0, constant: 0).isActive = true
        menuView.leadingAnchor.constraint(equalTo: self.mainView.trailingAnchor).isActive = true
        menuView.leadingAnchor.constraint(equalTo: self.scrollView.leadingAnchor, constant: scrollView.bounds.width).isActive = true
        menuView.widthAnchor.constraint(equalTo: self.contentView.widthAnchor, multiplier: 2/3).isActive = true
        
        
        
        
        
        
        mainView.addShadow()
    }
    
    func setUpMenuTableView(){
        // self.addViewController(viewController: menuTableView)
        guard let menuView = self.menuTableViewDelegate?.addMenuTableView(menuTableViewController: menuTableView) else{
            print("menu view is empty:")
            return
        }
        
        menuView.backgroundColor = colors.backgroundColor
        
        menuView.translatesAutoresizingMaskIntoConstraints = false
        menuView.topAnchor.constraint(equalTo: menuView.topAnchor, constant:0).isActive = true
        menuView.bottomAnchor.constraint(equalTo: menuView.bottomAnchor, constant: 0).isActive = true
        menuView.trailingAnchor.constraint(equalTo: menuView.trailingAnchor, constant: 0).isActive = true
        menuView.leadingAnchor.constraint(equalTo: menuView.leadingAnchor, constant: 0).isActive = true
    }
    
    func closeMenu(animated:Bool){
        scrollView.setContentOffset(CGPoint(x: menuView.frame.width, y: 0), animated: animated)
    }
    
    func openLeftMenu(animated:Bool){
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: animated)
    }
    
    func isLeftMenuOpen()->Bool{
        return scrollView.contentOffset.x == 0
    }
    
    func toggleLeftMenu(animated:Bool){
        if isLeftMenuOpen(){
            closeMenu(animated: animated)
        }
        else{
            openLeftMenu(animated: animated)
        }
    }
    
    //    private func addViewController(viewController: UIViewController)
    //    {
    //        viewController.view.translatesAutoresizingMaskIntoConstraints = false
    //        menuView.addSubview(viewController.view)
    //
    //        self.addChild(viewController)
    //        viewController.didMove(toParent: self)
    //    }
    
    
    @objc func followButtonTapped(){
        print("Follow Button Tapped")
        
        if self.doesSignedInUserFollow == true{
            print("User: \(self.userID) should be unfollowed")
            self.followButton.setTitleColor(tileColor, for: .normal)
            self.followButton.backgroundColor = backgroundColor
            self.followButton.setTitle("Follow", for: .normal)
            self.doesSignedInUserFollow = false
            
            self.functions.httpsCallable("unfollowTapped").call(["followID":self.userID,"userID":User.signedInUserID], completion: {[weak self] (result,error) in
                guard let self = self else{
                    assert(true,"self found nil")
                    print("self found nil")
                    return
                }
                if let error = error {
                    print("Error unfollowing user: \(error)")
                }
                print("Result from unfollowing user: \(result?.data)")
                User.signedInUserFollowingCache.removeObject(forKey: self.userID as NSString)
                print("\(self.userID) was removed from the cahce")
            })
            //            User.signedInUserFollowingCache.removeObject(forKey: self.userID as NSString)
        }else{
            print("User: \(userID) should be followed")
            
            self.followButton.setTitle("Following", for: .normal)
            self.followButton.setTitleColor(backgroundColor, for: .normal)
            self.followButton.backgroundColor = tileColor
            self.doesSignedInUserFollow = true
            
            self.functions.httpsCallable("followTapped").call(["followID":self.userID,"userID":User.signedInUserID], completion: {(result,error) in
                if let error = error{
                    print("Error following user: \(error)")
                }
                
                print("Result from following user: \(result?.data)")
                
            })
        }
    }
    
    func getUsernameProfilePicture(){
        var storagePath:String!
        if let user = User.UsersCache.object(forKey: self.userID as NSString){
            print("User datas gotten from cache")
            self.usernameLabel?.text = user.username
            storagePath = user.storagePath
            if let storagePath = storagePath{
                let storageRef = Storage.storage().reference(withPath: "Profile Pictures/\(storagePath)")
                let placeHolder = #imageLiteral(resourceName: "Screen Shot 2018-01-29 at 8.30.24 PM")
                self.userProfileImage.sd_setImage(with: URL(string: user.photoURL) , placeholderImage: placeHolder, options: SDWebImageOptions.refreshCached, completed: {(image,error,imageCacheType,storageReference) in
                    if let error = error{
                        print("Uh-Oh an error has occured in cache pull: \(error.localizedDescription)" )
                    }
                })
                //                        self.userProfileImage.sd_setImage(with: storageRef, placeholderImage: placeHolder, completion: {(image,error,imageCacheType,storageReference) in
                //                            if let error = error{
                //                                print("Uh-Oh an error has occured in cache pull: \(error.localizedDescription)" )
                //                            }
                //                        })
                
            }
            else{
                print("Failed to set storage path in cache sucessfully")
            }
        }else{
            print("User datas not from cache")
            Database.database().reference(withPath: "Users/\(userID)").observeSingleEvent(of: .value, with: {[weak self] (snapshot) in
                guard let self = self else{
                    assert(true,"self found nil")
                    print("self found nil")
                    return
                }
                print("Complete user data: \(snapshot.value)")
                if let data = snapshot.value as? [String:Any]{
                    self.usernameLabel?.text? = data["username"] as! String
                    storagePath = data["photoURL"] as! String
                    print("self.usernameLabel.text: \(self.usernameLabel?.text)")
                }else{
                    print("Failed to create username label")
                }
                
                if let user = User(snapshot: snapshot){
                    User.UsersCache.setObject(user, forKey: (user.userID as NSString))
                }else{
                    print("Failed to initialize user")
                }
                
                
                //let storageRef = Storage.storage().reference(forURL: storagePath)
                let storageRef = Storage.storage().reference(withPath: "Profile Pictures/\(self.userID).jpeg")
                let placeHolder = #imageLiteral(resourceName: "Screen Shot 2018-01-29 at 8.30.24 PM")
                
                self.userProfileImage.sd_setImage(with: URL(string: storagePath) , placeholderImage: placeHolder, options: SDWebImageOptions.refreshCached, completed: {(image,error,imageCacheType,storageReference) in
                    if let error = error{
                        print("Uh-Oh an error has occured in cache pull: \(error.localizedDescription)" )
                    }
                })
                //            self.userProfileImage.sd_setImage(with: storageRef, placeholderImage: placeHolder, completion: {(image,error,imageCacheType,storageReference) in
                //                print("Storage path: \(storagePath) not from cache")
                //                if let error = error{
                //                    print("Uh-Oh an error has occured: \(error.localizedDescription)" )
                //                }
                //            })
            })
        }
        
        
        
        
    }
    
    @objc func alertBackgroundViewTapped(_ sender:UIGestureRecognizer){
        print("Background view tapped")
        self.alert.dismiss(animated: true, completion: nil)
        
    }
    
    func setUpActionController(){
        let margin:CGFloat = 10.0
        let rect = CGRect(x: margin, y: margin, width: alert.view.bounds.size.width - margin * 4.0, height: 120)
        let customView = UIView(frame: rect)
        
        alert.view.translatesAutoresizingMaskIntoConstraints = false
        alert.view.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        
        customView.backgroundColor = .green
        
        let cameraButton:UIButton = UIButton(frame: CGRect(x: margin, y: margin, width: CGFloat(alert.view.bounds.width - margin*4.0)/3, height: 120)) //120
        let photoAlbumButton:UIButton = UIButton(frame:CGRect(x: margin, y: margin, width: CGFloat(alert.view.bounds.width - margin*4.0)/3, height: 120))
        let CancelButton:UIButton = UIButton(frame:CGRect(x: margin, y: margin, width: CGFloat(alert.view.bounds.width - margin*4.0)/3, height: 120))
        
        cameraButton.backgroundColor = UIColor(red: 230/255, green: 252/255, blue: 245/255, alpha: 1.0)
        photoAlbumButton.backgroundColor = UIColor(red: 230/255, green: 252/255, blue: 245/255, alpha: 1.0)
        CancelButton.backgroundColor = UIColor(red: 230/255, green: 252/255, blue: 245/255, alpha: 1.0)
        
        cameraButton.setTitleColor(UIColor(red: 56/255, green: 217/255, blue: 169/255, alpha: 1.0), for: .normal)
        photoAlbumButton.setTitleColor(UIColor(red: 56/255, green: 217/255, blue: 169/255, alpha: 1.0), for: .normal)
        CancelButton.setTitleColor(UIColor(red: 56/255, green: 217/255, blue: 169/255, alpha: 1.0), for: .normal)
        
        cameraButton.layer.cornerRadius = cameraButton.frame.width/20
        photoAlbumButton.layer.cornerRadius = photoAlbumButton.frame.width/20
        CancelButton.layer.cornerRadius = CancelButton.frame.width/20
        
        horrizontalStackView = UIStackView(frame: rect)
        horrizontalStackView.backgroundColor = .orange
        horrizontalStackView.spacing = 8
        horrizontalStackView.addArrangedSubview(cameraButton)
        horrizontalStackView.addArrangedSubview(photoAlbumButton)
        horrizontalStackView.addArrangedSubview(CancelButton)
        cameraButton.setTitle("Camera", for: .normal)
        photoAlbumButton.setTitle("Photo Album", for: .normal)
        CancelButton.setTitle("Cancel", for: .normal)
        horrizontalStackView.backgroundColor = .green
        horrizontalStackView.distribution = .fillEqually
        horrizontalStackView.axis = .horizontal
        
        
        cameraButton.addTarget(self, action: #selector(cameraButtonTapped), for: .touchUpInside)
        photoAlbumButton.addTarget(self, action: #selector(photoAlbumButtonTapped), for: .touchUpInside)
        CancelButton.addTarget(self, action: #selector(cancelButtonTapped), for: .touchUpInside)
        
        //alert.view.addSubview(customView)
        alert.view.addSubview(horrizontalStackView)
        
        horrizontalStackView.translatesAutoresizingMaskIntoConstraints = false
        horrizontalStackView.bottomAnchor.constraint(equalTo: self.alert.view.bottomAnchor, constant: 0).isActive = true
        horrizontalStackView.leadingAnchor.constraint(equalTo: self.alert.view.leadingAnchor, constant: 0).isActive = true
        horrizontalStackView.trailingAnchor.constraint(equalTo: self.alert.view.trailingAnchor, constant: 0).isActive = true
        horrizontalStackView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        
    }
    
    @objc func profileImage(_ sender:UIGestureRecognizer){
        print("profile image tapepd")
        self.alertControllerDelegate?.presentAlertController(alertController: self.alert, completion: {
            self.alert.view.superview?.isUserInteractionEnabled = true
            self.alert.view.superview?.subviews[0].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertBackgroundViewTapped(_:))))
        })
        //        self.present(self.alert, animated: true) {
        //            self.alert.view.superview?.isUserInteractionEnabled = true
        //            self.alert.view.superview?.subviews[0].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertBackgroundViewTapped(_:))))
        //        }
    }
    
    
    @objc func cancelButtonTapped(){
        print("Cancel Button Tapped")
        self.alert.dismiss(animated: true, completion: nil)
    }
    
    @objc func photoAlbumButtonTapped(){
        print("Photo Album Button Tapped")
//        let segmentedView = segmentedViewController()
//        alertPhotoAlbumDelegate?.showPhotoAlbumAlertControllerDelegate(sender:segmentedView)//self)
        self.alert.dismiss(animated: true, completion: nil)
    }
    
    @objc func cameraButtonTapped(){
        print("Camera Button Tapped 1")
//        let segmentedView = segmentedViewController()
//        alertCameraAlbumDelegate?.showCameraAlertControllerDelegate(sender:  segmentedView)
        self.alert.dismiss(animated: true, completion: nil)
    }
    
    @objc func segmentedControlValueChanged(_ sender: UISegmentedControl) {
        self.buttonBarLeftAnchor.constant = (sender.frame.width / CGFloat(sender.numberOfSegments)) * CGFloat(sender.selectedSegmentIndex)
        UIView.animate(withDuration: 0.3) {
            //self.buttonBar.frame.origin.x = (sender.frame.width / CGFloat(sender.numberOfSegments)) * CGFloat(sender.selectedSegmentIndex)
            
            self.layoutIfNeeded()
            
        }
        print("selected index: \(sender.selectedSegmentIndex)")
        segmentSelected = sender.selectedSegmentIndex
        switch sender.selectedSegmentIndex {
        case 0:
            print("following cells")
            getFollowing()
        case 1:
            print("followers cells")
            getFollowers()
        case 2:
            print("posts")
            getUserPosts()
        default:
            print("something went wrong default case")
        }
        collectionView.reloadData()
    }
    
    var labelTitle:String?
    
    var headerView:UIView!
    var segmentedControl:UISegmentedControl!
    var usernameLabel:UILabel?
    var userProfileImage: UIImageView = UIImageView()
    var collectionView:UICollectionView!
    var segmentSelected = 0
    let alert = UIAlertController(title: "Edit Profile Picture", message: "", preferredStyle: UIAlertController.Style.actionSheet)
    var horrizontalStackView:UIStackView!
    let buttonBar = UIView()
    
//    weak var alertPhotoAlbumDelegate:alertControllerPhotoAlbumDelegate?
//    weak var alertCameraAlbumDelegate:alertControllerCameraDelegate?
    
    weak var addOrRemoveCellDelegate:addOrRemoveCell?
    
    weak var profilePostPassDelegate:ProfilePostsSelected?
    
    var profileImage:UIImage = imagePost.defaultImages[0].image
    
    var followButton:FollowButton!
    
}


private extension UIView{
    func addShadow(){
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shadowRadius = 2.5
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowOpacity = 0.7
        layer.shadowColor = colors.toolBarColor.cgColor
    }
}



//
//import Foundation
//import UIKit

//class MainProfileScreenCell:UICollectionViewCell{
//
//    var segementedViewController:segmentedViewController = segmentedViewController()
//    var containerView:UIView!
//
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        //add layout and view controller
//
//        segementedViewController.usernameLabel?.text = "Your username"
//        containerView = UIView(frame: self.frame)
//        self.addSubview(containerView)
//        containerView.backgroundColor = backgroundColor
//
//        containerView.translatesAutoresizingMaskIntoConstraints = false
//        containerView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
//        containerView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
//        containerView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
//        containerView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//}
