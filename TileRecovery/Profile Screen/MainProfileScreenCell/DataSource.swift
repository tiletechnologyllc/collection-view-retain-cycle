//
//  DataSource.swift
//  TileRecovery
//
//  Created by 123456 on 3/25/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit
import  Firebase
import SDWebImage

extension MainProfileScreenCell:UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch segmentSelected{
        case 0:
            return self.following.count
        case 1:
            return self.followers.count
        case 2:
            return self.userPosts.count
        default:
            return 10
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch segmentSelected{
        case 1:
            let followersCell:FollowersCells = collectionView.dequeueReusableCell(withReuseIdentifier: "followersCells", for: indexPath) as! FollowersCells
            followersCell.backgroundColor = UIColor.cyan
            followersCell.layer.cornerRadius = 29.6//followersCell.frame.width/20
            
            //Add in pinned images from the server
            
            let photoURL = URL(string: followers[indexPath.item].photoURL)
            
            followersCell.followersImageView.sd_setImage(with: photoURL, placeholderImage: self.placeHolder, options: SDWebImageOptions.refreshCached, completed: {[weak self] (image,error,imageCacheType,storageReference) in
                guard let self = self else{
                    assert(true,"self found nil")
                    print("self found nil")
                    return
                }
                if let error = error{
                    print("Uh-Oh an error has occured: \(error.localizedDescription)" )
                }
                followersCell.followersImageView.image = image
                self.setImageToAspectRatio(image: image, imageView: followersCell.followersImageView, bounds: followersCell.bounds)
            })
            
            //followersCell.followersImageView.clipsToBounds = true
            followersCell.clipsToBounds = true
            
            followersCell.followingCellLabel.text = self.followers[indexPath.item].username//self.genericTagsArray[indexPath.item]
            return followersCell
        case 2:
            let postCell:PostsCells = collectionView.dequeueReusableCell(withReuseIdentifier: "postsCells", for: indexPath) as! PostsCells
            postCell.backgroundColor = UIColor.red
            postCell.layer.cornerRadius = 29.6//postCell.frame.width/20
            
            //Add in pinned images from the server
            //postCell.postImageView.image = imagePost.defaultImages[2].image
            let photoURL = URL(string: self.userPosts[indexPath.item].photoURL)
            postCell.postImageView.sd_setImage(with: photoURL, placeholderImage: self.placeHolder, options: SDWebImageOptions.refreshCached, completed: {[weak self] (image,error,imageCacheType,storageReference) in
                guard let self = self else{
                    return 
                }
                if let error = error{
                    print("Uh-Oh an error has occured: \(error.localizedDescription)" )
                }
                postCell.postImageView.image = image
                self.setImageToAspectRatio(image: image, imageView: postCell.postImageView, bounds: postCell.bounds)
            })
            
            //            storageRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
            //                if let error = error {
            //                    print(" Uh-oh, an error occurred! \(error)")
            //                } else {
            //                    // Data for "images/island.jpg" is returned
            //                    if let data = data{
            //                        let image = UIImage(data: data)
            //                        postCell.postImageView.image = image
            //                    }else{
            //                        print("The data doesnt exist")
            //                    }
            //
            //                    //postCell.postImageView.image = image
            //                }
            //            }
            
            //
            guard let image = postCell.postImageView.image else{
                print("Error in cellforitemat in pinned cella")
                return UICollectionViewCell()
            }
            
            let photoAspectRatio:CGFloat = image.size.width/image.size.height
            let cellAspectRatio:CGFloat = postCell.bounds.width/postCell.bounds.height
            if photoAspectRatio >= cellAspectRatio{
                postCell.postImageView.frame = CGRect(x: 0, y: 0, width: photoAspectRatio*postCell.bounds.height, height: postCell.bounds.height)
            }
            else{
                postCell.postImageView.frame = CGRect(x: 0, y: 0, width: postCell.bounds.width, height: postCell.bounds.width/photoAspectRatio)
            }
            //postCell.postImageView.clipsToBounds = true
            postCell.clipsToBounds = true
            postCell.postLabel.text = userPosts[indexPath.item].tagName//genericTagsArray[indexPath.item]
            return postCell
        default:
            let followingCell :FollowingCells = collectionView.dequeueReusableCell(withReuseIdentifier: "followingCells", for: indexPath) as! FollowingCells
            followingCell.backgroundColor = UIColor.magenta
            followingCell.layer.cornerRadius = 29.6 //pinnedCell.frame.width/20
            
            //Add in pinned images from the server
            //  followingCell.followingImageView.image = imagePost.defaultImages[1].image
            //            var storageRef:StorageReference!
            var name:String!
            var photoURL:URL? = nil
            followingCell.followingImageView.image = nil
            
            if let following = self.following[indexPath.item] as? User{
                print("Following was a user: \(following.userID)")
                //  print("user storageref: \(following.storagePath)")
                //            storageRef = Storage.storage().reference(withPath: "Profile Pictures/\(following.storagePath)")
                photoURL = URL(string: following.photoURL)
                print("\t: user URL: \(photoURL!)")
                name = following.username
                followingCell.followingImageView.sd_setImage(with: photoURL, placeholderImage: placeHolder, options: SDWebImageOptions.refreshCached, completed: {[weak self] (image,error,imageCacheType,storageReference) in
                    guard let self = self else{
                        assert(true,"self found nil")
                        print("self found nil")
                        return
                    }
                    if let error = error{
                        print("Uh-Oh an error has occured: \(error.localizedDescription)" )
                    }
                    followingCell.followingImageView.image = image
                    self.setImageToAspectRatio(image: image, imageView: followingCell.followingImageView, bounds: followingCell.bounds)
                })
            }
            else if let following = self.following[indexPath.item] as? Tag{
                print("following was a tag: \(following.tagID)")
                name = following.name
                
                print("\t: following URL: \(following.photoURL)")
                guard let followingImageView = followingCell.followingImageView else{
                    print("ImageView in followingCell was found nil")
                    return followingCell
                }
                followingImageView.sd_setImage(with: URL(string: following.photoURL), placeholderImage: placeHolder, options: SDWebImageOptions.refreshCached, completed: {[weak self] (image,error,imageCacheType,storageReference) in
                    guard let self = self else{
                        assert(true,"self found nil")
                        print("self found nil")
                        return
                    }
                    if let error = error{
                        print("Uh-Oh an error has occured: \(error.localizedDescription)" )
                    }
                    followingCell.followingImageView.image = image
                    self.setImageToAspectRatio(image: image, imageView: followingCell.followingImageView, bounds: followingCell.bounds)
                })
                
            }else{
                print("neither tag nor post: \(indexPath.item)")
            }
            
            
            
            followingCell.clipsToBounds = true
            followingCell.followingCellLabel.text = name//self.following[indexPath.item].username//genericTagsArray[indexPath.item]
            return followingCell
        }
    }
    
    func setImageToAspectRatio(image:UIImage?,imageView:UIImageView,bounds:CGRect){
        guard let image = image else{
            print("Image was nil")
            let placeHolder = #imageLiteral(resourceName: "Screen Shot 2018-01-29 at 8.30.24 PM")
            imageView.image = placeHolder
            return
        }
        let photoAspectRatio = image.size.width/image.size.height
        let cellAspectRatio = bounds.width/bounds.height
        if photoAspectRatio >= cellAspectRatio{
            imageView.frame = CGRect(x: 0, y: 0, width: photoAspectRatio*bounds.height, height: bounds.height)
        }
        else{
            imageView.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.width/photoAspectRatio)
        }
    }
    
}

