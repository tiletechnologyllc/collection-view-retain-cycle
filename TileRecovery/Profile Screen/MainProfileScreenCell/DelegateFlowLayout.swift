//
//  DelegateFlowLayout.swift
//  TileRecovery
//
//  Created by 123456 on 3/25/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

extension MainProfileScreenCell:UICollectionViewDelegateFlowLayout{
    
    
    func setupLayoutView(){
        //segmented controller
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        segmentedControl.topAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
        segmentedControl.leadingAnchor.constraint(equalTo: self.mainView.leadingAnchor).isActive = true
        segmentedControl.trailingAnchor.constraint(equalTo: self.mainView.trailingAnchor).isActive = true
        segmentedControl.heightAnchor.constraint(equalToConstant: 40).isActive = true
        segmentedControl.backgroundColor = colors.backgroundColor
        //button bar layout
        buttonBar.translatesAutoresizingMaskIntoConstraints = false
        buttonBar.backgroundColor = colors.toolBarColor
        contentView.addSubview(buttonBar)
        
        buttonBar.topAnchor.constraint(equalTo: segmentedControl.bottomAnchor).isActive = true
        buttonBar.heightAnchor.constraint(equalToConstant: 5).isActive = true
        //buttonBar.leftAnchor.constraint(equalTo: segmentedControl.leftAnchor).isActive = true
        self.buttonBarLeftAnchor = buttonBar.leftAnchor.constraint(equalTo: segmentedControl.leftAnchor)
        buttonBarLeftAnchor.isActive = true
        buttonBar.widthAnchor.constraint(equalTo: segmentedControl.widthAnchor, multiplier: 1 / CGFloat(segmentedControl.numberOfSegments)).isActive = true
        
        //header view
        headerView.translatesAutoresizingMaskIntoConstraints = false
        headerView.leadingAnchor.constraint(equalTo: self.mainView.leadingAnchor, constant: 0).isActive = true
        headerView.trailingAnchor.constraint(equalTo: self.mainView.trailingAnchor).isActive = true
        headerView.topAnchor.constraint(equalTo: self.mainView.topAnchor,constant: 16).isActive = true
        headerView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        //headerView.backgroundColor = .red
        headerView.backgroundColor = colors.backgroundColor
        //        //label layout
        usernameLabel?.translatesAutoresizingMaskIntoConstraints = false
        //usernameLabel.leadingAnchor.constraint(equalTo: userProfileImage.trailingAnchor, constant: ((self.headerView.bounds.width/4)-50)/4).isActive = true
        usernameLabel?.trailingAnchor.constraint(equalTo: headerView.trailingAnchor).isActive = true
        //usernameLabel?.topAnchor.constraint(equalTo: headerView.topAnchor).isActive = true
        //usernameLabel?.bottomAnchor.constraint(equalTo: headerView.bottomAnchor,constant: -20).isActive = true
        //  usernameLabel.widthAnchor.constraint(equalToConstant: self.headerView.frame.width*3/4).isActive = true
        usernameLabel?.centerYAnchor.constraint(equalTo: self.userProfileImage.topAnchor, constant: 10).isActive = true
        usernameLabel?.leadingAnchor.constraint(equalTo: self.userProfileImage.trailingAnchor, constant: 10).isActive = true
        usernameLabel?.backgroundColor = colors.backgroundColor
        usernameLabel?.heightAnchor.constraint(equalToConstant: 20).isActive = true
        usernameLabel?.textAlignment = .left
        usernameLabel?.baselineAdjustment = .alignBaselines
        usernameLabel?.font = UIFont.boldSystemFont(ofSize: 25)
        usernameLabel?.textColor = colors.toolBarColor
        
        //imageView layout
        userProfileImage.translatesAutoresizingMaskIntoConstraints = false
        userProfileImage.leadingAnchor.constraint(equalTo: self.headerView.leadingAnchor,constant: 5).isActive = true
        
        //userProfileImage.trailingAnchor.constraint(equalTo: self.usernameLabel.leadingAnchor, constant: headerView.bounds.width - (((self.headerView.bounds.width/4)-50)/2)+50).isActive = true
        
        //  userProfileImage.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        userProfileImage.bottomAnchor.constraint(equalTo: segmentedControl.topAnchor, constant: -10).isActive = true
        // userProfileImage.bottomAnchor.constraint(equalTo: self.headerView.bottomAnchor, constant: 5).isActive = true
        userProfileImage.heightAnchor.constraint(equalToConstant: (self.collectionView.frame.width-26)/4).isActive = true
        userProfileImage.backgroundColor = .gray
        userProfileImage.widthAnchor.constraint(equalToConstant: (self.collectionView.frame.width-26)/4).isActive = true
        
        userProfileImage.layer.cornerRadius = 29.6
        userProfileImage.layer.masksToBounds = true
        //changed the content mode to .scaleAspectFill
        self.userProfileImage.contentMode = UIView.ContentMode.scaleAspectFill
        
        //Add in profile picture from server
        //userProfileImage.image = imagePost.defaultImages[0].image
        userProfileImage.image = profileImage
        headerView.bringSubviewToFront(userProfileImage)
        //        //collectionView layout
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.topAnchor.constraint(equalTo: segmentedControl.bottomAnchor, constant: 0).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: 0).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: 0).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: 0).isActive = true
        
        
        followButton.backgroundColor = colors.backgroundColor
        followButton.translatesAutoresizingMaskIntoConstraints = false
        followButton.leadingAnchor.constraint(equalTo: (usernameLabel?.leadingAnchor)!,constant: 0).isActive = true
        //followButton.trailingAnchor.constraint(equalTo: (usernameLabel?.trailingAnchor)!,constant: 0).isActive = true
        followButton.topAnchor.constraint(equalTo: (usernameLabel?.bottomAnchor)!,constant: 5).isActive = true
        //followButton.bottomAnchor.constraint(equalTo: userProfileImage.bottomAnchor, constant: 0).isActive = true
        followButton.heightAnchor.constraint(equalToConstant:25).isActive = true
        followButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        followButton.setTitle("Follow", for: .normal)
        
        followButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        followButton?.setTitleColor(colors.toolBarColor, for: .normal)
        followButton.layer.borderColor = colors.toolBarColor.cgColor
        followButton.layer.borderWidth = 2
        followButton.layer.cornerRadius = followButton.bounds.width/5
        
        
        toggleButton.translatesAutoresizingMaskIntoConstraints = false
        toggleButton.topAnchor.constraint(equalTo: self.headerView.topAnchor, constant: 40).isActive = true
        toggleButton.trailingAnchor.constraint(equalTo: self.headerView.trailingAnchor, constant: -20).isActive = true
        toggleButton.widthAnchor.constraint(equalToConstant: 40).isActive = true
        toggleButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if segmentSelected == 2{
            return CGSize(width: (self.collectionView.frame.width-18)/2, height: (self.collectionView.frame.width-18)/2)
        }
        else if segmentSelected == 0{
            return  CGSize(width: collectionView.bounds.width/3.0-3-8, height: collectionView.bounds.width/3.0-3-8)
        }
        else{
            return CGSize(width: (self.collectionView.frame.width-26)/4, height: (self.collectionView.frame.width-26)/4)
        }
        return CGSize(width: (self.contentView.frame.width-22)/3, height: (self.contentView.frame.width - 10)/3)
    }
    
    func unselectedFollow(){
        print("User: \(self.userID) is not being followed")
        self.followButton.setTitle("Follow", for: .normal)
        self.followButton.setTitleColor(tileColor, for: .normal)
        self.followButton.backgroundColor = backgroundColor
        self.doesSignedInUserFollow = false
    }
    
    func selectedFollow(){
        print("User: \(self.userID) is being followed")
        self.followButton.setTitle("Following", for: .normal)
        self.followButton.setTitleColor(backgroundColor, for: .normal)
        self.followButton.backgroundColor = tileColor
        self.doesSignedInUserFollow = true
    }
    
    
}
