////
////  SearchBarTopTagsViewController.swift
////  TileRecovery
////
////  Created by Thomas M. Jumper on 9/20/18.
////  Copyright © 2018 Tile Technology LLC. All rights reserved.
////
//
//import UIKit
//
//class SearchBarTopTagsViewController: UIViewController {
//    //Gesture Test View
//    var gestureTestView:UIView!
//
//    var topTagsCollectionView:UICollectionView!
//    var myView: UIView!
//    var searchBar: UITextField!
//    var searchTagsCollectionView: UICollectionView!
//    var keyboardIsOpen:Bool = false
//    var PictureView: UIImageView!
//    var PictureViewBackground:UIView!
//
//    var genericArray:[String] = ["A","B","C","D","E","F","G","Ab","Abc","zach"]
//    var currentGenericArray:[String]!{
//        didSet{
//            searchTagsCollectionView.reloadData()
//        }
//    }
//    var tagsSelected:[String] = [String]()
//    var topFiveTags:[String] = ["First Top Tag", "Second Top Tag", "Third Top Tag", "Fourth Top Tag","Fifth Top Tag",""]
//
//    let keyboardSlider = KeyboardSlider()
//
//    //    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
//    //        print("Inside of test gesture recognizer")
//    //        return true
//    //    }
//
//
//
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        //Gesture Test View
//        gestureTestView = UIView(frame: self.view.bounds)
//        self.view.addSubview(gestureTestView)
//        self.view.bringSubview(toFront: gestureTestView)
//        gestureTestView.translatesAutoresizingMaskIntoConstraints = false
//        gestureTestView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
//        gestureTestView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
//        gestureTestView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
//        gestureTestView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
//
//        //Keyboard slider come back and undo
//        //keyboardSlider.subscribeToKeyboardNotifications(view: view)
//
//        //Top Tags
//        let topTagslayout = UICollectionViewFlowLayout()
//        topTagslayout.scrollDirection = .vertical
//        topTagslayout.minimumLineSpacing = 2
//        topTagslayout.minimumInteritemSpacing = 1
//        topTagslayout.itemSize = CGSize(width: (self.view.bounds.width-6)/3, height: self.view.bounds.width/6)
//        print("Top safe area insets: \(self.view.safeAreaInsets)")
//        topTagsCollectionView = UICollectionView(frame: CGRect(x: 0, y: 20, width: self.view.bounds.width, height: (self.view.bounds.width/3)),collectionViewLayout: topTagslayout)
//        topTagsCollectionView.register(TopTagsCollectionCell.self, forCellWithReuseIdentifier: "TopCellIdentifier")
//        topTagsCollectionView.dataSource = self
//        topTagsCollectionView.delegate = self
//        self.view.addSubview(topTagsCollectionView)
//
//        //myView
//        //Changed
//        //  myView = UIView(frame: CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height))
//        myView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
//        myView.backgroundColor = .orange
//        self.view.addSubview(myView)
//
//        //Picture View Background
//        //        PictureViewBackground = UIView(frame: self.view.frame)
//        //        self.view.addSubview(PictureViewBackground)
//        //        let viewTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped(gestureRecognizer:)))
//        //        viewTapGestureRecognizer.cancelsTouchesInView = false
//        //        viewTapGestureRecognizer.delegate = self as? UIGestureRecognizerDelegate
//        //        self.PictureViewBackground.addGestureRecognizer(viewTapGestureRecognizer)
//
//        //PictureView
//        PictureView = UIImageView(frame: self.view.frame)
//        self.view.addSubview(PictureView)
//        let viewTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped(gestureRecognizer:)))
//        viewTapGestureRecognizer.cancelsTouchesInView = false
//        viewTapGestureRecognizer.delegate = self as? UIGestureRecognizerDelegate
//        viewTapGestureRecognizer.delegate = PreviewController.self as? UIGestureRecognizerDelegate
//        self.PictureView.addGestureRecognizer(viewTapGestureRecognizer)
//
//
//
//        //SearchBar
//        searchBar = UITextField(frame: CGRect(x: 0, y: 0, width: self.myView.frame.width, height: self.myView.frame.height))
//        searchBar.delegate = self
//        searchBar.autocorrectionType = .no
//        searchBar.keyboardType = .default
//        searchBar.addTarget(self, action: #selector(SearchBarTopTagsViewController.textFieldDidChange), for: .editingChanged)
//        self.myView.addSubview(searchBar)
//
//        //Search Tags
//        let searchTagslayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
//        searchTagslayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
//        searchTagslayout.itemSize = CGSize(width: (UIScreen.main.bounds.width-1)/3, height: (UIScreen.main.bounds.width-1)/2)
//        searchTagslayout.minimumInteritemSpacing = 1
//        searchTagslayout.sectionInset = UIEdgeInsets(top: 0, left: 1, bottom: 0, right: 0)
//        searchTagslayout.minimumLineSpacing = 1
//        searchTagslayout.scrollDirection = .horizontal
//        self.searchTagsCollectionView = UICollectionView(frame: CGRect(x: 0, y: self.myView.frame.height, width: self.myView.frame.width, height: 100), collectionViewLayout: searchTagslayout)
//        searchTagsCollectionView.backgroundColor = .clear
//        searchTagsCollectionView.contentInset = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
//
//        //searchTagsCollectionView.delegate = self
//        searchTagsCollectionView.dataSource = self
//        searchTagsCollectionView.delegate = self
//        searchTagsCollectionView.showsHorizontalScrollIndicator = false
//        self.myView.addSubview(searchTagsCollectionView)
//        searchTagsCollectionView.register(SearchTagsCollectionCell.self, forCellWithReuseIdentifier: "SearchCellIdentifier")
//        currentGenericArray = genericArray
//
//        searchBar.becomeFirstResponder()
//        searchTagsCollectionView.allowsSelection = true
//        //Keyboard slider come back and undo
//
//        keyboardSlider.subscribeToKeyboardNotifications(view: self.myView)
//        setUpLayout()
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        keyboardSlider.unsubscribeFromKeyboardNotifications()
//    }
//
//}
//
////import Foundation
////import UIKit
////
////extension SearchBarTopTagsViewController:UICollectionViewDataSource{
////    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
////        //Search Cells
////        if collectionView == self.searchTagsCollectionView{
////            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchCellIdentifier", for: indexPath) as! SearchTagsCollectionCell
////            if indexPath.section == 0{
////                cell.collectionLabel.text = tagsSelected[indexPath.item]
////                print("cell in section: \(indexPath.section)\titem:\(indexPath.item) turned blue")
////                cell.backgroundColor = .blue
////                cell.collectionLabel.textColor = .white
////            }
////            else if indexPath.section == 1{
////                cell.backgroundColor = .white
////                cell.collectionLabel.textColor = UIColor.black
////                cell.collectionLabel.text = currentGenericArray[indexPath.item]
////            }
////            cell.layer.masksToBounds = true
////            cell.layer.cornerRadius = cell.bounds.width/20
////            return cell
////        }
////            //Top Cells
////        else{
////            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopCellIdentifier", for: indexPath) as! TopTagsCollectionCell
////            cell.backgroundColor = .blue
////            if indexPath.item == 5{
////                //cell.setUpButton()
////                //cell.searchButtonDelegate = self
////                cell.collectionLabel.text = "Tap To Search"
////                return cell
////            }
////            cell.collectionLabel.textColor = .white
////            cell.collectionLabel.text = topFiveTags[indexPath.item]
////            return cell
////        }
////    }
////
////    func numberOfSections(in collectionView: UICollectionView) -> Int {
////        if collectionView == searchTagsCollectionView{
////            return 2
////        }
////        else{
////            return 1
////        }
////    }
////
////    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
////        print("section number: \(section)")
////        if collectionView == self.searchTagsCollectionView{
////            if section == 0{
////                return tagsSelected.count
////            }
////            else{
////                return currentGenericArray.count
////            }
////        }
////        else{
////            return 6
////        }
////    }
////
////    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
////        print("Inside of didSelectItemAterwertw")
////        if collectionView == searchTagsCollectionView{
////            print("Inside of didSelectItemAterwertw")
////            if indexPath.section == 1{
////                print("indexPath selected: \(indexPath.item)")
////                print("Size of currentGenericArray: \(currentGenericArray.count)")
////
////                print("Selected item: \(currentGenericArray[indexPath.item])")
////                if(tagsSelected.contains(currentGenericArray[indexPath.item])){
////                    print("This element is already in the array")
////                }
////
////
////                //tagsSelected.insert(currentGenericArray[indexPath.item], at: 0)
////                tagsSelected.append(currentGenericArray[indexPath.item])
////                //tagsSelected.insert(currentGenericArray[indexPath.item], at: tagsSelected.count)
////                if genericArray.count > 0{
////                    for i in 0...genericArray.count-1{
////                        if(currentGenericArray[indexPath.item] == genericArray[i]){
////                            print("index:\(i)->Item removed: \(genericArray[i])")
////                            genericArray.remove(at: i)
////                            break
////                        }
////                    }
////                }
////                currentGenericArray.remove(at: indexPath.item)
////                //new stuff
////                //currentGenericArray = genericArray
////                searchBar.text = ""
////                //
////                searchTagsCollectionView.reloadData()
////                if searchTagsCollectionView.numberOfItems(inSection: 1)>0{
////                    searchTagsCollectionView.scrollToItem(at: IndexPath(item: 0, section: 1), at: .right, animated: true)
////                }
////            }
////            else if indexPath.section == 0{
////                currentGenericArray.append(tagsSelected[indexPath.item])
////                tagsSelected.remove(at: indexPath.item)
////                searchTagsCollectionView.reloadData()
////            }
////        }
////        else if collectionView == topTagsCollectionView && indexPath == IndexPath(item: 5, section: 0) {
////            myView.isHidden = false
////            keyboardIsOpen = !keyboardIsOpen
////            searchBar.becomeFirstResponder()
////            return
////        }
////    }
////
////
////
////}
////
////import Foundation
////import UIKit
////
////extension SearchBarTopTagsViewController:UICollectionViewDelegateFlowLayout{
////
////    func setUpLayout(){
////        //Top Tags CollectionView
////        topTagsCollectionView.contentInset = UIEdgeInsets(top: 2, left: 2, bottom: -2, right: 2)
////        topTagsCollectionView.translatesAutoresizingMaskIntoConstraints = false
////        topTagsCollectionView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
////        topTagsCollectionView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
////        topTagsCollectionView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
////        topTagsCollectionView.heightAnchor.constraint(equalToConstant:(self.view.bounds.width/3)+4).isActive = true
////
////        //myView
////        self.myView.translatesAutoresizingMaskIntoConstraints = false
////        self.myView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
////        self.myView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
////        self.myView.heightAnchor.constraint(equalToConstant: 98).isActive = true
////        self.myView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
////
////        //PictureView
////        self.PictureView.translatesAutoresizingMaskIntoConstraints = false
////        self.PictureView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
////        self.PictureView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
////        self.PictureView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
////        self.PictureView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
////        self.PictureView.backgroundColor = .purple
////        self.view.sendSubview(toBack: PictureView)
////
////        //        //PictureView Background
////        //        self.PictureViewBackground.translatesAutoresizingMaskIntoConstraints = false
////        //        self.PictureViewBackground.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
////        //        self.PictureViewBackground.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
////        //        self.PictureViewBackground.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
////        //        self.PictureViewBackground.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
////        //        self.PictureViewBackground.backgroundColor = .yellow
////        //        self.view.sendSubview(toBack: PictureViewBackground)
////
////        //Search Tags CollectionView
////        self.searchTagsCollectionView.translatesAutoresizingMaskIntoConstraints = false
////        self.searchTagsCollectionView.leadingAnchor.constraint(equalTo: self.myView.leadingAnchor, constant: 0).isActive = true
////        self.searchTagsCollectionView.trailingAnchor.constraint(equalTo: self.myView.trailingAnchor, constant: 0).isActive = true
////        self.searchTagsCollectionView.bottomAnchor.constraint(equalTo: self.myView.bottomAnchor, constant: -4).isActive = true
////        self.searchTagsCollectionView.topAnchor.constraint(equalTo: self.searchBar.bottomAnchor, constant: 4).isActive = true
////        //self.searchTagsCollectionView.heightAnchor.constraint(equalToConstant: 45).isActive = true
////
////        //Search Bar
////        self.searchBar.translatesAutoresizingMaskIntoConstraints = false
////        self.searchBar.topAnchor.constraint(equalTo: self.myView.topAnchor, constant: 0).isActive = true
////        self.searchBar.leadingAnchor.constraint(equalTo: self.myView.leadingAnchor, constant: 4).isActive = true
////        self.searchBar.trailingAnchor.constraint(equalTo: self.myView.trailingAnchor, constant: -4).isActive = true
////        self.searchBar.heightAnchor.constraint(equalToConstant: 45).isActive = true
////        searchBar.backgroundColor = .white
////        searchBar.layer.borderWidth = 1
////        searchBar.layer.cornerRadius = searchBar.frame.width/50
////        searchBar.layer.borderColor = UIColor.black.cgColor
////    }
////}
////
////import Foundation
////import UIKit
////
////extension SearchBarTopTagsViewController:UIGestureRecognizerDelegate{
////    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
////        //        if (touch.view?.isDescendant(of: self.collectionView))!{
////        //            return false
////        //        }
////        //        return true
////
////        print("Inside of recognizer")
////        return true
////    }
////
////
////
////    @objc func viewTapped(gestureRecognizer:UIGestureRecognizer){
////
////        print("Picture View Tapped keyboardIsOpen: \(keyboardIsOpen):")
////        ////        let p = gestureRecognizer.location(in: self.collectionView)
////        ////        let indexPath = self.collectionView.indexPathForItem(at: p)
////        ////
////        ////        print("The currentFirst responder is: \(self.view.currentFirstResponder())")
////        ////
////        ////        if let index = indexPath {
////        ////            var cell = self.collectionView.cellForItem(at: index)
////        ////            // do stuff with your cell, for example print the indexPath
////        ////            print(index.row)
////        ////        } else {
////        ////            print("Could not find index path")
////        ////        }
////        //   //     self.view.becomeFirstResponder()
////
////
////        //Good Code
////        //        if searchBar.isFirstResponder{
////        //            myView.isHidden = true
////        //            searchBar.isHidden = true
////        //            collectionView.isHidden = true
////        //            searchBar.resignFirstResponder()
////        //        }
////        //        else{
////        //            myView.isHidden = false
////        //            searchBar.isHidden = false
////        //            collectionView.isHidden = false
////        //            self.view.bringSubview(toFront: collectionView)
////        //            //searchBar.becomeFirstResponder()
////        //            gestureRecognizer.cancelsTouchesInView = false
////        //            self.searchBar.becomeFirstResponder()
////        //            //self.collectionView.becomeFirstResponder()
////        //        }
////
////        if keyboardIsOpen{
////            myView.isHidden = true
////            keyboardIsOpen = !keyboardIsOpen
////            searchBar.resignFirstResponder()
////        }
////
////        else{
////            myView.isHidden = false
////            keyboardIsOpen = !keyboardIsOpen
////            searchBar.becomeFirstResponder()
////        }
////
////
////    }
////}
////
////import Foundation
////import UIKit
////
////extension SearchBarTopTagsViewController:UITextFieldDelegate{
////    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
////        textField.resignFirstResponder()
////
////        //        currentGenericArray = genericArray.filter({letter -> Bool in
////        //            letter.lowercased().contains(searchBar.text!)
////        //        })
////        //        if currentGenericArray.isEmpty{
////        //            print("This element is not in the current array")
////        //        }
////        searchTagsCollectionView.reloadData()
////        return true
////    }
////
////    /// Helper to dismiss keyboard
////    @objc func didStopEditing() {
////        // textFieldShouldReturn(phoneNumberTextField)
////    }
////
////    func textFieldDidEndEditing(_ textField: UITextField) {
////        UIView.setAnimationCurve(UIViewAnimationCurve.easeInOut)
////        UIView.animate(withDuration: 0.2) {
////            self.view.frame.origin.y = 0
////        }
////    }
////
////    @objc func textFieldDidChange(){
////        print("inside of text field did change")
////        guard(!(searchBar.text?.isEmpty)!) else{
////            print("The search bar is empty")
////            currentGenericArray = genericArray
////            searchTagsCollectionView.reloadData()
////            return
////        }
////        print("The search bar was not empty it says: \(searchBar.text)")
////
////        currentGenericArray = genericArray.filter({letter -> Bool in
////            //letter.lowercased().contains(searchBar.text!)
////            //mew stuff
////            if searchBar.text!.count > letter.count{
////                return false
////            }
////            let stringRange = letter.index(letter.startIndex, offsetBy: searchBar.text!.count)
////            let subword = letter[..<stringRange]
////            return subword.lowercased().contains(searchBar.text!.lowercased())
////            //
////            //If broken delete above and uncoment below
////            //letter.lowercased().contains(searchBar.text!.lowercased())
////
////        })
////
////        if currentGenericArray.isEmpty{
////            print("text being inserted \(searchBar.text!)")
////            //currentGenericArray.insert(searchBar.text!, at: 0)
////            currentGenericArray.append(searchBar.text!)
////        }
////
////        searchTagsCollectionView.reloadData()
////    }
////}
////




//
//  ViewController.swift
//  topCollectionView
//
//  Created by 123456 on 8/9/18.
//  Copyright © 2018 123456. All rights reserved.
//

import UIKit

protocol postsButtonDelegate: class{
    func disablePostButton(isEnabled:Bool)
}

class SearchBarTopTagsViewController: UIViewController {
    
    weak var selectedSearchTagDelegate:SelectedSearchTagDelegate!
    
    //Gesture Test View
    var gestureTestView:UIView!
    
    var topTagsCollectionView:UICollectionView!
    var myView: UIView!
    var searchBar: UITextField!
    var searchTagsCollectionView: UICollectionView!
    var keyboardIsOpen:Bool = false
    var PictureView: UIImageView!
    var myViewBottomLayoutConstraint:NSLayoutConstraint!
    //var PictureViewBackground:UIView!
    weak var postsButtonDelegate:postsButtonDelegate!
    
    var genericArray:[String] = []//["A","B","C","D","E","F","G","Ab","Abc","zach"]
    var searchTagsArray:[String]!{
        didSet{
            searchTagsCollectionView.reloadData()
        }
    }
    var tagsSelected:[String] = [String]()
    var topFiveTags:[String] = ["Delta Chi", "La Villa", "1 Oak"]//, "","",""]
    var wasSearchTagSelected:Bool = false
//    var wasTopTagSelected:Bool = false
    
    var selectedIndexPath:IndexPath? = nil{
        didSet{
            guard let selectedIndexPath = selectedIndexPath else {
                return
            }
            self.topTagsCollectionView.reloadData()
        }
    }
    //let keyboardSlider = KeyboardSlider()
    
    //    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    //        print("Inside of test gesture recognizer")
    //        return true
    //    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Gesture Test View
        gestureTestView = UIView(frame: self.view.bounds)
        self.view.addSubview(gestureTestView)
        self.view.bringSubviewToFront(gestureTestView)
        gestureTestView.translatesAutoresizingMaskIntoConstraints = false
        gestureTestView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        gestureTestView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        gestureTestView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        gestureTestView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
        //Keyboard slider come back and undo
        //keyboardSlider.subscribeToKeyboardNotifications(view: view)
        
        //Top Tags
        let topTagslayout = UICollectionViewFlowLayout()
        topTagslayout.scrollDirection = .vertical
        topTagslayout.minimumLineSpacing = 10
        topTagslayout.minimumInteritemSpacing = 2
        topTagslayout.itemSize = CGSize(width: (self.view.bounds.width-6)/3.1, height: self.view.bounds.width/10 + 10)  //6
        
        
        
        //topTagslayout.layer.borderColor = colors.toolBarColor.cgColor
        // topTagslayout.layer.borderWidth = 2
        //topTagslayout.layer.cornerRadius = followButton.bounds.width/5
        
        print("Top safe area insets: \(self.view.safeAreaInsets)")
        print("self.view.bounds: \(self.view.bounds)")
        topTagsCollectionView = UICollectionView(frame: CGRect(x: 0, y: 20, width: self.view.bounds.width, height: (self.view.bounds.width/3)),collectionViewLayout: topTagslayout)
        topTagsCollectionView.register(TopTagsCollectionCell.self, forCellWithReuseIdentifier: "TopCellIdentifier")
        topTagsCollectionView.dataSource = self
        topTagsCollectionView.delegate = self
        topTagsCollectionView.backgroundColor = UIColor.clear
        self.view.addSubview(topTagsCollectionView)
        
        //myView
        //Changed
        //  myView = UIView(frame: CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height))
        myView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        myView.backgroundColor = UIColor(red: 230/255, green: 252/255, blue: 245/255, alpha: 1.0)
        self.view.addSubview(myView)
        
        //Picture View Background
        //        PictureViewBackground = UIView(frame: self.view.frame)
        //        self.view.addSubview(PictureViewBackground)
        //        let viewTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped(gestureRecognizer:)))
        //        viewTapGestureRecognizer.cancelsTouchesInView = false
        //        viewTapGestureRecognizer.delegate = self as? UIGestureRecognizerDelegate
        //        self.PictureViewBackground.addGestureRecognizer(viewTapGestureRecognizer)
        
        //PictureView
        PictureView = UIImageView(frame: self.view.frame)
        self.view.addSubview(PictureView)
        //uncomment out
        let viewTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped(gestureRecognizer:)))
        viewTapGestureRecognizer.cancelsTouchesInView = false
        viewTapGestureRecognizer.delegate = self as? UIGestureRecognizerDelegate
        viewTapGestureRecognizer.delegate = PreviewController.self as? UIGestureRecognizerDelegate
        self.PictureView.addGestureRecognizer(viewTapGestureRecognizer)
        
        
        
        //SearchBar
        searchBar = UITextField(frame: CGRect(x: 0, y: self.view.bounds.maxY-98, width: self.myView.bounds.width, height: 98))
        searchBar.delegate = self
        searchBar.autocorrectionType = .no
        searchBar.keyboardType = .default
        searchBar.addTarget(self, action: #selector(SearchBarTopTagsViewController.textFieldDidChange), for: .editingChanged)
        self.myView.addSubview(searchBar)
        
        //Search Tags
        let searchTagslayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        // searchTagslayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        //    searchTagslayout.itemSize = CGSize(width: (UIScreen.main.bounds.width-1)/3, height: (UIScreen.main.bounds.width-1)/2)
        searchTagslayout.itemSize = CGSize(width: (UIScreen.main.bounds.width-1)/3, height: (UIScreen.main.bounds.width-1)/2)
        searchTagslayout.minimumInteritemSpacing = 1
        searchTagslayout.sectionInset = UIEdgeInsets(top: 0, left: 1, bottom: 0, right: 0)
        searchTagslayout.minimumLineSpacing = 1
        searchTagslayout.scrollDirection = .horizontal
        self.searchTagsCollectionView = UICollectionView(frame: CGRect(x: 0, y: self.myView.bounds.maxY - 45, width: self.myView.frame.width, height: 45), collectionViewLayout: searchTagslayout)
        //searchTagsCollectionView.backgroundColor = .clear
        searchTagsCollectionView.backgroundColor = colors.toolBarColor
        searchTagsCollectionView.contentInset = UIEdgeInsets(top: 4, left: 0, bottom: 4, right: 4)
        searchTagsCollectionView.allowsSelection = true
        searchTagsCollectionView.allowsMultipleSelection = true
        searchTagsCollectionView.delegate = self
        searchTagsCollectionView.dataSource = self
        // searchTagsCollectionView.delegate = self
        searchTagsCollectionView.showsHorizontalScrollIndicator = false
        self.myView.addSubview(searchTagsCollectionView)
        searchTagsCollectionView.register(SearchTagsCollectionCell.self, forCellWithReuseIdentifier: "SearchCellIdentifier")
        self.searchTagsArray = genericArray
        
        //searchBar.becomeFirstResponder()
        searchTagsCollectionView.allowsSelection = true
        //Keyboard slider come back and undo
        
        //keyboardSlider.subscribeToKeyboardNotifications(view: self.myView)
        //   setUpLayout()
        
        myView.isHidden = true
        self.view.translatesAutoresizingMaskIntoConstraints = false
        
        postsButtonDelegate.disablePostButton(isEnabled: false)
        print("made it to the end of viewdidload in searchbartoptags")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //keyboardSlider.unsubscribeFromKeyboardNotifications()
    }
    
}
