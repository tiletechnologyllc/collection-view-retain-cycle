//
//  Extensions-SearchBarTopTags 16.2 copy.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 9/20/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import Foundation
//import UIKit
//import Foundation
//
//extension UIView {
//    func currentFirstResponder() -> UIResponder? {
//        if self.isFirstResponder {
//            return self
//        }
//
//        for view in self.subviews {
//            if let responder = view.currentFirstResponder() {
//                return responder
//            }
//        }
//
//        return nil
//    }
//}
//
//extension Notification.Name{
//    static let showKeyboard = Notification.Name("showKeyboard")
//    static let hideKeyboard = Notification.Name("hideKeyboard")
//}
//
//class KeyboardSlider: NSObject {
//    // variables to hold and process information from the view using this class
//    weak var view: UIView?
//    weak var searchBarTopTags:SearchBarTopTagsViewController?
//    var originalOrigin:CGFloat = CGFloat(0)
//    var firstHide = true
//    @objc func keyboardWillShow(notification: NSNotification) {
//        // method to move keyboard up
//        print("previous origin: \(view?.frame.origin.y)")
//        if firstHide{
//            originalOrigin = (view?.frame.origin.y)!
//            firstHide = !firstHide
//        }
//        //view?.frame.origin.y = 0 - getKeyboardHeight(notification as Notification)
//        view?.frame.origin.y = (view?.frame.origin.y)! - getKeyboardHeight(notification as Notification) + (self.searchBarTopTags?.view.safeAreaInsets.bottom)!
//
//        print("new origin: \(view?.frame.origin.y)")
//    }
//
//    @objc func keyboardWillHide(notification: NSNotification){
//        //view?.frame.origin.y = 0
//        print("frame origin in keyBoardWillHide: \(originalOrigin)")
//        view?.frame.origin.y = originalOrigin
//    }
//
//    func getKeyboardHeight(_ notification:Notification) -> CGFloat {
//        // get exact height of keyboard on all devices and convert to float value to return for use
//        let userInfo = notification.userInfo
//        let keyboardSize = userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue
//        return keyboardSize.cgRectValue.height
//    }
//
//    func subscribeToKeyboardNotifications(view: UIView, searchBarTopTags:SearchBarTopTagsViewController? = nil) {
//        // assigning view to class' counterpart'
//        print("Inside of subscribe to notifications")
//        self.view = view
//        if let searchBarTopTags = searchBarTopTags{
//            self.view = searchBarTopTags.myView
//            self.searchBarTopTags = searchBarTopTags
//            originalOrigin = view.frame.origin.y
//            print("myView Origin in initialization: \(view.frame.origin.y)")
//        }
//        // when UIKeyboardWillShow do keyboardWillShow function
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
//    }
//
//    func unsubscribeFromKeyboardNotifications() {
//        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
//    }
//
//
//}

//import UIKit
//import Foundation
//
//extension UIView {
//    func currentFirstResponder() -> UIResponder? {
//        if self.isFirstResponder {
//            return self
//        }
//
//        for view in self.subviews {
//            if let responder = view.currentFirstResponder() {
//                return responder
//            }
//        }
//
//        return nil
//    }
//}
//
//extension Notification.Name{
//    static let showKeyboard = Notification.Name("showKeyboard")
//}
//
//class KeyboardSlider: NSObject {
//    // variables to hold and process information from the view using this class
//    weak var view: UIView?
//    var searchBarTopTags:SearchBarTopTagsViewController?
//    var amountToShiftBy:CGFloat!
//    var originalFrame:CGRect!
//
//    @objc func keyboardWillShow(notification: NSNotification) {
//        // method to move keyboard up
//       // view?.frame.origin.y = 0 - getKeyboardHeight(notification as Notification)
//
//        ///
//        self.originalFrame = self.searchBarTopTags?.myView.frame
//        //self.amountToShiftBy = (self.searchBarTopTags?.view.frame.maxY)! - self.getKeyboardHeight(notification as! Notification) - (self.searchBarTopTags?.myView.frame.height)!
//     //   self.searchBarTopTags?.myView.frame.origin.y = self.amountToShiftBy
//      //  self.searchBarTopTags?.myView.frame = CGRect(x: 0, y: self.amountToShiftBy, width: (self.searchBarTopTags?.view.frame.width)!, height: 98)
//        self.amountToShiftBy = (searchBarTopTags?.view.bounds.height)! - self.getKeyboardHeight(notification as! Notification) - (searchBarTopTags?.myView.bounds.height)!-100
//
//        self.searchBarTopTags?.myViewBottomLayoutConstraint.constant =  -self.amountToShiftBy
//        print("Keyboard height: \(self.getKeyboardHeight(notification as! Notification))")
//        print("viewHeight: \(searchBarTopTags?.myView.bounds.height)")
//        print("total view height: \(searchBarTopTags?.view.bounds.height)")
//        print("amount shifting: \(self.amountToShiftBy)")
//        UIView.animate(withDuration: 0, animations: {
//            self.view?.layoutIfNeeded()
//          //  self.searchBarTopTags?.view.layoutIfNeeded()
//            self.searchBarTopTags?.myView.layoutIfNeeded()
//
//            //self.searchBarTopTags.myTextField.frame.origin.y = self.amountToShiftBy
//
//        })
//
//        ///
//        print("made it to keyboard will show")
//    }
//
//    @objc func keyboardWillHide(notification:NSNotification){
//
//         self.searchBarTopTags?.myViewBottomLayoutConstraint.constant = 0
//        UIView.animate(withDuration: 0, animations: {
//            self.view?.layoutIfNeeded()
//          //  self.searchBarTopTags?.view.layoutIfNeeded()
//            self.searchBarTopTags?.myView.layoutIfNeeded()
//
//            //self.searchBarTopTags.myTextField.frame.origin.y = self.amountToShiftBy
//
//        })
//        print("made it to keyboard will show") //he
//    }
//    func getKeyboardHeight(_ notification:Notification) -> CGFloat {
//        // get exact height of keyboard on all devices and convert to float value to return for use
//        let userInfo = notification.userInfo
//        let keyboardSize = userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue
//        return keyboardSize.cgRectValue.height
//    }
//
//    func subscribeToKeyboardNotifications(view: UIView) {
//        // assigning view to class' counterpart
//        self.view = view
//        // when UIKeyboardWillShow do keyboardWillShow function
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
//    }
//
//
//    func subscribeToKeyboardNotifications(view: UIView, seachBarTopTagsVC:SearchBarTopTagsViewController? = nil) {
//        // assigning view to class' counterpart
//        self.view = view
//        self.searchBarTopTags = seachBarTopTagsVC
//        print("inside of subscribe: \(self.searchBarTopTags?.myView.frame.origin)")
//        print("inside of subscribe: \(self.searchBarTopTags?.myView.frame.maxY)")//hello
//
//        // when UIKeyboardWillShow do keyboardWillShow function
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
//    }
//
//
//    func unsubscribeFromKeyboardNotifications() {
//        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
//    }
//
//}

//import UIKit
//import Foundation
//
//extension UIView {
//    func currentFirstResponder() -> UIResponder? {
//        if self.isFirstResponder {
//            return self
//        }
//
//        for view in self.subviews {
//            if let responder = view.currentFirstResponder() {
//                return responder
//            }
//        }
//
//        return nil
//    }
//}
//
//extension Notification.Name{
//    static let showKeyboard = Notification.Name("showKeyboard")
//}
//
//class KeyboardSlider: NSObject {
//    // variables to hold and process information from the view using this class
//    weak var view: UIView?
//    var searchBarTopTags:SearchBarTopTagsViewController?
//    var amountToShiftBy:CGFloat!
//    var originalFrame:CGRect!
//    var previewController:PreviewController!
//
//    @objc func keyboardWillShow(notification: NSNotification) {
//        // method to move keyboard up
//        // view?.frame.origin.y = 0 - getKeyboardHeight(notification as Notification)
//
//        ///
//        self.originalFrame = self.searchBarTopTags?.myView.frame
//        //self.amountToShiftBy = (self.searchBarTopTags?.view.frame.maxY)! - self.getKeyboardHeight(notification as! Notification) - (self.searchBarTopTags?.myView.frame.height)!
//        //   self.searchBarTopTags?.myView.frame.origin.y = self.amountToShiftBy
//        //  self.searchBarTopTags?.myView.frame = CGRect(x: 0, y: self.amountToShiftBy, width: (self.searchBarTopTags?.view.frame.width)!, height: 98)
//        //self.amountToShiftBy = (searchBarTopTags?.view.bounds.height)! - self.getKeyboardHeight(notification as! Notification) - (searchBarTopTags?.myView.frame.height)!
//        self.searchBarTopTags?.myViewBottomLayoutConstraint.constant =  -self.getKeyboardHeight(notification as! Notification) + previewController.view.safeAreaInsets.bottom
//        print("Keyboard height: \(self.getKeyboardHeight(notification as! Notification))")
//        print("viewHeight: \(searchBarTopTags?.myView.bounds.height)")
//        print("total view height: \(previewController.view.bounds.height)")
//        print("amount shifting: \(self.amountToShiftBy)")
//        UIView.animate(withDuration: 0, animations: {
//
//            self.previewController.view.layoutIfNeeded()
//           // self.view?.layoutIfNeeded()
//
//            //  self.searchBarTopTags?.view.layoutIfNeeded()
//            //self.searchBarTopTags?.myView.layoutIfNeeded()
//
//            //self.searchBarTopTags.myTextField.frame.origin.y = self.amountToShiftBy
//
//        })
//
//        ///
//        print("made it to keyboard will show")
//    }
//
//    @objc func keyboardWillHide(notification:NSNotification){
//
//        self.searchBarTopTags?.myViewBottomLayoutConstraint.constant = 0
//        UIView.animate(withDuration: 0, animations: {
//            self.view?.layoutIfNeeded()
//            self.previewController.view.layoutIfNeeded()
////            self.searchBarTopTags?.view.layoutIfNeeded()
////            self.searchBarTopTags?.myView.layoutIfNeeded()
//
//            //self.searchBarTopTags.myTextField.frame.origin.y = self.amountToShiftBy
//
//        })
//        print("made it to keyboard will show")
//    }
//    func getKeyboardHeight(_ notification:Notification) -> CGFloat {
//        // get exact height of keyboard on all devices and convert to float value to return for use
//        let userInfo = notification.userInfo
//        let keyboardSize = userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue
//        return keyboardSize.cgRectValue.height
//    }
//
//    func subscribeToKeyboardNotifications(view: UIView) {
//        // assigning view to class' counterpart
//        self.view = view
//        // when UIKeyboardWillShow do keyboardWillShow function
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
//    }
//
//
//    func subscribeToKeyboardNotifications(view: UIView, previewController:PreviewController? = nil) {
//        // assigning view to class' counterpart
//        self.view = view
//        self.searchBarTopTags = previewController?.searchBarTopTags
//        self.previewController = previewController
//        print("inside of subscribe: \(self.searchBarTopTags?.myView.frame.origin)")
//        print("inside of subscribe: \(self.searchBarTopTags?.myView.frame.maxY)")
//
//        // when UIKeyboardWillShow do keyboardWillShow function
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
//    }
//
//
//    func unsubscribeFromKeyboardNotifications() {
//        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
//    }
//
//}


//import UIKit
//import Foundation
//
//extension UIView {
//    func currentFirstResponder() -> UIResponder? {
//        if self.isFirstResponder {
//            return self
//        }
//
//        for view in self.subviews {
//            if let responder = view.currentFirstResponder() {
//                return responder
//            }
//        }
//
//        return nil
//    }
//}
//
//extension Notification.Name{
//    static let showKeyboard = Notification.Name("showKeyboard")
//}
//
//class KeyboardSlider: NSObject {
//    // variables to hold and process information from the view using this class
//    weak var view: UIView?
//    var searchBarTopTags:SearchBarTopTagsViewController?
//    var amountToShiftBy:CGFloat!
//    var originalFrame:CGRect!
//    var previewController:PreviewController!
//
//    @objc func keyboardWillShow(notification: NSNotification) {
//        // method to move keyboard up
//        // view?.frame.origin.y = 0 - getKeyboardHeight(notification as Notification)
//
//        ///
//        self.originalFrame = self.searchBarTopTags?.myView.frame
//        //self.amountToShiftBy = (self.searchBarTopTags?.view.frame.maxY)! - self.getKeyboardHeight(notification as! Notification) - (self.searchBarTopTags?.myView.frame.height)!
//        //   self.searchBarTopTags?.myView.frame.origin.y = self.amountToShiftBy
//        //  self.searchBarTopTags?.myView.frame = CGRect(x: 0, y: self.amountToShiftBy, width: (self.searchBarTopTags?.view.frame.width)!, height: 98)
//        //self.amountToShiftBy = (searchBarTopTags?.view.bounds.height)! - self.getKeyboardHeight(notification as! Notification) - (searchBarTopTags?.myView.frame.height)!
//        self.searchBarTopTags?.myViewBottomLayoutConstraint.constant =  -self.getKeyboardHeight(notification as! Notification) + previewController.view.safeAreaInsets.bottom
//        print("Keyboard height: \(self.getKeyboardHeight(notification as! Notification))")
//        print("viewHeight: \(searchBarTopTags?.myView.bounds.height)")
//        print("total view height: \(previewController.view.bounds.height)")
//        print("amount shifting: \(self.amountToShiftBy)")
//        UIView.animate(withDuration: 0, animations: {
//
//            self.previewController.view.layoutIfNeeded()
//            // self.view?.layoutIfNeeded()
//
//            //  self.searchBarTopTags?.view.layoutIfNeeded()
//            //self.searchBarTopTags?.myView.layoutIfNeeded()
//
//            //self.searchBarTopTags.myTextField.frame.origin.y = self.amountToShiftBy
//
//        })
//
//        ///
//        print("made it to keyboard will show")
//    }
//
//    @objc func keyboardWillHide(notification:NSNotification){
//
//        self.searchBarTopTags?.myViewBottomLayoutConstraint.constant = 0
//        UIView.animate(withDuration: 0, animations: {
//            self.view?.layoutIfNeeded()
//            self.previewController.view.layoutIfNeeded()
//            //            self.searchBarTopTags?.view.layoutIfNeeded()
//            //            self.searchBarTopTags?.myView.layoutIfNeeded()
//
//            //self.searchBarTopTags.myTextField.frame.origin.y = self.amountToShiftBy
//
//        })
//        print("made it to keyboard will show")
//    }
//    func getKeyboardHeight(_ notification:Notification) -> CGFloat {
//        // get exact height of keyboard on all devices and convert to float value to return for use
//        let userInfo = notification.userInfo
//        let keyboardSize = userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue
//        return keyboardSize.cgRectValue.height
//    }
//
//    func subscribeToKeyboardNotifications(view: UIView) {
//        // assigning view to class' counterpart
//        self.view = view
//        // when UIKeyboardWillShow do keyboardWillShow function
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
//    }
//
//
//    func subscribeToKeyboardNotifications(view: UIView, previewController:PreviewController? = nil) {
//        // assigning view to class' counterpart
//        self.view = view
//        self.searchBarTopTags = previewController?.searchBarTopTags
//        self.previewController = previewController
//        print("inside of subscribe: \(self.searchBarTopTags?.myView.frame.origin)")
//        print("inside of subscribe: \(self.searchBarTopTags?.myView.frame.maxY)")
//
//        // when UIKeyboardWillShow do keyboardWillShow function
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
//    }
//
//
//    func unsubscribeFromKeyboardNotifications() {
//        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
//    }
//
//}

import UIKit
import Foundation

extension UIView {
    func currentFirstResponder() -> UIResponder? {
        if self.isFirstResponder {
            return self
        }
        
        for view in self.subviews {
            if let responder = view.currentFirstResponder() {
                return responder
            }
        }
        
        return nil
    }
}

extension Notification.Name{
    static let showKeyboard = Notification.Name("showKeyboard")
}

class SearchBarTopTagsKeyboardSlider: NSObject {
    // variables to hold and process information from the view using this class
    weak var view: UIView?
    var searchBarTopTags:SearchBarTopTagsViewController?
    var amountToShiftBy:CGFloat!
    var originalFrame:CGRect!
    var previewController:PreviewController!
    
    @objc func keyboardWillShow(notification: NSNotification) {
        // method to move keyboard up
        // view?.frame.origin.y = 0 - getKeyboardHeight(notification as Notification)
        
        ///
        self.originalFrame = self.searchBarTopTags?.myView.frame
        //self.amountToShiftBy = (self.searchBarTopTags?.view.frame.maxY)! - self.getKeyboardHeight(notification as! Notification) - (self.searchBarTopTags?.myView.frame.height)!
        //   self.searchBarTopTags?.myView.frame.origin.y = self.amountToShiftBy
        //  self.searchBarTopTags?.myView.frame = CGRect(x: 0, y: self.amountToShiftBy, width: (self.searchBarTopTags?.view.frame.width)!, height: 98)
        //self.amountToShiftBy = (searchBarTopTags?.view.bounds.height)! - self.getKeyboardHeight(notification as! Notification) - (searchBarTopTags?.myView.frame.height)!
        self.searchBarTopTags?.myViewBottomLayoutConstraint.constant =  -self.getKeyboardHeight(notification as Notification) + previewController.view.safeAreaInsets.bottom
        print("Keyboard height: \(self.getKeyboardHeight(notification as Notification))")
        print("viewHeight: \(searchBarTopTags?.myView.bounds.height)")
        print("total view height: \(previewController.view.bounds.height)")
        print("amount shifting: \(self.amountToShiftBy)")
        UIView.animate(withDuration: 0, animations: {
            
            self.previewController.view.layoutIfNeeded()
            // self.view?.layoutIfNeeded()
            
            //  self.searchBarTopTags?.view.layoutIfNeeded()
            //self.searchBarTopTags?.myView.layoutIfNeeded()
            
            //self.searchBarTopTags.myTextField.frame.origin.y = self.amountToShiftBy
            
        })
        
        ///
        print("made it to keyboard will show")
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        
        self.searchBarTopTags?.myViewBottomLayoutConstraint.constant = 0
        UIView.animate(withDuration: 0, animations: {
            self.view?.layoutIfNeeded()
            self.previewController.view.layoutIfNeeded()
            //            self.searchBarTopTags?.view.layoutIfNeeded()
            //            self.searchBarTopTags?.myView.layoutIfNeeded()
            
            //self.searchBarTopTags.myTextField.frame.origin.y = self.amountToShiftBy
            
        })
        print("made it to keyboard will show")
    }
    func getKeyboardHeight(_ notification:Notification) -> CGFloat {
        // get exact height of keyboard on all devices and convert to float value to return for use
        let userInfo = notification.userInfo
        let keyboardSize = userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        return keyboardSize.cgRectValue.height
    }
    
    func subscribeToKeyboardNotifications(view: UIView) {
        // assigning view to class' counterpart
        self.view = view
        // when UIKeyboardWillShow do keyboardWillShow function
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    
    func subscribeToKeyboardNotifications(view: UIView, previewController:PreviewController? = nil) {
        // assigning view to class' counterpart
        self.view = view
        self.searchBarTopTags = previewController?.searchBarTopTags
        self.previewController = previewController
        print("inside of subscribe: \(self.searchBarTopTags?.myView.frame.origin)")
        print("inside of subscribe: \(self.searchBarTopTags?.myView.frame.maxY)")
        
        // when UIKeyboardWillShow do keyboardWillShow function
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    func unsubscribeFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
}

