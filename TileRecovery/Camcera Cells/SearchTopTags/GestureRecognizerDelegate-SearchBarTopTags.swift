//
//  GestureRecognizerDelegate-SearchBarTopTags.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 9/20/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

extension SearchBarTopTagsViewController:UIGestureRecognizerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        //        if (touch.view?.isDescendant(of: self.collectionView))!{
        //            return false
        //        }
        //        return true
        
        print("Inside of recognizer")
        return true
    }
    
    
    
    @objc func viewTapped(gestureRecognizer:UIGestureRecognizer){
        
        print("Picture View Tapped")
        ////        let p = gestureRecognizer.location(in: self.collectionView)
        ////        let indexPath = self.collectionView.indexPathForItem(at: p)
        ////
        ////        print("The currentFirst responder is: \(self.view.currentFirstResponder())")
        ////
        ////        if let index = indexPath {
        ////            var cell = self.collectionView.cellForItem(at: index)
        ////            // do stuff with your cell, for example print the indexPath
        ////            print(index.row)
        ////        } else {
        ////            print("Could not find index path")
        ////        }
        //   //     self.view.becomeFirstResponder()
        
        
        //Good Code
        //        if searchBar.isFirstResponder{
        //            myView.isHidden = true
        //            searchBar.isHidden = true
        //            collectionView.isHidden = true
        //            searchBar.resignFirstResponder()
        //        }
        //        else{
        //            myView.isHidden = false
        //            searchBar.isHidden = false
        //            collectionView.isHidden = false
        //            self.view.bringSubview(toFront: collectionView)
        //            //searchBar.becomeFirstResponder()
        //            gestureRecognizer.cancelsTouchesInView = false
        //            self.searchBar.becomeFirstResponder()
        //            //self.collectionView.becomeFirstResponder()
        //        }
        
        if keyboardIsOpen{
            myView.isHidden = true
            keyboardIsOpen = !keyboardIsOpen
            searchBar.resignFirstResponder()
        }
            
        else{
            myView.isHidden = false
            keyboardIsOpen = !keyboardIsOpen
            searchBar.becomeFirstResponder()
        }
        
        
    }
}


