//
//  delegate-SearchBarTopTags.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 9/20/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

extension SearchBarTopTagsViewController:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Inside of didSelectItemAterwertw1")
        if collectionView == searchTagsCollectionView{
            print("Inside of didSelectItemAterwertw1: \(self.searchTagsArray[indexPath.item])")
            if indexPath.section == 1{
                //                print("indexPath selected: \(indexPath.item)")
                //                print("Size of currentGenericArray: \(currentGenericArray.count)")
                //
                //                print("Selected item: \(currentGenericArray[indexPath.item])")
                if(tagsSelected.contains(self.searchTagsArray[indexPath.item])){
                    print("This element is already in the array")
                }
                
                
                //tagsSelected.insert(currentGenericArray[indexPath.item], at: 0)
                tagsSelected.append(self.searchTagsArray[indexPath.item])
                //tagsSelected.insert(currentGenericArray[indexPath.item], at: tagsSelected.count)
                if genericArray.count > 0{
                    for i in 0...genericArray.count-1{
                        if(self.searchTagsArray[indexPath.item] == genericArray[i]){
                            //                            print("index:\(i)->Item removed: \(genericArray[i])")
                            genericArray.remove(at: i)
                            break
                        }
                    }
                }
                self.searchTagsArray.remove(at: indexPath.item)
                //new stuff
                //currentGenericArray = genericArray
                searchBar.text = ""
                //
                searchTagsCollectionView.reloadData()
                if searchTagsCollectionView.numberOfItems(inSection: 1)>0{
                    searchTagsCollectionView.scrollToItem(at: IndexPath(item: 0, section: 1), at: .right, animated: true)
                }
            }
            else if indexPath.section == 0{
                self.searchTagsArray.append(tagsSelected[indexPath.item])
                tagsSelected.remove(at: indexPath.item)
                searchTagsCollectionView.reloadData()
            }
        }
        else if collectionView == topTagsCollectionView && indexPath == IndexPath(item: 5, section: 0) {
            myView.isHidden = false
            keyboardIsOpen = !keyboardIsOpen
            searchBar.becomeFirstResponder()
            return
        }
    }
}

