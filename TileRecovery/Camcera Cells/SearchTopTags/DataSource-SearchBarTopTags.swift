//
//  DataSource-SearchBarTopTags.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 9/20/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//
import Foundation
import UIKit

extension SearchBarTopTagsViewController:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //Search Cells
        print("cell for item at")
        if collectionView == self.searchTagsCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchCellIdentifier", for: indexPath) as! SearchTagsCollectionCell
            if indexPath.section == 0{
                cell.collectionLabel.text = tagsSelected[indexPath.item]
                print("cell in section: \(indexPath.section)\titem:\(indexPath.item) turned blue")
                cell.backgroundColor = tileColor//.blue
                cell.collectionLabel.textColor = backgroundColor//.white
            }
            else if indexPath.section == 1{
                cell.backgroundColor = backgroundColor//.white
                cell.collectionLabel.textColor = tileColor//UIColor.black
                cell.collectionLabel.text = self.searchTagsArray[indexPath.item]
            }

            cell.layer.masksToBounds = true
            cell.layer.cornerRadius = cell.bounds.width/20
            return cell
        }
            //Top Cells
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopCellIdentifier", for: indexPath) as! TopTagsCollectionCell
            if indexPath.item == 0 && self.wasSearchTagSelected{
                cell.isHidden = true
            }else{
                cell.isHidden = false
            }
            print("selectedIndexPath: \(self.selectedIndexPath)")
            if let selectedIndexPath = selectedIndexPath, selectedIndexPath == indexPath{
                                cell.backgroundColor = tileColor
                                cell.layer.borderColor = backgroundColor.cgColor
                                cell.layer.borderWidth = 4
                                cell.layer.cornerRadius = cell.bounds.width/15
                                cell.collectionLabel.textColor = backgroundColor
            }else{
//            if self.wasTopTagSelected == false{
            cell.backgroundColor = UIColor(red: 230/255, green: 252/255, blue: 245/255, alpha: 0.9)//colors.backgroundColor
            cell.layer.borderColor = colors.toolBarColor.cgColor
            cell.layer.borderWidth = 4
            cell.layer.cornerRadius = cell.bounds.width/15
            cell.collectionLabel.textColor = colors.toolBarColor
            }
            
            if indexPath.item == 3{
                //cell.setUpButton()
                //cell.searchButtonDelegate = self
                cell.labelTrailingAnchor.constant = 0
                cell.layoutIfNeeded()
                cell.collectionLabel.text = "       Search"
                cell.collectionLabel.textAlignment = .center
                var searchImageView = UIImageView()
                searchImageView.image = #imageLiteral(resourceName: "icons8-widesearch-filled-100")
                cell.collectionLabel.addSubview(searchImageView)
                searchImageView.translatesAutoresizingMaskIntoConstraints = false
                searchImageView.leadingAnchor.constraint(equalTo: cell.collectionLabel.leadingAnchor, constant: 0).isActive = true
                searchImageView.centerYAnchor.constraint(equalTo: cell.collectionLabel.centerYAnchor, constant: 0).isActive = true
                searchImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
                searchImageView.heightAnchor.constraint(equalToConstant: cell.bounds.height).isActive = true
                return cell
            }
            cell.collectionLabel.text = topFiveTags[indexPath.item]
            return cell
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == searchTagsCollectionView{
            return 2
        }
        else{
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("section number: \(section)")
        if collectionView == self.searchTagsCollectionView{
            if section == 0{
                return tagsSelected.count
            }
            else{
                return self.searchTagsArray.count
            }
        }
        else{
            return topFiveTags.count
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        print("Inside of didSelectItemAterwertw")
//        if collectionView == searchTagsCollectionView{
//            print("Inside of didSelectItemAterwertw")
//            if indexPath.section == 1{
//                print("indexPath selected: \(indexPath.item)")
//                print("Size of currentGenericArray: \(currentGenericArray.count)")
//                
//                print("Selected item: \(currentGenericArray[indexPath.item])")
//                if(tagsSelected.contains(currentGenericArray[indexPath.item])){
//                    print("This element is already in the array")
//                }
//                
//                
//                //tagsSelected.insert(currentGenericArray[indexPath.item], at: 0)
//                tagsSelected.append(currentGenericArray[indexPath.item])
//                //tagsSelected.insert(currentGenericArray[indexPath.item], at: tagsSelected.count)
//                if genericArray.count > 0{
//                    for i in 0...genericArray.count-1{
//                        if(currentGenericArray[indexPath.item] == genericArray[i]){
//                            print("index:\(i)->Item removed: \(genericArray[i])")
//                            genericArray.remove(at: i)
//                            break
//                        }
//                    }
//                }
//                currentGenericArray.remove(at: indexPath.item)
//                //new stuff
//                //currentGenericArray = genericArray
//                searchBar.text = ""
//                //
//                searchTagsCollectionView.reloadData()
//                if searchTagsCollectionView.numberOfItems(inSection: 1)>0{
//                    searchTagsCollectionView.scrollToItem(at: IndexPath(item: 0, section: 1), at: .right, animated: true)
//                }
//            }
//            else if indexPath.section == 0{
//                currentGenericArray.append(tagsSelected[indexPath.item])
//                tagsSelected.remove(at: indexPath.item)
//                searchTagsCollectionView.reloadData()
//            }
//        }
//        else if collectionView == topTagsCollectionView && indexPath == IndexPath(item: 5, section: 0) {
//            myView.isHidden = false
//            keyboardIsOpen = !keyboardIsOpen
//            searchBar.becomeFirstResponder()
//            return
//        }
//    }
    
    
    
}

