import Foundation
import UIKit
import FirebaseDatabase

extension SearchBarTopTagsViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        
        //        currentGenericArray = genericArray.filter({letter -> Bool in
        //            letter.lowercased().contains(searchBar.text!)
        //        })
        //        if currentGenericArray.isEmpty{
        //            print("This element is not in the current array")
        //        }
        searchTagsCollectionView.reloadData()
        self.myView.isHidden = true
        return true
    }
    
    
    /// Helper to dismiss keyboard
    @objc func didStopEditing() {
        // textFieldShouldReturn(phoneNumberTextField)
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.setAnimationCurve(UIView.AnimationCurve.easeInOut)
        UIView.animate(withDuration: 0.2) {
            self.view.frame.origin.y = 0
        }
    }
    
    
    @objc func textFieldDidChange(){
        print("inside of text field did change")
        guard(!(searchBar.text?.isEmpty)!) else{
            print("The search bar is empty")
            self.searchTagsArray = genericArray
            searchTagsCollectionView.reloadData()
            postsButtonDelegate.disablePostButton(isEnabled: false)
            return
        }
        postsButtonDelegate.disablePostButton(isEnabled: true)
        
        
        print("The search bar was not empty it says: \(searchBar.text)")
        
        
        self.searchTagsArray = genericArray.filter({letter -> Bool in
            //letter.lowercased().contains(searchBar.text!)
            //mew stuff
            if searchBar.text!.count > letter.count{
                return false
            }
            let stringRange = letter.index(letter.startIndex, offsetBy: searchBar.text!.count)
            let subword = letter[..<stringRange]
            return subword.lowercased().contains(searchBar.text!.lowercased())
            //
            //If broken delete above and uncoment below
            //letter.lowercased().contains(searchBar.text!.lowercased())
            
            
        })
        
        
        if self.searchTagsArray.isEmpty{
            print("text being inserted \(searchBar.text!)")
            //currentGenericArray.insert(searchBar.text!, at: 0)
//            self.searchTagsArray.append(searchBar.text!)
            self.searchDataBase(name: searchBar.text!)
            self.searchTagsCollectionView.reloadData()
        }
        
        
        searchTagsCollectionView.reloadData()
    }
}

extension SearchBarTopTagsViewController{
func searchDataBase(name:String){
    print("Search Database called")
    let searchName = name.lowercased()
    
    let dispatchGroup:DispatchGroup = DispatchGroup()
    
    let ref = Database.database().reference(withPath: "TagIDs")
    print("name: \(name), name + ~: \(name + "~")")
    let startValue = searchName
    let endValue = searchName + "~"
    
    var isItemFound = false
    
    
    dispatchGroup.enter()
    Database.database().reference(withPath: "TagIDs").queryOrdered(byChild: "name").queryStarting(atValue: startValue).queryEnding(atValue: endValue).observeSingleEvent(of: .value, with: {(snapshot) in
        
        if !snapshot.exists() || snapshot.childrenCount == 0{
            print("The snap shot is empty make a new tag")
            //                self.tagsUsersArray.insert(Tag(name: name, location:["longitude" :self.userLocation.longitude, "latitude":self.userLocation.latitude] , tagID: name, storageRef: "")! , at: 0)
            //                self.collectionView.reloadData()
            dispatchGroup.leave()
            return
        }
        
        isItemFound = true
        
        print("snapshot values: \(snapshot.value)")
        let data = snapshot.value as? [[String:Any]]
        let l = snapshot.value as? [String:Any]
        let d = snapshot.value as? [Any]
        print("Children count \(snapshot.childrenCount)")
        print("Data L: \(l)")
        print("Data d: \(d)")
        print("Data returned from query: \(data)")
        
        for child in snapshot.children{
            let node = child as? DataSnapshot
            print("Key: \(node?.key)")
            guard let tagID = node?.key else{
                print("no tagID")
                continue
            }
            print("Child in snapshot: \(node?.value)")
            
            guard let nodeData = node?.value as? [String:Any] else{
                print("unable to convert nodeData to [String:Any]: \(node?.value)")
                continue
            }
            
            guard let name = nodeData["name"] as? String, let location = nodeData["location"] as? [String:Any], let storageRef = nodeData["storageRef"] as? String, let photoURL = nodeData["photoURL"] as? String else{
                return print("Unable to parse firebase data2")
            }
            
            guard let tag = Tag(name: name, location: location, tagID: tagID, storageRef: storageRef, photoURL: photoURL) else{
                print("Error parsing tag")
                continue
            }
            let hasTag = self.searchTagsArray.contains(where: {(name) in
                return name == tag.name
            })
            
            if hasTag{
                return
            }
            print("TAg about to be added to array: \(tag)")
            //                self.tagsArray.append(tag)
            self.searchTagsArray.append(tag.name)
            //                self.collectionView.reloadData()
        }
        dispatchGroup.leave()
    }, withCancel: {(error) in
        print("Error getting the snapshot \(error.localizedDescription)")
        //            self.tagsUsersArray.insert(Tag(name: name, location:["longitude" :self.userLocation.longitude, "latitude":self.userLocation.latitude] , tagID: name, storageRef: "")! , at: 0)
        dispatchGroup.leave()
    })
    
    
    dispatchGroup.notify(queue: .main, execute: {
        if isItemFound == false{
            self.searchTagsArray.insert(name , at: 0)
        }
        self.searchTagsCollectionView.reloadData()
    })
}
}
