//
//  CollectionViewCells.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 9/20/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

class SearchTagsCollectionCell:UICollectionViewCell{
    
    var collectionLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        collectionLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height))
        self.addSubview(collectionLabel)
        collectionLabel.textAlignment = .center
        collectionLabel.translatesAutoresizingMaskIntoConstraints = false
        collectionLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        collectionLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        collectionLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        collectionLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class TopTagsCollectionCell:UICollectionViewCell{
    var searchButton:UIButton?
    var collectionLabel: UILabel!
    var view:UIView!
    var labelTrailingAnchor:NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        collectionLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height))
        self.addSubview(collectionLabel)
        collectionLabel.textAlignment = .center
        collectionLabel.translatesAutoresizingMaskIntoConstraints = false
        collectionLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        collectionLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        labelTrailingAnchor = collectionLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0)
        labelTrailingAnchor.isActive = true
        collectionLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//
//  PreviewController.swift
//  CustomCamera
//
//  Created by Thomas M. Jumper on 1/22/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
////
//  collectionViewCells.swift
//  previewControllerKeyboard
//
//  Created by 123456 on 8/13/18.
//  Copyright © 2018 123456. All rights reserved.
//

import Foundation

