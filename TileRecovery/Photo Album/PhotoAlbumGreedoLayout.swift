////
////  ViewController.swift
////  programaticCollectionView
////
////  Created by 123456 on 3/22/18.
////  Copyright © 2018 123456. All rights reserved.
////
//
//import UIKit
//import Photos
//
//protocol subPDelegate{
//    func subPDelegate(image: UIImage)
//}
//
//protocol testDelegate{
//    func testFuncOfDelegate()
//}
//
//class PhotoAlbumViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource{
//
//
//
//    var imageArray:[UIImage] = [UIImage]()
//
//   var collectionView:UICollectionView! = {
//        let layout = UICollectionViewFlowLayout()
//        layout.minimumInteritemSpacing = 1
//        layout.minimumLineSpacing = 1
//        layout.scrollDirection = .vertical
//        print("There is a problem?")
//        let CV = UICollectionView(frame: UIScreen.main.bounds, collectionViewLayout: layout)
//        CV.translatesAutoresizingMaskIntoConstraints = false
//        return CV
//    }()
//
//    var PhotoAlbumTitleLabel:UILabel = {
//        let lbl = UILabel()
//        lbl.translatesAutoresizingMaskIntoConstraints = false
//        lbl.textColor = .white
//        lbl.textAlignment = .center
//        lbl.font = lbl.font.withSize(36)
//        lbl.text = "Photo Album"
//        return lbl
//    }()
//
//    var fetchResult: PHFetchResult<PHAsset> = PHFetchResult()
//    var collectionViewSizeCalculator:GreedoCollectionViewLayout?
//    var MainVC = MainViewController()
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//
//      //  let layout =  customCollectionViewLayout()
//        //        layout.minimumInteritemSpacing = 1
//        //        layout.minimumLineSpacing = 1
//        //        layout.scrollDirection = .vertical
//       // collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
//        //        layout.itemSize = CGSize(width: UIScreen.main.bounds.width/3 - 1, height: UIScreen.main.bounds.width/3 - 1)
//        self.collectionView.delegate = self
//        self.collectionView.dataSource = self
//        collectionView.register(customPhotoCell.self, forCellWithReuseIdentifier: "Cell")
//
//        //self.collectionViewSizeCalculator = GreedoCollectionViewLayout(collectionView: self.collectionView)
//       //  self.collectionViewSizeCalculator?.dataSource = self
//      //  self.collectionViewSizeCalculator?.rowMaximumHeight = self.collectionView.bounds.height / 3;
//        let layout = UICollectionViewFlowLayout()
//        layout.minimumInteritemSpacing = 5.0
//        layout.minimumLineSpacing = 5.0
//        layout.sectionInset = UIEdgeInsetsMake(10.0, 5.0, 5.0, 5.0)
//        self.collectionView.collectionViewLayout = layout
//
//        self.view.addSubview(collectionView)
//        self.view.addSubview(PhotoAlbumTitleLabel)
//        setUpLayout()
//        fetchAssets()
//
//        let photos = PHPhotoLibrary.authorizationStatus()
//        if photos == .notDetermined {
//            PHPhotoLibrary.requestAuthorization({status in
//                if status == .authorized{
//                    print("permission granted")
//                    DispatchQueue.main.async {
//                        self.fetchAssets()
//                        self.collectionView.reloadData()
//                    }
//                } else {
//                    print("permission not granted")
//                }
//            })
//        }
//
//
//    }
//
//
//    func setUpLayout(){
//        collectionView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 50).isActive = true
//        collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
//        collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
//        collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
//
//        //Label Layout constraints
//        PhotoAlbumTitleLabel.topAnchor.constraint(equalTo: self.view.topAnchor, constant: -45).isActive = true
//        PhotoAlbumTitleLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
//    }
//
//    func fetchAssets(){
//        let fetchOptions = PHFetchOptions()
//        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
//        fetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
//    }
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return fetchResult.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! customPhotoCell
//        //        cell.backgroundColor = .blue
//        //        cell.imgView.contentMode = .scaleToFill
//        //        cell.imgView.image = #imageLiteral(resourceName: "ManOnTheMoon")
//        let imageView = cell.imgView
//        let requestOptions = PHImageRequestOptions()
//        requestOptions.isSynchronous = true
//        requestOptions.deliveryMode = .highQualityFormat
//        let scale = min(2.0, UIScreen.main.scale)
//        let requestImageSize = CGSize(width: cell.bounds.width * scale, height: cell.bounds.height * scale)
//        PHImageManager.default().requestImage(for: fetchResult.object(at: indexPath.item) , targetSize: requestImageSize, contentMode: .aspectFill, options: requestOptions, resultHandler: {
//            image,error in
//            if let error = error{
//                print(error)
//            }
//            self.imageArray.append(image!)
//            imageView.image = image!
//            imageView.contentMode = .scaleAspectFill
//        })
//        cell.layer.cornerRadius = 10.0
//        cell.layer.masksToBounds = true
//        return cell
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 5.0
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 5.0
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let width = self.view.bounds.width/3 - 1
//        return CGSize(width: width, height: width)
//       // return self.collectionViewSizeCalculator!.sizeForPhoto(at: indexPath)
//    }
//
//    func greedoCollectionViewLayout(_ layout: GreedoCollectionViewLayout?, originalImageSizeAt indexPath: IndexPath?) -> CGSize {
//        if (indexPath?.item)! < fetchResult.count{
//            let asset = fetchResult[(indexPath?.item)!]
//            return CGSize(width: asset.pixelWidth, height: asset.pixelHeight)
//        }
//        return CGSize(width: 0.1, height: 0.1)
//    }
//
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        print("Was tapped")
//        //pdelegate?.previewSegueDelegate(image: imageArray[indexPath.item], device: nil)
//     //   subP?.subPDelegate(image:imageArray[indexPath.item])
//    }
//
//}
//
//class customPhotoCell:UICollectionViewCell{
//
//    var imgView:UIImageView = {
//        let imgV = UIImageView()
//        imgV.backgroundColor = .orange
//        imgV.translatesAutoresizingMaskIntoConstraints = false
//        return imgV
//    }()
//
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        print("Inside of init")
//        self.addSubview(imgView)
//        imgView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
//        imgView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
//        imgView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
//        imgView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
//    }
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//
//}
//
////extension UIImageView{
////    func fetchImage(asset: PHAsset, contentMode: PHImageContentMode, targetSize: CGSize) {
////        let options = PHImageRequestOptions()
////        options.version = .original
////        PHImageManager.default().requestImage(for: asset, targetSize: targetSize, contentMode: contentMode, options: options) { image, _ in
////            guard let image = image else { return }
////            switch contentMode {
////            case .aspectFill:
////                self.contentMode = .scaleAspectFill
////            case .aspectFit:
////                self.contentMode = .scaleAspectFit
////            }
////            self.image = image
////        }
////    }
////}
////
////extension UIImage{
////    func resizeImageWith() -> UIImage {
////        let aspectRatio = CGFloat(self.size.width / self.size.height)
////        let newWidth = UIScreen.main.bounds.width
////        let newSize = CGSize(width: newWidth, height: newWidth/aspectRatio)
////        UIGraphicsBeginImageContextWithOptions(newSize, true, 0)
////        self.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
////        let  newImage = UIGraphicsGetImageFromCurrentImageContext()
////        UIGraphicsEndImageContext()
////        return newImage!
////    }
////}

//import UIKit
//import Photos
//
//class  PhotoAlbumViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,GreedoCollectionViewLayoutDataSource {
//
//    var previewSegueDelegate:previewSegueDelegate?
//
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        print("item was tapped")
//        //        let imageView = UIImageView()
//        //        imageView.fetchImage(asset: fetchResult[(indexPath.item)], contentMode: .aspectFill, targetSize: CGSize(width: 1000,height: 1000))
//        //        imageView.contentMode = .scaleAspectFill
//        //  previewSegueDelegate?.previewSegueDelegate(image: self.imageArray[indexPath.item], device: nil)
//        let image = getAssetThumbnail(asset: fetchResult[(indexPath.item)])
//        previewSegueDelegate?.previewSegueDelegate(image: image, device: nil, albumTypeSender:nil,  cameraTypeSender:nil)
//    }
//
//    func getAssetThumbnail(asset: PHAsset) -> UIImage {
//        //        let manager = PHImageManager.default()
//        //        let option = PHImageRequestOptions()
//        //        option.deliveryMode = .highQualityFormat
//        //        var thumbnail = UIImage()
//        //        option.isSynchronous = true
//        //        manager.requestImage(for: asset, targetSize: CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
//        //            thumbnail = result!
//        //        })
//        //        return thumbnail
//        let requestOptions = PHImageRequestOptions()
//        requestOptions.isSynchronous = true
//        requestOptions.deliveryMode = .highQualityFormat
//        let scale = min(2.0, UIScreen.main.scale)
//
//        var returnImage:UIImage = UIImage()
//
//        // let requestImageSize = CGSize(width: cell.bounds.width * scale, height: cell.bounds.height * scale)
//        let requestImageSize = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
//        PHImageManager.default().requestImage(for: asset , targetSize: requestImageSize, contentMode: .aspectFill, options: requestOptions, resultHandler: {
//            image,error in
//            if let error = error{
//                print(error)
//            }
//            returnImage = image!
//        })
//        return returnImage
//    }
//
//
//    var imageArray:[UIImage] = [UIImage]()
//    var collectionViewSizeCalculator:GreedoCollectionViewLayout?
//    var assetCollection: PHAssetCollection!
//    var photosAsset: PHFetchResult<AnyObject>!
//    var assetThumbnailSize: CGSize!
//
//
//    var collectionView:UICollectionView! = {
//        let layout = UICollectionViewFlowLayout()
//        layout.minimumInteritemSpacing = 1
//        layout.minimumLineSpacing = 2.0
//        layout.scrollDirection = .vertical
//        let CV = UICollectionView(frame: UIScreen.main.bounds, collectionViewLayout: layout)
//        CV.translatesAutoresizingMaskIntoConstraints = false
//        return CV
//    }()
//
//
//    var fetchResult: PHFetchResult<PHAsset> = PHFetchResult()
//
//
//    var titleBar:UIView!
//    var titleLabel:UILabel!
//    var downArrowImageView:UIImageView!
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        titleBar = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
//        self.view.addSubview(titleBar)
//        //UIColor(red: 56/255, green: 217/255, blue: 169/255, alpha: 1.0)
//
//        titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.titleBar.bounds.width/2, height: 50))
//        titleBar.addSubview(titleLabel)
//
//        downArrowImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.titleBar.bounds.width/2, height: 50))
//        titleBar.addSubview(downArrowImageView)
//
//        //        let layout =  customCollectionViewLayout()
//        //               layout.minimumInteritemSpacing = 1
//        //              layout.minimumLineSpacing = 1
//        //                layout.scrollDirection = .vertical
//        // collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
//        //        layout.itemSize = CGSize(width: UIScreen.main.bounds.width/3 - 1, height: UIScreen.main.bounds.width/3 - 1)
//
//        var collection:PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: nil)
//
//        if let first_Obj:AnyObject = collection.firstObject{
//            //found the album
//            self.assetCollection = first_Obj as! PHAssetCollection
//        }
//
//
//        //New Greedo Stuff
//        self.collectionViewSizeCalculator = GreedoCollectionViewLayout(collectionView: self.collectionView)
//        self.collectionViewSizeCalculator?.dataSource = self
//        self.collectionViewSizeCalculator?.rowMaximumHeight = self.collectionView.bounds.height / 3;
//        let layout = UICollectionViewFlowLayout()
//        layout.minimumInteritemSpacing = 5.0
//        layout.minimumLineSpacing = 5.0
//        layout.sectionInset = UIEdgeInsetsMake(10.0, 5.0, 5.0, 5.0)
//        self.collectionView.collectionViewLayout = layout
//        //End Of new Greedo Stuff in ViewDidLoad()
//
//
//
//        self.collectionView.delegate = self
//        self.collectionView.dataSource = self
//        collectionView.register(customPhotoCell.self, forCellWithReuseIdentifier: "Cell")
//        self.collectionView.backgroundColor = .white
//        self.view.addSubview(collectionView)
//        //Add to the official project
//
//        setUpLayout()
//        fetchAssets()
//        setSizes()
//
//        let photos = PHPhotoLibrary.authorizationStatus()
//        if photos == .notDetermined {
//            PHPhotoLibrary.requestAuthorization({status in
//                if status == .authorized{
//                    print("permission granted")
//                    DispatchQueue.main.async {
//                        self.fetchAssets()
//                        collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: nil)
//                        self.photosAsset = PHAsset.fetchAssets(with: .image, options: nil) as! PHFetchResult<AnyObject>
//
//                        self.collectionView.reloadData()
//                    }
//                } else {
//                    print("permission not granted")
//                }
//            })
//        }
//
//
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        // Get size of the collectionView cell for thumbnail image
//
//        if let layout = self.collectionView!.collectionViewLayout as? UICollectionViewFlowLayout{
//            let cellSize = layout.itemSize
//
//            self.assetThumbnailSize = CGSize(width: cellSize.width, height: cellSize.height)
//
//        }
//        //fetch the photos from collection
//        self.photosAsset = PHAsset.fetchAssets(with: .image, options: nil) as! PHFetchResult<AnyObject>
//
//        self.collectionView!.reloadData()
//    }
//
//    func setUpLayout(){
//        titleBar.translatesAutoresizingMaskIntoConstraints = false
//        titleBar.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
//        titleBar.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
//        titleBar.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
//        titleBar.heightAnchor.constraint(equalToConstant: 100).isActive = true
//        titleBar.backgroundColor = colors.backgroundColor
//
//        collectionView.topAnchor.constraint(equalTo: self.titleBar.bottomAnchor, constant: 0).isActive = true
//        collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
//        collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
//        collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
//
//        titleLabel.translatesAutoresizingMaskIntoConstraints = false
//        titleLabel.leadingAnchor.constraint(equalTo: self.titleBar.leadingAnchor, constant: 20).isActive = true
//        titleLabel.centerYAnchor.constraint(equalTo: self.titleBar.centerYAnchor, constant: 10).isActive = true
//        titleLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
//        titleLabel.widthAnchor.constraint(equalToConstant: 300).isActive = true
//        titleLabel.textColor = colors.toolBarColor
//        titleLabel.font = UIFont.boldSystemFont(ofSize: 25)
//        titleLabel.text = "Photo Album"
//
//        downArrowImageView.translatesAutoresizingMaskIntoConstraints = false
//        downArrowImageView.bottomAnchor.constraint(equalTo: titleBar.bottomAnchor, constant: 0).isActive = true
//        downArrowImageView.centerXAnchor.constraint(equalTo: titleBar.centerXAnchor, constant: 0).isActive = true
//        downArrowImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
//        downArrowImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
//        downArrowImageView.image = #imageLiteral(resourceName: "icons8-expand-arrow-96")
//    }
//
//    func fetchAssets(){
//        let fetchOptions = PHFetchOptions()
//        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
//        fetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
//    }
//
//
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! customPhotoCell
//        //        cell.backgroundColor = .blue
//        //        cell.imgView.contentMode = .scaleToFill
//        //        cell.imgView.image = #imageLiteral(resourceName: "ManOnTheMoon")
//        let imageView = cell.imgView
//        let requestOptions = PHImageRequestOptions()
//        requestOptions.isSynchronous = true
//        requestOptions.deliveryMode = .highQualityFormat
//        let scale = min(2.0, UIScreen.main.scale)
//
//        let requestImageSize = CGSize(width: cell.bounds.width * scale, height: cell.bounds.height * scale)
//        // let requestImageSize = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
//        PHImageManager.default().requestImage(for: fetchResult.object(at: indexPath.item) , targetSize: requestImageSize, contentMode: .aspectFill, options: requestOptions, resultHandler: {
//            image,error in
//            if let error = error{
//                print(error)
//            }
//            //  self.imageArray.append(image!)
//            self.imageArray.insert(image!, at: indexPath.item)
//            imageView.image = image!
//            imageView.clipsToBounds = false
//            imageView.contentMode = .scaleAspectFill
//            //imageView.layer.cornerRadius = 100
//            //imageView.layer.masksToBounds = true
//        })
//        cell.layer.masksToBounds = true
//        cell.layer.cornerRadius = 10
//        cell.backgroundColor = .white
//        return cell
//
//        //        let asset: PHAsset = self.photosAsset[indexPath.item] as! PHAsset
//        //        self.assetThumbnailSize = CGSize(width: cell.bounds.width, height: cell.bounds.height)
//        //        PHImageManager.default().requestImage(for: asset, targetSize: self.assetThumbnailSize, contentMode: .aspectFill, options: nil, resultHandler: {(result, info)in
//        //            if let image = result {
//        //                cell.imgView.image = image
//        //                self.imageArray.append(image)
//        //                imageView.clipsToBounds = false
//        //                imageView.contentMode = .scaleAspectFill
//        //                //imageView.layer.cornerRadius = 100
//        //                //imageView.layer.masksToBounds = true
//        //            }
//        //        })
//        //            cell.layer.masksToBounds = true
//        //            cell.layer.cornerRadius = 10
//        //            cell.backgroundColor = .white
//        //        return cell
//    }
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return fetchResult.count
//        // return self.photosAsset.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 5.0
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 5.0
//    }
//
//    //Original Size for itemAt
//    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//    //        let width = self.view.bounds.width/3 - 1
//    //        return CGSize(width: width, height: width)
//    //    }
//
//    //    //Greedo Size for itemAt
//    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//    //        return self.collectionViewSizeCalculator!.sizeForPhoto(at: indexPath)
//    //    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        // print("sizes.count = :\(sizes.count)")
//        //  print("collectionView.numberOfItems(inSection: 0) = :\(collectionView.numberOfItems(inSection: 0))")
//        if sizes.count == fetchResult.count{
//            return sizes[indexPath.item]
//        }
//        else{
//            return CGSize(width: 100, height: 100)
//        }
//    }
//
//    var sizes:[CGSize] = [CGSize]()
//
//    func setSizes(){
//
//        DispatchQueue.main.async {
//            for i in 0..<self.collectionView.numberOfItems(inSection: 0){
//                self.sizes.append(self.collectionViewSizeCalculator!.sizeForPhoto(at: IndexPath(item: i, section: 0)))
//                //    print("sizes count in setSizes: \(self.sizes.count)")
//            }
//            self.collectionView.reloadData()
//        }
//
//    }
//
//    func greedoCollectionViewLayout(_ layout: GreedoCollectionViewLayout?, originalImageSizeAt indexPath: IndexPath?) -> CGSize {
//        if (indexPath?.item)! < fetchResult.count{
//            let asset = fetchResult[(indexPath?.item)!]
//            return CGSize(width: asset.pixelWidth, height: asset.pixelHeight)
//        }
//        return CGSize(width: 0.1, height: 0.1)
//    }
//
//    //New stuff
//    var lastOffsetY:CGFloat = 0
//    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//        lastOffsetY = scrollView.contentOffset.y
//    }
//
//    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
//        if(scrollView.contentOffset.y > self.lastOffsetY){
//            UIView.animate(withDuration: 1.0, animations: {
//                self.collectionView.transform = CGAffineTransform(translationX: 0, y: -50)
//            }, completion: nil)
//        }
//    }
//}
//
//
//
//class customPhotoCell:UICollectionViewCell{
//
//    var imgView:UIImageView = {
//        let imgV = UIImageView()
//        imgV.backgroundColor = .orange
//        imgV.translatesAutoresizingMaskIntoConstraints = false
//        return imgV
//    }()
//
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        print("Inside of init")
//        self.addSubview(imgView)
//        self.layer.cornerRadius = 10.0
//        imgView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
//        imgView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
//        imgView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
//        imgView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
//    }
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//
//
//}
////
////extension UIImageView{
////    func fetchImage(asset: PHAsset, contentMode: PHImageContentMode, targetSize: CGSize) {
////        let options = PHImageRequestOptions()
////        options.version = .original
////        PHImageManager.default().requestImage(for: asset, targetSize: targetSize, contentMode: contentMode, options: options) { image, _ in
////            guard let image = image else { return }
////            switch contentMode {
////            case .aspectFill:
////                self.contentMode = .scaleAspectFill
////            case .aspectFit:
////                self.contentMode = .scaleAspectFit
////            }
////            self.image = image
////        }
////    }
////}
////
////extension UIImage{
////    func resizeImageWith() -> UIImage {
////        let aspectRatio = CGFloat(self.size.width / self.size.height)
////        let newWidth = UIScreen.main.bounds.width
////        let newSize = CGSize(width: newWidth, height: newWidth/aspectRatio)
////        UIGraphicsBeginImageContextWithOptions(newSize, true, 0)
////        self.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
////        let  newImage = UIGraphicsGetImageFromCurrentImageContext()
////        UIGraphicsEndImageContext()
////        return newImage!
////    }
////}
//
//
//
//






//  Converted to Swift 4 by Swiftify v4.1.6697 - https://objectivec2swift.com/
//
//  GreedoCollectionViewLayout.h
//  500px
//  Copyright (c) 2016 500px. All rights reserved.
//

import Foundation
import UIKit

protocol GreedoCollectionViewLayoutDataSource {
    func greedoCollectionViewLayout(_ layout: GreedoCollectionViewLayout?, originalImageSizeAt indexPath: IndexPath?) -> CGSize
}

class GreedoCollectionViewLayout:GreedoSizeCalculatorDataSource {
    var dataSource: GreedoCollectionViewLayoutDataSource?
    var rowMaximumHeight: CGFloat = 0.0
    var isFixedHeight = false
    
    var collectionView:UICollectionView?
    private var _greedo: GreedoSizeCalculator?
    var greedo: GreedoSizeCalculator? {
        if _greedo == nil {
            _greedo = GreedoSizeCalculator()
            _greedo?.rowMaximumHeight = 200
            _greedo?.isFixedHeight = false
            _greedo?.dataSource = self
        }
        return _greedo
    }
    
    init(collectionView: UICollectionView?) {
        self.collectionView = collectionView
    }
    
    func sizeForPhoto(at indexPath: IndexPath?) -> CGSize {
        var contentWidth:CGFloat = (self.collectionView?.bounds.size.width)!
        var interitemspacing:CGFloat = 2.0
        guard let layout:UICollectionViewFlowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout else{
            print("Bad layout in sizeForPhoto(at:)")
            return CGSize()
        }
        
        
        contentWidth -= (layout.sectionInset.left) + (layout.sectionInset.right )
        interitemspacing = (layout.minimumInteritemSpacing)
        layout.minimumLineSpacing = 5.0
        layout.minimumInteritemSpacing = 5.0
        
        //        else{
        //            interitemspacing = (layout!.minimumInteritemSpacing)
        //            layout?.minimumLineSpacing = 5.0
        //            layout?.minimumInteritemSpacing = 5.0
        //        }
        
        if let greedo = greedo{
            greedo.contentWidth = contentWidth
            greedo.interItemSpacing = interitemspacing
            return greedo.sizeForPhoto(at: indexPath)
        }
        
        return CGSize.zero
    }
    
    func clearCache() {
        greedo?.clearCache()
    }
    
    func clearCache(after indexPath: IndexPath?) {
        greedo?.clearCache(after: indexPath)
    }
    
    func rowmaximumHeight() -> CGFloat {
        return (greedo?.rowMaximumHeight)!
    }
    
    func setRowMaximumHeight(_ rowMaximumHeight: CGFloat) {
        greedo?.rowMaximumHeight = rowMaximumHeight
    }
    
    func greedoSizeCalculator(_ layout: GreedoSizeCalculator?, originalImageSizeAt indexPath: IndexPath?) -> CGSize {
        return dataSource!.greedoCollectionViewLayout(self, originalImageSizeAt: indexPath)
    }
    
    
}
