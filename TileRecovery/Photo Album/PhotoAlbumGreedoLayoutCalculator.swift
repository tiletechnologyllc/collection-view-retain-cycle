//
//  GreedoSizeCalculator.swift
//  FixCustomLayoutOnceAndForAll
//
//  Created by 123456 on 5/4/18.
//  Copyright © 2018 123456. All rights reserved.
//

import UIKit

import Foundation
import UIKit

protocol GreedoSizeCalculatorDataSource: class {
    func greedoSizeCalculator(_ layout: GreedoSizeCalculator?, originalImageSizeAt indexPath: IndexPath?) -> CGSize
}

//class GreedoSizeCalculator: NSObject {
//    weak var dataSource: GreedoSizeCalculatorDataSource?
//    var rowMaximumHeight: CGFloat = 0.0
//    var isFixedHeight = false
//    var contentWidth: CGFloat = 0.0
//    var interItemSpacing: CGFloat = 0.0
//
//    func sizeForPhoto(at indexPath: IndexPath?) -> CGSize {
//    }
//
//    func clearCache() {
//    }
//
//    func clearCache(after indexPath: IndexPath?) {
//    }
//}

class GreedoSizeCalculator {
    //private var _sizeCache = [AnyHashable: Any]()
    private var sizeCache = [IndexPath:CGSize]()
    private var leftOvers:[CGSize] = [CGSize]()
    var lastIndexPathAdded:IndexPath?
    
    
    var dataSource: GreedoSizeCalculatorDataSource?
    var rowMaximumHeight: CGFloat = 0.0
    var isFixedHeight = false
    var contentWidth: CGFloat = 0.0
    var interItemSpacing: CGFloat = 2.0
    
    
    func sizeForPhoto(at indexPath:IndexPath?)->CGSize{
        // print("I made it rowHeight: \(rowMaximumHeight)")
        if let indexPath = indexPath{
            if sizeCache[indexPath] == nil{
                lastIndexPathAdded = indexPath
                computeSizes(at:indexPath)
            }
        }
        var size: CGSize? = sizeCache[indexPath!]
        if((size?.width)! < CGFloat(0.0) || (size?.height)! < CGFloat(0.0)){
            size = CGSize.zero
        }
        return size!
    }
    
    func clearCache(){
        self.sizeCache.removeAll()
    }
    
    func clearCache(after indexPath:IndexPath?){
        sizeCache.removeValue(forKey: indexPath!)
        
        for existingIndexPath in sizeCache.keys{
            if indexPath?.compare(existingIndexPath) == .orderedDescending{
                sizeCache.removeValue(forKey: existingIndexPath)
            }
        }
        
    }
    
    func computeSizes(at indexPath:IndexPath?){
        var photoSize = dataSource?.greedoSizeCalculator(self, originalImageSizeAt: indexPath)
        if (photoSize?.width)! < CGFloat(1) || (photoSize?.height)! < CGFloat(1) {
            // Photo with no height or width
            print("Photo has no height or width")
            photoSize?.width = rowMaximumHeight
            photoSize?.height = rowMaximumHeight
        }
        print("The photo size at index: \((indexPath?.item)!) is \nwidth: \((photoSize?.width)!), height: \((photoSize?.height)!)")
        leftOvers.append(photoSize!)
        print("Index:\(indexPath?.item) leftOvers.count: \(leftOvers.count)")
        var enoughContentForTheRow = false
        var rowHeight:CGFloat = rowMaximumHeight
        var widthMultiplier:CGFloat = 1.0
        //Calculations For Variable HeightGrid
        var totalAspectRatio:CGFloat = 0.0
        for leftOver in leftOvers{
            print("current aspect ratio: \(leftOver.width/leftOver.height)")
            totalAspectRatio += leftOver.width/leftOver.height
        }
        print("totalAspectRatio: \(totalAspectRatio)")
        //Aspect Ratio Calculation how to find the height of the row!!!!!!!!!!!!!!!!!!!!!!!
        rowHeight = CGFloat(contentWidth)/CGFloat(totalAspectRatio)
        print("rowHeight: \(rowHeight)")
        enoughContentForTheRow = rowHeight < rowMaximumHeight
        
        if enoughContentForTheRow {
            var availableSpace: CGFloat = contentWidth
            print("Available space: \(availableSpace)")
            var index: Int = 0
            for leftOver in leftOvers{
                var newWidth:CGFloat = floor((rowHeight*leftOver.width)/leftOver.height)
                print("new width: \(newWidth)")
                newWidth = min(availableSpace, newWidth)
                
                //add the size into the cahce
                self.sizeCache[lastIndexPathAdded!] = CGSize(width: newWidth, height: rowHeight)
                availableSpace -= newWidth
                availableSpace -= self.interItemSpacing
                // We need to keep track of the last index path added
                self.lastIndexPathAdded = IndexPath(item:(lastIndexPathAdded?.item)!+1,section:(lastIndexPathAdded?.section)!)
                index += 1
            }
            leftOvers.removeAll()
        }
        else{
            // The line is not full, let's ask the next photo and try to fill up the line
            computeSizes(at: IndexPath(item:(indexPath?.item)!+1,section:(indexPath?.section)!))
        }
    }
    
    
}
