import UIKit
import Photos
import PhotosUI

class  PhotoAlbumViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource {
    
    weak var previewSegueDelegate:previewSegueDelegate?
    
    var assets:[PHAsset] = [PHAsset](){
        willSet{
            imageManager.stopCachingImagesForAllAssets()
        }
        didSet{
            print("Did set called")
            imageManager.startCachingImages(for: assets, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFill, options: nil)
        }
    }
    var fetchResult: PHFetchResult<PHAsset>!
    var collectionView:UICollectionView!
    
    fileprivate let imageManager = PHCachingImageManager()
    
//    var fetchResult: PHFetchResult<PHAsset> = PHFetchResult()
    
    
    var titleBar:UIView!
    var titleLabel:UILabel!
    var downArrowImageView:UIImageView!
    var mosaicLayout:MosaicLayout = MosaicLayout()
    override func viewDidLoad() {
        super.viewDidLoad()
        titleBar = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
        self.view.addSubview(titleBar)
        //UIColor(red: 56/255, green: 217/255, blue: 169/255, alpha: 1.0)
        
        titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.titleBar.bounds.width/2, height: 50))
        titleBar.addSubview(titleLabel)
        
        downArrowImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.titleBar.bounds.width/2, height: 50))
        titleBar.addSubview(downArrowImageView)
   
        //New Greedo Stuff
//        let mosaicLayout = MosaicLayout()
        collectionView = UICollectionView(frame: UIScreen.main.bounds, collectionViewLayout: self.mosaicLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false

        
        
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        collectionView.register(customPhotoCell.self, forCellWithReuseIdentifier: "Cell")
        self.collectionView.backgroundColor = .white
        self.view.addSubview(collectionView)
        //Add to the official project
        
        
        
   
        
        setUpLayout()

        print("request authorization called")
        PHPhotoLibrary.requestAuthorization { (status: PHAuthorizationStatus) in
            if status == .authorized && self.fetchResult == nil {
                let fetchOptions = PHFetchOptions()
                fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
                //self.fetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
                //results.enumerateObjects({asset, index, stop in
                //    self.assets.append(asset)
                //})
                var tempArr:[PHAsset] = []
                self.fetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
                self.fetchResult.enumerateObjects({asset, index, stop in
                    tempArr.append(asset)
                })
                self.assets = tempArr
                tempArr.removeAll()
                print("Asset count after initial fetch: \(self.assets.count)")
                
                PHPhotoLibrary.shared().register(self)
                DispatchQueue.main.async {
                    // Reload collection view once we've determined our Photos permissions.
                    self.collectionView.reloadData()
                }
            } else {
                print("photo access denied")
                self.displayPhotoAccessDeniedAlert()
            }
        }
        
        
    }
    
    deinit {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }
    
    
    func setUpLayout(){
        titleBar.translatesAutoresizingMaskIntoConstraints = false
        titleBar.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        titleBar.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        titleBar.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        titleBar.heightAnchor.constraint(equalToConstant: 100).isActive = true
        titleBar.backgroundColor = colors.backgroundColor
        
        collectionView.topAnchor.constraint(equalTo: self.titleBar.bottomAnchor, constant: 0).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.leadingAnchor.constraint(equalTo: self.titleBar.leadingAnchor, constant: 20).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: self.titleBar.centerYAnchor, constant: 10).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        titleLabel.widthAnchor.constraint(equalToConstant: 300).isActive = true
        titleLabel.textColor = colors.toolBarColor
        titleLabel.font = UIFont.boldSystemFont(ofSize: 25)
        titleLabel.text = "Photo Album"
        
        downArrowImageView.translatesAutoresizingMaskIntoConstraints = false
        downArrowImageView.bottomAnchor.constraint(equalTo: titleBar.bottomAnchor, constant: 0).isActive = true
        downArrowImageView.centerXAnchor.constraint(equalTo: titleBar.centerXAnchor, constant: 0).isActive = true
        downArrowImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        downArrowImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        downArrowImageView.image = #imageLiteral(resourceName: "icons8-expand-arrow-96")
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! customPhotoCell

        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = true
        requestOptions.deliveryMode = .highQualityFormat
        //let scale = min(2.0, UIScreen.main.scale)
        let scale = UIScreen.main.scale
        let targetSize = CGSize(width: cell.bounds.width * scale, height: cell.bounds.height * scale)
        
//        let asset = assets[indexPath.item]
        let asset = fetchResult.object(at: indexPath.item)
        let assetIdentifier = asset.localIdentifier
        
        cell.representedAssetIdentifier = assetIdentifier
        
        imageManager.requestImage(for: asset, targetSize: cell.frame.size,
                                              contentMode: .aspectFill, options: nil) { (image, hashable)  in
                                                if let loadedImage = image, let cellIdentifier = cell.representedAssetIdentifier {
                                                    
                                                    // Verify that the cell still has the same asset identifier,
                                                    // so the image in a reused cell is not overwritten.
                                                    if cellIdentifier == assetIdentifier {
                                                        cell.imageView.image = loadedImage
                                                    }
                                                }
        }
        
        cell.layer.masksToBounds = true
        cell.layer.cornerRadius = 10
        cell.backgroundColor = .white
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let fetchResult = fetchResult else{
            return 0
        }
        return fetchResult.count
        // return self.photosAsset.count
       // print("assets.count: \(assets.count)")
       // return fetchResult.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("item was tapped")
        let asset = fetchResult.object(at: indexPath.item)//assets[indexPath.item]
        let assetIdentifier = asset.localIdentifier
        let targetSize = UIScreen.main.bounds.size
        let image = UIImage()
        self.previewSegueDelegate?.previewSegueDelegate(image: nil, device: nil, albumTypeSender:nil,  cameraTypeSender:nil, asset: asset)
//            self.imageManager.requestImage(for: asset, targetSize: targetSize, contentMode: .aspectFill, options: nil, resultHandler: {(image,hashable) in
//                if let image = image{
//                    print("previewSegueDelegateCalled")
////                        self.previewSegueDelegate?.previewSegueDelegate(image: nil, device: nil, albumTypeSender:nil,  cameraTypeSender:nil, asset: asset)
//    //                     self.previewSegueDelegate?.previewSegueDelegate(image: #imageLiteral(resourceName: "85154-rininger_2.jpg"), device: nil, albumTypeSender: nil, cameraTypeSender: nil)
//                    }
//            })
       // self.previewSegueDelegate?.previewSegueDelegate(image: #imageLiteral(resourceName: "85154-rininger_2.jpg"), device: nil, albumTypeSender: nil, cameraTypeSender: nil)
    }
    
    var lastOffsetY:CGFloat = 0
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        lastOffsetY = scrollView.contentOffset.y
    }
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        if(scrollView.contentOffset.y > self.lastOffsetY){
            UIView.animate(withDuration: 1.0, animations: {
                self.collectionView.transform = CGAffineTransform(translationX: 0, y: -50)
            }, completion: nil)
        }
    }
    
    
    fileprivate func resetCachedAssets() {
        imageManager.stopCachingImagesForAllAssets()
    }
    

    
    private func displayPhotoAccessDeniedAlert() {
        let message = "Access to photos has been previously denied by the user. Please enable photo access for this app in Settings -> Privacy."
        let alertController = UIAlertController(title: "Photo Access",
                                                message: message,
                                                preferredStyle: .alert)
        let openSettingsAction = UIAlertAction(title: "Settings", style: .default) { (_) in
            if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                // Take the user to the Settings app to change permissions.
                UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(openSettingsAction)
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
}






extension PhotoAlbumViewController: PHPhotoLibraryChangeObserver {
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        
        guard let changes = changeInstance.changeDetails(for: fetchResult)
            else { return }
        
        // Change notifications may originate from a background queue.
        // As such, re-dispatch execution to the main queue before acting
        // on the change, so you can update the UI.
        DispatchQueue.main.sync {
            // Hang on to the new fetch result.
            fetchResult = changes.fetchResultAfterChanges
            // If we have incremental changes, animate them in the collection view.
            if changes.hasIncrementalChanges {
                guard let collectionView = self.collectionView else { fatalError() }
                // Handle removals, insertions, and moves in a batch update.
                collectionView.performBatchUpdates({
                    if let removed = changes.removedIndexes, !removed.isEmpty {
                        collectionView.deleteItems(at: removed.map({ IndexPath(item: $0, section: 0) }))
                    }
                    if let inserted = changes.insertedIndexes, !inserted.isEmpty {
                        collectionView.insertItems(at: inserted.map({ IndexPath(item: $0, section: 0) }))
                    }
                    changes.enumerateMoves { fromIndex, toIndex in
                        collectionView.moveItem(at: IndexPath(item: fromIndex, section: 0),
                                                to: IndexPath(item: toIndex, section: 0))
                    }
                })
                // We are reloading items after the batch update since `PHFetchResultChangeDetails.changedIndexes` refers to
                // items in the *after* state and not the *before* state as expected by `performBatchUpdates(_:completion:)`.
                if let changed = changes.changedIndexes, !changed.isEmpty {
                    collectionView.reloadItems(at: changed.map({ IndexPath(item: $0, section: 0) }))
                }
            } else {
                // Reload the collection view if incremental changes are not available.
                collectionView.reloadData()
            }
            resetCachedAssets()
        }
    }
}



