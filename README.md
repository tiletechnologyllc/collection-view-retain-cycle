**Collection View Retain Cycle**

Contained is a main UICollectionView which contains full paged cells that also contain a sub UICollectionView.

When the user taps on a cell in the sub UICollectionView a new full page cell is created. 

When the user taps on the picture in the top left corner of the screen a full page cell is deleted.

As the user creates more cells more memory is used as expected. 

As the user deletes these cells the memory is retained and not released.